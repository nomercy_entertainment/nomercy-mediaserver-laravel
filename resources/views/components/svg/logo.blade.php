@if(strtolower(brand()) != 'sony')
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 400" class="w-full h-auto">
        <title>nomercy-entertainment</title>
        <g>
            <text transform="translate(345 297.39)" style="isolation: isolate;font-size: 68.9487533569336px;fill:{{ themeColor() }};font-family: SegoeUI-Bold, Segoe UI;font-weight: 700; letter-spacing: 2" class="DarkVibrant">The Effortless Encoder</text>
            <text transform="translate(346.87 202.4)" style="isolation: isolate;font-size: 148.31417846679688px;fill: #fff;font-family: SegoeUI-Bold, Segoe UI;font-weight: 700">NoMercyTV</text>
            <polygon class="DarkVibrant" points="324.65 91.98 324.65 300.15 286.66 300.15 286.66 142.5 253.72 172.27 240.74 184.88 202.71 146.01 202.71 300.15 167.13 254.69 164 250.93 173.59 244.57 140.38 222.6 89.63 161.7 89.63 300.15 51.64 300.15 51.64 92.26 89.15 92.26 89.63 92.86 164.16 188.76 164.16 91.98 202.71 91.98 202.71 92.56 203.03 92.26 241.89 131.99 286.03 92.12 286.66 92.8 286.66 91.98 324.65 91.98" style="fill:{{ themeColor() }}" />
            <g>
                <rect x="202.7" y="188.21" width="22.84" height="111.94" style="fill: #fff" />
                <rect x="263.82" y="188.21" width="22.84" height="111.94" style="fill: #fff" />
            </g>
            <polygon points="173.59 244.57 164 250.93 89.63 300.15 89.63 189 140.38 222.6 173.59 244.57" style="fill: #fff" />
        </g>
    </svg>

@else
    <img src="https://cdn.nomercy.tv/img/logo-brand-white.svg" class="w-full h-auto">
@endif
