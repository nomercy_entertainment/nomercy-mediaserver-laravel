
<style>
</style>
<div class="flex items-center mb-6 text-sm text-gray-700 w-56">
    <div class="relative ml-auto cursor-pointer" onclick="$(`#remember`).is(':checked') ? $(`#remember`).prop( 'checked', false ).val('off') : $(`#remember`).prop( 'checked', true ).val('on')">
        <input type="checkbox" name="{{ $name }}" id="{{ $id }}" class="" {{ old($name) || ($checked ?? false) ? 'checked' : '' }}>
        <div class="w-10 h-3 bg-gray-400 rounded-full shadow-inner toggle__line" ></div>
        <div class="absolute inset-y-0 left-0 w-6 h-3 bg-white rounded-full shadow toggle__dot"></div>
    </div>
    @if(isset($label))
    <span class="ml-2 text-gray-100">{{ $label }}</span>
    @endif
</div>