<div class="flex flex-col flex-wrap mb-6">
    <label for="name" class="block mb-2 text-xs font-bold text-gray-100">
        {{ __($label) }}:
    </label>
    <div class="flex flex-row w-4/12 px-1">
        <div class="relative w-full ml-auto">
            <select multiple class="block appearance-none w-full form-input bg-gray-700 border-none rounded-r rounded-l-none h-64 @error($id.$name)  border-red-500 @enderror" name="language-{{ $id }}[]" @if($required ?? false) required @endif>
                @foreach($data as $language)
                <option value="{{ json_encode($language) }}" class="selected:bg-purple-600"> {{ $language['language_name']}}</option>
                @endforeach
            </select>
        </div>
    </div>
    @error($name)
    <p class="mt-4 text-xs italic text-red-500">
        {{ $message }}
    </p>
    @enderror
</div>
