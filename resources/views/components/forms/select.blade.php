<div class="flex flex-row flex-wrap justify-between mb-6">
    @if(isset($label))
        <label for="{{ $name }}" class="self-center block w-auto mb-2 text-xs font-bold text-gray-100">
            {{ __($label) }}{{ $required == true ? '*' : ''}}:
        </label>
    @endif
    <div class="flex flex-row @if(isset($label)) w-8/12 @else w-full @endif">
        <i class="{{ $icon }} w-16 mb-6 text-center py-8 rounded-l"></i>
        <select name="roles[]" id="{{ $name }}" @if($multiple ?? false) multiple="multiple" @endif class="flex flex-row flex-wrap justify-between w-full px-2 pt-2 mb-6">
            @foreach($options as $key => $item)
                <option value="{{$item}}">{{$item}}</option>
            @endforeach
        </select>
    </div>
</div>
