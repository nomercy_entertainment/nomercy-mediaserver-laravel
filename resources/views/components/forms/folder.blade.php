<div id="folders-{{ $id }}" class="flex flex-col w-full h-32 mx-3 my-1 text-xs folders items-left bg-bray-900">
    <div class="flex flex-col flex-wrap mb-1">
        <label for="name-{{ $id }}" class="block px-1 mb-2 text-xs font-bold text-gray-100">
            {{ __($label) }}:
        </label>
        <div class="flex flex-row">
            <div class="flex flex-row w-4/12 h-8 px-1">
                <input id="name-{{ $id }}" type="text" class="form-input bg-gray-700 border-none w-full rounded-l rounded-r-none @error($id.$name)  border-red-500 @enderror" name="name-{{ $id }}" placeholder="{{ __('ui.name') }}" value="{{ old('name-' . $id) ?? $value['name'] ?? '' }}" @if($required ?? false) required @endif>
            </div>

            <div class="flex flex-row w-4/12 h-8 px-1">
                <div class="relative w-full ml-auto">
                    <select class="block appearance-none w-full form-input bg-gray-700 border-none rounded-r rounded-l-none h-8 @error($id.$name)  border-red-500 @enderror" name="type-{{ $id }}" placeholder="{{ __('setup.type') }}" value="{{ old('type-' . $id) ?? $value['type'] ?? '' }}" @if($required ?? false) required @endif>
                        <option value="{{ $value['type'] ?? '' }}" class="hidden" disabled selected>{{ __('setup.type') }}</option>
                        <option value="anime" class="">Anime</option>
                        <option value="movie" class="">Movies</option>
                        <option value="tv" class="">Tv Shows</option>
                        <option value="music" class="hidden" disabled title="Not implemented yet">Music</option>
                    </select>
                    <button class="absolute inset-y-0 right-0 flex items-center pl-2 text-gray-700 pointer-events-none">
                        <i id="button-{{ $id }}" data-id="{{ $id }}" class="w-12 h-8 py-1 text-xl font-extrabold text-center text-white bg-purple-500 rounded-r rounded-br-none folder-button fa fa-angle-down fa-2x"></i>
                    </button>
                </div>
            </div>

            <div class="flex flex-col items-end w-2/12 h-8 px-1">

                <div class="flex flex-col w-full h-5 px-1" title="{{ __('setup.hq-help') }}">
                    <div class="flex items-center justify-center w-full">
                        <label for="#hq-{{ $id }}" class="flex items-center">
                            <div class="mr-3 font-medium text-gray-700">
                                <span class="ml-2 text-gray-100">{{ __('setup.hq') }}</span>
                            </div>
                        </label>
                        <div class="relative ml-auto cursor-pointer" onclick="($(`#hq-{{ $id }}`).val() == ' false ') ? $(`#hq-{{ $id }}`).prop( 'checked', true ).val('on') : $(`#hq-{{ $id }}`).prop( 'checked', false ).val('off')">
                            <input type="checkbox" name="hq-{{ $id }}" id="hq-{{ $id }}" class="hidden toggle__dot" value="@if( $value['hq'] ?? false == true) true @else false @endif">
                            <div class="w-10 h-3 bg-gray-400 rounded-full shadow-inner toggle__line"></div>
                            <div class="absolute inset-y-0 left-0 w-6 h-3 bg-white rounded-full shadow toggle__dot"></div>
                            @if(isset($value['hq']))
                            <script>
                                $(`#hq-{{ $id }}`).prop('checked', true).val(true);
                            </script>
                            @endif
                        </div>
                    </div>
                </div>


                <div class="flex flex-col w-full h-5 px-1" title="{{ __('setup.direction-help') }}">
                    <div class="flex items-center justify-center w-full">
                        <label for="#direction-{{ $id }}" class="flex items-center">
                            <div class="mr-3 font-medium text-gray-700">
                                <span class="ml-2 text-gray-100">{{ __('setup.direction') }}:<span id="direction-state-{{ $id }}" class="ml-6 text-gray-100">{{ __('in') }}</span></span>
                            </div>
                        </label>
                        <div class="relative ml-auto cursor-pointer" onclick="($(`#direction-{{ $id }}`).val() == 'in') ? $(`#direction-{{ $id }}`).prop( 'checked', true ).val('out') : $(`#direction-{{ $id }}`).prop( 'checked', false ).val('in');
                                                                              ($(`#direction-{{ $id }}`).val() == 'in') ? $(`#direction-state-{{ $id }}`).text('in') : $(`#direction-state-{{ $id }}`).text('out')">

                            <input type="checkbox" name="direction-{{ $id }}" id="direction-{{ $id }}" class="hidden toggle__dot" value="@if( $value['direction'] ?? 'in' == 'out') out @else in @endif">
                            @if($value['direction'] ?? 'in' == "out")
                            <script>
                                $(`#direction-{{ $id }}`).prop('checked', true).val('out');
                                $(`#direction-state-{{ $id }}`).text('out')

                            </script>
                            @else
                            <script>
                                $(`#direction-{{ $id }}`).prop('checked', false).val('in');
                                $(`#direction-state-{{ $id }}`).text('in')
                            </script>
                            @endif

                            <div class="w-10 h-3 bg-gray-400 rounded-full shadow-inner"></div>
                            <div class="absolute inset-y-0 left-0 w-6 h-3 bg-white rounded-full shadow toggle__dot"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @error($name)
    <p class="mt-4 text-xs italic text-red-500">
        {{ $message }}
    </p>
    @enderror
    <div class="flex flex-col">
        <div class="flex flex-row items-center px-1 mt-2 bg-gray-800" onclick="$('#folder-{{ $id }}').parent().next().toggle()">
            <input id="folder-{{ $id }}" type="text" class="folder-folder form-input bg-gray-700 border-none w-full rounded-l rounded-r-none rounded-bl-none py-1 h-8 @error($id.$name)  border-red-500 @enderror" name="folder-{{ $id }}" placeholder="{{ __('setup.location') }}" value="{{ old('folder-' . $id) }} {{ $value['folder'] ?? '' }}" @if($required ?? false) required @endif>
            <i id="button-{{ $id }}" data-id="{{ $id }}" class="w-12 h-8 py-2 text-center bg-purple-500 rounded-r rounded-br-none folder-button fa fa-file"></i>
        </div>
        <div id='folder-body-{{ $id }}' data-id="{{ $id }}" class="z-10 hidden h-32 px-3 py-2 mx-1 mt-1 overflow-y-auto bg-gray-700 rounded rounded-tl-none folder-body">
            @php
            foreach ($base_folder as $folder) {
            $full_path = $disk->base_path . $folder;
            if(is_writable($full_path) && $folder !== '.$EXTEND' && $folder !== '.' && $folder !== '..' && $disk->directories($folder) != null){
            echo "<li data-folder=" . $folder . " data-full_path=" . $full_path . " class='w-full cursor-pointer hover:bg-purple-600'>" . $folder . "</li>";
            }
            }
            @endphp
        </div>
    </div>
</div>

<script>
    function read_folder(folder, id) {

        $.ajax({
            type: 'post',
            url: "{{ route('setup.folders') }}",
            data: {
                folder: folder,
                back: back,
                '_token': '{{ csrf_token() }} ',
            },
            success: function (data) {

                $('*').css('cursor', 'initial');
                $('li').css("cursor", "pointer");
                $('body').css('cursor', 'pointer');

                if (data.current_path != '/' && data.current_path != '//' && data.current_path != '') {
                    $(`#${id}`).append(`
                        <li data-folder="back" class='w-full cursor-pointer hover:bg-purple-600'>..</li>
                    `)
                }

                $.each(data.results, function (index, d) {
                    if (d.last == false) {
                        $(`#${id}`).append(`
                            <li data-folder="${d.folder}" data-full_path="${d.full_path}" class='w-full cursor-pointer hover:bg-purple-600'>${d.name}</li>
                        `)
                    }
                    if (d.last == true) {
                        $(`#${id}`).append(`
                            <li data-folder="${d.folder}" data-full_path="${d.full_path}" title="select this folder" ondblclick="alert('no more sub folders')" class='w-full cursor-pointer hover:bg-purple-600'>${d.name}</li>
                        `)
                    }
                });

                clickhandler(`${id}`);

                back = data.back;
            },
            error: function (data) {
                alert(data.responseJSON.message)
            }
        });
    }

    clickhandler('{{ $id }}');

</script>
