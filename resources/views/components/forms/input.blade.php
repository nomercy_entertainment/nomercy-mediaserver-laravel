<div class="flex flex-{{ $direction ?? 'row' }} flex-wrap justify-between mb-6">

    <div class="flex flex-row w-full">
        <i class="{{ $icon }} w-16 @if(isset($label)) h-16 pt-3 py-2 @else h-12 py-3 @endif text-center rounded-l flex flex-col">
            @if(isset($label))
            <label for="name" class="text-3xs " style="white-space: nowrap;">
                {{ __($label) }}{{ $required == true ? '*' : ''}}
            </label>
        @endif</i>
        <input id="{{ $id }}" type="{{ $type }}"
            class="theme form-input bg-gray-700 w-full rounded-r rounded-l-none @error($name) form-input-error @enderror @if(Session::has($name)) border-red-500 border-4 @else border-l-none border-gray-500  @endif"
            name="{{ $name }}" @if($required) required @endif
            placeholder="{{ $placeholder ?? '' }}"
            value="{{ old($id) ??  $value ?? '' }}"
            @if($autofocus ?? false) autofocus @endif
            @if($required ?? false) required @endif
            @if($autocomplete ?? false) autocomplete="{{ $autocomplete}}" @endif
        >
    </div>
</div>
