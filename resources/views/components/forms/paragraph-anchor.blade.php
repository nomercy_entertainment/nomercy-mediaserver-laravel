
<p class="w-full mt-8 -mb-4 text-xs text-center text-gray-100">
    {{ $paragraph }}
    <a class="w-full mt-8 -mb-4 text-xs text-center cursor-pointer" href="{{ $href }}" style="color: {{ themeColor() }};">
        {{ $anchor }}
    </a>
</p>
