
<button type="submit" class="px-4 py-1 mx-auto font-medium hover:text-gray-800 font-bolder" style="background: {{ themeColor() }};">
    {{ $label }}
</button>
