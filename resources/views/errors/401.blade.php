@extends('errors::illustrated-layout')

@section('code', '401')
@section('title', __('Unauthorized'))

@section('image')
<div style="background-image: url({{ asset('/svg/401.svg') }});" class="absolute bg-no-repeat bg-cover pin md:bg-left lg:bg-center">
</div>
@endsection

@section('message', __($exception->getMessage() ?: __('error.403')))
