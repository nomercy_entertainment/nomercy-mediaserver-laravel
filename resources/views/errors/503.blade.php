@extends('errors::illustrated-layout')

@section('code', '503')
@section('title', __('Service Unavailable'))

@section('image')
<div style="background-image: url({{ asset('/svg/503.svg') }});" class="absolute bg-no-repeat bg-cover pin md:bg-left lg:bg-center">
</div>
@endsection

@section('message', __($exception->getMessage() ?: 'error.503'))
