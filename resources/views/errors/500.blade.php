@extends('errors::illustrated-layout')

@section('code', '500')
@section('title', __('Error'))

@section('image')
<div style="background-image: url({{ asset('/svg/500.svg') }});" class="absolute bg-no-repeat bg-cover pin md:bg-left lg:bg-center">
</div>
@endsection

@section('message', __('error.500'))
