<style>
    .bg-theme{
        color: #fff;
        background: {{ themeColor() }};
    }
    nav a {
        filter: brightness(1.3) hue-rotate(-15deg) saturate(0.6);
    }
    .hover\:bg-theme:hover{
        filter: brightness(1.3) hue-rotate(-15deg) saturate(0.6);
    }

    .text-theme{
        color: {{ themeColor() }};
    }
    .hover\:text-theme:hover{
        filter: brightness(1.3) hue-rotate(-15deg) saturate(0.6);
    }

    i {
        background: {{ themeColor() }};
    }
    i {
        filter: brightness(1.3) hue-rotate(-15deg) saturate(0.6);
    }

    .fa, .fab, .fas {
        font-size: 25px;
    }

    ::-webkit-scrollbar {
        background-color: #181a1b;
    }
    ::-webkit-scrollbar-corner {
        background-color: #4a5568;
    }
    ::-webkit-scrollbar-thumb {
        background-color: {{ themeColor() }};
    }

    button.theme {
        background: {{ themeColor() }} !important;
    }

    input.theme,
    input.theme:focus,
    input.theme::placeholder,
    input.theme:-webkit-autofill,
    input.theme:-webkit-autofill:hover,
    input.theme:-webkit-autofill:focus,
    input.theme:-webkit-autofill:first-line,
    textarea,
    textarea:-webkit-autofill,
    textarea:-webkit-autofill:hover,
    textarea:-webkit-autofill:focus,
    select,
    select:-webkit-autofill,
    select:-webkit-autofill:hover,
    select:-webkit-autofill:focus {
        color: {{ themeColor() }} !important;
        -webkit-text-fill-color: {{ themeColor() }} !important;
        --webkit-autofill: {{ themeColor() }}!important;
        ::placeholder-color: {{themeColor()}}!important;
        font-weight: 800 !important;
        font-size: 1rem;
        -webkit-box-shadow: 0 0 0px 1000px #323232 inset;
        filter: hue-rotate(-10deg) saturate(0.6);

    }
    /* path {
        fill: {{ themeColor() }};
    } */

    input:checked~.toggle__line {
        background: {{ themeColor() }};
        filter: brightness(1.3) hue-rotate(-15deg) saturate(0.6);
    }

    ::placeholder {
        -webkit-text-fill-color: {{ themeColor() }} !important;
    }

    .sidebar-link {
        color: {{ themeColor() }} !important;
    }
    .sidebar-button{
        color: {{ themeColor() }} !important;
        filter: brightness(1.3) hue-rotate(-15deg) saturate(0.6);
    }
    .sidebar-link::active {
        color: #fff !important;
        background: {{ themeColor() }} !important;
    }

    .jw-progress {
        background-color: {{ themeColor() }} !important;
    }


</style>
