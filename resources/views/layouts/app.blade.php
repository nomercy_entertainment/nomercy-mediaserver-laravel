<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {{-- <link rel="icon" href="{{ asset('/img/favicon.ico')}}" type="image/x-icon" /> --}}
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <title>@yield('title', 'NoMercy TV')</title>
        <meta name="title" content="@yield('title', 'NoMercy TV')">
        <meta name="description" content="@yield('description', __('ui.description'))">
        <meta name="copyleft" content="Stoney Eagle">


        <meta name="og:image:type" content="@yield('type', 'image/png')">
        <meta name="og:image:width" content="512" />
        <meta name="og:image:height" content="512" />
        <meta property="og:title" content="@yield('title', 'NoMercy TV')">
        <meta property="og:description" content="@yield('description', __('ui.description'))">
        <meta property="og:type" content="website">
        <meta property="og:url" content="{{url()->current()}}">
        <meta property="og:title" content="@yield('title', 'NoMercy TV')">
        <meta property="og:description" content="@yield('description', __('ui.description'))">
        <meta property="og:image" content="@yield('image', asset('/img/apple-touch-icon-120x120.png'))">
        <meta property="twitter:card" content="summary_large_image">
        <meta property="twitter:url" content="{{url()->current()}}">
        <meta property="twitter:title" content="@yield('title', 'NoMercy TV')">
        <meta property="twitter:description" content="@yield('description', __('ui.description'))">
        <meta property="twitter:image" content="@yield('image', asset('/img/apple-touch-icon-120x120.png'))">

        <link rel="preload" as="style"      href="{{ asset('/css/app.css') }}">
        <link rel="stylesheet"              href="{{ asset('/css/app.css') }}">

        <link rel="preload" as="style"      href="{{ asset('/css/fontawesome.css') }}">
        <link rel="stylesheet"              href="{{ asset('/css/fontawesome.css') }}">


        <script src="{{ asset('/js/jquery-3.5.1.min.js?v=' . asset_tag()) }}"></script>
        <script defer src="{{ asset('/js/app.js?v=' . asset_tag()) }}"></script>

        <link rel="apple-touch-icon" sizes="180x180" href="/img/apple-touch-icon.png?v=OmKEGJ3xRW">
        <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png?v=OmKEGJ3xRW">
        <link rel="icon" type="image/png" sizes="192x192" href="/img/android-chrome-192x192.png?v=OmKEGJ3xRW">
        <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png?v=OmKEGJ3xRW">
        <link rel="manifest" href="/img/site.webmanifest?v=OmKEGJ3xRW">
        <link rel="mask-icon" href="/img/safari-pinned-tab.svg?v=OmKEGJ3xRW">
        <link rel="shortcut icon" href="/img/favicon.ico?v=OmKEGJ3xRW">
        <meta name="apple-mobile-web-app-title" content="NoMercy MediaServer">
        <meta name="application-name" content="NoMercy MediaServer">
        <meta name="msapplication-TileColor" content="#000000">
        <meta name="msapplication-TileImage" content="/img/mstile-144x144.png?v=OmKEGJ3xRW">
        <meta name="msapplication-config" content="/img/browserconfig.xml?v=OmKEGJ3xRW">
        <meta name="theme-color" content="#000000">

        @yield('head')
        @include('layouts.theme')

    </head>

    <body class="relative h-screen font-sans text-xs text-white bg-gray-700">
        @include('layouts.navbar')
        <div id="content" class="content-center justify-center py-16 mx-auto bg-gray-700 h-min">
            @yield('content')
        </div>

        @if (View::hasSection('footer'))
            @include('layouts.footer')
        @endif
        @yield('scripts')

        <script src="{{ asset('/js/app.js?v=OmKEGJ3xRW') }}"></script>

    </body>

</html>
