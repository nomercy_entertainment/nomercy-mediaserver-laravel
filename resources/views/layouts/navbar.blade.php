<nav class="fixed top-0 z-50 w-full h-16 bg-black border-b border-none ">
    <div class="flex flex-col w-full h-16 text-center whitespace-no-wrap md:flex-row xl:h-16 lg:px-20 lg:py-3">
        <ul class="flex flex-col items-center w-screen pt-16 mb-0 overflow-hidden bg-black md:w-full md:pt-0 md:flex-row md:bg-transparent">
            <li class="absolute top-0 left-0 w-48 h-16 nav-logo md:relative">
                <a class="" href="/">
                    @include('components.svg.logo')
                </a>
            </li>
            <li class="mt-1 lg:w-32 lg:ml-16">
                <a href="{{ route('anime') }}" class="px-32 py-1 font-bold hover:text-gray-300 md:px-4 lg:px-4 lg:py-3">{{__('route.anime') }}</a>
            </li>
            <li class="mt-1 lg:w-32">
                <a href="{{ route('movies') }}" class="px-32 py-1 font-bold hover:text-gray-300 md:px-4 lg:px-4 lg:py-3">{{__('route.movies') }}</a>
            </li>
            <li class="mt-1 lg:w-32">
                <a href="{{ route('tv') }}" class="px-32 py-1 font-bold hover:text-gray-300 md:px-4 lg:px-4 lg:py-3">{{__('route.tv') }}</a>
            </li>
            <li class="mt-1 lg:w-32">
                <a href="{{ route('collections') }}" class="px-32 py-1 font-bold hover:text-gray-300 md:px-4 lg:px-4 lg:py-3">{{ trans_choice('route.collection', 2) }}</a>
            </li>

        </ul>
        @auth

        <div id="navSearch" class="absolute top-0 right-0 h-16 py-2 mr-0 text-gray-100 md:mr-20 navSearch darkVibrant">
            <input id="search" class="h-full pl-4 text-sm bg-transparent border-gray-300 border-none rounded-lg rounded-r border-1 focus:outline-none" type="search" name="search">
            <button id="search-btn" type="button" class="absolute top-0 right-0 px-4 py-6 border-none xl:mt-1 focus:outline-none" onclick="searchToggle()">
                <svg class="w-4 h-4 text-gray-100 fill-current" xmlns="http://www.w3.org/2000/svg" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 56.966 56.966" style="enable-background:new 0 0 56.966 56.966;" xml:space="preserve" width="512px" height="512px">
                    <path d="M55.146,51.887L41.588,37.786c3.486-4.144,5.396-9.358,5.396-14.786c0-12.682-10.318-23-23-23s-23,10.318-23,23  s10.318,23,23,23c4.761,0,9.298-1.436,13.177-4.162l13.661,14.208c0.571,0.593,1.339,0.92,2.162,0.92  c0.779,0,1.518-0.297,2.079-0.837C56.255,54.982,56.293,53.08,55.146,51.887z M23.984,6c9.374,0,17,7.626,17,17s-7.626,17-17,17  s-17-7.626-17-17S14.61,6,23.984,6z" />
                </svg>
            </button>
        </div>

        <button id="profile-button" class="absolute top-0 right-0 flex flex-row items-center self-center float-right w-40 h-12 mt-2 ml-0 md:mr-12 md:ml-12 focus:outline-none" onclick="navToggle()">
            <span class="px-2 py-4 text-sm text-gray-300 float-center focus:outline-none">{{ Auth::user()->name }}</span>
            <img src="{{ Auth::user()->gravatar ?? '' }}" alt="avatar" class="object-contain w-10 h-10 overflow-hidden border-2 border-gray-600 rounded-full focus:outline-none focus:border-white" style="min-width: 2.5rem;">
        </button>


        <nav id="profile-menu" class="fixed right-0 flex-col hidden w-56 pt-12 mr-20 border border-gray-700 rounded md:pt-0 md:mt-12 click-menu">
            <a href="{{ route('profile') }}" class="px-32 py-1 font-bold bg-black hover:text-gray-300 md:px-4 lg:px-4 lg:py-3" target="_blank">{{__('route.profile') }}</a>
            {{-- @if(Auth::user()->isOwner) --}}
            <a href="{{ route('queue') }}" class="px-32 py-1 font-bold bg-black hover:text-gray-300 md:px-4 lg:px-4 lg:py-3">{{__('route.queue') }}</a>
            <a href="{{ route('supervisor') }}" class="px-32 py-1 font-bold bg-black hover:text-gray-300 md:px-4 lg:px-4 lg:py-3">{{__('route.supervisor') }}</a>
            <a href="{{ route('add') }}" class="px-32 py-1 font-bold bg-black hover:text-gray-300 md:px-4 lg:px-4 lg:py-3">{{__('route.add') }}</a>
            {{-- @endif --}}
            <a href="{{ route('logout') }}" class="px-32 py-1 font-bold bg-black border-b border-gray-700 hover:text-gray-300 md:px-4 lg:px-4 lg:py-3" onclick="event.preventDefault();document.getElementById('logout-form').submit();">{{ __('route.logout') }}</a>
            <div class="w-full h-12 px-4 py-3 text-sm text-center text-gray-300 bg-gray-900 hover:bg-gray-700 hover:text-gray-100">
                <a href="https://nomercy.tv" class="block float-left w-auto" role="menuitem">NoMercy.tv
                </a>
                <svg class="float-right w-10 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"></path></svg>
            </div>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
                {{ csrf_field() }}
            </form>
        </nav>
        <div id="results" class="fixed top-0 right-0 flex-col hidden mt-1 overflow-y-auto w-326 lg:w-4/12"></div>

        @endauth
        @guest
        <ul class="flex flex-col items-center md:flex-row lg:ml-auto">
            <li class="mt-2 md:ml-6">
                <a href="/" class="py-2 hover:text-gray-300">{{__('ui.back') }}</a>
            </li>

            <li class="mt-2 md:ml-6">
                <a href="{{ route('login') }}" class="py-2 hover:text-gray-300">login</a>
            </li>
            @if (Route::has('register'))
            <li class="mt-2 md:ml-6">
                <a href="{{ route('register') }}" class="py-2 hover:text-gray-300">register</a>
            </li>
            @endif
        </ul>
        @endguest
    </div>
</nav>

<script>
    let open = false;
    window.searchToggle = function () {
        $('#search-btn').prev().toggleClass('expanded').delay(4000);
        if (!open) {
            $('#search').attr('placeholder', "Search..").css({
                background: $('meta[name="themeColor"]').attr('content') || 'darkorchid',
            });
            $('input#search').css('display', 'block').focus();
            open = true;
        }
        else {
            $('#search').attr('placeholder', '').css({
                background: 'transparent',
            });
            $('input#search').blur();
            open = false;
        }
    };

    let toggleNavStatus = false;
    window.navToggle = navToggle = () => {
        let getMenu = document.querySelector('#profile-menu');

        if (toggleNavStatus === false) {
            $('ul').removeClass('overflow-hidden');
            getMenu.classList.add("show")
            toggleNavStatus = true;
        } else {
            $('ul').addClass('overflow-hidden');
            getMenu.classList.remove("show")
            toggleNavStatus = false;
        }
    };

    $("[href]").each(function () {
        if (this.href == window.location.href) {
            $(this).css({
                color: $('meta[name="themeColor"]').attr('content') || 'darkorchid'
            });
        }
    });

</script>
