<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" href="{{ asset('/img/favicon.ico')}}" type="image/x-icon" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <title>@yield('title', 'NoMercy TV')</title>
        <meta name="title" content="@yield('title', 'NoMercy TV')">
        <meta name="description" content="@yield('description', __('ui.description'))">
        <meta name="copyleft" content="Stoney Eagle">

        <link rel="preload" as="style"      href="{{ asset('/css/app.css') }}">
        <link rel="stylesheet"              href="{{ asset('/css/app.css') }}">

        <link rel="preload" as="style"      href="{{ asset('/css/fontawesome.css') }}">
        <link rel="stylesheet"              href="{{ asset('/css/fontawesome.css') }}">


        <script defer src="{{ asset('/js/jquery-3.5.1.min.js?v=' . asset_tag()) }}"></script>
        <script defer src="{{ asset('/js/app.js?v=' . asset_tag()) }}"></script>

        <meta name="theme-color" content="#000" />

        @yield('head')
        @include('layouts.theme')

    </head>

    <body class="relative h-screen font-sans text-white bg-gray-700 text-xs">
        <div id="content" class="grid items-center content-center justify-center h-screen px-8 py-16 mx-auto bg-gray-700">
            @yield('content')
        </div>

        @yield('scripts')
        @if (View::hasSection('footer'))
            @include('layouts.footer')
        @endif


    </body>

</html>
