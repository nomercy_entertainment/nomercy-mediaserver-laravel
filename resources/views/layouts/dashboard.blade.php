<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <title>@yield('title', 'NoMercy TV')</title>
        <meta name="title" content="@yield('title', 'NoMercy TV')">
        <meta name="description" content="@yield('description', __('ui.description'))">
        <meta name="copyleft" content="Stoney Eagle">


        <meta name="og:image:type" content="@yield('type', 'image/png')">
        <meta name="og:image:width" content="512" />
        <meta name="og:image:height" content="512" />
        <meta property="og:title" content="@yield('title', 'NoMercy TV')">
        <meta property="og:description" content="@yield('description', __('ui.description'))">
        <meta property="og:type" content="website">
        <meta property="og:url" content="{{url()->current()}}">
        <meta property="og:title" content="@yield('title', 'NoMercy TV')">
        <meta property="og:description" content="@yield('description', __('ui.description'))">
        <meta property="og:image" content="@yield('image', asset('/img/apple-touch-icon-120x120.png'))">
        <meta property="twitter:card" content="summary_large_image">
        <meta property="twitter:url" content="{{url()->current()}}">
        <meta property="twitter:title" content="@yield('title', 'NoMercy TV')">
        <meta property="twitter:description" content="@yield('description', __('ui.description'))">
        <meta property="twitter:image" content="@yield('image', asset('/img/apple-touch-icon-120x120.png'))">

        <link rel="preload" as="style"      href="{{ asset('/css/app.css?v=' . asset_tag()) }}">
        <link rel="stylesheet"              href="{{ asset('/css/app.css?v=' . asset_tag()) }}">

        <link rel="preload" as="style"      href="{{ asset('/css/fontawesome.css') }}">
        <link rel="stylesheet"              href="{{ asset('/css/fontawesome.css') }}">


        <script src="{{ asset('/js/jquery-3.5.1.min.js') }}"></script>
        <script defer src="{{ asset('/js/app.js?v=' . asset_tag()) }}"></script>

        <link rel="apple-touch-icon" sizes="180x180" href="/img/apple-touch-icon.png?v={{ asset_tag() }}">
        <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png?v={{ asset_tag() }}">
        <link rel="icon" type="image/png" sizes="192x192" href="/img/android-chrome-192x192.png?v={{ asset_tag() }}">
        <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png?v={{ asset_tag() }}">
        <link rel="manifest" href="/img/site.webmanifest?v={{ asset_tag() }}">
        <link rel="mask-icon" href="/img/safari-pinned-tab.svg?v={{ asset_tag() }}">
        <link rel="shortcut icon" href="/img/favicon.ico?v={{ asset_tag() }}">
        <meta name="apple-mobile-web-app-title" content="NoMercy MediaServer">
        <meta name="application-name" content="NoMercy MediaServer">
        <meta name="msapplication-TileColor" content="#000000">
        <meta name="msapplication-TileImage" content="/img/mstile-144x144.png?v={{ asset_tag() }}">
        <meta name="msapplication-config" content="/img/browserconfig.xml?v={{ asset_tag() }}">
        <meta name="theme-color" content="#000000">
        <script src="{{ asset('/js/jquery-3.5.1.min.js') }}"></script>

        <script>

            window.isTizen = (/Tizen/).test(navigator.userAgent);
            window.isPlaystation = (/Playstation/).test(navigator.userAgent);
            window.isSony = (/Chrome\/49/).test(navigator.userAgent);

            if(window.isTizen || window.isPlaystation || window.isSony){

                window.addEventListener('warning', function (e) {
                    console.log(e);
                    alert(e.source + ', ' + e.message + ', ' + e.lineno + ', ' + e.colno);
                });

                window.addEventListener('error', function (e) {
                    console.log(e);
                    alert(e.source + ', ' + e.message + ', ' + e.lineno + ', ' + e.colno);
                });
            }
        </script>

        <style>
            body {

                overflow-x: hidden;
            }
        </style>

        @yield('head')
        @include('layouts.theme')
    </head>

    <body class="w-screen h-screen pt-16 overflow-x-hidden font-sans text-xs text-white bg-black">
        @include('layouts.navbar')
        @yield('content')

        @if (View::hasSection('footer'))
            @include('layouts.footer')
        @endif
        @yield('scripts')

    </body>

</html>
