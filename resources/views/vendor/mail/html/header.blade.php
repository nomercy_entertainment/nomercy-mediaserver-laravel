<tr>
    <td class="header">
        <a href="{{ $url }}" style="display: inline-block;">
            <img src="{{ asset('/img/nomercy-entertainment.svg') }}" class="logo" alt="NoMercy Logo">
        </a>
    </td>
</tr>
