@extends('layouts.dashboard')

@section('title', __('route.supervisor') )
@section('description', __('ui.description'))
@section('content')

<div id="content" class="content-center justify-center px-2 py-4 mx-auto bg-gray-700 h-min md:px-6">
    <div class="w-full h-full text-xs bg-gray-800">
        <div class="flex flex-row w-full h-12 px-4 py-3 text-white bg-gray-900" id="header-4">
            <div class="float-left w-1/12" id="tail-title"></div>
            <div class="w-full text-center">
                @foreach($files as $file)
                <button id="button-{{ $loop->index }}" data-id="{{ $loop->index }}" class="px-2 py-1 mx-1 text-xs font-medium bg-purple-600 rounded tail-btn">{{ str_replace(['var/www/log/supervisor/laravel-', '.log'], '',$file) }}</button>
                @endforeach
            </div>
            <div class="float-right w-3/12">
                <button id="button-clear" class="float-right px-4 py-1 mx-2 ml-auto font-medium bg-purple-600 rounded">{{ __('ui.clear.log')}}</button>
                <button id="button-enable" class="float-right px-4 py-1 mx-2 ml-auto font-medium bg-purple-600 rounded" onclick="update=true">{{ __('ui.scroll-true')}}</button>
                <button id="button-disable" class="float-right px-4 py-1 mx-2 font-medium bg-purple-600 rounded" onclick="update=false">{{ __('ui.scroll-false')}}</button>
            </div>
        </div>
        @foreach($files as $file)
        <div id="tail-switch-{{ $loop->index }}" class="w-full text-xs tail-card" style="@if(!$loop->first) display:none @endif">
            <div class="w-full px-8 py-8 overflow-y-auto text-xs bg-gray-800 tail-data" id="tail-data-{{ $loop->index }}""></div>
                </div>
            @endforeach
        </div>
    </div>

    <script>

        let update = true;

        $(function () {
            setInterval(() => {

                if(!document.hidden){
                    tail();
                }
            }, 5000);
        });

        function tail(){
            $.get(" {{ route('tail') }}", function (data) {
                data.forEach(function (log, index) {
                    $(`#tail-title`).text(log.file.replace('var/www/log/supervisor/', '' ));
                    $(`#tail-data-${index}`).empty();
                    $(`#tail-data-${index}`).append(log.data);
                    if(update){
                        $(`#tail-data-${index}`).animate({ scrollTop: $("#tail-data-0").height() * 1000});
                    }
                });
                if(data=='' ){
                    $(`#tail-data-${index}`).empty();
                }
            });
        }
        tail();


        document.addEventListener("visibilitychange", function() {
            if (document.visibilityState === 'visible') {
                tail();
            }
        });


        $(".tail-btn").each(function (index) {
            $(this).on("click", function () {
                console.log($(this));
                $('.tail-card').css('display', 'none' );
                $(`#tail-switch-${index}`).css('display', 'block' );
                $(`#tail-title`).text($(this).text());
            });
        });
        $('#button-clear').on('click', function() {
            $.ajax({
                type: 'post' ,
                data: { '_token' : "{{ csrf_token() }}" },
                url: "{{ route('clear_log') }}" ,
                success: function(data) {
                    $("#message").append(` <div class="px-4 py-3 mb-2 text-blue-700 bg-blue-100 border-t border-b border-blue-500 message"" role=" alert">
                        <button type="button" class="float-right close" data-dismiss="alert">&times;</button>
                        <p class="font-bold">${success}</p>
                        <p class="text-sm">${data.message}</p>
                        </div>
                    `);
                    $('.close,#add').on('click', function () {
                    $(this).parents('.message').fadeOut();
                    });
                },
                error: function (data) {
                    $("#message").append(`
                        <div class="px-4 py-3 text-red-700 bg-red-100 border-t border-b border-red-500" role="alert">
                            <button type="button" class="float-right close" data-dismiss="alert">&times;</button>
                            <p class="font-bold">${error}</p>
                            <p class="text-sm">${data.message}</p>
                        </div>
                    `);
                    $('.close,#add').on('click', function () {
                        $(this).parents('.message').fadeOut();
                    });
                },
            });
        });

    </script>
@endsection
