@extends('layouts.dashboard')

@section('content')
<div id="content" class="content-center justify-center px-2 mx-auto bg-gray-700 h-min md:px-12">
    <iframe id="rtmp" src="/stats" frameborder="0" class="absolute inset-x-0 w-3/4 mx-auto -mt-1 bg-white rounded-lg rounded-t-none shadow-xl md:w-2/5" style="height: 43vh; width: 37vw;"></iframe>
</div>
<script>
    setInterval(() => {
        $('#rtmp').attr("src", $('#rtmp').attr("src"));
    }, 5000);
</script>
@endsection
