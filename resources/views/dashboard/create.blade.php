@extends('layouts.dashboard')

@section('title', 'NoMercy Search')
@section('description', __('ui.description'))

@section('content')
<style>
    #results {
        height: calc(100vh - 150px);
    }
</style>

<div id="content" class="content-center justify-center px-2 pt-6 mx-auto bg-gray-700 h-min md:px-6">
    <div id="message" class="w-full"></div>
    <div class="panel-body">
        <div class="flex justify-center w-full bg-gray-900 input min:h-12">
            @csrf
            <div class="flex flex-col text-white md:flex-row">
                <input id="query" name="query" type="text" class="w-48 px-2 m-2 text-black" value="" required autofocus placeholder="{{ __('ui.name')}}">
                <input id="year" name="year" type="text" class="hidden px-2 m-2 text-black" value="" required autofocus placeholder="{{ __('ui.year')}}">

                @php
                $language_codes = [
                    'en' => 'English' ,
                    'aa' => 'Afar' ,
                    'ab' => 'Abkhazian' ,
                    'af' => 'Afrikaans' ,
                    'am' => 'Amharic' ,
                    'ar' => 'Arabic' ,
                    'as' => 'Assamese' ,
                    'ay' => 'Aymara' ,
                    'az' => 'Azerbaijani' ,
                    'ba' => 'Bashkir' ,
                    'be' => 'Byelorussian' ,
                    'bg' => 'Bulgarian' ,
                    'bh' => 'Bihari' ,
                    'bi' => 'Bislama' ,
                    'bn' => 'Bengali/Bangla' ,
                    'bo' => 'Tibetan' ,
                    'br' => 'Breton' ,
                    'ca' => 'Catalan' ,
                    'co' => 'Corsican' ,
                    'cs' => 'Czech' ,
                    'cy' => 'Welsh' ,
                    'da' => 'Danish' ,
                    'de' => 'German' ,
                    'dz' => 'Bhutani' ,
                    'el' => 'Greek' ,
                    'eo' => 'Esperanto' ,
                    'es' => 'Spanish' ,
                    'et' => 'Estonian' ,
                    'eu' => 'Basque' ,
                    'fa' => 'Persian' ,
                    'fi' => 'Finnish' ,
                    'fj' => 'Fiji' ,
                    'fo' => 'Faeroese' ,
                    'fr' => 'French' ,
                    'fy' => 'Frisian' ,
                    'ga' => 'Irish' ,
                    'gd' => 'Scots/Gaelic' ,
                    'gl' => 'Galician' ,
                    'gn' => 'Guarani' ,
                    'gu' => 'Gujarati' ,
                    'ha' => 'Hausa' ,
                    'hi' => 'Hindi' ,
                    'hr' => 'Croatian' ,
                    'hu' => 'Hungarian' ,
                    'hy' => 'Armenian' ,
                    'ia' => 'Interlingua' ,
                    'ie' => 'Interlingue' ,
                    'ik' => 'Inupiak' ,
                    'in' => 'Indonesian' ,
                    'is' => 'Icelandic' ,
                    'it' => 'Italian' ,
                    'iw' => 'Hebrew' ,
                    'ja' => 'Japanese' ,
                    'ji' => 'Yiddish' ,
                    'jw' => 'Javanese' ,
                    'ka' => 'Georgian' ,
                    'kk' => 'Kazakh' ,
                    'kl' => 'Greenlandic' ,
                    'km' => 'Cambodian' ,
                    'kn' => 'Kannada' ,
                    'ko' => 'Korean' ,
                    'ks' => 'Kashmiri' ,
                    'ku' => 'Kurdish' ,
                    'ky' => 'Kirghiz' ,
                    'la' => 'Latin' ,
                    'ln' => 'Lingala' ,
                    'lo' => 'Laothian' ,
                    'lt' => 'Lithuanian' ,
                    'lv' => 'Latvian/Lettish' ,
                    'mg' => 'Malagasy' ,
                    'mi' => 'Maori' ,
                    'mk' => 'Macedonian' ,
                    'ml' => 'Malayalam' ,
                    'mn' => 'Mongolian' ,
                    'mo' => 'Moldavian' ,
                    'mr' => 'Marathi' ,
                    'ms' => 'Malay' ,
                    'mt' => 'Maltese' ,
                    'my' => 'Burmese' ,
                    'na' => 'Nauru' ,
                    'ne' => 'Nepali' ,
                    'nl' => 'Dutch' ,
                    'no' => 'Norwegian' ,
                    'oc' => 'Occitan' ,
                    'om' => '(Afan)/Oromoor/Oriya' ,
                    'pa' => 'Punjabi' ,
                    'pl' => 'Polish' ,
                    'ps' => 'Pashto/Pushto' ,
                    'pt' => 'Portuguese' ,
                    'qu' => 'Quechua' ,
                    'rm' => 'Rhaeto-Romance' ,
                    'rn' => 'Kirundi' ,
                    'ro' => 'Romanian' ,
                    'ru' => 'Russian' ,
                    'rw' => 'Kinyarwanda' ,
                    'sa' => 'Sanskrit' ,
                    'sd' => 'Sindhi' ,
                    'sg' => 'Sangro' ,
                    'sh' => 'Serbo-Croatian' ,
                    'si' => 'Singhalese' ,
                    'sk' => 'Slovak' ,
                    'sl' => 'Slovenian' ,
                    'sm' => 'Samoan' ,
                    'sn' => 'Shona' ,
                    'so' => 'Somali' ,
                    'sq' => 'Albanian' ,
                    'sr' => 'Serbian' ,
                    'ss' => 'Siswati' ,
                    'st' => 'Sesotho' ,
                    'su' => 'Sundanese' ,
                    'sv' => 'Swedish' ,
                    'sw' => 'Swahili' ,
                    'ta' => 'Tamil' ,
                    'te' => 'Tegulu' ,
                    'tg' => 'Tajik' ,
                    'th' => 'Thai' ,
                    'ti' => 'Tigrinya' ,
                    'tk' => 'Turkmen' ,
                    'tl' => 'Tagalog' ,
                    'tn' => 'Setswana' ,
                    'to' => 'Tonga' ,
                    'tr' => 'Turkish' ,
                    'ts' => 'Tsonga' ,
                    'tt' => 'Tatar' ,
                    'tw' => 'Twi' ,
                    'uk' => 'Ukrainian' ,
                    'ur' => 'Urdu' ,
                    'uz' => 'Uzbek' ,
                    'vi' => 'Vietnamese' ,
                    'vo' => 'Volapuk' ,
                    'wo' => 'Wolof' ,
                    'xh' => 'Xhosa' ,
                    'yo' => 'Yoruba' ,
                    'zh' => 'Chinese' ,
                    'zu' => 'Zulu' ,
                ];
                @endphp

                <select id="language" class="hidden px-2 mx-2 text-black">
                    @foreach ($language_codes as $key => $value)
                    <option value="{{ $key }}">{{ $value }}</option>
                    @endforeach
                </select>
                <input id="submit" name="submit" type="submit" class="float-right h-8 px-2 py-1 m-2 mt-2 text-xs font-medium bg-purple-600 rounded md:mb-2" value="{{ __('ui.submit')}}">

                <button id="add-in" type="button" class="float-right h-8 px-2 py-1 m-2 text-xs font-medium bg-purple-600 rounded md:mb-2">{{ __('Add New') }}</button>
                <button id="add-out" type="button" class="float-right h-8 px-2 py-1 m-2 text-xs font-medium bg-purple-600 rounded md:mb-2">{{ __('Add Existing') }}</button>
            </div>

        </div>
    </div>

    <div id="result" class="ml-lg-2 col-12"></div>
</div>



<script type="text/javascript">

    let error = '{{ __('ui.error')}}';
    let warning = '{{ __('ui.warning')}}';
    let success = '{{ __('ui.success')}}';
    $('#query, #language, #year').on('keyup, change', function () {

        $query = $('#query').val();
        $language = $('.language').val() || 'en';
        $year = $('#year').val() || 0;

        $.ajax({
            type: 'post',
            url: "{{ route('searchtmdb') }}",
            data: {
                'search': $query,
                'year': $year,
                'language': $language,
                '_token': "{{ csrf_token() }}"
            },
            success: function (data) {
                $('#result').html(data);
            },
            error: function (data) {
                $("#message").append(`
                    <div class="px-4 py-3 text-red-900 bg-red-100 border-t-4 border-red-500 rounded-b shadow-md" role="alert">
                        <div class="flex">
                            <div class="float-left py-1">
                                <svg class="w-6 h-6 mr-4 text-teal-500 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"></path></svg>
                            </div>
                            <p class="font-bold">${error}</p>
                            <p class="pl-3 text-sm">${data.responseJSON.message}</p>
                            <button type="button" class="float-right px-4 ml-auto close" data-dismiss="alert" onclick="$(this).parent().parent().remove()">×</button>
                        </div>
                    </div>
                `);
            },

        });
    });

    $('#add-in').on('click', function () {
        $.ajax({
            type: 'post',
            data: {
                _token: "{{ csrf_token() }}",
                direction: 'in'
            },
            url: "{{ route('find_media') }}",
            success: function (data) {
                $("#message").append(`
                    <div class="px-4 py-3 text-green-900 bg-green-100 border-t-4 border-green-500 rounded-b shadow-md" role="alert">
                        <div class="flex">
                            <div class="float-left py-1">
                                <svg class="w-6 h-6 mr-4 text-teal-500 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"></path></svg>
                            </div>
                            <p class="font-bold">${success}</p>
                            <p class="pl-3 text-sm">${data.message}</p>
                            <button type="button" class="float-right ml-auto close" data-dismiss="alert" onclick="$(this).parent().parent().remove()">×</button>
                        </div>
                    </div>
                `);
            },
            error: function (data) {
                $("#message").append(`
                    <div class="px-4 py-3 text-red-900 bg-red-100 border-t-4 border-red-500 rounded-b shadow-md" role="alert">
                        <div class="flex">
                            <div class="float-left py-1">
                                <svg class="w-6 h-6 mr-4 text-teal-500 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"></path></svg>
                            </div>
                            <p class="font-bold">${error}</p>
                            <p class="pl-3 text-sm">${data.responseJSON.message}</p>
                            <button type="button" class="float-right ml-auto close" data-dismiss="alert" onclick="$(this).parent().parent().remove()">×</button>
                        </div>
                    </div>
                `);
            },
        });
    });
    $('#add-out').on('click', function () {
        $.ajax({
            type: 'post',
            data: {
                _token: "{{ csrf_token() }}",
                direction: 'out'
            },
            url: "{{ route('find_media') }}",
            success: function (data) {
                $("#message").append(`
                    <div class="px-4 py-3 text-green-900 bg-green-100 border-t-4 border-green-500 rounded-b shadow-md" role="alert">
                        <div class="flex">
                            <div class="float-left py-1">
                                <svg class="w-6 h-6 mr-4 text-teal-500 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"></path></svg>
                            </div>
                            <p class="font-bold">${success}</p>
                            <p class="pl-3 text-sm">${data.message}</p>
                            <button type="button" class="float-right ml-auto close" data-dismiss="alert" onclick="$(this).parent().parent().remove()">×</button>
                        </div>
                    </div>
                `);
            },
            error: function (data) {
                $("#message").append(`
                    <div class="px-4 py-3 text-red-900 bg-red-100 border-t-4 border-red-500 rounded-b shadow-md" role="alert">
                        <div class="flex">
                            <div class="float-left py-1">
                                <svg class="w-6 h-6 mr-4 text-teal-500 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"></path></svg>
                            </div>
                            <p class="font-bold">${error}</p>
                            <p class="pl-3 text-sm">${data.responseJSON.message}</p>
                            <button type="button" class="float-right ml-auto close" data-dismiss="alert" onclick="$(this).parent().parent().remove()">×</button>
                        </div>
                    </div>
                `);
            },
        });
    });

    $('.close,#add').click(function () {
        $(this).parents('.message').fadeOut();
    });

</script>


@endsection
