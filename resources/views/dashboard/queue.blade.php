@extends('layouts.dashboard')

@section('title', __('route.queue') )
@section('description', __('ui.description'))
@section('content')

<div id="content" class="flex flex-col content-center justify-start px-2 pt-4 mx-auto bg-gray-700 md:flex-row h-min md:px-6">
    <div class="flex flex-col float-left w-full mb-2 overflow-y-auto lg:w-5/12 md:pr-3" style="max-height: 100%;">
        @foreach($workers as $key => $progress)

        <div id="block-{{ $key }}" class="flex flex-col w-full mb-2">
            <div id="header-4" class="flex flex-row h-12 px-4 py-3 text-white bg-gray-900">
                <div id="encoder-{{ $key }}" class="float-left">Encoder: {{ $progress->workerName }}</div>
                <div id="id-{{ $key }}" class="float-left"></div>
                <div id="active-{{ $key }}" class="ml-auto"></div>
            </div>

            @for($i = 0; $i < $progress->workers; $i++)
                <div class="px-4 py-1 bg-gray-800 bg-dark" id="card-{{ $key }}-{{ $i }}" style="display:none">
                    <div class="flex flex-col justify-center w-3/12 mx-auto md:w-2/12 py-4">
                        <img id="img-{{ $key }}-{{ $i }}" src="" onerror="this.src='{{ asset('/img/still-not-available.png')}}'" alt="" style="display: block;" width="100%">
                    </div>
                    <div class="w-9/12 p-3 md:w-10/12">
                        <div class="mt-1">
                            <div class="progress-bars">
                                <div class="progress-bar-background"></div>
                                <div class="inner-progress-bars" tabindex="0">
                                    <div class="progress-bar-working" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                        <span class="progress-bar-text">
                                            <span id="progress_text-{{ $key }}-{{ $i }}" class="progress-bar-percentage"></span>
                                        </span>
                                        <div id="progress-{{ $key }}-{{ $i }}" class="progress-bar-progress" style="width: 0%"></div>
                                    </div>
                                    <span id="eta-{{ $key }}-{{ $i }}" class="upload-time-remaining hid" style="display: block;">{{ __(' ') }}</span>
                                </div>
                            </div>
                            <div class="flex flex-col mt-1">
                                <div id="title-{{ $key }}-{{ $i }}" class="item-title w-100" style="max-height: 16px;max-width: 100%;white-space: nowrap;overflow: hidden; "></div>
                                <div id="speed-{{ $key }}-{{ $i }}" class="item-title w-100"></div>
                                <div id="log-{{ $key }}-{{ $i }}" class="hidden item-title w-100" style="height: min-content;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                @endfor
        </div>
        @endforeach
    </div>
    <div class="flex flex-col float-left w-full pb-4 overflow-y-auto lg:w-7/12 md:pl-3">
        <div class="flex flex-row h-12 px-4 py-3 text-white bg-gray-900" style="overflow-x: hidden;">
            <div class="float-left w-1/12 my-1 ml-2">
                <div class="item-title">{{ __('ui.id') }}</div>
            </div>
            <div class="float-left w-1/12 my-1 ml-2 mr-2">
                <div class="item-title">{{ __('ui.queue') }}</div>
            </div>
            <div class="float-left w-1/12 my-1 ml-2 mr-2">
                <div class="item-title">{{ __('ui.title') }}</div>
            </div>
            <div id="remaining" class="w-1/12 pt-1 item-title"></div>
            <div class="float-left w-4/12 text-right">
                <button id="refresh" onclick="location.reload()" class="float-right px-4 mx-2 text-sm bg-purple-600 rounded md:px-2 md:mx-1">{{ __('route.refresh') }}</button>
                <button id="less" onclick="limit= (limit - increments) > increments ? ( limit - increments ) : increments; queue()" class="hidden float-right px-4 mx-2 text-sm bg-purple-600 rounded md:px-2 md:mx-1 xl:flex">{{ __('ui.less') }}...</button>
                <button id="more" onclick="limit= limit > total ? ( limit + increments ) : increments; queue()" class="hidden float-right px-4 mx-2 text-sm bg-purple-600 rounded md:px-2 md:mx-1 xl:flex">{{ __('ui.more') }}...</button>
            </div>
        </div>
        <div class="w-full px-2 py-2 mt-0 bg-gray-800" id="queue"></div>
    </div>
</div>

<script type="text/javascript">
    function progress() {
        $.ajax({
            type: 'POST',
            url: "{{ route('progress_list') }}",
            headers: {
                'Authorization': 'Bearer ' . $token,
            },
            data: {
                '_token': '{{ csrf_token() }} ',
            },
            success: function (data) {
                $.each(data, function (index2, data2) {
                    if (data2 && !data2.message && !data2.disabled) {
                        $(`#active-${index2}`).html('');
                        $(`#card-${index2}-0`).css('display', 'none');
                        $(`#card-${index2}-1`).css('display', 'none');
                    } else {
                        $(`#card-${index2}-0`).css('display', 'none');
                        $(`#card-${index2}-1`).css('display', 'none');
                        $(`#active-${index2}`).html('Inactive');
                    }
                    $.each(data2, function (index, k) {
                        if (!k.message && !k.disabled) {
                            // if (!k.message && !k.disabled) {
                            $(`#title-${index2}-${index}`).html(k.title);
                            $(`#log-${index2}-${index}`).html(k.log);
                            if (k.speed != null) {
                                $(`#speed-${index2}-${index}`).html('Encoding speed: ' + k.speed + 'X');
                            }
                            if (k.img != null && k.img != "") {
                                if (k.external == true) {
                                    img = k.img;
                                } else {
                                    img = k.img;
                                }
                                $(`#img-${index2}-${index}`).attr('src', img);
                            }
                            if (k.percent != null) {
                                $(`#progress_text-${index2}-${index}`).html(k.percent + '%');
                                $(`#progress-${index2}-${index}`).width(k.percent + '%');
                            }
                            $(`#card-${index2}-${index}`).css('display', 'flex');
                            if (k.remaining != null) {
                                // $(`#eta-${index2}-${index}`).html(` {{ __('ui.timeremain') }}: ` + k.days + ' ' + k.hours + ' ' + k.minutes);
                                $(`#eta-${index2}-${index}`).html(` {{ __('ui.timeremain') }}: ` + k.days + ' ' + k.hours + ' ' + k.minutes + ' ' + k.seconds);
                            }
                        } else {
                            $(`#card-${index2}-${index}`).css('display', 'none');
                            $(`#card-${index2}-${index}`).css('display', 'none');
                        }
                    });
                });
                if (!data) {
                    $(`#card-${index2}-0`).css('display', 'none');
                    $(`#card-${index2}-1`).css('display', 'none');
                }
            }
        });
    };
    $(function () {
        setInterval(() => {
            if(!document.hidden){
                progress();
            }
        }, 5000);
    });

    document.addEventListener("visibilitychange", function() {
    if (document.visibilityState === 'visible') {
        progress();
    }
    });
    $('img[id^="img"]').click(function () {
        $(this).each(function () {
            let item = $(this)[0].id.replace('img', 'log');
            if ($(`#${item}`).hasClass('showing')) {
                $(`#${item}`).removeClass('showing');
            } else {
                $(`#${item}`).addClass('showing');
            }
        })
    })

    let increments = 30;
    let total = null;
    let value = null;
    let limit = increments;
    let current = null;
    let id = null;
    let seriename;

    function queue() {
        $.ajax({
            type: 'post',
            url: "{{ route('queue_list') }}",
            headers: {
                'Authorization': 'Bearer ' . $token,
            },
            data: {
                limit: limit,
                '_token': '{{ csrf_token() }} ',
            },
            success: function (data) {
                $("#id").html('');
                $("#queue").html("");
                $("#remaining").html(`${data.count} {{ __('ui.itemsremain') }} `);
                $.each(data.output, function (index, value) {
                    seriename = value.season ? value.serie.replace(/\./g, " ").replace("\u00e9", "") + ' S' + value.season + 'E' + value.episode + ' ' + value.mediatype : value.serie.replace("\u00e9", "é").replace("u00e9", "") + ' ' + value.mediatype;
                    $("#queue").append(`
                        <div class="flex flex-row">
                            <div class="float-left w-1/12 mx-2 mt-1 mb-1">
                                <div class="item-title">${value.id}</div>
                            </div>
                            <div class="float-left w-1/12 mx-2 mt-1 mb-1">
                                <div class="item-title">${value.queue.replace(/_encoder/g, " ").replace("u00e9", "")}</div>
                            </div>
                            <div class="float-left w-9/12 mx-2 mt-1 mb-1">
                                <div class="item-title" style="max-height: 16px;max-width: 100%;white-space: nowrap;overflow: hidden; ">${seriename}</div>
                            </div>
                        </div>
                    `);
                });
            }
        });
    }
    (function better() {
        if(!document.hidden){
            queue();
        }
        setTimeout(better, 10000);
    })();

    document.addEventListener("visibilitychange", function() {
    if (document.visibilityState === 'visible') {
        queue();
    }
    });

    $(window).resize(function () {
        queue();
    });

</script>
<script type="text/javascript">
    // $.ajaxSetup({
    //     headers: {
    //         'csrftoken': '{{ csrf_token() }}',
    //         'Authorization': 'Bearer ' . $token,
    //     },
    // });

</script>


@endsection
