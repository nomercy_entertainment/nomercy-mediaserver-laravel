@extends('layouts.dashboard')

@section('title', 'NoMercy Search')
@section('description', __('ui.description'))

@section('content')

<style>
    .btn-xs {
        padding: 5px 10px;
        font-size: 10px;
        height: 30px;
    }

    body {
        overflow-y: auto;
    }

    .nm-container {
        margin-left: -8px;
    }

    .btn-static {
        position: fixed;
        left: 50vw;
        transform: translate(-50%, -50%);
        bottom: 0px;
        z-index: 5;
    }

    @media (min-width: 980px) {
        .col-lg-6 {
            max-width: 47%;
            height: 220px;
        }
    }

    @media (max-width: 980px) {
        .nm-container {
            margin-left: 0;
            padding: 0 1rem 0 0;
        }
    }
</style>

<div id="message" class=""></div>
<div class="ml-2 col-12">
    <div class="ml-3 panel-body">

        <button id="tv" type="button" class="px-4 py-1 mx-2 font-medium bg-purple-600 rounded">{{ __('Add Existing Series') }}</button>
        <button id="movie" type="button" class="px-4 py-1 mx-2 font-medium bg-purple-600 rounded">{{ __('Add Existing  Movies') }}</button>
    </div>
</div>
</div>



<script type="text/javascript">
    $('#tv').on('click', function() {
        $.ajax({
            type: 'post',
            headers: {
                'csrftoken': '{{ csrf_token() }}',
                "Authorization": 'Bearer {{ $token }}'
            },
            url: '{{ route('find_tv_media', 'out') }}',
            success: function(data) {
                console.log(data);
                document.querySelector("#message").innerHTML +=
                    '<div class="alert alert-success">' +
                        '<button type="button" class="float-right close" data-dismiss="alert">&times;</button>' +
                        '<p>' + data.message + '</p>'+
                    '</div>';
            }
        });
    });
    $('#movie').on('click', function() {
        $.ajax({
            type: 'post',
            headers: {
                'csrftoken': '{{ csrf_token() }}',
                "Authorization": 'Bearer {{ $token }}'
            },
            url: '{{ route('find_movie_media', 'out') }}',
            success: function(data) {
                console.log(data);
                document.querySelector("#message").innerHTML +=
                    '<div class="alert alert-success">' +
                        '<button type="button" class="float-right close" data-dismiss="alert">&times;</button>' +
                        '<p>' + data.message + '</p>'+
                    '</div>';
            }
        });
    });
</script>
@endsection
