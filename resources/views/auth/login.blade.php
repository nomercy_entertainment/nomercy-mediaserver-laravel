@extends('layouts.auth')

@section('description', __('auth.login'))

@section('content')
    <div class="flex flex-wrap justify-center">
        <div class="w-screen max-w-lg px-8">
            <div class="flex flex-col justify-center break-words bg-gray-800 rounded shadow-md">

                <div class="px-6 py-3 mb-0 text-3xl font-semibold text-center text-gray-100 bg-gray-900">
                    @include('components.svg.logo')
                      </svg>
                    {{ __('auth.login') }}
                </div>

                <form class="w-full p-6 mb-8" method="POST" action="{{ route('login') }}">
                    @csrf

                    @include('components.forms.input', [
                        'id' => "email",
                        'name' => "email",
                        'autocomplete' => 'email',
                        'type' => "text",
                        'errors' => $errors,
                        'placeholder' => __('auth.email'),
                        'class' => '',
                        'icon' => 'fa fa-envelope',
                        'required' => true,
                        'themeColor' => themeColor(),
                    ])


                    @include('components.forms.input', [
                        'id' => "password",
                        'name' => "password",
                        'autocomplete' => 'password',
                        'type' => "password",
                        'errors' => $errors,
                        'placeholder' => __('auth.password'),
                        'class' => '',
                        'icon' => 'fa fa-key',
                        'required' => true,
                        'themeColor' => themeColor(),
                    ])

                    <div class="flex flex-row justify-between">
                        @foreach(['email', 'password'] as $error)
                            @error($error)
                                <p class="text-xs italic text-red-500 w-full -mt-2">
                                    {!! $message !!}
                                </p>
                            @enderror
                        @endforeach

                        @if (Session::has('email') || Session::has('password') || Session::has('any'))
                            <p class="text-xs italic text-red-500 w-full -mt-2">
                                {!! Session::get('email') !!}
                                {!! Session::get('password') !!}
                                {!! Session::get('any') !!}
                            </p>
                        @else
                            <p class=""></p>
                        @endif

                        @include('components.forms.checkbox', [
                            'id' => "remember",
                            'name' => "remember",
                            'type' => "checkbox",
                            'placeholder' => __('auth.password'),
                            'checked' => true,
                            'themeColor' => themeColor(),
                            'label' => __('auth.remember')
                        ])

                    </div>

                    <div class="flex flex-wrap justify-center text-white">

                        @include('components.forms.button', [
                            'label' => __('auth.login'),
                        ])

                        @if (Route::has('password.request'))

                            @include('components.forms.paragraph-anchor', [
                                'paragraph' => __("auth.forgot"),
                                'anchor' => __("auth.resetLink"),
                                'href' =>  route('password.request')
                            ])

                        @endif

                        @if (Route::has('register'))

                        @include('components.forms.paragraph-anchor', [
                            'paragraph' => __("auth.new"),
                            'anchor' => __("auth.register"),
                            'href' =>  route('register')
                        ])
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
