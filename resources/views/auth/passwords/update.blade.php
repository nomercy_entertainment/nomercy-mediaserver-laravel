@extends('layouts.auth')

@section('content')
<div id="content" class="grid items-center content-center justify-center h-screen px-8 py-16 mx-auto bg-gray-700">
    <div class="flex flex-wrap justify-center">
        <div class="w-full max-w-lg">
            <div class="flex flex-col break-words bg-gray-800 rounded shadow-md">

                <div class="px-6 py-3 mb-0 text-3xl font-semibold text-center text-gray-100 bg-gray-900">
                    <img src="{{ asset('/img/logo-wide-transparent-original.png') }}" alt="">
                    {{ __('auth.update') }}
                </div>

                @if(Session::has('error'))
                    <div class="px-4 py-3 text-red-700 bg-red-100 border-t border-b border-red-500" role="alert">
                        <button type="button" class="float-right close" data-dismiss="alert">&times;</button>
                        <p class="font-bold">Error</p>
                        <p class="text-sm">{{ Session::get('error') }}</p>
                    </div>
                @endif


                    <form class="w-full p-6 mb-8" method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <div class="flex flex-row flex-wrap justify-between mb-6">
                            <input id="email" type="email" class="form-input bg-gray-700 border-none w-full rounded-r rounded-l-none  @error('email') border-red-500 @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus placeholder="{{ __('E-Mail Address') }}:">

                            @error('email')
                            <p class="mt-4 text-xs italic text-red-500">
                                {{ $message }}
                            </p>
                            @enderror
                        </div>

                        <div class="flex flex-row flex-wrap justify-between mb-6">
                            <input id="password" type="password" class="form-input bg-gray-700 border-none w-full rounded-r rounded-l-none  @error('password') border-red-500 @enderror" name="password" required autocomplete="new-password" placeholder="{{ __('Password') }}">

                            @error('password')
                            <p class="mt-4 text-xs italic text-red-500">
                                {{ $message }}
                            </p>
                            @enderror
                        </div>

                        <div class="flex flex-row flex-wrap justify-between mb-6">
                            <input id="password-confirm" type="password" class="w-full form-input" name="password_confirmation" required autocomplete="new-password" placeholder="{{ __('Confirm Password') }}">
                        </div>

                        <div class="flex flex-wrap justify-center text-white">
                            <button type="submit" class="px-4 py-1 mx-auto font-medium bg-purple-600 rounded hover:bg-purple-600 hover:text-gray-800 font-bolder">
                                {{ __('auth.update') }}
                            </button>
                        </div>
                    </form>
            </div>

            </div>
        </div>
    </div>
</div>
@endsection
