@extends('layouts.auth')

@section('description', __('auth.reset'))

@section('content')
    <div class="flex flex-wrap justify-center">
        <div class="w-screen max-w-lg px-8">
            <div class="flex flex-col justify-center break-words bg-gray-800 rounded shadow-md">

                <div class="px-6 py-3 mb-0 text-3xl font-semibold text-center text-gray-100 bg-gray-900">
                    @include('components.svg.logo')
                    {{ __('auth.reset') }}
                </div>

                @if(Session::has('error'))
                    <div class="px-4 py-3 text-red-700 bg-red-100 border-t border-b border-red-500" role="alert">
                        <button type="button" class="float-right close" data-dismiss="alert">&times;</button>
                        <p class="font-bold">Error</p>
                        <p class="text-sm">{{ Session::get('error') }}</p>
                    </div>
                @endif

                <form class="w-full p-6 mb-8" method="POST" action="{{ route('password.update') }}">
                    @csrf

                    @if (session('status'))
                    <div class="px-3 py-4 mb-4 text-sm text-green-700 bg-green-100 border border-t-8 border-green-600 rounded" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif

                    @include('components.forms.input', [
                        'id' => "email",
                        'name' => "email",
                        'autocomplete' => 'email',
                        'type' => "text",
                        'errors' => $errors,
                        'placeholder' => __('auth.email'),
                        'icon' => 'fa fa-envelope',
                        'required' => true,
                        'themeColor' => themeColor(),

                        ])

                        @include('components.forms.input', [
                        'id' => "password",
                        'name' => "password",
                        'autocomplete' => 'new-password',
                        'type' => "password",
                        'errors' => $errors,
                        'placeholder' => __('auth.password'),
                        'icon' => 'fa fa-key',
                        'required' => true,
                        'themeColor' => themeColor(),

                        ])

                        @include('components.forms.input', [
                        'id' => "password-confirm",
                        'name' => "password_confirmation",
                        'autocomplete' => 'new-password',
                        'type' => "password",
                        'errors' => $errors,
                        'placeholder' => __('auth.confirm'),
                        'icon' => 'fa fa-key',
                        'required' => true,
                        'themeColor' => themeColor(),

                        ])
                    <div class="flex flex-wrap justify-center text-white">
                        <button type="submit" class="px-4 py-1 mx-auto font-medium hover:text-gray-800 font-bolder" style="background: {{ themeColor() }};">
                            {{ __('Reset Password') }}
                        </button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
@endsection
