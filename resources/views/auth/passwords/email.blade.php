@extends('layouts.auth')

@section('description', __('auth.reset'))

@section('content')
    <div class="flex flex-wrap justify-center">
        <div class="w-screen max-w-lg px-8">
            <div class="flex flex-col justify-center break-words bg-gray-800 rounded shadow-md">

                <div class="px-6 py-3 mb-0 text-3xl font-semibold text-center text-gray-100 bg-gray-900">
                    @include('components.svg.logo')
                    {{ __('auth.reset') }}
                </div>


                @if(Session::has('error'))
                <div class="px-4 py-3 text-red-700 bg-red-100 border-t border-b border-red-500" role="alert">
                    <button type="button" class="float-right close" data-dismiss="alert">&times;</button>
                    <p class="font-bold">{{__('Error')}}</p>
                    <p class="text-sm">{{ Session::get('error') }}</p>
                </div>
                @endif


                <form class="w-full p-6 mb-8" method="POST" action="{{ route('password.email') }}">
                    @csrf


                    @if (session('status'))
                    <div class="px-3 py-4 mb-4 text-sm text-green-700 bg-green-100 border border-t-8 border-green-600 rounded" role="alert">
                        {{ session('status') }}
                    </div>
                    @else


                    @include('components.forms.input', [
                    'id' => "email",
                    'name' => "email",
                    'autocomplete' => 'email',
                    'type' => "text",
                    'errors' => $errors,
                    'placeholder' => __('auth.email'),
                    'icon' => 'fa fa-envelope',
                    'required' => true,
                    'themeColor' => themeColor(),

                    ])


                    <div class="flex flex-wrap justify-center mt-6 text-white">

                        <button type="submit" class="px-4 py-1 mx-auto font-medium hover:text-gray-800 font-bolder" style="background: {{ themeColor() }};">
                            {{ __('auth.resetLink') }}
                        </button>

                    </div>
                    @endif


                    <div class="flex flex-wrap justify-center mt-6 text-white">
                        <p class="w-full mt-8 -mb-4 text-xs text-center text-gray-100">
                            {{ __("auth.back") }}
                            <a class="w-full mt-8 -mb-4 text-xs text-center cursor-pointer" href="{{ route('login') }}" style="color: {{ themeColor() }};">
                                {{ __('auth.login') }}
                            </a>
                        </p>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection
