@extends('layouts.auth')

@section('description', __('auth.confirm'))

@section('content')
    <div class="flex flex-wrap justify-center">
        <div class="w-screen max-w-lg px-8">
            <div class="flex flex-col justify-center break-words bg-gray-800 rounded shadow-md">

                <div class="px-6 py-3 mb-0 text-3xl font-semibold text-center text-gray-100 bg-gray-900">
                    @include('components.svg.logo')
                    {{ __('auth.confirm') }}
                </div>


                    <div class="px-6 py-3 mb-0 font-semibold text-gray-700 bg-gray-200">
                        {{ __('Confirm Password') }}
                    </div>

                    <form class="w-full p-6" method="POST" action="{{ route('password.confirm') }}">
                        @csrf

                        <p class="leading-normal">
                            {{ __('Please confirm your password before continuing.') }}
                        </p>

                        <div class="flex flex-wrap my-6">
                            <input id="password" type="password" class="form-input bg-gray-700 border-none w-full rounded-r rounded-l-none  @error('password') border-red-500 @enderror" name="password" required autocomplete="new-password" placeholder="{{ __('Password') }}">

                            @error('password')
                                <p class="mt-4 text-xs italic text-red-500">
                                    {{ $message }}
                                </p>
                            @enderror
                        </div>

                        <div class="flex flex-wrap items-center">
                            <button type="submit" class="px-4 py-1 mx-auto font-medium hover:text-gray-800 font-bolder" style="background: {{ themeColor() }};">
                                {{ __('Confirm Password') }}
                            </button>

                            @if (Route::has('password.request'))
                                <a class="w-full mt-8 -mb-4 text-xs text-center cursor-pointer" href="{{ route('password.request') }}" style="color: {{ themeColor() }};">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
