@extends('layouts.auth')

@section('content')
    <div class="flex flex-wrap justify-center">
        <div class="w-full max-w-xl px-8">
            <div class="flex flex-col break-words bg-gray-800 rounded shadow-md">
                @if (session('resent'))
                    <div class="px-3 py-4 mb-4 text-xl text-green-800 bg-green-100 border border-t-8 border-green-600 rounded" role="alert">
                        {{ __('A fresh verification link has been sent to your email address.') }}
                    </div>
                @endif

                <div class="px-6 py-3 mb-0 font-semibold text-gray-100 bg-gray-900">
                    <img src="{{ asset('/img/logo-wide-transparent-original.png') }}" alt="">
                    {{ __('auth.register') }}
                </div>

                    <div class="px-6 py-3 mb-0 font-semibold text-gray-100 bg-gray-800">
                        {{ __('Verify Your Email Address') }}
                    </div>

                    <div class="flex flex-wrap w-full p-6">
                        <p class="leading-normal">
                            {{ __('Before proceeding, please check your email for a verification link.') }}
                        </p>

                        <p class="mt-6 leading-normal">
                            {{ __('If you did not receive the email') }}, <a class="text-blue-500 no-underline hover:text-blue-700" onclick="event.preventDefault(); document.getElementById('resend-verification-form').submit();">{{ __('click here to request another') }}</a>.
                        </p>

                        <form id="resend-verification-form" method="POST" action="{{ route('verification.resend') }}" class="hidden">
                            @csrf
                        </form>
                    </div>

            </div>
        </div>
    </div>
@endsection
