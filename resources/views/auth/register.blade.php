@extends('layouts.auth')

@section('description', __('auth.register'))

@section('content')
    <div class="flex flex-wrap justify-center">
        <div class="w-screen max-w-lg px-8">
            <div class="flex flex-col justify-center break-words bg-gray-800 rounded shadow-md">

                <div class="px-6 py-3 mb-0 text-3xl font-semibold text-center text-gray-100 bg-gray-900">
                    @include('components.svg.logo')
                    {{ __('auth.register') }}
                </div>

                <form class="w-full p-6 mb-8" method="POST" action="{{ route('register') }}">
                    @csrf



                    @include('components.forms.input', [
                        'id' => "name",
                        'name' => "name",
                        'autocomplete' => 'name',
                        'type' => "text",
                        'placeholder' => __('auth.name'),
                        'icon' => 'fa fa-user',
                        'required' => true,
                        'themeColor' => themeColor(),

                    ])

                    @include('components.forms.input', [
                        'id' => "email",
                        'name' => "email",
                        'autocomplete' => 'email',
                        'type' => "text",
                        'placeholder' => __('auth.email'),
                        'icon' => 'fa fa-envelope',
                        'required' => true,
                        'themeColor' => themeColor(),

                    ])

                    @include('components.forms.input', [
                        'id' => "password",
                        'name' => "password",
                        'autocomplete' => 'password',
                        'type' => "password",
                        'placeholder' => __('auth.password'),
                        'icon' => 'fa fa-key',
                        'required' => true,
                        'themeColor' => themeColor(),

                    ])

                    @include('components.forms.input', [
                        'id' => "password-confirm",
                        'name' => "password_confirmation",
                        'autocomplete' => 'password_confirmation',
                        'type' => "password",
                        'placeholder' => __('auth.confirm'),
                        'icon' => 'fa fa-key',
                        'required' => true,
                        'themeColor' => themeColor(),

                    ])

                    <div class="flex flex-row justify-between">
                        @foreach(['name', 'email', 'password'] as $error)
                            @error($error)
                                <p class="text-xs italic text-red-500">
                                    {{ $message }}
                                </p>
                            @enderror
                        @endforeach

                        @if (Session::has('name') || Session::has('email') || Session::has('password'))
                            <p class="text-xs italic text-red-500">
                                {{ Session::get('name') }}
                                {{ Session::get('email') }}
                                {{ Session::get('password') }}
                            </p>
                        @else
                            <p class="text-xs italic text-red-500"></p>

                        @endif

                        @include('components.forms.checkbox', [
                            'id' => "remember",
                            'name' => "remember",
                            'type' => "checkbox",
                            'placeholder' => __('auth.password'),
                            'checked' => true,
                            'themeColor' => themeColor(),
                            'label' => __('auth.remember')
                        ])

                    </div>

                    <div class="flex flex-wrap justify-center text-white">

                        @include('components.forms.button', [
                            'label' => __('auth.continue'),
                        ])

                        @include('components.forms.paragraph-anchor', [
                            'paragraph' => __("auth.exist"),
                            'anchor' => __("auth.login"),
                            'href' =>  route('login')
                        ])

                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
