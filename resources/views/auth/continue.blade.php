@extends('layouts.auth')

@section('content')
<div id="content" class="grid items-center content-center justify-center py-16 mx-auto bg-gray-700 h-min">
    <div class="flex flex-wrap justify-center px-8">
        <div class="w-full max-w-sm">
            <div class="flex flex-col break-words bg-gray-800 rounded shadow-md">

                <div class="px-6 py-3 mb-0 font-semibold text-gray-100 bg-gray-900">
                    <img src="{{ asset('/img/logo-wide-transparent-original.png') }}" alt="">
                    {{ __('auth.register') }}
                </div>

                <form class="w-full p-6" method="POST" action="{{ route('register') }}">
                    @csrf

                    @include('components.forms.input', [
                    'id' => "name",
                    'name' => "name",
                    'autocomplete' => 'new-password',
                    'type' => "text",
                    'errors' => $errors,
                    'label' => __('auth.name'),
                    'placeholder' => __('auth.name'),
                    'class' => 'fa fa-user',
                    ])

                    @include('components.forms.input', [
                    'id' => "email",
                    'name' => "email",
                    'autocomplete' => 'email',
                    'type' => "text",
                    'errors' => $errors,
                    'label' => __('auth.email'),
                    'placeholder' => __('auth.email'),
                    'class' => 'fa fa-user',
                    ])

                    @include('components.forms.input', [
                    'id' => "password",
                    'name' => "password",
                    'autocomplete' => 'new-password',
                    'type' => "password",
                    'errors' => $errors,
                    'label' => __('auth.password'),
                    'placeholder' => __('auth.password'),
                    'class' => 'fa fa-key',
                    ])

                    @include('components.forms.input', [
                    'id' => "password-confirm",
                    'name' => "password_confirmation",
                    'autocomplete' => 'new-password',
                    'type' => "password",
                    'errors' => $errors,
                    'label' => __('auth.confirm'),
                    'placeholder' => __('auth.confirm'),
                    'class' => 'fa fa-key',
                    ])

                    <div class="flex flex-wrap">
                        {{-- <button type="submit" class="px-4 py-1 mx-auto font-medium bg-purple-600 rounded">
                            {{ __('auth.register') }}
                        </button> --}}

                        <button type="submit" class="px-4 py-1 mx-auto font-medium bg-purple-600 rounded">
                            {{ __('Continue') }}
                        </button>

                        <p class="w-full mt-8 -mb-4 text-xs text-center text-gray-100">
                            {{ __('auth.exist') }}
                            <a class="px-4 py-1 mx-2 font-medium bg-purple-600 rounded" href="{{ route('login') }}">
                                {{ __('auth.login') }}
                            </a>
                        </p>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection
