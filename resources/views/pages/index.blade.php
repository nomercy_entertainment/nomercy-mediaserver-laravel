@extends('layouts.dashboard')

@section('content')


<link rel="preload" as="style"      href="{{ asset('/css/index.css?v=' . asset_tag()) }}">
<link rel="stylesheet"              href="{{ asset('/css/index.css?v=' . asset_tag()) }}">


<style>
    video {
        z-index: -3;
    }

    header {
        padding: 30px 0;
    }

    header .contenedor {
        display: flex;
        opacity: 0;
        justify-content: space-between;
        align-items: center;
    }

    header .logotipo {
        font-family: 'Bebas Neue', cursive;
        font-weight: normal;
        color: var(--rojo);
        font-size: 40px;
    }

    header nav button {
        color: #AAA;
        text-decoration: none;
        margin-right: 20px;
    }

    header nav button:hover,
    header nav button.active {
        color: #FFF;
    }

    /* ---- ----- ----- Pelicula Principal ----- ----- ----- */
    .pelicula-principal {
        font-size: 16px;
        margin-top: -4rem;
        color: #fff;
        background-position: center center;
        background-size: cover;
        position: absolute;
        top: 55vh;
        height: max-content;
        width: 100%;
    }

    .pelicula-principal .contenedor {
        margin: 1em 2vh 1em 1em;
        background: #00000075;
        padding: 4vh 3vh 5vh;
        border-radius: 15px;
    }

    .pelicula-principal .titulo {
        font-weight: 600;
        font-size: 1.7em;
        margin: 0.4em;
    }

    .pelicula-principal .descripcion {
        font-weight: normal;
        font-size: 1em;
        line-height: 1.75em;
        margin-bottom: 1.25em;
    }

    .pelicula-principal .boton {
        border: none;
        border-radius: 0.31em;
        padding: 0.93em 1.87em;
        color: #fff;
        margin: 0 0.68em;
        cursor: pointer;
        transition: .3s ease all;
        font-size: 1.12em;
    }

    button:focus {
        outline: 0;
    }

    .pelicula-principal .boton i {
        margin-right: 1.25em;
    }

    /* ---- ----- ----- Contenedor Titulo y Controles ----- ----- ----- */
    .contenedor-titulo-controles {
        display: flex;
        justify-content: space-between;
        align-items: end;
        height: 45px;
        padding: 0 2%;
    }

    .contenedor-titulo-controles h3 {
        color: #fff;
        font-size: 24px;
        font-weight: 600;
    }

    .contenedor-titulo-controles .indicadores button {
        background: #fff;
        height: 3px;
        width: 15px;
        cursor: pointer;
        border: none;
        margin-right: 2px;
    }

    .contenedor-titulo-controles .indicadores button:hover,
    .contenedor-titulo-controles .indicadores button.active {
        background: red;
    }

    .slide-recomendadas .contenedor-principal {
        display: flex;
        align-items: center;
        position: relative;
    }

    .slide-recomendadas .contenedor-principal .flecha-izquierda,
    .slide-recomendadas .contenedor-principal .flecha-derecha {
        position: absolute;
        border: none;
        background: rgba(0, 0, 0, 0.3);
        font-size: 40px;
        height: 50%;
        top: calc(50% - 25%);
        line-height: 40px;
        width: 50px;
        color: #fff;
        cursor: pointer;
        z-index: 5;
        transition: .2s ease all;
    }

    .slide-recomendadas .contenedor-principal .flecha-izquierda {
        left: 0px;
    }

    .slide-recomendadas .contenedor-principal .flecha-derecha {
        right: 0px;
    }

    /* ---- ----- ----- Carousel ----- ----- ----- */
    .slide-recomendadas .contenedor-carousel {
        width: 100%;
        overflow: hidden;
        scroll-behavior: smooth;
        width: 100%;
        margin: 0;
        overflow: hidden;
        scroll-behavior: smooth;
    }

    .slide-recomendadas .contenedor-carousel .carousel {
        display: flex;
        flex-wrap: nowrap;
    }

    .slide-recomendadas .contenedor-carousel .carousel .pelicula {
        transition: .3s ease all;
        padding: 0 5px;
    }

    .slide-recomendadas .contenedor-carousel .carousel .pelicula.hover {
        transform: scale(1.2);
        transform-origin: center;
    }

    .slide-recomendadas .contenedor-carousel .carousel .pelicula img {
        width: 100%;
        height: 100%;
        vertical-align: top;
    }


    /* ---- ----- ----- Media Queries ----- ----- ----- */
    @media screen and (max-width: 800px) and (orientation: portrait) {


        .contenedor-titulo-controles .indicadores button {
            padding: 2px 0;
            width: 7px;
        }

        .pelicula-principal .contenedor {
            /* margin-top: 28%; */
        }

        .slide-recomendadas .contenedor-carousel .carousel .pelicula {
            min-width: calc(100vw / 2.2);
        }

        header .logotipo {
            margin-bottom: 10px;
            font-size: 30px;
        }

        header .contenedor {
            flex-direction: column;
            text-align: center;
        }

        .pelicula-principal {
            font-size: 14px;
            min-height: 56vh;
            top: 41vh;
            height: max-content;
            width: 100%;
        }

        .pelicula-principal .descripcion {
            max-width: 100%;
        }
        .pelicula-principal .contenedor {

            padding: 1vh 1vh 1vh
        }

    }

    @media screen and (max-width: 800px) and (orientation: landscape) {
        .pelicula-principal {
            font-size: 14px;
            min-height: 65vh;
            position: sticky;
            bottom: 0;
            height: 100vh;
        }
    }

    .prev::before {
        content: "\2039";
        font-size: 5rem;
    }

    .next::before {
        content: "\203A";
        font-size: 5rem;
    }

    .card {
        background-color: transparent;
        border: 0;
    }

    .card-container {
        transform: translateY(-3rem);
    }

    .delete-btn {
        bottom: 20px;
        right: 0px;
    }

    @media (min-width: 1366px) {
        .pelicula-principal {
            width: 60%;
        }
    }

    @media (min-width: 1366px) {

        .slide-recomendadas .contenedor-principal .flecha-izquierda:hover,
        .slide-recomendadas .contenedor-principal .flecha-derecha:hover {
            background: rgba(0, 0, 0, .9);
        }
    }

    @media (min-width: 1920px) {
        .pelicula-principal {
            width: 40%;
        }
    }

    @media (min-width: 3000px) {
        main {
            /* width: 81vw; */
        }
    }

    h3 {
        text-shadow: black 0px 0px 4px, black 0px 0px 4px, black 0px 0px 4px, black 0px 0px 4px, black 0px 0px 4px, black 0px 0px 4px !important;
    }

    .\-mt {
        margin-top: -53rem;
    }

</style>

<script>
    // alert(window.innerWidth + "w " + window.innerHeight + "h");

</script>

<div class="absolute w-screen h-screen -mt-16 video-container">
    @if(isset($last) && count($last) > 0)

    @if(isset($last['trailer'][0]['key']))
    <style>
        main {
            position: absolute;
            top: 83vh;
        }
        @media screen and (max-width: 800px) and (orientation: portrait) {
            main {
                position: absolute;
                top: 71vh;
            }
        }
    </style>
    <div class="relative w-full video">
        <div id="player" class="mt-16 md:mt-0"></div>
    </div>
    @else
    <style>
        main {
            position: absolute;
            top: 86vh;
        }
        @media screen and (max-width: 800px) and (orientation: portrait) {
            main {
                position: absolute;
                top: 85vh;
            }
        }
    </style>
    <video id="video" src="{{ asset('/assets/trailer/'. $last['id'] . '.trailer.mp4') }}" class="absolute top-0 object-fill w-full h-auto mt-16 sm:h-screen md:mt-0" poster="{{ $last['backdrop'] }}"></video>
    @endif
    <div id="spinner" class="-mt-48 spinner"></div>

    <div class="pelicula-principal @if(!isset($last['trailer'][0])) @else absolute top-0 @endif">
        <div class="z-40 contenedor">
            <h3 class="titulo">
                {{ $last['title'] }}
            </h3>
            <p class="descripcion">
                {{ substr($last['overview'], 0, 400) }}...
            </p>

            @php
            if($last['type'] == 'movie'){
            $color = 'green';
            }
            else if($last['type'] == 'tv'){
            $color = 'red';
            }
            else {
            $color = 'yellow';
            }
            @endphp
            <a role="button" class="boton bg-{{ $color }}-600 hover:bg-{{ $color }}-500 play" href="/{{ $last['type']}}/{{ $last['id']}}/watch" id="{{$last['id']}}" data-type="watch" data-name="{{ ucwords( str_replace("\\","", $last['title_sort'])) }}">{{ __('ui.watch') }}</a>
            {{-- <a role="button" class="boton bg-{{ $color }}-600 hover:bg-{{ $color }}-500 play" href="/beta?id={{ $last['id']}}" id="{{$last['id']}}" data-type="watch" data-name="{{ ucwords( str_replace("\\","", $last['title_sort'])) }}">{{ __('ui.beta') }}</a> --}}
            <a role="button" class="boton bg-{{ $color }}-600 hover:bg-{{ $color }}-500" href="/{{ $last['type']}}/{{ $last['id']}}/info" id="{{$last['id']}}" data-type="watch" data-name="{{ ucwords( str_replace("\\","", $last['title_sort'])) }}">{{ __('ui.info') }}</a>
        </div>
    </div>
    @endif

    <main class="w-screen" style="">

        @if(isset($continue) && count($continue) > 0)
        <div class="z-40 hidden content slide-recomendadas contenedor @if(!isset($last['trailer'][0])) mt-2  @else mt-8 @endif">
            <div class="contenedor-titulo-controles header">
                <h3 class="z-40">{{ __('ui.continue') }}</h3>
                <div class="indicadores page"></div>
            </div>

            <div class="contenedor-principal slide-container">
                <button role="button" id="flecha-izquierda" class="flecha-izquierda prev"></button>

                <div class="contenedor-carousel slide">
                    <div class="carousel cards">
                        @foreach($continue as $item)
                        @php
                        if(isset($item['type']) && $item['type'] == 'collection') {
                        $itemtype = 'collection';
                        }
                        else if(isset($item['type']) && $item['type'] != 'movie'){
                        $itemtype = 'tv';
                        $percentage = $item['have_episodes'] !=0 ? ($item['have_episodes'] / $item['number_of_episodes']) * 100 : 0;
                        }
                        else {
                        $itemtype = 'movie';
                        }
                        @endphp
                        <div class="overflow-hidden pelicula card">
                            <a href="/{{ $item['type']}}/{{ $item['id']}}/watch" id="{{$item['id']}}" data-type="watch" data-name="{{ ucwords( str_replace("\\","", $item['title_sort'])) }}">
                                <img src="{{ $item['poster'] ?? asset('/img/poster-not-available.png') }}" onerror="this.src='/img/poster-not-available.png'" alt="">
                            </a>
                            <div id="" class="relative hidden card-container">
                                <div id="" class="h-12 px-1 py-2 overflow-hidden text-xs text-white whitespace-no-wrap bg-black card-title 3xl:h-16 md:px-2">

                                    @if ( strlen($item["title"]) > 30 && contains($item["title"], array(':')) !== false )
                                    {!! str_replace(':', ':<br><span class="text-3xs">', $item["title"]) !!} </span>
                                    @elseif ( strlen($item["title"]) > 30)
                                    {!! str_replace(' and', '<br><span class="text-3xs">And', $item["title"]) !!} </span>
                                    @else
                                    {{ $item["title"] }}
                                    @endif

                                </div>
                                <button id="remove-{{$item['id']}}" data-id="{{$item['id']}}" type="button" class="absolute top-0 right-0 z-10 float-right h-4 px-1 pb-1 m-2 mt-6 font-medium bg-purple-600 rounded delete-btn md:block md:ml-4 text-3xs">{{ __('ui.delete') }}</button>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

                <button role="button" id="flecha-derecha" class="flecha-derecha next"></button>
            </div>
        </div>
        @endif
        @foreach($data as $key => $genre)
        @if(count($genre) > 0)

        <div class="hidden mt-8 content slide-recomendadas contenedor">
            <div class="contenedor-titulo-controles header">
                <h3>{{ $key }}</h3>
                <div class="indicadores page"></div>
            </div>

            <div class="contenedor-principal slide-container">
                <button role="button" id="flecha-izquierda" class="flecha-izquierda prev"></button>

                <div class="contenedor-carousel slide">
                    <div class="carousel cards">
                        @foreach($genre as $item)

                        @php
                        if(isset($item['type']) && $item['type'] == 'collection') {
                        $itemtype = 'collection';
                        }
                        else if(isset($item['type']) && $item['type'] != 'movie'){
                        $itemtype = 'tv';
                        $percentage = $item['have_episodes'] !=0 ? ($item['have_episodes'] / $item['number_of_episodes']) * 100 : 0;
                        }
                        else {
                        $itemtype = 'movie';
                        }
                        @endphp
                        <div class="pelicula card">
                            <a href="/{{ $item['type']}}/{{ $item['id']}}/info" id="{{$item['id']}}" data-type="watch" data-name="{{ ucwords( str_replace("\\","", $item['title_sort'])) }}">
                                <img src="{{ $item['poster'] }}" onerror="this.src='/img/poster-not-available.png'" alt="">
                            </a>
                            <div id="" class="relative hidden card-container">
                                <div id="" class="h-12 px-1 py-2 overflow-hidden text-xs text-white whitespace-no-wrap bg-black card-title 3xl:h-16 md:px-2">

                                    @if ( strlen($item["title"]) > 30 && contains($item["title"], array(':')) !== false )
                                    {!! str_replace(':', ':<br><span class="text-3xs">', $item["title"]) !!} </span>
                                    @elseif ( strlen($item["title"]) > 30)
                                    {!! str_replace(' and', '<br><span class="text-3xs">And', $item["title"]) !!} </span>
                                    @else
                                    {{ $item["title"] }}
                                    @endif

                                </div>
                                {{-- <button id="remove-{{$item['id']}}" data-id="{{$item['id']}}" type="button" class="absolute top-0 right-0 z-10 float-right h-4 px-1 pb-1 m-2 mt-6 font-medium bg-purple-600 rounded delete-btn md:block md:ml-4 text-3xs">{{ __('ui.delete') }}</button> --}}
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

                <button role="button" id="flecha-derecha" class="flecha-derecha next"></button>
            </div>
        </div>

        @endif
        @endforeach
    </main>
</div>

<script>
    var timer = 1600;
    var firstLoad = true;

    const video = document.querySelector('video');

    function scroll() {
        if ($(this).scrollTop() > $("nav").height()) {
            $("nav").css({
                'background': 'black',
                'transition': 'background .1s linear'
            });
            if (video) {

                video.pause();
            }
        } else if ($(window).width() < 900) {
            $("nav").css({
                'background': 'black',
                'transition': 'background .1s linear'
            });
            if (video) {

                video.play();
            }
        } else {
            $("nav").css({
                'background': 'rgba(17, 17, 17, .3)',
                'transition': 'background .5s linear'
            });
            if (video) {
                video.play();

            }
        }
    }

    $(function () {

        setTimeout(() => {
            if (video) {
                video.currentTime = 5;
                video.play();
            }
        }, 300);

        $('video').on('error', function () {
            this.src = '';
            $(window).unbind('scroll');
            $(document).unbind('scroll');
        })
        $('video').on('ended', function () {
            this.src = '';
            $(window).unbind('scroll');
            $(document).unbind('scroll');
        })

        if (!window.isSony) {
            $(document).scroll(scroll);
            if ($(window).width() > 900) {
                $('nav').css({
                    background: 'rgba(17, 17, 17, .3)'
                });
            }
            $('body, .video-container').css({
                left: 0,
                overflowX: "auto"
            }).scrollLeft(0).css({
                overflowX: "hidden"
            });

            $(".slide").show();
            $("body").css("position", "absolute");
            $(".row-container").css("display", "block");
        }
        resize();
    });

    $(window).resize(function () {
        resize();
    });

    let showCount;

    function resize() {
        var windowWidth = $(window).width();

        if (windowWidth >= 0 && windowWidth <= 414) {
            showCount = 2;
            document.addEventListener('contextmenu', event => event.preventDefault());
        } else if (windowWidth > 414 && windowWidth <= 768) {
            showCount = 6;
        } else if (windowWidth > 768 && windowWidth <= 1366) {
            showCount = 8;
        } else if (windowWidth > 1366 && windowWidth <= 1600) {
            showCount = 9;
        } else if (windowWidth > 1600 && windowWidth <= 2000) {
            showCount = 12;
        } else {
            showCount = 16;
        }

        let size = windowWidth / showCount;

        $('.card').css({
            width: size,
            minWidth: size,
            maxWidth: size,
            height: size / 2 * 3 - 10,
            minHeight: size / 2 * 3 - 10,
            maxHeight: size / 2 * 3 - 10,
        });


        init();
    }

    function init() {
        $('.cards').children().mouseover(function () {
            $(this).css({
                zIndex: 2,
            })
            $(this).children().last().css({
                display: "block",
            });
        }).mouseout(function () {
            $(this).css({
                zIndex: 0,
            })
            $(this).children().last().css({
                display: "none",
            });
        });

        $(".content").each(function () {
            const container = $(this);
            const header = container.children('.header');
            const page = header.children('.page');
            const slideContainer = container.children('.slide-container');
            const prev = slideContainer.children('.prev');
            const slide = slideContainer.children('.slide');
            const cards = slide.children('.cards');
            const card = cards.children('.card');
            const next = slideContainer.children('.next');

            const pages = Math.ceil(card.length / showCount);

            let currentPage = 0;

            next.on('click', (e) => {
                nextScroll();
            });

            prev.on('click', (e) => {
                prevScroll()
            });

            if (!window.isSony) {
                cards.each(function () {
                    page.empty();
                    for (let index = 0; index < pages; index++) {
                        page.append(`
                            <button class="${index == 0 ?  'active' : ''  }"></button>
                        `)
                    }
                });
            }

            function nextScroll() {
                next.off();
                page.children().removeClass('active');

                if (currentPage + 1 < pages) {
                    slide.scrollLeft(slide.scrollLeft() + slide.width());
                    currentPage++;
                } else {
                    timer = 1300;
                    currentPage = 0;
                    slide.scrollLeft(0);
                    timer = 500;
                }

                page.children().each(function (e) {
                    if (currentPage == $(this).index()) {
                        $(this).addClass('active');
                    }
                });

                setTimeout(() => {
                    next.on('click', (e) => {
                        nextScroll();
                    });
                }, timer);
            }

            function prevScroll() {
                prev.off();

                if (currentPage > 0) {
                    slide.scrollLeft(slide.scrollLeft() - slide.width());
                    currentPage--
                } else {
                    currentPage = 0;
                    slide.scrollLeft(0);
                }

                page.children().removeClass('active');
                page.children().each(function (e) {
                    if (currentPage == $(this).index()) {
                        $(this).addClass('active');
                    }
                });

                setTimeout(() => {
                    prev.on('click', (e) => {
                        prevScroll();
                    });
                }, timer);
            }
        });

        $('.content.slide-recomendadas.contenedor').removeClass('hidden');

        if (firstLoad && !window.isSony) {
            setTimeout(() => {
                $(window).scrollTop(0);
                if (video) {
                    video.currentTime = 5;
                    $('video').on('canplay', function () {
                        video.play();
                    });
                }

                $('#spinner').addClass('hidden');
            }, 1000);
            firstLoad = false;
        }
    }

    $('.delete-btn').on('click', function (e) {
        $.ajax({
            type: 'post',
            data: {
                _token: "{{ csrf_token() }}",
                id: e.target.dataset.id
            },
            url: "{{ route('delete_watched') }}",
            success: function (data) {
                $(`#${e.target.dataset.id}`).parent().remove();
            },
            error: function (data) {
                $("#message").append(`
                    <div class="px-4 py-3 text-red-900 bg-red-100 border-t-4 border-red-500 rounded-b shadow-md" role="alert">
                        <div class="flex">
                            <div class="float-left py-1">
                                <svg class="w-6 h-6 mr-4 text-teal-500 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z"></path></svg>
                            </div>
                            <p class="font-bold">Error!</p>
                            <p class="pl-3 text-sm">${data.responseJSON.message}</p>
                            <button type="button" class="float-right ml-auto close" data-dismiss="alert" onclick="$(this).parent().parent().remove()">×</button>
                        </div>
                    </div>
                `);
            },
        });
        $(this).parent().parent().remove();
        $(this).parent().first().removeClass('hover-center');
        resize();
    });

</script>

@if(isset($last['trailer'][0]['key']))

<script>
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";

    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    const height = window.innerWidth < 900 ? 240 : window.innerHeight
    const width = window.innerWidth

    var player;
    function onYouTubeIframeAPIReady() {
      player = new YT.Player('player', {
        height: height,
        width: width,
        videoId: "{{ $last['trailer'][0]['key'] }}",
        events: {
          'onReady': onPlayerReady,
          'onStateChange': onPlayerStateChange
        },
        autoplay: true,
        showinfo: false,
        autohide: true,
        controls: false,
        rel: false,
        enablejsapi: true,
        modestbranding: true,
        stretch: '16:9',
      });
    }

    function onPlayerReady(event) {
      event.target.playVideo();
    }

    var done = false;
    function onPlayerStateChange(event) {
      if (event.data == YT.PlayerState.PLAYING && !done) {
        done = true;
      }
    }
    function stopVideo() {
      player.stopVideo();
    }
  </script>
  @endif
@endsection
