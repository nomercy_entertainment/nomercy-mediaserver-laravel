@extends('layouts.app')

@section('head')

<script src="{{ asset('js/videojs/video.js?v=' . asset_tag()) }}"></script>
<script src="{{ asset('js/videojs/videojs-playlist.js?v=' . asset_tag()) }}"></script>
<script src="{{ asset('js/videojs/videojs-contrib-quality-levels.js?v=' . asset_tag()) }}"></script>

<link href="{{ asset('css/nmplayer.videojs.css?v=' . asset_tag()) }}" rel="stylesheet"/>
<script src="{{ asset('js/videojs/nmplugin.videojs.js?v=' . asset_tag()) }}"></script>
<script src="{{ asset('js/videojs/nmsync.videojs.js?v='. asset_tag())}}"></script>
<script src="{{ asset('js/videojs/nmremote.videojs.js?v=' . asset_tag())}}"></script>
<script src="{{ asset('js/dist/subtitles-octopus.js?v=' . asset_tag()) }}"></script>

<style>
    video, #content {
        background: #000;
    }
    video, #overlay {
        top: 0;
        position: absolute;
    }

</style>
@endsection

@section('content')

<img src="#" class="preload-images" alt="">

<div id="spinner-main" class="spinner">
    <span style="position: absolute;top: 89%;width: max-content;transform: translateX(-35%);font-size: 20px;">Loading playlist</span>
</div>

<video id="video-{{$id}}" class="video-js" playsinline data-setup='{}'></video>

{{-- @dd($data) --}}

<script>


    let user = "{{$user}}";
    let tmdb_id = "{{$id}}";
    let type = "{{$data[0]['video_type']}}";

    var overrideNative = false;
    if(window.isPlaystation || window.isSony){
        overrideNative = true;
    }

    var options = {
        height: window.innerHeight,
        width: window.innerWidth,
        autoplay: true,
        muted: false,
        // controls: true,
        preload: 'auto',
        disableremoteplayback: true,
        html5: {
            manualCleanup: false,
            vhs: {
                overrideNative: !overrideNative,
            },
        },
        locale: '{{ app()->getLocale() }}',
        storage: 'server',
        bearer: @json(csrf_token()),
        playlist: 'https://server.nomercy.tv/api/tv/{{$id}}/watch',
        id: tmdb_id,
        type: type,
        translations: @json(translations()),
    };

    window.HELP_IMPROVE_VIDEOJS = @json(config('nomercy.privacy.HELP_IMPROVE_VIDEOJS'));
    var player = videojs("video-{{$id}}", options);
        player.nmplayer();
        player.nmremote();
        player.nmsync();



</script>


@endsection
