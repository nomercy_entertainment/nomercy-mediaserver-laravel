@extends('layouts.dashboard')


@section('title', $data['title_sort'] ?? '')
@section('description', $data['toverview'] ?? '')
@section('image', $data['backdrop'] ?? '' )
@section('type', 'image/jpg')

@section('head')

<link rel="preload" as="style"      href="{{ asset('/css/info.css?v=' . asset_tag()) }}">
<link rel="stylesheet"              href="{{ asset('/css/info.css?v=' . asset_tag()) }}">

<style>
    @media (max-width: 900px) and (orientation:landscape) {
        .jwplayer {
            max-height: calc(100vh - 5rem) !important;
        }
    }
</style>

@endsection

@section('content')

<img id="swatch" crossorigin="anonymous" src="{{ $data['backdrop'] }}" alt="swatch" class="hidden"/>

<div class="fixed top-0 w-full h-screen pt-12 overflow-hidden" style="z-index: -1;background: url(' {{ $data['backdrop'] }}');background-repeat: no-repeat;background-size: cover;background-position: center;width: 100vw;height: 100vh;">
    <div class="w-full h-screen bg-black bg-opacity-50"></div>
</div>
<div class="right-0 z-10 flex flex-col justify-center w-full overflow-hidden text-white scrollable h-min">
    <div class="flex flex-col justify-center w-full px-8 py-8 lg:flex-row h-min-2" style="">
        <div id="trailer_info" class="flex flex-col self-center float-left w-full h-full m-8 mx-auto mb-24 lg:w-6/12 lg:justify-center md:pr-8">
            <h1 class="mr-auto text-2xl font-bold 3xl:text-5xl 2xl:text-3xl xl:text-3xl" data-automation-id="title"> {{ isset($data['translated']['title']) && $data['translated']['title'] != null ? $data['translated']['title'] : $data['title'] }} ({{ $data['first_air_date'] ?? $data['release_date'] }})</h1>
            <div class="mt-4 mb-6 _3qsVvm _1wxob_ text-2sm xl:text-sm 3xl:text-lg" style="height: 100px;min-height: 100px;overflow-y: auto;">{{ $data['overview'] }}</div>
            <div class="_2vWb4y dv-dp-node-meta-info avu-full-width">
                <div class="_1GTSsh LgxvCy" id="meta-info" data-automation-id="meta-info">
                    <div class="_1ONDJH">
                        <dl>
                            <dt>
                                <span class="_36qUej">{{ __('ui.duration') }}</span>
                            </dt>
                            <dd>
                                {{ $data['duration']  }}
                            </dd>
                        </dl>
                        <dl>
                            @if(isset($data['directors']))
                            <dt>
                                <span class="_36qUej">{{ trans_choice('ui.director', count($data['directors'])) }}</span>
                            </dt>
                            <dd>
                                @foreach($data['directors'] as $director)
                                <a href="/person?id={{ $director['id'] }}" class="">{{ $director['name'] }}</a>@if (!$loop->last && !$loop->first),@endif
                                @endforeach
                            </dd>
                            @endif
                        </dl>
                        <dl>
                            <dt><span class="_36qUej">{{ trans_choice('ui.lead', count($data['lead_actors'])) }}</span>
                            </dt>
                            <dd>
                                @foreach($data['lead_actors'] as $actors)
                                    @if(isset($actors['id']))
                                        <a href="/person?id={{ $actors['id'] }}" class="">{{ $actors['name'] }}</a>@if (!$loop->last),@endif
                                    @endif
                                @endforeach
                            </dd>
                        </dl>
                        <dl>
                            <dt><span class="_36qUej">{{ trans_choice('ui.genre', count($data['genres']) ?? 0) }}</span>
                            </dt>
                            <dd>
                                @foreach($data['genres'] as $genre)
                                    {{ $genre['name'] ?? $genre }}@if (!$loop->last), @endif
                                @endforeach
                            </dd>
                        </dl>
                        @if(isset($data['languages']))
                        @if(config('nomercy.language_display_type') != 'images')
                        <dl class="_2czKtE">
                            <dt>
                                <span class="_36qUej">{{ trans_choice('ui.audios', $data['languages'][0] == 'none' || count( explode(',', $data['languages'])) > 1 ? 2 : 1 ) }}</span>
                            </dt>
                            <dd>
                                @foreach( $data['audio'] ?? explode(',', $data['languages']) as $audio)
                                    @if($audio != 'und')
                                    {{ __('lang.'. replace_name_country($languages ?? [],$audio)) }}@if (!$loop->last), @endif
                                    @endif
                                    @endforeach

                                </dd>
                        </dl>
                        @else
                        <dl class="_2czKtE">
                            <dt>
                                <span class="_36qUej">{{ trans_choice('ui.audios', $data['languages'][0] == 'none' || count( explode(',', $data['languages'])) > 1 ? 2 : 1 ) }}</span>
                            </dt>
                            <dd>
                                @foreach( $data['audio'] ?? explode(',', $data['languages']) as $audio)
                                    @if($audio != 'und')
                                        <div class="float-left w-5 h-3 my-1 mr-1" title="{{ __('lang.'. replace_name_country($languages ?? [],$audio)) }}">
                                            {!! replace_name_flag($languages ?? [],$audio) !!}
                                        </div>
                                    @endif
                                @endforeach

                            </dd>
                        </dl>
                        @endif
                        @endif
                        @if(isset($data['languages']))
                        @if(config('nomercy.language_display_type') != 'images')
                            <dl class="_2czKtE">
                                <dt>
                                    <span class="_36qUej">{{ trans_choice('ui.subtitles', $data['subtitles'][0] == 'none' || count($data['subtitles']) > 1 ? 2 : 1 ) }}</span>
                                </dt>
                                <dd>
                                    @foreach($data['subtitles'] as $subtitle)
                                    {{--  @dd($subtitle)  --}}
                                    @if($subtitle[0] != '')
                                        {{ __('lang.'. replace_name_country($languages, $subtitle)) }}@if (!$loop->last), @endif
                                        @endif
                                    @endforeach
                                </dd>
                            </dl>
                        @else
                            <dl class="_2czKtE">
                                <dt>
                                    <span class="_36qUej">{{ trans_choice('ui.subtitles', $data['subtitles'][0] == 'none' || count($data['subtitles']) > 1 ? 2 : 1 ) }}</span>
                                </dt>
                                <dd>
                                    @foreach($data['subtitles'] as $subtitle)
                                        @if($subtitle != 'Geen')
                                            <div class="float-left w-5 h-3 my-1 mr-1" title="{{ __('lang.'. replace_name_country($languages ?? '',$subtitle)) }}">
                                                {!! replace_name_flag($languages ?? '',$subtitle) !!}
                                            </div>
                                        @else
                                            {{ $subtitle }}
                                        @endif
                                    @endforeach
                                </dd>
                            </dl>
                        @endif

                        @endif
                    </div>
                </div>
            </div>
            <div id="dv-action-box-wrapper" class="avu-full-width">
                <a class="px-4 py-3 mt-4 rounded btn btn-lg theme-btn DarkVibrant" href="/{{ $data['type'] }}/{{ $id }}/watch">
                    <span class="text-white _36qUej DarkVibrant">{{ __('ui.watch') }}</span>
                </a>
            </div>
        </div>
        <div id="trailer_season" class="flex flex-col self-center float-left w-full h-full m-8 mx-auto mb-24 lg:w-6/12 lg:justify-center md:pr-8">
            <div id="{{$data['id']}}" class="nmplayer jwplayer" tabindex="0" style="position: absolute;"></div>
            @if(isset($data['trailer']) && $data['trailer'] != null && contains($data['trailer'], array('youtube')) != false)
                <div class="relative w-full video" class="relative w-full" style="padding-bottom: 56.25%;">
                    <iframe id="backup-trailer" allowFullScreen src="{{ $data['trailer'] }}" class="absolute top-0 left-0 w-full h-full"></iframe>
                </div>
            @endif
        </div>
    </div>

    <div id="tabs" class="fixed bottom-0 flex flex-row flex-wrap w-full h-16 px-8 py-2 mx-auto text-center border-b-2 md:px-10 DarkVibrant" onclick="scrollToTargetAdjusted();$(this).next().css({'display': 'block', 'position': 'relative'}); $(this).css({'position': 'relative', 'height': '100%' })" style="background-color:{{ themeColor() }}77;">
        @if(isset($data['seasons']))
        <div class="w-20 h-8 p-2 px-2 m-2 rounded cursor-pointer tab-button" onclick="$('.tab').addClass('hidden');$('.tab-episodes').removeClass('hidden');$('.tab-button').removeClass('btn-active');$(this).addClass('btn-active');">
            Episodes
        </div>
        @endif
        <div class="w-20 h-8 p-2 px-2 m-2 rounded cursor-pointer tab-button" onclick="$('.tab').addClass('hidden');$('.tab-cast').removeClass('hidden').addClass('flex');$('.tab-button').removeClass('btn-active');$(this).addClass('btn-active');">
            Cast
        </div>
        <div class="w-20 h-8 p-2 px-2 m-2 rounded cursor-pointer tab-button" onclick="$('.tab').addClass('hidden');$('.tab-crew').removeClass('hidden').addClass('flex');$('.tab-button').removeClass('btn-active');$(this).addClass('btn-active');">
            Crew
        </div>
        <div class="w-20 h-8 p-2 px-2 m-2 rounded cursor-pointer tab-button" onclick="$('.tab').addClass('hidden');$('.tab-media').removeClass('hidden').addClass('flex');$('.tab-button').removeClass('btn-active');$(this).addClass('btn-active');">
            Media
        </div>
        <div class="w-32 h-8 p-2 px-2 m-2 rounded cursor-pointer tab-button" onclick="$('.tab').addClass('hidden');$('.tab-francise').removeClass('hidden').addClass('flex');$('.tab-button').removeClass('btn-active');$(this).addClass('btn-active');">
            Recommendations
        </div>
        <div class="w-20 h-8 p-2 px-2 m-2 rounded cursor-pointer tab-button" onclick="$('.tab').addClass('hidden');$('.tab-trivia').removeClass('hidden').addClass('flex');$('.tab-button').removeClass('btn-active');$(this).addClass('btn-active');">
            Trivia
        </div>
    </div>
    <div class="hidden w-full pb-2 mx-auto overflow-auto info-container DarkVibrant" style="height: calc(100vh - 8rem);background-color:{{ themeColor() }}77;">

        <div class="flex-wrap justify-center hidden w-full px-4 pb-8 mx-auto tab tab-episodes">
            @if(isset($data['seasons']))
                <div class="flex flex-wrap w-full pb-2 mx-auto my-2 border-b-2 h-14">
                    <div class="p-3 px-4 w-36">{{ __('ui.season')}}</div>
                    @foreach($data['seasons'] as $season)
                        <div class="season_number mx-1 h-10 px-3 py-2 cursor-pointer rounded @if((int)$season['season_number'] == '01') LightVibrant bg-opacity-50 @endif" onclick="
                                $('.season').addClass('hidden');
                                $('.season_number').removeClass('LightVibrant');
                                $('.season-{{$season['season_number']}}').removeClass('hidden');
                                $(this).addClass('LightVibrant')
                            ">{{$season['season_number']}}
                        </div>
                    @endforeach
                </div>
                @foreach($data['seasons'] as $season)
                    <div class="season season-{{$season['season_number']}} mb-8 @if((int)$season['season_number'] > 1 || (int)$season['season_number'] == 0) hidden @endif">
                        @foreach(collect($season['episodes'])->sortBy('episode_number') as $episode)
                        <div class="p-2 pb-3 flex flex-row border-b-2  @if(  !isset($episode['video_file']['duration']) || $episode['video_file']['duration'] == null ) grayscaled cursor-not-allowed @else cursor-pointer @endif "
                                @if(isset($episode['video_file']['duration']) && $episode['video_file']['duration'] !=null) onclick="window.location.href='/tv/{{ $id }}/watch?season={{ $episode['season_number'] }}&episode={{ $episode['episode_number'] }}'" @endif
                                @if(  !isset($episode['video_file']['duration']) || $episode['video_file']['duration'] == null ) style="filter:grayscale(1)" @endif
                        >

                            <img class="self-center w-20 h-auto mt-10 rounded md:w-auto md:h-32 lazy"
                                crossorigin="anonymous"
                                 src="{{ asset('img/still-not-available.png') }}" data-src="{{ $episode['still'] ?? asset('img/still-not-available.png') }}"
                                onerror="this.src='{{ asset('/img/still-not-available.png')}}'"
                                alt="" style="min-width: calc(9vh/9*16 ); height: 9vh;"/>
                            <div class="w-16 px-2 mr-3 -mx-10 text-center">{{ $episode['episode_number'] }}.</div>
                            <div class="flex flex-col w-full">
                                <div class="mb-2 text-left text-md">
                                    {{ $episode['title'] ?? $episode['name'] }}
                                </div>
                                <div class="pb-1 overflow-y-auto text-xs text-left" style="max-height: 11vh;">
                                    {{ $episode['overview'] ?? $episode['overview'] }}
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                @endforeach
            @endif
        </div>

        <div class="flex-wrap justify-center hidden w-full px-4 pb-8 mx-auto tab tab-cast">
            @foreach($data['cast'] as $cast)
            <div class="flex flex-col w-5/12 m-3 rounded cursor-pointer md:w-48">
                <img src="{{$cast['profile_path']}}" class="w-full h-full rounded-t" onerror="this.src='/img/poster-not-available.png'" style="max-height: 240px" alt=""/>
                <div class="text-center text-gray-900 bg-gray-100">
                    <div class="h-4 px-1 overflow-hidden text-sm">
                        {{$cast['name']}}
                    </div>
                    <div class="h-4 px-1 overflow-hidden text-sm">
                        {{ str_replace('(voice)', '', $cast['character']) }}
                    </div>
                    <div class="h-4 px-1 py-2 text-sm rounded-b-lg">
                        {{ null }}
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="flex-wrap justify-center hidden w-full px-4 mx-auto tab tab-crew">
            @foreach($data['crew'] as $crew)
            <div class="flex flex-col w-5/12 m-3 rounded cursor-pointer md:w-48">
                <img src="{{$crew['profile_path']}}" class="w-full h-full rounded-t" onerror="this.src='/img/poster-not-available.png'" style="max-height: 240px" alt=""/>
                <div class="text-center text-gray-900 bg-gray-100">
                    <div class="h-4 px-1 overflow-hidden text-sm">
                        {{$crew['name']}}
                    </div>
                    <div class="h-4 px-1 overflow-hidden text-sm">
                        {{ $crew['job'] }}
                    </div>
                    <div class="h-4 px-1 py-2 text-sm rounded-b-lg">
                        {{ null }}
                    </div>
                </div>
            </div>
            @endforeach
        </div>


        <div class="flex-wrap justify-center hidden w-full px-4 pb-8 mx-auto tab tab-media">
            <div class="flex flex-wrap justify-center w-full pb-8 pl-4 md:px-20">
                @foreach($data['posters'] as $poster)
                <div class="relative flex flex-col w-5/12 m-3 rounded cursor-pointer md:w-48">
                    <img src="{{ asset('img/poster-not-available.png') }}" data-src="{{$poster['file_path']}}" class="lazy" alt="" onclick="$(this).next().next().toggle();$(body).css('overflow-y','hidden')" onmouseout="$(this).next().toggle()" onmouseover="$(this).next().toggle()"/>
                    <div class="absolute self-center hidden px-2 py-1 my-48 bg-gray-900 rounded cursor-pointer lazy">
                        {{ __('ui.click-enlarge')}}
                    </div>
                    <div class="fixed bottom-0 left-0 z-50 flex-row hidden w-full h-full p-0 m-0 bg-gray-900" onclick="$(this).toggle();$(body).css('overflow-y','auto')">
                        <img src="{{ asset('img/poster-not-available.png') }}" data-src="{{$poster['file_path']}}" alt="" class="w-full h-auto m-auto rounded cursor-pointer lg:w-auto lg:h-full center lazy"/>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="flex flex-wrap justify-center w-full pb-8 pl-4 md:px-12 2xl:px32">
                @foreach($data['backdrops'] as $backdrop)
                <div class="relative flex flex-col mx-6 my-2 rounded cursor-pointer md:w-2/12">
                    <img src="{{ asset('img/backdrop-not-available.png') }}" data-src="{{$backdrop['file_path']}}" class="lazy" alt="" class="cursor-pointer lazy" onclick="$(this).next().next().toggle();$(body).css('overflow-y','hidden')" onmouseout="$(this).next().toggle()" onmouseover="$(this).next().toggle()"/>
                    <div class="absolute self-center hidden px-2 py-1 my-20 bg-gray-900 cursor-pointer">
                        {{ __('ui.click-enlarge')}}
                    </div>
                    <div class="fixed bottom-0 left-0 z-50 flex-row hidden w-full h-full p-0 m-0 bg-gray-900" onclick="$(this).toggle();$(body).css('overflow-y','auto')">
                        <img src="{{ asset('img/backdrop-not-available.png') }}" data-src="{{$backdrop['file_path']}}" alt="" class="w-full h-auto m-auto cursor-pointer md:w-auto md:h-full center lazy"/>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="flex flex-wrap justify-center w-full pb-8 pl-4 md:px-20">
                @foreach($data['videos'] as $video)
                <div class="relative flex flex-col m-3 cursor-pointer">
                    <iframe allowFullScreen="allowFullScreen" src="{{ $video['key'] }}" class="w-full h-full"></iframe>
                </div>
                @endforeach
            </div>
        </div>

        <div class="flex-wrap justify-center hidden w-full px-4 pb-8 mx-auto tab tab-francise">
            <div class="flex flex-wrap justify-center w-full pb-8 pl-4 md:px-20">
                @foreach($data['recommendations'] as $recommendation)
                <div class="relative flex flex-col w-5/12 m-3 rounded cursor-pointer md:w-48">
                    <img src="{{ asset('img/poster-not-available.png') }}" data-src="{{$recommendation['poster']}}" alt="" class=" lazy" onclick="$(this).next().next().toggle();$(body).css('overflow-y','hidden')" onmouseout="$(this).next().toggle()" onmouseover="$(this).next().toggle()"/>
                    <div class="absolute self-center hidden px-2 py-1 my-48 bg-gray-900 rounded cursor-pointer">
                        {{ __('ui.click-enlarge')}}
                    </div>
                    <div class="fixed bottom-0 left-0 z-50 flex-row hidden w-full h-full p-0 m-0 bg-gray-900" onclick="$(this).toggle();$(body).css('overflow-y','auto')">
                        <img src="{{ asset('img/poster-not-available.png') }}" data-src="{{$recommendation['poster']}}" alt="" class="w-full h-auto m-auto rounded cursor-pointer lg:w-auto lg:h-full center lazy"/>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="flex-wrap justify-center hidden w-full px-4 pb-8 mx-auto tab tab-trivia">

        </div>

    </div>

</div>

<script src="https://cdn.nomercy.tv/js/Vibrant.min.js"></script>
<script>
    if(!window.isSony){
        let img = document.querySelector('#swatch');

        img.addEventListener('load', function() {
            var vibrant = new Vibrant(img, 12, 5);
            var swatches = vibrant.swatches()
            for (var swatch in swatches){
                if (swatches.hasOwnProperty(swatch) && swatches[swatch]){
                    let hex = swatches[swatch].getHex();
                    if(swatch == 'Vibrant'){
                        $('.Vibrant').css({
                            background: hex,
                            'background-color': hex,
                            color: '#000'
                        });

                        jwplayer().on('ready', () => {
                            $('.jw-knob').css({
                                'background-color': hex,
                            });
                        });
                    }
                    else if(swatch == 'LightVibrant'){
                        $('.LightVibrant').css({
                            background: hex,
                            'background-color': hex,
                            color: '#000'
                        });
                    }
                    else if(swatch == 'DarkVibrant'){
                        $('.DarkVibrant,.tab-button,.season_number').css({
                            background: hex,
                            'background-color': hex,
                            fill: hex,
                        });
                        $('.jw-button-color,.jw-text').css({
                            color: hex
                        });
                        jwplayer().on('ready', () => {
                            $('.jw-progress,.jw-buffer').css({
                                'background-color': hex,
                            });
                            $('.jw-button-color,.jw-text').css({
                                color: hex
                            });
                        });
                        document.styleSheets[2].addRule("::-webkit-scrollbar-thumb", `background-color: ${hex}aa !important;`);
                    }
                    else if(swatch == 'DarkMuted'){
                        $('.DarkMuted').css({
                            background: hex,
                            'background-color': hex,
                            color: '#000'
                        });
                    }
                    else if(swatch == 'LightMuted'){
                        $('.LightMuted').css({
                            background: hex,
                            'background-color': hex,
                            color: '#000'
                        });
                    }
                }
            }
        });
    }
</script>

<script>
    !function(window){
    var $q = function(q, res){
            if (document.querySelectorAll) {
            res = document.querySelectorAll(q);
            } else {
            var d=document
                , a=d.styleSheets[0] || d.createStyleSheet();
            a.addRule(q,'f:b');
            for(var l=d.all,b=0,c=[],f=l.length;b<f;b++)
                l[b].currentStyle.f && c.push(l[b]);

            a.removeRule(0);
            res = c;
            }
            return res;
        }
        , addEventListener = function(evt, fn){
            window.addEventListener
            ? this.addEventListener(evt, fn, false)
            : (window.attachEvent)
                ? this.attachEvent('on' + evt, fn)
                : this['on' + evt] = fn;
        }
        , _has = function(obj, key) {
            return Object.prototype.hasOwnProperty.call(obj, key);
        }
        ;

    function loadImage (el, fn) {
        var img = new Image()
        , src = el.getAttribute('data-src');
        img.onload = function() {
        if (!! el.parent)
            el.parent.replaceChild(img, el)
        else
            el.src = src;

        fn? fn() : null;
        }
        img.src = src;
    }

    function elementInViewport(el) {
        var rect = el.getBoundingClientRect()

        return (
        rect.top    >= 0
        && rect.left   >= 0
        && rect.top <= (window.innerHeight || document.documentElement.clientHeight)
        )
    }

        var images = new Array()
        , query = $q('img.lazy')
        , processScroll = function(){
            for (var i = 0; i < images.length; i++) {
                if (elementInViewport(images[i])) {
                loadImage(images[i], function () {
                    images.splice(i, i);
                });
                }
            };
            }
        ;
        // Array.prototype.slice.call is not callable under our lovely IE8
        for (var i = 0; i < query.length; i++) {
        images.push(query[i]);
        };

        processScroll();
        addEventListener('scroll',processScroll);

    }(this);
</script>


@if($JWPLAYER_KEY != '')
    <script src="{{ asset('/js/jwplayer.licensed/jwplayer.js?v=OmKEGJ3xRW') }}"></script>
@else

<script src="{{ asset('/js/jwplayer.open/jwplayer.js?v=OmKEGJ3xRW') }}"></script>

@endif

<script src="{{ asset('/js/jwplayer/jwplayer.defaults.js?v=OmKEGJ3xRW') }}"></script>

<script>
    var player = jwplayer("{{$data['id']}}");
        player.setup({
            file: "{{ asset('assets/trailer/'. $data['id'] . '.trailer.mp4') }}",
            type: 'mp4',
            image: "{{ $data['backdrop'] }}",
            controls: true,
            repeat: false,
            autostart: 'viewable',
            width: '100%',
            key: "{{ $JWPLAYER_KEY }}"
        });


    if(!window.isSony){
        function scrollToTargetAdjusted() {
            setTimeout(() => {
                window.scrollTo({
                    top: (window.innerHeight * 2),
                    behavior: "smooth"
                });
            }, 300);
        }

        jwplayer().on("ready", (e) => {
            $('.jwplayer').prepend(`<div id="spinner" class="spinner"></div>`);
        });
        jwplayer().on("play", (e) => {
            $('#spinner').css('display', 'none');
        });

        jwplayer().on('error', function(){
            $('#{{ $data['id'] }}').css('display', 'none');
            $('#backup-trailer').css('display', 'block');
        });

        $('._1NNx6V').click(function (e) {
            e.preventDefault();
            jwplayer().stop();

            $('._1FuiMq').css('display', 'none');
            if (e.target.dataset.season == 'trailer') {
                $('#season-toggle').text(`Trailer`);
                $('.jwplayer').css('display', 'block');
                jwplayer().play();
            } else {
                $('._3kgCxW').css('display', 'block');
                $('#season-toggle').text(`Season ${e.target.dataset.season}`);
            }

            $(`.season-${e.target.dataset.season}`).css('display', 'block');
            $('._1TO3hV._3ra7oO').click()

        });
    }

</script>


@endsection
