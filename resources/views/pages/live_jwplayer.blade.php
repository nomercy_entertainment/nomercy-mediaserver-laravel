@extends('layouts.dashboard')

@section('title', $streamer->name . '\'s Live stream')
@section('description', $streamer->stream->title)
@section('image', $streamer->stream->image . '&s=150')
@section('type', 'image/png')

@section('head')

<link rel="stylesheet" href="{{ asset('/css/nmplayer.css?v=OmKEGJ3xRW') }}">
<style>
    @media (max-width: 900px) {
        #submit {
            top: 50%;
            transform: translateY(-50%);
        }
    }

</style>

<div src="" class="preload-images" alt=""></div>

@endsection

@section('content')

<div id="container" class="fixed w-full h-full pt-16">

    <div id="sidebar" class="flex flex-col float-left h-full bg-gray-700 sidebar">
        <div class="flex flex-row content-center w-full">
        <div class="float-left w-11/12 py-3"> {{ __('ui.livechannels') }}</div>
            <button id="btn-sidebar" class="float-left w-1/12" onclick="toggleSidebar()">⇤</button>
        </div>
    </div>

    <div id="main" class="float-left overflow-y-auto bg-gray-900 player ">
        <div id="{{ $id }}" class="jwplayer"></div>
        <div id="about-mobile" class="flex flex-col float-left w-6/12 h-12 px-5 py-3">
            {{ __('ui.chat')}}
        </div>
        <div id="mobile-viewer-container" class="flex flex-row float-right w-4/12 h-12 px-5 py-3 text-right viewer-container">
            <i class="mt-1 mr-4 fa fa-user"></i>
            <div class="mr-3 text-right viewers">0</div>
        </div>
        <div id="banner" class="float-left w-full overflow-y-auto">
            <div class="w-full">
                <div class="relative flex flex-col float-left h-full px-2 py-5 lg:w-1/12">
                    <button id="profile-button" class="w-20 h-20 mx-auto overflow-hidden border-2 border-gray-600 rounded-full focus:outline-none focus:border-white">
                        <img src="{{ $streamer->stream->image }}" alt="avatar" class="object-contain w-full h-full">
                        <div id="btn-live" class="bg-red-700 btn-live"> {{ __('ui.offline') }}</div>
                    </button>
                </div>
                <div class="flex flex-col float-left h-full px-2 py-5 lg:w-4/12">
                    <div class="flex flex-row w-full h-8">
                        <div class="float-left pr-1 name">{{ $streamer->name }}</div>
                        <div class="float-left bg-blue-700 btn-verified">✓</div>

                    </div>
                    <div class="">{{ $streamer->stream->title }} </div>
                    <div class="text-xs ">{{ $streamer->stream->game }}</div>

                </div>
                <div class="float-left w-4/12 h-full px-2 py-5 spacer">

                </div>
                <div class="float-right w-3/12 h-full px-2 py-5">
                    <div id="" class="flex flex-row w-24 viewer-container">
                        <i class="float-left mt-1 mr-4 fa fa-user"></i>
                        <div class="float-left w-8/12 mr-auto viewers">0</div>
                    </div>
                </div>
            </div>
        </div>
        <div id="about" class="flex flex-col w-full h-full">
            <div class="float-left w-9/12 h-64 p-12 mx-auto my-24 bg-gray-800">
                <div class="flex flex-col float-left w-2/12 px-2 py-5">
                    <div id="profile-button" class="w-24 h-24 mx-auto overflow-hidden border-2 border-gray-600 rounded-full focus:outline-none focus:border-white">
                        <img src="{{ $streamer->stream->image }}" alt="avatar" class="object-contain w-full h-full">
                    </div>
                    <div class="w-full mt-2 text-center">{{ $streamer->stream->followers }} {{ __('followers') }}</div>
                </div>
                <div class="flex flex-col float-left w-7/12 h-40 px-2 py-5">
                    <div class="text-xl font-bold text-left">About {{ $streamer->name }}</div>
                    <div class="pt-2 text-xs text-left">
                        {!! $streamer->stream->description !!}
                    </div>
                </div>
                <div class="flex flex-col float-right w-3/12 h-40 px-2 py-2">
                    <ul class="links">
                        @if($streamer->stream->youtube)
                        <li class="link">
                            <i class="float-left w-6 mt-1 fa fa-youtube-play"></i>
                            <a href="https://youtube.com/{{ $streamer->stream->youtube }}" target="_blank">
                                <div class="w-9 link-name">Youtube</div>
                            </a>
                        </li>
                        @endif
                        @if($streamer->stream->twitch)
                        <li class="link">
                            <i class="float-left w-6 mt-1 fa fa-twitch"></i>
                            <a href="https://twitch.tv/{{ $streamer->stream->twitch }}" target="_blank">
                                <div class="w-9 link-name">Twitch</div>
                            </a>
                        </li>
                        @endif
                        @if($streamer->stream->facebook)
                        <li class="link">
                            <i class="float-left w-6 mt-1 fa fa-facebook-official"></i>
                            <a href="https://facebook.com/{{ $streamer->stream->facebook }}" target="_blank">
                                <div class="w-9 link-name">Facebook</div>
                            </a>
                        </li>
                        @endif
                        @if($streamer->stream->instagram)
                        <li class="link">
                            <i class="float-left w-6 mt-1 fa fa-instagram"></i>
                            <a href="https://instagram.com/{{ $streamer->stream->instagram }}" target="_blank">
                                <div class="w-9 link-name">Instagram</div>
                            </a>
                        </li>
                        @endif
                        @if($streamer->stream->twitter)
                        <li class="link">
                            <i class="float-left w-6 mt-1 fa fa-twitter"></i>
                            <a href="https://twitter.com/{{ $streamer->stream->twitter }}" target="_blank">
                                <div class="w-9 link-name">Twitter</div>
                            </a>
                        </li>
                        @endif
                        @if($streamer->stream->spotify)
                        <li class="link">
                            <i class="float-left w-6 mt-1 fa fa-spotify"></i>
                            <a href="https://spotify.com/{{ $streamer->stream->spotify }}" target="_blank">
                                <div class="w-9 link-name">Spotify</div>
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div id="chat" class="float-left h-full bg-gray-800 chat">
        <div id="chat-flow" class="flex flex-col chat-flow" style="overflow-wrap: break-word;"></div>
        <div class="relative bottom-0 py-3 bg-gray-900 chat-form-container">
            <form id="chat-form" class="flex w-full h-full px-3 lg:flex-col sm:flex-row">
            <textarea id="msg" type="text" placeholder="{{ __('ui.typemessage') }}" required="" autocomplete="off" rows="3" disabled="disabled"></textarea>
                <button id="submit" class="relative flex w-32 h-8 py-1 pl-4 mx-2 my-0 bg-purple-800 md:my-3 lg:my-2 xl:ml-auto">{{ __('ui.send')}}
                    <i class="py-1 ml-auto fa fa-paper-plane"></i>
                </button>
            </form>
        </div>
    </div>
</div>
</div>

@if($JWPLAYER_KEY != '')
{{-- <script src="{{ asset('/js/jwplayer.licensed/jwplayer.js?v=' . asset_tag())}}"></script> --}}
<script src="https://ssl.p.jwpcdn.com/player/v/8.17.7/jwplayer.js"></script>
@else

<script src="{{ asset('/js/jwplayer.open/jwplayer.js?v=' . asset_tag())}}"></script>
<script src="{{ asset('/js/jwplayer.open/hls.min.js?v=' . asset_tag())}}"></script>
<script src="{{ asset('/js/jwplayer.open/jwplayer.hlsjs.js?v=' . asset_tag())}}"></script>

@endif

<script src="{{ asset('/js/dist/subtitles-octopus.js?v=' . asset_tag())}}"></script>
<script src="{{ asset('/js/jwplayer/jwplayer.defaults.js?v=' . asset_tag())}}"></script>
{{-- <script src="{{ asset('/js/jwplayer/provider.cast.js?v=' . asset_tag())}}"></script> --}}
<script src="https://ssl.p.jwpcdn.com/player/v/8.17.7/provider.cast.js"></script>

<script src="{{ asset('/js/jwplayer/nmlive.jwplayer.js?v=' . asset_tag())}}"></script>

<script>
    let i = 0;
    var player = jwplayer("{{$id}}");
    var nmplayer = nmplayer("{{$id}}");
    nmplayer.setup({
        jwplayerInstance: player,
        jwplayerSetup: {
            name: '{{ $streamer->name }}',
            key: `{{ $JWPLAYER_KEY }}`,
            image: `/img/{{ $streamer->name }}-offline.png`,
            file: `/show/{{ $streamer->name }}.m3u8`,
            width: '100%',
        }
    });

    let sidebar = 'open';

    function toggleSidebar() {
        if (sidebar == 'open') {
            sidebar = 'closed';
            $('#btn-sidebar').html('&#8677;');
            $('#container').addClass('closed');
        } else {
            sidebar = 'open';
            $('#btn-sidebar').html('&#8676;');
            $('#container').removeClass('closed');
        }
    }

    var scrolled = false;
    $('#chat-flow').on('scroll', function () {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            scrolled = false;
        } else {
            scrolled = true;
        }
    })
    scrolled = false;

    $('#msg').keypress(function (e) {
        console.log(e);
        if (e.which == 13) {
            $('#submit').click();
            return false;
        }
    });

</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/qs/6.9.2/qs.min.js" integrity="sha256-TDxXjkAUay70ae/QJBEpGKkpVslXaHHayklIVglFRT4=" crossorigin="anonymous" defer=""></script>
{{-- <script src="{{ env('APP_URL') }}:2096/socket.io/socket.io.js"></script> --}}
<script src="/js/socket.io.js"></script>
<script src={{ asset('/js/nmchat.js?v=' . asset_tag())}}"></script>

<script>

// $( document ).ready(function() {

    let host = location.origin;
    // let host = 'https://server.nomercy.tv';

    var nmchat = new nmchat(host + ':2096', '{{ $streamer->name }}', '{{ $username }}', '{{ $userimage }}');

    if (io === undefined) {
        alert('Socket connection error.');
        document.getElementById('msg').innerHTML = 'It was a dark and stormy night...';
    }
// });
</script>

@endsection
