@extends('layouts.dashboard')

@section('title', $name ?? null)
@section('description', __('ui.description'))

@section('head')
<style>
.nm-card .poster {
    background-color: #000000;
}
</style>
@endsection

@section('content')

<div id="content" class="hidden clear-both bg-gray-700 xl:pl-2 xl:pr-6 3xl:pl-16 3xl:pr-6">
    <div src="" class="preload-images" alt=""></div>
    @foreach($data as $item)
    @php
    if(isset($item['type']) && $item['type'] == 'collection') {
    $itemtype = 'collection';
    }
    else if(isset($item['type']) && $item['type'] != 'movie'){
    $itemtype = 'tv';
    $percentage = $item['have_episodes'] !=0 ? ($item['have_episodes'] / $item['number_of_episodes']) * 100 : 0;
    }
    else {
    $itemtype = 'movie';
    }
    @endphp

    @if( isset($item['id']) && ( ( isset($item['have_episodes']) && $item['have_episodes'] !== 0) || ( isset($item['video_file']) && $item['video_file']['duration'] !== null) || $itemtype == 'movie' || (isset($item['parts']) && $item['parts'] > 0)))

    <div class="nm-card @if($loop->first) active @endif" href="{{$item['type']}}/{{ $item['id'] }}/info">
        <div href="{{$item['type']}}/{{ $item['id'] }}/info" id="{{$item['id']}}" data-type="{{ $itemtype ?? $item['type'] }}" data-name="{{ ucwords( str_replace("\\","", $item['title_sort'])) }}" class="poster redirect" style="background:url('{{ $item['poster'] ?? 'https://cdn.nomercy.tv/img/noimage.poster.large.jpg' }}'), url('https://cdn.nomercy.tv/img/poster-not-available.png') no-repeat left top;"></div>
        <div class="button-box">
            {{-- {!! $qualityDiv !!} --}}
        </div>
        <div class="info">
            <div class="title2">{{ ucwords( str_replace("\\","", $item['name'] ?? $item['title'])) }}</div>
            <div class="counter" style="{{ isset($item['type']) && $item['type'] != null ? 'background: #040404;' : ''}}">
                @if(isset($percentage))
                <div class="progress progress-text" style="@if($percentage < 75 && $percentage > 45) font-weight:700; mix-blend-mode: difference; @endif"> {{ $item['have_episodes'] ?? 0 }} / {{ $item['number_of_episodes']}} </div>
                <div class="progress" data-percent="{{ $percentage }}"></div>
                @endif
            </div>
        </div>
    </div>
    @elseif(isset($item['type']) && $item['type'] == 'collection')

    <div class="nm-card" href="{{$item['type']}}/{{ $item['id'] }}/info">
        <div class="poster redirect" style="background:url('{{ $item['poster'] ?? 'https://cdn.nomercy.tv/img/noimage.poster.large.jpg' }}'), url('https://cdn.nomercy.tv/img/poster-not-available.png') no-repeat left top;" ></div>
        <div class="button-box">
        </div>
        <div class="info">
            <div class="title2">No results</div>
            <div class="counter" style="background: #353535">
            </div>
        </div>
    </div>

    @endif

    @unset($percentage)

    @endforeach

</div>
</div>
<div id="menu" class="ytp-popup ytp-contextmenu" style="visibility:hidden;position:fixed">
    <div class="ytp-panel" style="min-width: 250px; width: 300px;">
        <div id="menuSpace" class="ytp-panel-menu" role="menu">
        </div>
    </div>
</div>

<script>
    $(document).keydown(function (e) {
        var $activeSection = $(".active"),
            $newActiveSection;

        if(e.key == 'Enter' || e.key == 5){
            location.href = $(".active").attr('href');
        }

        if (e.which == 38 || e.key == 2) {
        e.preventDefault();
        // Up
            $newActiveSection = $activeSection.prev(".nm-card") || $(".nm-card")[0];
        if(!$newActiveSection.length) {
            $newActiveSection = $(".nm-card").last();
        }
        } else if (e.which == 40 || e.key == 8) {
            e.preventDefault();
            // Down
            $newActiveSection = $activeSection.next(".nm-card") || $(".nm-card")[0];
            if(!$newActiveSection.length) {
                $newActiveSection = $(".nm-card").first();
            }
        }

        if(typeof $newActiveSection != 'undefined'){
            $newActiveSection.addClass("active").focus().css({
                'border': '4px #961cc9 solid',
            }).children().css({
                "background-color": '#a4a4a4',
                // 'background-blend-mode': 'overlay',
            });

            $activeSection.removeClass("active").css({
                'border': '4px #353535 solid',

            }).children().css({
                "background-color": '#000000',
                // 'background-blend-mode': 'lighten',

            });

            scrollToObject($newActiveSection);
        }
    });

    function scrollToObject(object) {
        window.scrollTo(0, $(object)[0].offsetTop - $('nav').height())
    }

</script>

<script>
    var imgs = document.images,
    len = imgs.length,
    counter = 0;

    [].forEach.call( imgs, function( img ) {
        if(img.complete)
        incrementCounter();
        else
        img.addEventListener( 'loaded', incrementCounter, false );
    } );

    function incrementCounter() {
        counter++;
        if ( counter === len ) {
            $('#content').removeClass('hidden');
            console.log( 'All images loaded!' );
        }
    }

</script>

@endsection
