@extends('layouts.dashboard')


@section('head')

<link rel="stylesheet" href="{{ asset('/css/nmplayer.css?v=' . asset_tag())}}">

@endsection

@section('content')
<style>
    .season-list {
        height: 100%;
    }
</style>


<div class="w-screen h-16 xl:h-16"></div>
<div class="flex flex-col w-full h-screen overflow-hidden bg-gray-800">
    <div class="w-full px-8 pt-4 pb-4" style="flex-basis: 5%;">
        <button data-button="back" id="back" class="float-left w-8 h-8 p-5 mx-2 text-white back"></button>
        <button data-button="seasons" id="seasons" class="float-right w-6 h-6 p-5 mx-2 text-white seasons"></button>
    </div>

    <div class="flex flex-col justify-center w-full h-full px-20" style="flex-basis: auto;">
        <img id="image" src="" class="self-center " style="min-width:50%; max-width:500px">

    </div>
    <div id="optionsMenu" class="absolute right-0 flex-col w-full px-4 bg-gray-800 bg-opacity-75">
        <div class="flex-row w-full px-2 py-2 bg-gray-800" style="height:60px;">
            <button data-button="close" class="float-right w-10 h-10 close"></button>
        </div>
        <div class="flex flex-row w-full h-full mt-6">
            <div id="subtitleMenu" class="flex-col float-left w-6/12 text-3xl font-bold text-center lg:px-5 lg:mx-10">
                Subtitles
                <div id="subtitles" class="flex flex-col w-full mt-4 overflow-x-hidden overflow-y-auto text-base xl:px-4 lg:mt-6 lext-left">
                </div>
            </div>
            <div id="audioMenu" class="flex-col float-left w-6/12 text-3xl font-bold text-center lg:px-5 lg:mx-10">
                Audio
                <div id="audios" class="flex flex-col w-full mt-4 overflow-x-hidden overflow-y-auto text-base xl:px-4 lg:mt-10 lext-left">
                </div>
            </div>
        </div>
    </div>

    <div id="seasonsMenu" class="absolute right-0 flex-col w-full h-full px-2 bg-gray-800 sm:h-screen" style="width: 100%; right: 0px; height: calc(100vh - 4rem);">
        <div class="flex-row w-full px-2 py-2 bg-gray-800" style="height:60px;">
            <button data-button="close" class="float-right w-10 h-10 close"></button>
            <button data-button="season-back" class="float-left w-2/12 h-4 left-arrow" style="height: 35px; display: flex;background-position: center;width: 35px;"></button>
            <div id="season-menu-name" class="float-right w-9/12 h-12 py-2 text-2xl text-center text-bold"></div>
        </div>
        <div id="season-forward-container" class="flex-col hidden w-full h-full px-2 overflow-y-auto season-forward-container"></div>
        <div id="seasons-container" class="w-full overflow-hidden seasons-container" style=";height: 95%;"></div>

    </div>

    <div id="qualityMenu" class="absolute flex-col hidden bg-gray-800 bg-opacity-75">
        <button data-button="close" class="float-right w-10 h-10 close"></button>
        <div id="qualities" class="flex-row w-full px-2 py-2"></div>
    </div>

    <div class="bottom-0 w-full px-4 py-4 mb-64 bg-gray-600" style="flex-basis: 5%;">
        <div class="w-32 h-10 custom-number-input">
            <div class="relative flex flex-row w-full h-10 mt-1 bg-transparent rounded-lg">
                <button data-action="decrement" class="w-20 h-full text-gray-100 bg-purple-600 rounded-l outline-none cursor-pointer hover:text-gray-700 hover:bg-gray-400">
                    <span class="m-auto text-2xl font-thin">−</span>
                </button>
                <input type="number" id="subtitleOffset" class="flex items-center w-full font-semibold text-center text-gray-700 bg-gray-300 outline-none focus:outline-none text-md hover:text-black focus:text-black md:text-basecursor-default" name="custom-input-number" value="0"></input>
                <button data-action="increment" class="w-20 h-full text-gray-100 bg-purple-600 rounded-r cursor-pointer hover:text-gray-700 hover:bg-gray-400">
                    <span class="m-auto text-2xl font-thin">+</span>
                </button>
            </div>
        </div>
        <div class="absolute bottom-0 left-0 w-full px-4 py-4 bg-gray-600 h-42" style="flex-basis: 25%;height: 200px !important;flex-shrink: 0;bottom: 0;position: absolute;">

            <div class="w-full h-12">
                <div id="title" class="text-white"></div>
            </div>

            <input type="range" value="0" min="0" max="100" name="discount_credits" id="remote-slider" />

            <div class="flex flex-row w-full h-8">
                <div id="current-time" class="w-24 h-8 pb-5 mr-4 text-left text-white time current-time">00:00</div>
                <div id="remaining-time" class="w-24 h-8 pb-5 ml-auto text-right text-white time remaining-time">00:00</div>

            </div>
            <div class="flex flex-row justify-center w-full h-12">
                <button data-button="previous" id="previous" class="w-8 h-8 p-5 mx-2 text-white previous"></button>
                <button id="btn-playback" data-button="playback" class="w-8 h-8 p-5 mx-2 text-white pause" type="button"></button>
                <button data-button="next" id="next" class="w-8 h-8 p-5 mx-2 text-white next"></button>
            </div>

        </div>


    </div>

    <script src="{{ env('APP_URL') }}:2096/socket.io/socket.io.js"></script>

    <script src="/js/nmremote.js?v={{asset_tag()}}" deffer=""></script>
    <script>
        if (typeof io === 'undefined') {
        console.warn('Socket connection error.');
    }
    else {
        var nmremote = new nmremote(location.origin + ':2096', '{{ Auth::user()->id }}', 'remote');
    }

    </script>

    <script>
        let i = 0, timeOut = 0, timeOut2 = 0;

    $('[data-action="increment"]').on('mousedown touchstart', function(e) {
        let current = $(this).prev().val();

        timeOut = setInterval(function(e){
            current ++;
            console.log(current);
            $('#subtitleOffset').val(current);
        }, 100);

    }).bind('mouseup mouseleave touchend', function() {
        clearInterval(timeOut);
        nmremote.subtitleOffset($('#subtitleOffset').val())
    });

    $('[data-action="decrement"]').on('mousedown touchstart', function(e) {
        let current = $(this).next().val();
        console.log(current);

        timeOut2 = setInterval(function(){
            current--;
            console.log(current);
            $('#subtitleOffset').val(current);
        }, 100);

    }).bind('mouseup mouseleave touchend', function(e) {
        clearInterval(timeOut2);
        nmremote.subtitleOffset($('#subtitleOffset').val())
    });

    </script>

    @endsection
