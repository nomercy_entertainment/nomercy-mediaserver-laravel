@extends('layouts.dashboard')

@section('content')



<div class="flex flex-col content-center justify-center p-5 mx-auto bg-gray-700 h-min lg:w-6/12 md:w-full">
    <div class="flex flex-col justify-center w-full p-5 text-center bg-gray-900">
        <div class="flex flex-col justify-center w-full p-5 text-center bg-gray-900">
            <textarea id="message" class="w-full p-4 mx-auto my-4 text-left whitespace-pre-wrap bg-gray-800 border-0 form-control min-h-48" rows="10" cols="70"></textarea>

            <div class="flex flex-col justify-center w-full p-5 text-center bg-gray-900">
                <form class="w-full d-flex align-items-center h-100" autocomplete="off">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <div id="error" class="hidden w-full p-4 mx-auto my-4 text-center bg-gray-800 border-0 "></div>
                    <input type="password" class="w-full p-4 mx-auto my-4 text-center bg-gray-800 border-0 form-control" id="salt" placeholder="{{ __('auth.password') }}" />
                </form>
                <button id="submit" class="content-center justify-center w-7/12 py-3 mx-auto font-bold text-black bg-gray-500 submit">Submit</button>
            </div>
        </div>
    </div>
</div>
<script>
    function submit() {
        $('#error').html('');
        $('#error').css('display', 'none');

        $('#message').css('display', 'block');
        $.ajax({
            type: 'POST',
            url: '{{ route("crypto.decrypt_key") }}',
            data: {
                "_token": $('#token').val(),
                'key': key,
                'salt': $('#salt').val()
            },
            success: function (data) {
                $('#error').html(data.data).css('display', 'none');
                $('#message').html(data.data);
            },
            error: function (data) {
                $('#error').html(data.responseJSON.message);
                $('#error').css('display', 'block');
            }
        });
    }

    $(document).ready(function () {
        $(window).keydown(function (event) {
            if (event.keyCode == 13) {
                submit();
            }
        });
    });

    const urlQuery = new URLSearchParams(window.location.search);
    let key = urlQuery.get('key');
    $('#key').html(key);

    var typingTimer;


    $('#submit').click(function (e) {
        if ($('#salt').val() == '') {
            $('#error').css('display', 'block');
            $('#error').html('You need to fill in the password for this file');
        } else {
            submit();
        }
    });

</script>

@endsection
