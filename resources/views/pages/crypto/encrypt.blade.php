@extends('layouts.dashboard')

@section('content')
<script>
    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        $temp.remove();
        alert('copied to clipboard');
    }

</script>

<div class="flex flex-col content-center justify-center p-5 mx-auto bg-gray-700 h-min lg:w-6/12 md:w-full">
    <div class="flex flex-col justify-center w-full p-5 text-center bg-gray-900">
        <div class="flex flex-col justify-center w-full p-5 text-center bg-gray-900">
            <textarea id="message" class="w-full p-4 mx-auto my-4 text-left whitespace-pre-wrap bg-gray-800 border-0 form-control min-h-48" rows="10" cols="70"></textarea>

            <div id="url" type="text" class="hidden w-full p-5 mx-auto mb-5 text-center bg-gray-800 border" onclick="copyToClipboard('#url')"></div>
            <form id="form" class="w-full d-flex align-items-center h-100" autocomplete="off">
                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}" autocomplete="new-password">
                <input id="salt" type="password" class="w-full p-5 mx-auto mb-5 text-center bg-gray-800 border" placeholder="{{ __('auth.password') }}" autocomplete="new-password" />
                <div id="error" class="hidden w-full p-5 mx-auto mb-5 text-center bg-gray-800 border"></div>
            </form>
            <button id="submit" class="content-center justify-center w-7/12 py-3 mx-auto font-bold text-black bg-gray-500 submit">Submit</button>
        </div>
    </div>
</div>

<script>
    function submit() {
        $('#error').html('');
        $('#error').css('display', 'none');
        $.ajax({
            type: 'POST',
            url: '{{ route("crypto.encrypt_key") }}',
            data: {
                "_token": $('#token').val(),
                'message': $('#message').val(),
                'salt': $('#salt').val()
            },
            success: function (data) {
                $('#message').html(data.message);
                $('#message').css('display', 'block');
                $('#url').html(data.url);
                $('#url').css('display', 'block');
            },
            error: function (data) {
                $('#error').html(data.responseJSON.message);
                $('#error').css('display', 'block');
            }
        });
    }

    $('#submit').click(function (e) {
        if ($('#salt').val() == '') {
            $('#error').css('display', 'block');
            $('#error').html('You need to fill in the password for this message');
        } else if ($('#message').val() == '') {
            $('#error').css('display', 'block');
            $('#error').html('You need to fill in a message');
        } else {
            submit();
        }
    });

</script>

@endsection
