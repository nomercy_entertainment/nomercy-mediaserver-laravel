@extends('layouts.dashboard')

@section('head')

<link rel="stylesheet" href="{{ asset('/css/nmplayer.jwplayer.css?v=' . asset_tag())}}">

<img src="" class="preload-images" alt=""></img>

@if($JWPLAYER_KEY != '')

    <script src="https://ssl.p.jwpcdn.com/player/v/{{ env('JWPLAYER_VERSION') }}/jwplayer.js"></script>
    <script src="https://ssl.p.jwpcdn.com/player/v/{{ env('JWPLAYER_VERSION') }}/jwplayer.core.js"></script>
    <script src="https://ssl.p.jwpcdn.com/player/v/{{ env('JWPLAYER_VERSION') }}/jwplayer.core.controls.js"></script>
    <script src="https://ssl.p.jwpcdn.com/player/v/{{ env('JWPLAYER_VERSION') }}/jwplayer.core.controls.polyfills.js"></script>
    <script src="https://ssl.p.jwpcdn.com/player/v/{{ env('JWPLAYER_VERSION') }}/jwplayer.compatibility.js"></script>
    <script src="https://ssl.p.jwpcdn.com/player/v/{{ env('JWPLAYER_VERSION') }}/polyfills.intersection-observer.js"></script>
    <script src="https://ssl.p.jwpcdn.com/player/v/{{ env('JWPLAYER_VERSION') }}/polyfills.webvtt.js"></script>
    <script src="https://ssl.p.jwpcdn.com/player/v/{{ env('JWPLAYER_VERSION') }}/related.js"></script>
    <script src="https://ssl.p.jwpcdn.com/player/v/{{ env('JWPLAYER_VERSION') }}/vast.js"></script>
    <script src="https://ssl.p.jwpcdn.com/player/v/{{ env('JWPLAYER_VERSION') }}/vttparser.js"></script>
    <script src="https://ssl.p.jwpcdn.com/player/v/{{ env('JWPLAYER_VERSION') }}/provider.cast.js"></script>

@else

    <script src="{{ asset('/js/jwplayer.open/jwplayer.js?v=' . asset_tag())}}"></script>
    <script src="{{ asset('/js/jwplayer.open/hls.min.js?v=' . asset_tag())}}"></script>
    <script src="{{ asset('/js/jwplayer.open/jwplayer.hlsjs.js?v=' . asset_tag())}}"></script>

@endif

<script src="{{ asset('/js/dist/subtitles-octopus.js?v=' . asset_tag())}}"></script>
<script src="{{ asset('/js/jwplayer/jwplayer.defaults.js?v=' . asset_tag())}}"></script>

<script src="{{ asset('/js/jwplayer/nmplayer.jwplayer.js?v='. asset_tag())}}"></script>
<script src="{{ asset('/js/jwplayer/nmsync.jwplayer.js?v='. asset_tag())}}"></script>
<script src="{{ asset('/js/jwplayer/nmremote.jwplayer.js?v=' . asset_tag())}}"></script>


@endsection

@section('content')

<div id="spinner-main" class="spinner">
    <span style="position: absolute;top: 89%;width: max-content;transform: translateX(-35%);font-size: 20px;">Loading playlist</span>
</div>

<div id="{{$id}}" class="jwplayer" tabindex="0"></div>

<script>
    document.write(`<script type="text/javascript" src="{{ asset('/js/jquery-3.5.1.min.js') }}"><\/script>`);

    const supportsBackgroundLoading = jwplayer("{{$id}}").getEnvironment().Features.backgroundLoading && !(/PlayStation|Chrome\/49/).test(navigator.userAgent);
    var player = jwplayer("{{$id}}");
    var nmplayer = nmplayer("{{$id}}");
    nmplayer.setup({
        jwplayerInstance: player,
        jwplayerSetup: {
            playlist: 'https://server.nomercy.tv/api/tv/{{$id}}/watch',
            // controls: true,
            key: "{{ $JWPLAYER_KEY }}",
            backgroundLoading: supportsBackgroundLoading,

        },
    });

    jwplayer().on('ready', function(e){
        $('#spinner-main').remove();
    });

    var nmsync = nmsync({
        storage: 'server',
        bearer: '{{ csrf_token() }}',
    });

</script>

<script src="{{ asset('/js/jwplayer/nmremote.jwplayer.js?v=' . asset_tag()) }}"></script>
<script>
    var nmremote = nmremote();
</script>


@endsection
