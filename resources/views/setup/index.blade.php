@extends('layouts.auth')

@section('content')

<script src="{{ asset('/js/setup.js') }}"></script>
<script>
    back = '/';
</script>
<style>

</style>
<div class="flex flex-wrap justify-center h-min">
    <div class="w-10/12 my-16 h-10/12">
        <div class="px-6 py-3 mb-0 text-3xl font-semibold text-center text-gray-100 bg-gray-900">
            {{ __('route.setup') }}
        </div>
        @if(Session::has('error'))
        <div class="px-4 py-3 text-red-700 bg-red-100 border-t border-b border-red-500" role="alert">
            <button type="button" class="float-right close" data-dismiss="alert">&times;</button>
            <p class="font-bold">Error</p>
            <p class="text-sm">{{ Session::get('error') }}</p>
        </div>
        @endif
        <div class="flex flex-col h-full break-words bg-gray-800 rounded shadow-md">

            <div id="todo" class="flex flex-col w-9/12 h-full mx-auto bg-gray-800 ">
                @if( env('APP_URL') == "")
                <div class="px-4 py-3 text-red-700 bg-red-100 border-t border-b border-red-500" role="alert">
                    <p class="font-bold">Todo</p>
                    <p class="text-sm">{{ __('You need to setup base.') }}</p>
                </div>
                @else
                <div class="px-4 py-3 text-green-700 bg-green-100 border-t border-b border-green-500" role="alert">
                    <p class="font-bold">Done</p>
                    <p class="text-sm">Base setup.</p>
                </div>
                @endif

                @if($FolderRoots->count() == 0)
                <div class="px-4 py-3 text-red-700 bg-red-100 border-t border-b border-red-500" role="alert">
                    <p class="font-bold">Todo</p>
                    <p class="text-sm">{{ __('You need to add folder roots.') }}</p>
                </div>
                @else
                <div class="px-4 py-3 text-green-700 bg-green-100 border-t border-b border-green-500" role="alert">
                    <p class="font-bold">Done</p>
                    <p class="text-sm">Folder roots: {{ $FolderRoots->count() }}</p>
                </div>
                @endif

                @if($SupportedLanguages->count() == 0)
                <div class="px-4 py-3 text-red-700 bg-red-100 border-t border-b border-red-500" role="alert">
                    <p class="font-bold">Todo</p>
                    <p class="text-sm">{{ __('You need to add allowed languages.') }}</p>
                </div>
                @else
                <div class="px-4 py-3 text-green-700 bg-green-100 border-t border-b border-green-500" role="alert">
                    <p class="font-bold">Done</p>
                    <p class="text-sm">Allowed languages: {{ $SupportedLanguages->count() }}</p>
                </div>
                @endif

                <div class="flex flex-wrap items-center my-8">
                    <button type="button" data-self="todo" class="px-4 py-1 mx-auto font-medium bg-purple-600 rounded" onclick="next();">
                        {{ __('continue') }}
                    </button>
                </div>

            </div>
            @if(env('APP_URL') == "")
                <form id="base" class="hidden float-left w-9/12 p-6 mx-auto mb-8 active" method="POST" action="{{ route('setup.base') }}">
                    @csrf

                    @include('components.forms.input', [
                        'id' => 'APP_URL',
                        'name' => 'APP_URL',
                        'value' => URL::to('/'),
                        'required' => true,
                        'icon' => 'globe',
                        'type' => 'text',
                        'label' => __("setup.APP_URL"),
                    ])

                    @include('components.forms.input', [
                        'id' => 'WORKER_NAME',
                        'name' => 'WORKER_NAME',
                        'value' => 'Server',
                        'required' => true,
                        'icon' => 'name',
                        'type' => 'text',
                        'label' => __("setup.WORKER_NAME"),
                    ])

                    @include('components.forms.checkbox', [
                        'id' => 'APP_PRIVACY',
                        'name' => 'APP_PRIVACY',
                        'required' => true,
                        'value' => 'true',
                        'label' => __("setup.APP_PRIVACY"),
                    ])
                    @include('components.forms.input', [
                        'id' => 'FEATURED',
                        'name' => 'FEATURED',
                        'value' => 'random',
                        'required' => true,
                        'icon' => 'media',
                        'type' => 'text',
                        'label' => __("setup.FEATURED"),
                    ])










                    @include('components.forms.input', [
                        'id' => 'TVDB_APIKEY',
                        'name' => 'TVDB_APIKEY',
                        'value' => '',
                        'required' => false,
                        'icon' => 'key',
                        'type' => 'password',
                        'label' => __("setup.TVDB_APIKEY"),
                    ])
                    @include('components.forms.input', [
                        'id' => 'IMDB_TOKEN',
                        'name' => 'IMDB_TOKEN',
                        'value' => '',
                        'required' => false,
                        'icon' => 'key',
                        'type' => 'password',
                        'label' => __("setup.IMDB_TOKEN"),
                    ])
                    @include('components.forms.input', [
                        'id' => 'OMDB_APIKEY',
                        'name' => 'OMDB_APIKEY',
                        'value' => '',
                        'required' => false,
                        'icon' => 'key',
                        'type' => 'password',
                        'label' => __("setup.OMDB_APIKEY"),
                    ])
                    @include('components.forms.input', [
                        'id' => 'PEXELS_API_KEY',
                        'name' => 'PEXELS_API_KEY',
                        'value' => '',
                        'required' => false,
                        'icon' => 'key',
                        'type' => 'password',
                        'label' => __("setup.PEXELS_API_KEY"),
                    ])
                    @include('components.forms.input', [
                        'id' => 'JWPLAYER_KEY',
                        'name' => 'JWPLAYER_KEY',
                        'value' => '',
                        'required' => false,
                        'icon' => 'key',
                        'type' => 'password',
                        'label' => __("setup.JWPLAYER_KEY"),
                    ])
                    @include('components.forms.input', [
                        'id' => 'JWPLAYER_VERSION',
                        'name' => 'JWPLAYER_VERSION',
                        'value' => '8.17.7',
                        'required' => false,
                        'icon' => 'hashtag',
                        'type' => 'text',
                        'label' => __("setup.JWPLAYER_VERSION"),
                    ])








                    @include('components.forms.input', [
                        'id' => 'OPENSUBTITLES_USERNAME',
                        'name' => 'OPENSUBTITLES_USERNAME',
                        'value' => '',
                        'required' => false,
                        'icon' => 'user',
                        'type' => 'text',
                        'label' => __("setup.OPENSUBTITLES_USERNAME"),
                    ])
                    @include('components.forms.input', [
                        'id' => 'OPENSUBTITLES_PASSWORD',
                        'name' => 'OPENSUBTITLES_PASSWORD',
                        'value' => '',
                        'required' => false,
                        'icon' => 'key',
                        'type' => 'password',
                        'label' => __("setup.OPENSUBTITLES_PASSWORD"),
                    ])
                    @include('components.forms.input', [
                        'id' => 'OPENSUBTITLES_DEFAULT_LANGUAGE',
                        'name' => 'OPENSUBTITLES_DEFAULT_LANGUAGE',
                        'value' => '',
                        'required' => false,
                        'icon' => 'language',
                        'type' => 'text',
                        'label' => __("setup.OPENSUBTITLES_DEFAULT_LANGUAGE"),
                    ])
                    @include('components.forms.input', [
                        'id' => 'OPENSUBTITLES_LANGUAGE',
                        'name' => 'OPENSUBTITLES_LANGUAGE',
                        'value' => '',
                        'required' => false,
                        'icon' => 'key',
                        'type' => 'language',
                        'label' => __("setup.OPENSUBTITLES_LANGUAGE"),
                    ])
                    @include('components.forms.input', [
                        'id' => 'OPENSUBTITLES_USERAGENT',
                        'name' => 'OPENSUBTITLES_USERAGENT',
                        'value' => '',
                        'required' => false,
                        'icon' => 'chrome',
                        'type' => 'text',
                        'label' => __("setup.OPENSUBTITLES_USERAGENT"),
                    ])





                    <div class="flex flex-wrap items-center">
                        <button type="submit" class="px-4 py-1 mx-auto font-medium bg-purple-600 rounded">
                            {{ __('continue') }}
                        </button>

                    </div>


                </form>
            @endif

            @if($FolderRoots->count() == 0)
                <form id="folders" class="hidden float-left w-9/12 p-6 mx-auto mb-8 active" method="POST" action="{{ route('setup.addfolders') }}">
                    @csrf

                    @include('components.forms.folder', [
                        'id' => 1,
                        'name' => 1,
                        'value' => [
                            "name" => "Anime in",
                            "folder" => "/mnt/media/Anime/download",
                        ],
                        'required' => true,
                        'label' => 'Input folder 1',
                    ])

                    @include('components.forms.folder', [
                        'id' => 2,
                        'name' => 2,
                        'value' => [
                            'direction' => "out",
                            "name" => "Anime out",
                            "folder" => "/mnt/media/Anime/Anime",
                        ],
                        'required' => true,
                        'label' => 'outpur folder 1',
                    ])

                    @include('components.forms.folder', [
                        'id' => 3,
                        'name' => 3,
                        'value' => [
                            "name" => "Movies in",
                            "folder" => "/mnt/media/Films/Download",
                        ],
                        'required' => false,
                        'label' => 'input folder 2',
                    ])

                    @include('components.forms.folder', [
                        'id' => 4,
                        'name' => 4,
                        'value' => [
                            'direction' => "out",
                            "name" => "Movies out",
                            "folder" => "/mnt/media/Films/Films",
                        ],
                        'required' => false,
                        'label' => 'output folder 2',
                    ])

                    @include('components.forms.folder', [
                        'id' => 5,
                        'name' => 5,
                        'value' => [
                            "hq" => true,
                            "name" => "Marvels in",
                            "folder" => "/mnt/media/Marvels/Download",
                        ],
                        'required' => false,
                        'label' => 'input folder 3',
                    ])

                    @include('components.forms.folder', [
                        'id' => 6,
                        'name' => 6,
                        'value' => [
                            "name" => "Tv in",
                            "folder" => "/mnt/media/TV.Shows/Download",
                        ],
                        'required' => false,
                        'label' => 'input folder 4',
                    ])

                    @include('components.forms.folder', [
                        'id' => 7,
                        'name' => 7,
                        'value' => [
                            'direction' => "out",
                            "name" => "Tv out",
                            "folder" => "/mnt/media/TV.Shows/TV.Shows",
                        ],
                        'required' => false,
                        'label' => 'output folder 4',
                    ])

                    <div class="flex flex-wrap items-center">
                        <button type="submit" class="px-4 py-1 mx-auto font-medium bg-purple-600 rounded">
                            {{ __('continue') }}
                        </button>

                    </div>
                </form>
            @endif

            @if($SupportedLanguages->count() == 0)
                <form id="languages" class="hidden float-left w-9/12 p-6 mx-auto mb-8 active" method="POST" action="{{ route('setup.addlanguages') }}">
                    @csrf

                    @include('components.forms.languages', [
                        'id' => 1,
                        'name' => 1,
                        'required' => true,
                        'label' => 'Allowed languages',
                        'data' => $languages,
                    ])

                    <div class="flex flex-wrap items-center">
                        <button type="submit" class="px-4 py-1 mx-auto font-medium bg-purple-600 rounded">
                            {{ __('continue') }}
                        </button>

                    </div>


                </form>
            @endif


        </div>
    </div>
</div>

<script>
    function next() {
        event.preventDefault();

        $('.active').css('display', 'none').removeClass('active');

        if (event.target.dataset.self == 'todo') {
            $('#todo').css('display', 'none');
            if("{{ env('APP_URL') == "" }}"){
                $('#base').css('display', 'block');
            }
            else if('{{ $FolderRoots->count() == 0}}'){
                $('#folders').css('display', 'block');
            }
            else if('{{ $SupportedLanguages->count() == 0 }}'){
                $('#languages').css('display', 'block');
            }
        }
    }

</script>

@endsection
