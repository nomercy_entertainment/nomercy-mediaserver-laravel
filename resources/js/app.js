
$(document).ready(function () {

  let toggleNavStatus = false;
  let toggleNav = function () {
    let getMenu = document.querySelector('#profile-menu');

    if (toggleNavStatus === false) {
      getMenu.classList.add("show");
      toggleNavStatus = true;
    }
    else {
      getMenu.classList.remove("show");
      toggleNavStatus = false;
    }
  };

  $('#profile-button').click(toggleNav);

  $(".poster.redirect").each(function (e) {
    $(this).click(function (e) {
      console.log(e.target.dataset);
      if (e.target.dataset.type == "movie" || e.target.dataset.type == "tv" || e.target.dataset.type == "genre" || e.target.dataset.type == "all") {
        window.location.href = (window.subroute || '') + '/' +  e.target.dataset.type + "/" + e.target.id + '/info';
      }
      else if (e.target.dataset.type == "collection") {
        window.location.href = (window.subroute || '') + "/collection?id=" + e.target.id;
      }
      else if (e.target.dataset.type == "special") {
        window.location.href = (window.subroute || '') + "/watch?type=special&id=" + e.target.id;
      }
      else if (e.target.dataset.type == "watch") {
        window.location.href = (window.subroute || '') + "/watch?id=" + e.target.id;
      }
    });
  });


  var percentColorsInverse = [
    {
      pct: 0,
      color: {
        r: 0x00,
        g: 0xff,
        b: 0
      }
    },
    {
      pct: 50,
      color: {
        r: 0xff,
        g: 0xff,
        b: 0
      }
    },
    {
      pct: 100,
      color: {
        r: 0xff,
        g: 0x00,
        b: 0
      }
    }
  ];

  var percentColors = [
    {
      pct: 10,
      color: {
        r: 0xcc,
        g: 0,
        b: 0
      }
    },
    {
      pct: 50,
      color: {
        r: 0xee,
        g: 0xee,
        b: 0
      }
    },
    {
      pct: 99,
      color: {
        r: 0,
        g: 0x90,
        b: 0
      }
    },
    {
      pct: 100,
      color: {
        r: 0,
        g: 0x40,
        b: 0
      }
    }
  ];

  var getColorForPercentage = function (pct, scheme) {
    for (var i = 1; i < scheme.length - 1; i++) {
      if (pct < scheme[i].pct) {
        break;
      }
    }
    var lower = scheme[i - 1];
    var upper = scheme[i];
    var range = upper.pct - lower.pct;
    var rangePct = (pct - lower.pct) / range;
    var pctLower = 1 - rangePct;
    var pctUpper = rangePct;
    var color = {
      r: Math.floor(lower.color.r * pctLower + upper.color.r * pctUpper),
      g: Math.floor(lower.color.g * pctLower + upper.color.g * pctUpper),
      b: Math.floor(lower.color.b * pctLower + upper.color.b * pctUpper)
    };
    return "rgb(" + [color.r, color.g, color.b].join(",") + ")";
    // or output as hex if preferred
  };

  $("div[data-value]").each(function () {
    $(this).css({
      "background-color": getColorForPercentage($(this).data("percent"), percentColors),
    });
    if ($(this).data("percent") > 45 && $(this).data("percent") < 75) {
      $(this).prev().css({
        "color": getColorForPercentage($(this).data("percent"), percentColors),
      });
    }
    $(this).change(function () {
      $(this).css({
        "background-color": getColorForPercentage($(this).data("percent"), percentColors),
      });
      if ($(this).data("percent") > 45 && $(this).data("percent") < 75) {
        $(this).prev().css({
          "color": getColorForPercentage($(this).data("percent"), percentColors),
        });
      }
    });
  });

  $("div[data-percent]").each(function () {
    $(this).css({
      "background-color": getColorForPercentage($(this).data("percent"), percentColors),
    });
    if ($(this).data("percent") > 45 && $(this).data("percent") < 75) {
      $(this).prev().css({
        "color": getColorForPercentage($(this).data("percent"), percentColors),
      });
    }
    $(this).width($(this).data("percent") + "%");
  });

  if (window.innerWidth < 900) {
    $('#sidebar').removeClass('w-1/12');
    $('#main').removeClass('w-11/12');
    $('#main').addClass('w-full');
  }

  if (window.location.href.split("#")[1]) {
    let div = $(`#${window.location.href.split("#")[1].toUpperCase()}`);
    console.log(div);

    scroll_to_div(div);
  }


});


function scroll_to_div(target) {
  $(target).get(0).scrollIntoView({ block: 'center' });
}


if ($(".nm-card").length > 0 && window.location.pathname != "/") {
  $("#content").append("<div id='index' class='color-right'></div>");

  let previous = "";
  $("div[data-name]").each(function (e) {
    let current = String($(this).data("name"))
      .substr(0, 1)
      .toUpperCase();
    if (current != previous) {
      $(this)
        .parent()
        .attr("id", "scroll_" + current);
      previous = current;
      if (current != null) {
        $("#index").append(
          "<li><a class='scroller' href='#scroll_" +
          current +
          "'>" +
          current +
          "</a><li>"
        );
      }
    }
  });

  $("#index a").click(function (event) {
    event.preventDefault();
    scroll_to_div($(this).attr("href"));
  });
}
if (window.location.hash) {
  scroll_to_div(window.location.hash);
  window.history.pushState(
    "page2",
    "Title",
    window.location.href.split("#")[0]
  );
}

if (document.getElementById("menu")) {
  let i = document.getElementById("menu").style;
  if (document.addEventListener) {
    document.addEventListener(
      "contextmenu",
      function (e) {
        let posX = e.clientX;
        let posY = e.clientY;
        if (isNaN(e.target.id) == false && e.target.id !== '') {
          e.preventDefault();
          $("#menuSpace").empty().append(`
              <a class="ytp-menuitem" target="_blank" role="menuitem" href="/dashboard/content/missing/${e.target.id}">
                  <div class="ytp-menuitem-icon"></div>
                  <div class="ytp-menuitem-label">Get missing episodes.</div>
                  <div class="ytp-menuitem-content"></div>
              </a>
              <a class="ytp-menuitem" target="_blank" role="menuitem" href="/dashboard/content/lowquality/${e.target.id}?q=1080">
                  <div class="ytp-menuitem-icon"></div>
                  <div class="ytp-menuitem-label">Get lower than 1080p episodes.</div>
                  <div class="ytp-menuitem-content"></div>
              </a>
              <a class="ytp-menuitem" target="_blank" role="menuitem" href="/dashboard/content/lowquality/${e.target.id}?q=720">
                  <div class="ytp-menuitem-icon"></div>
                  <div class="ytp-menuitem-label">Get lower than 720p episodes.</div>
                  <div class="ytp-menuitem-content"></div>
              </a>
              <hr>
              <div class="ytp-menuitem" role="menuitemcheckbox" aria-checked="false" tabindex="0">
                  <div class="ytp-menuitem-icon"></div>
                  <div class="ytp-menuitem-label" style="font-size: 100%;font-weight: 100;padding: 13px 0 0 75px;">NoMercy Entertainment</div>
                  <div class="ytp-menuitem-content">
                      <div class="ytp-menuitem-toggle-checkbox"></div>
                  </div>
              </div>
            `);

          if (posX + $("#menu").width() > window.innerWidth) {
            posX = e.clientX - $("#menu").width();
          }
          if (
            posY +
            $(".ytp-menuitem").length *
            $(".ytp-menuitem").height() >
            window.innerHeight
          ) {
            posY =
              e.clientY -
              $(".ytp-menuitem").length *
              $(".ytp-menuitem").height();
          }
          menu(posX, posY);
        } else {
          $("#menuSpace").empty().append(`
                <div class="ytp-menuitem" role="menuitemcheckbox" aria-checked="false" tabindex="0">
                    <div class="ytp-menuitem-icon"></div>
                    <div class="ytp-menuitem-label" style="font-size: 100%;font-weight: 100;padding: 13px 0 0 75px;">NoMercy Entertainment</div>
                    <div class="ytp-menuitem-content">
                        <div class="ytp-menuitem-toggle-checkbox"></div>
                    </div>
                </div>
            `);
          i.visibility = "hidden";
        }
      },
      false
    );

    document.addEventListener(
      "click",
      function (e) {
        i.opacity = "0";
        setTimeout(function () {
          i.visibility = "hidden";
        }, 501);
      },
      false
    );
  }

}
function menu(x, y) {
  i.top = y + "px";
  i.left = x + "px";
  i.visibility = "visible";
  i.opacity = "1";
}
