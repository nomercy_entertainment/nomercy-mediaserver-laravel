<?php

return [

    'mustLogin'              => 'You need to login to access this page.',
    'noAccess'               => 'You do not have permission to access this page access.',

    '404'                   => 'Sorry this page doesn\'t exist.',
    '401'                   => 'Sorry, you are forbidden from accessing this page.',
    '419'                   => 'Sorry, your session has expired. Please refresh and try again.',
    '429'                   => 'Sorry, you are making too many requests to our servers.',
    '500'                   => 'Whoops, something went wrong on our servers.',
    '503'                   => 'Sorry, we are doing some maintenance. Please check back soon.',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',

];
