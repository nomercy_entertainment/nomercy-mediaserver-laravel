<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'registered' => 'User successfully registered',
    'loggedOut' => 'User successfully signed out',

    'name'                  => 'Name',
    'email'                 => 'E-mail address',
    'password'              => 'Password',
    'confirm'               => 'Confirm password',
    'register'              => 'Register',
    'login'                 => 'Login',
    'logout'                => 'Logout',
    'forgot'                => 'Forgot your password?',
    'remember'              => 'Stay signed in',
    'new'                   => 'New here?',
    'exist'                 => 'Already registered?',
    'privacy'               => 'Privacy policy',
    'terms'                 => 'I accept Terms of Service',
    'captcha'               => 'You must complete the recapcha',
    'resetLink'             => 'Reset',
    'reset'                 => 'Reset password',
    'change'                => 'Change Password',
    'currentpass'           => 'Current Password',
    'newpass'               => 'New Password',
    'repeatnewpass'         => 'Repeat New Password',
    'verifiedat'            => 'Verified At',
    'users'                 => 'Users',
    'roles'                 => 'Roles',
    'permissions'           => 'Permissions',
    'newpermission'         => 'New Permission',
    'permissionname'        => 'Permission name',
    'editpermission'        => 'Edit Permission',
    'newrole'               => 'New Roll',
    'rolename'              => 'Roll Name',
    'editrole'              => 'Edit Roll',
    'showrole'              => 'Show Roll',
    'showuser'              => 'Edit User',
    'edituser'              => 'Edit User',
    'createuser'            => 'Add User',
    'last_request_at'       => 'Last visit',
    'last_login_at'         => 'Last login',
    'last_login_ip'         => 'Last login IP',
    'valid'                 => 'You need a valid email adres',
    'back'                  => 'Back to',
    'continue'              => 'Continue',
    'deleted'               => 'Deleted',
    'never'                 => 'Never',
    'unverified'            => 'Not',
    ''                          => '',
    'appid'                 => 'App ID',
    'location'              => 'Location',
    ''               => '',
    ''               => '',
    ''               => '',



];
