<?php

return [
    'langcode'      => 'en-EN',
    'lang'          => 'EN',
    'language'      => 'English',
    'description'   => 'The effortless encoder.',
    'welcome'       => 'Welcome at NoMercy TV',
    'home'          => 'Home',
    'guest'         => 'Guest',
    'noitemsfound'  => 'No items Found!',
    'options'       => 'Options',
    'management'    => 'Management',
    'collapse'      => 'Collapse',
    'help'          => 'Help',
    'search'        => 'Search',
    'update'        => 'Update',
    'delete'        => 'Delete',
    'show'          => 'Show',
    'edit'          => 'Edit',
    'more'          => 'More',
    'less'          => 'Less',
    'timeremain'    => 'Time Remaining',
    'itemsremain'   => 'Items Remaining',
    'title'         => 'Title',
    'priority'      => 'Priority',
    'id'            => 'ID',
    'action'        => 'Action',
    'no'            => 'No',
    'back'          => 'Go back',
    'watch'         => 'Play',
    'itemnotexist'  => 'Sorry this item doesn\'t exist.',
    'none'          => 'None',
    'scroll-true'   => 'Enable scroll',
    'scroll-false'  => 'Disable scroll',
    'clear.log'     => 'Clear log',
    'queue'         => 'Queue',
    'info'          => 'More info',
    'chat'          => 'Chat',
    'send'          => 'Send',
    'typemessage'   => 'Type your message...',
    ''              => '',
    'director'      => 'Director|Directors',
    'lead'          => 'Lead character|Lead characters',
    'genre'         => 'Genre|Genres',
    'subtitles'     => 'Subtitle|Subtitles',
    'audios'        => 'Audio language|Audio languages',
    'chooseseason'  => 'Choose a season',
    'season'        => 'Season',
    'trailer'       => 'Trailer',
    'hours'         => 'hour',
    'minutes'       => 'minutes',

    'name'          => 'Name',
    'year'          => 'Year',
    'submit'        => 'Submit',
    'send'          => 'Send',
    'success'       => 'Success',
    'error'         => 'Error',
    'season'        => 'Season',
    'episode'       => 'Episode',
    'trailer'       => 'Trailer',
    'title'         => 'Title',
    'priority'      => 'Priority',
    'continue'      => 'Continue watching',
    'click-enlarge' => 'Click to enlarge',
    'duration'      => 'Duration',
    ''              => '',
    'add.new'       => 'Search new',
    'add.existing'  => 'Search existing',
    'remote'        => 'Remote',
    'offline'       => 'Offline',
    'livechannels'  => 'Live channels',
    'typemessage'   => 'Type your message...',
    ''              => '',
];
