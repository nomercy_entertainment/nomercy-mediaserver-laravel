<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Deze combinatie van e-mailadres en wachtwoord is niet geldig.',
    'password' => 'Wachtwoord onjuist.',
    'throttle' => 'Te veel mislukte loginpogingen. Probeer het over :seconds seconden nogmaals.',


    'registered' => 'Gebruiker registratie succesvol',
    'loggedOut' => 'Gebruiker succesvol uitgelogd.',


    'name'                  => 'Naam',
    'email'                 => 'E-mailadres',
    'password'              => 'Wachtwoord',
    'confirm'               => 'Bevestig wachtwoord',
    'register'              => 'Registreren',
    'login'                 => 'Log in',
    'logout'                => 'Uitloggen',
    'forgot'                => 'Je wachtwoord vergeten?',
    'remember'              => 'Blijf ingelogd',
    'new'                   => 'Nieuw hier?',
    'exist'                 => 'Al geregistreerd?',
    'privacy'               => 'Privacy beleid',
    'terms'                 => 'Ik acceptreer de voorwaardes',
    'captcha'               => 'Je moet de bewijzen dat je een mens bent',
    'resetLink'             => 'Herstel',
    'reset'                 => 'Herstel Wachtwoord',
    'change'                => 'Verrander Wachtwoord',
    'currentpass'           => 'Huidig Wachtwoord',
    'newpass'               => 'Nieuw Wachtwoord',
    'repeatnewpass'         => 'Herhaal Nieuw Wachtwoord',
    'verifiedat'            => 'Geverifieërd Op',
    'users'                 => 'Gebruikers',
    'servers'               => 'Servers',
    'permissions'           => 'Machtigingen',
    'newpermission'         => 'Nieuwe Machtiging',
    'permissionname'        => 'Machtigings Naam',
    'editpermission'        => 'Berwerk Machtiging',
    'showuser'              => 'Bekijk Gebruiker',
    'edituser'              => 'Bewerk Gebruiker',
    'createuser'            => 'Gebruiker Toevoegen',
    'last_request_at'       => 'Laatste bezoek',
    'last_login_at'         => 'Laatste login',
    'last_login_ip'         => 'Laatste login IP',
    'valid'                 => 'Je moet een geldig email adres opgeven',
    'back'                  => 'Terug naar',
    'continue'              => 'Doorgaan',
    'deleted'               => 'Verwijderd',
    'never'                 => 'Nooit',
    'unverified'            => 'Niet',
    'appid'                 => 'App ID',
    'location'              => 'Locatie',
    ''               => '',
    ''               => '',
    ''               => '',
    ''               => '',

];
