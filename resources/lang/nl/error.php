<?php

return [

    'mustLogin'             => 'Voor deze pagina moet je ingeloged zijn.',
    'noAccess'              => 'Je heb geen toegang tot deze pagina.',
    '404'                   => 'Sorry deze pagina bestaat niet.',
    '401'                   => 'Sorry, je heb niet de bevoegdheden voor deze pagina.',
    '419'                   => 'Sorry, je sessie is afgelopen. Ververs de pagina.',
    '429'                   => 'Help ik kan het niet meer aan... te veel drukte!!!',
    '500'                   => 'Whoops, er is iets mis gegaan met de server.',
    '503'                   => 'Sorry, wij zijn onderhoud aan het plegen. Probeer het straks nog eens.',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',
    ''              => '',

];
