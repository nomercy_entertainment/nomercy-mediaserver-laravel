# Your Favicon Package

This package was generated with [RealFaviconGenerator](https://realfavicongenerator.net/) [v0.16](https://realfavicongenerator.net/change_log#v0.16)

## Install instructions

To install this package:

Extract this package in <code>&lt;web site&gt;/img/</code>. If your site is <code>http://www.example.com</code>, you should be able to access a file named <code>http://www.example.com/img/favicon.ico</code>.

Insert the following code in the `head` section of your pages:

    <link rel="apple-touch-icon" sizes="180x180" href="/img/apple-touch-icon.png?v=OmKEGJ3xRW">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png?v=OmKEGJ3xRW">
    <link rel="icon" type="image/png" sizes="192x192" href="/img/android-chrome-192x192.png?v=OmKEGJ3xRW">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png?v=OmKEGJ3xRW">
    <link rel="manifest" href="/img/site.webmanifest?v=OmKEGJ3xRW">
    <link rel="mask-icon" href="/img/safari-pinned-tab.svg?v=OmKEGJ3xRW" color="#9f00a7">
    <link rel="shortcut icon" href="/img/favicon.ico?v=OmKEGJ3xRW">
    <meta name="apple-mobile-web-app-title" content="NoMercy MediaServer">
    <meta name="application-name" content="NoMercy MediaServer">
    <meta name="msapplication-TileColor" content="#000000">
    <meta name="msapplication-TileImage" content="/img/mstile-144x144.png?v=OmKEGJ3xRW">
    <meta name="msapplication-config" content="/img/browserconfig.xml?v=OmKEGJ3xRW">
    <meta name="theme-color" content="#000000">

*Optional* - Check your favicon with the [favicon checker](https://realfavicongenerator.net/favicon_checker)