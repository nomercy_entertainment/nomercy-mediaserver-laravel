self.addEventListener('install', (event) => {
    console.log('Inside the install handler:', event);
});

self.addEventListener('activate', (event) => {
    console.log('Inside the activate handler:', event);
});

self.addEventListener(fetch, (event) => {
    console.log('Inside the fetch handler:', event);
});

self.addEventListener('install', (event) => {
    console.log('Inside the install handler:', event);
    event.waitUntil(
        caches.open('v1').then(function (cache) {
            return cache.addAll([
                // '/img',
                // '/img/tv',
                // '/img/movie',
                // '/img/profile',
                // '/img/collection',
                // '/js',
                // '/js/dist',
                // '/logos',
            ]);
        })
    );
});
self.addEventListener(fetch, (event) => {
    console.log('Inside the fetch handler:', event);

    event.respondWith(
        caches.match(event.request).then(function (response) {
            // caches.match() always resolves
            // but in case of success response will have value
            if (response !== undefined) {
                return response;
            } else {
                return fetch(event.request).then(function (response) {
                    // response may be used only once
                    // we need to save clone to put one copy in cache
                    // and serve second one
                    let responseClone = response.clone();
                    caches.open('v1').then(function (cache) {
                        cache.put(event.request, responseClone);
                    });
                    return response;
                }).catch(function () {
                    return caches.match('/img/noimage.thumbnail.jpg');
                });
            }
        }));
});
