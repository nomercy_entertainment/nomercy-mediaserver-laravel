
function setWaitCursor(elem) {
    elem.css('cursor', 'wait');
    $('body').css('cursor', 'wait');
}

function add(name, input) {
    event.preventDefault();
    alert(name + ' selected to go in: ' + input);
}

function clickhandler(id) {

    $('li').on('click', function (e) {

        let folder = $(this)[0].dataset.full_path;
        $(this).parent().children().removeClass('bg-purple-700');
        $(this).addClass('bg-purple-700').css('-webkit-text-fill-color', '#fff');

        if (folder != 'back') {
            e.target.parentNode.parentNode.firstChild.nextSibling.firstElementChild.value = folder;
        }
    })

    $(`li`).on('dblclick', function (e) {
        let folder = $(this)[0].dataset.folder;
        let id = e.target.parentNode != null ? e.target.parentNode.id : 'location-';

        $(`#folder-body-${id}`).empty();
        $(this).parent().empty();
        if (folder != 'back') {
            $(`#folder-title-${id}`).text(`${folder}`);
        }

        $('*').css('cursor', 'wait');

        if (folder == 'back') {
            folder = back;
        }

        read_folder(folder, id);
    })


    $('select').each(function () {

        // Cache the number of options
        var $this = $(this),
            numberOfOptions = $(this).children('option').length;

        // Hides the select element
        $this.addClass('hidden');

        // Wrap the select element in a div
        $this.wrap('<div class="select"></div>');

        // Insert a styled div to sit over the top of the hidden select element
        $this.after(`<div class="styledSelect"></div>`);

        // Cache the styled div
        var $styledSelect = $this.next('div.styledSelect');

        // Show the first select option in the styled div
        $styledSelect.text($this.children('option').eq(0).text());

        // Insert an unordered list after the styled div and also cache the list
        var $list = $('<ul />', {
            'class': 'options',
            'style': 'margin-top: 35px'

        }).insertAfter($styledSelect);

        // Insert a list item into the unordered list for each select option
        for (var i = 1; i < numberOfOptions; i++) {
            $('<li />', {
                text: $this.children('option').eq(i).text(),
                rel: $this.children('option').eq(i).val()
            }).appendTo($list);
        }

        // Cache the list items
        var $listItems = $list.children('li');

        // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)
        $styledSelect.click(function (e) {
            e.stopPropagation();
            $(this).toggleClass('active').next().toggle();
        });

        // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
        // Updates the select element to have the value of the equivalent option
        $listItems.click(function (e) {
            e.stopPropagation();
            $styledSelect.text($(this).text()).removeClass('active');
            $this.val($(this).attr('rel'));
            $list.hide();
            /* alert($this.val()); Uncomment this for demonstration! */
        });

        // Hides the unordered list when clicking outside of it
        $(document).click(function () {
            $styledSelect.removeClass('active');
            $list.hide();
        });

    });
}

