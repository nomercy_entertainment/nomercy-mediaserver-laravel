/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/app.js":
/*!*****************************!*\
  !*** ./resources/js/app.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(document).ready(function () {
  var toggleNavStatus = false;

  var toggleNav = function toggleNav() {
    var getMenu = document.querySelector('#profile-menu');

    if (toggleNavStatus === false) {
      getMenu.classList.add("show");
      toggleNavStatus = true;
    } else {
      getMenu.classList.remove("show");
      toggleNavStatus = false;
    }
  };

  $('#profile-button').click(toggleNav);
  $(".poster.redirect").each(function (e) {
    $(this).click(function (e) {
      console.log(e.target.dataset);

      if (e.target.dataset.type == "movie" || e.target.dataset.type == "tv" || e.target.dataset.type == "genre" || e.target.dataset.type == "all") {
        window.location.href = (window.subroute || '') + '/' +  e.target.dataset.type + "/" + e.target.id + '/info';
      } else if (e.target.dataset.type == "collection") {
        window.location.href = (window.subroute || '') + "/collection?id=" + e.target.id;
      } else if (e.target.dataset.type == "special") {
        window.location.href = (window.subroute || '') + "/watch?type=special&id=" + e.target.id;
      } else if (e.target.dataset.type == "watch") {
        window.location.href = (window.subroute || '') + "/watch?id=" + e.target.id;
      }
    });
  });
  var percentColorsInverse = [{
    pct: 0,
    color: {
      r: 0x00,
      g: 0xff,
      b: 0
    }
  }, {
    pct: 50,
    color: {
      r: 0xff,
      g: 0xff,
      b: 0
    }
  }, {
    pct: 100,
    color: {
      r: 0xff,
      g: 0x00,
      b: 0
    }
  }];
  var percentColors = [{
    pct: 10,
    color: {
      r: 0xcc,
      g: 0,
      b: 0
    }
  }, {
    pct: 50,
    color: {
      r: 0xee,
      g: 0xee,
      b: 0
    }
  }, {
    pct: 99,
    color: {
      r: 0,
      g: 0x90,
      b: 0
    }
  }, {
    pct: 100,
    color: {
      r: 0,
      g: 0x40,
      b: 0
    }
  }];

  var getColorForPercentage = function getColorForPercentage(pct, scheme) {
    for (var i = 1; i < scheme.length - 1; i++) {
      if (pct < scheme[i].pct) {
        break;
      }
    }

    var lower = scheme[i - 1];
    var upper = scheme[i];
    var range = upper.pct - lower.pct;
    var rangePct = (pct - lower.pct) / range;
    var pctLower = 1 - rangePct;
    var pctUpper = rangePct;
    var color = {
      r: Math.floor(lower.color.r * pctLower + upper.color.r * pctUpper),
      g: Math.floor(lower.color.g * pctLower + upper.color.g * pctUpper),
      b: Math.floor(lower.color.b * pctLower + upper.color.b * pctUpper)
    };
    return "rgb(" + [color.r, color.g, color.b].join(",") + ")"; // or output as hex if preferred
  };

  $("div[data-value]").each(function () {
    $(this).css({
      "background-color": getColorForPercentage($(this).data("percent"), percentColors)
    });

    if ($(this).data("percent") > 45 && $(this).data("percent") < 75) {
      $(this).prev().css({
        "color": getColorForPercentage($(this).data("percent"), percentColors)
      });
    }

    $(this).change(function () {
      $(this).css({
        "background-color": getColorForPercentage($(this).data("percent"), percentColors)
      });

      if ($(this).data("percent") > 45 && $(this).data("percent") < 75) {
        $(this).prev().css({
          "color": getColorForPercentage($(this).data("percent"), percentColors)
        });
      }
    });
  });
  $("div[data-percent]").each(function () {
    $(this).css({
      "background-color": getColorForPercentage($(this).data("percent"), percentColors)
    });

    if ($(this).data("percent") > 45 && $(this).data("percent") < 75) {
      $(this).prev().css({
        "color": getColorForPercentage($(this).data("percent"), percentColors)
      });
    }

    $(this).width($(this).data("percent") + "%");
  });

  if (window.innerWidth < 900) {
    $('#sidebar').removeClass('w-1/12');
    $('#main').removeClass('w-11/12');
    $('#main').addClass('w-full');
  }

  if (window.location.href.split("#")[1]) {
    var div = $("#".concat(window.location.href.split("#")[1].toUpperCase()));
    console.log(div);
    scroll_to_div(div);
  }
});

function scroll_to_div(target) {
  $(target).get(0).scrollIntoView({
    block: 'center'
  });
}

if ($(".nm-card").length > 0 && window.location.pathname != "/") {
  $("#content").append("<div id='index' class='color-right'></div>");
  var previous = "";
  $("div[data-name]").each(function (e) {
    var current = String($(this).data("name")).substr(0, 1).toUpperCase();

    if (current != previous) {
      $(this).parent().attr("id", "scroll_" + current);
      previous = current;

      if (current != null) {
        $("#index").append("<li><a class='scroller' href='#scroll_" + current + "'>" + current + "</a><li>");
      }
    }
  });
  $("#index a").click(function (event) {
    event.preventDefault();
    scroll_to_div($(this).attr("href"));
  });
}

if (window.location.hash) {
  scroll_to_div(window.location.hash);
  window.history.pushState("page2", "Title", window.location.href.split("#")[0]);
}

if (document.getElementById("menu")) {
  var menu = function menu(x, y) {
    i.top = y + "px";
    i.left = x + "px";
    i.visibility = "visible";
    i.opacity = "1";
  };

  var i = document.getElementById("menu").style;

  if (document.addEventListener) {
    document.addEventListener("contextmenu", function (e) {
      var posX = e.clientX;
      var posY = e.clientY;

      if (isNaN(e.target.id) == false && e.target.id !== '') {
        e.preventDefault();
        $("#menuSpace").empty().append("\n              <a class=\"ytp-menuitem\" target=\"_blank\" role=\"menuitem\" href=\"/dashboard/content/missing/".concat(e.target.id, "\">\n                  <div class=\"ytp-menuitem-icon\"></div>\n                  <div class=\"ytp-menuitem-label\">Get missing episodes.</div>\n                  <div class=\"ytp-menuitem-content\"></div>\n              </a>\n              <a class=\"ytp-menuitem\" target=\"_blank\" role=\"menuitem\" href=\"/dashboard/content/lowquality/").concat(e.target.id, "?q=1080\">\n                  <div class=\"ytp-menuitem-icon\"></div>\n                  <div class=\"ytp-menuitem-label\">Get lower than 1080p episodes.</div>\n                  <div class=\"ytp-menuitem-content\"></div>\n              </a>\n              <a class=\"ytp-menuitem\" target=\"_blank\" role=\"menuitem\" href=\"/dashboard/content/lowquality/").concat(e.target.id, "?q=720\">\n                  <div class=\"ytp-menuitem-icon\"></div>\n                  <div class=\"ytp-menuitem-label\">Get lower than 720p episodes.</div>\n                  <div class=\"ytp-menuitem-content\"></div>\n              </a>\n              <hr>\n              <div class=\"ytp-menuitem\" role=\"menuitemcheckbox\" aria-checked=\"false\" tabindex=\"0\">\n                  <div class=\"ytp-menuitem-icon\"></div>\n                  <div class=\"ytp-menuitem-label\" style=\"font-size: 100%;font-weight: 100;padding: 13px 0 0 75px;\">NoMercy Entertainment</div>\n                  <div class=\"ytp-menuitem-content\">\n                      <div class=\"ytp-menuitem-toggle-checkbox\"></div>\n                  </div>\n              </div>\n            "));

        if (posX + $("#menu").width() > window.innerWidth) {
          posX = e.clientX - $("#menu").width();
        }

        if (posY + $(".ytp-menuitem").length * $(".ytp-menuitem").height() > window.innerHeight) {
          posY = e.clientY - $(".ytp-menuitem").length * $(".ytp-menuitem").height();
        }

        menu(posX, posY);
      } else {
        $("#menuSpace").empty().append("\n                <div class=\"ytp-menuitem\" role=\"menuitemcheckbox\" aria-checked=\"false\" tabindex=\"0\">\n                    <div class=\"ytp-menuitem-icon\"></div>\n                    <div class=\"ytp-menuitem-label\" style=\"font-size: 100%;font-weight: 100;padding: 13px 0 0 75px;\">NoMercy Entertainment</div>\n                    <div class=\"ytp-menuitem-content\">\n                        <div class=\"ytp-menuitem-toggle-checkbox\"></div>\n                    </div>\n                </div>\n            ");
        i.visibility = "hidden";
      }
    }, false);
    document.addEventListener("click", function (e) {
      i.opacity = "0";
      setTimeout(function () {
        i.visibility = "hidden";
      }, 501);
    }, false);
  }
}

/***/ }),

/***/ "./resources/sass/app.scss":
/*!*********************************!*\
  !*** ./resources/sass/app.scss ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!*************************************************************!*\
  !*** multi ./resources/js/app.js ./resources/sass/app.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /var/www/html/resources/js/app.js */"./resources/js/app.js");
module.exports = __webpack_require__(/*! /var/www/html/resources/sass/app.scss */"./resources/sass/app.scss");


/***/ })

/******/ });