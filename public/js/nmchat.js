window.nmchat = function (host, room, username, userimage) {
    if (!(this instanceof window.nmchat)) {
        return new window.nmchat(host, room, username, userimage);
    }

    Date.prototype.addHours = function (time, h) {
        this.setTime(this.getTime(time) + (h * 60 * 60 * 1000));
        return this;
    }

    const chatForm = document.querySelector('#chat-form');
    const chatMessages = document.querySelector('#chat-flow');
    const messageBox = document.getElementById("msg");
    const zeroPad = (num, places) => String(num).padStart(places, '0');

    const socket = io(host, {
        reconnection: true,
        reconnectionDelay: 100,
        reconnectionDelayMax: 500,
        reconnectionAttempts: Infinity,
        timeout: 1000,
    });

    if (typeof io == "undefined") {
        socketLog('init');
    }
    else {
        document.querySelector('#chat-flow').i
    }

    function socketLog(type) {
        switch (type) {
            case 'init':
                text = 'Failed to connect to the socket server.'
                messageBox.disabled = true;
                messageBox.innerHTML = text;
                break;
            case 'connected':
                text = 'Socket connected.'
                messageBox.disabled = false;
                messageBox.innerHTML = '';
                break;
            case 'reconnecting':
                setTimeout(() => {
                    socket.emit('reConnect', { username, room });
                }, 5000);
                text = 'You got disconnected, retrying to connect.'
                messageBox.disabled = true;
                messageBox.innerHTML = text;
                break;
            default:
                break;
        }
        console.log(text);
    }

    let i = 0;

    function insertMessage(message) {
        const date = new Date(message.time || null);
        const div = document.createElement('div');
        div.classList.add('chatter', 'mx-1', 'my-1', 'p-2');
        div.style.background = i % 2 == 0 ? '#1f1e1e' : '#171616';
        div.innerHTML = `
            <span class="float-left mx-2 text-sm chat-text" style="line-height: 21px;font-size: 11px;" data-timestamp="${date.toLocaleTimeString()}">
                <span class="float-left mx-0 chat-icon">
                ${message.username != 'System' && message.username != 'guest' ? "<img src=\"" + message.userimage + "\" alt=\"\" class=\"h-5 w-5 object-contain mx-0\">" : ''}
                </span>
                <span class="float-left h-5 mx-1 text-xs text-purple-500 chat-name">${username != message.username ? message.username : message.username} ${message.username == 'stoney_eagle' ? '<i style="color:white">&#9812;</i>' : ''}</span>
                ${message.text}
            </span>
		`;
        document.querySelector('#chat-flow').appendChild(div);
        chatMessages.scrollTop = chatMessages.scrollHeight;
    }

    console.log(socket);

    socket.emit('joinRoom', { username, room });

    socket.on('connected', function (client) {
        socketLog('connected');

    });
    socket.on('connect', function (client) {
        socketLog('connected');
    })

    socket.on('connect_error', function () {
        socketLog('reconnecting');
    });

    socket.on('disconnect', function () {
        socketLog('reconnecting');
    });

    socket.on('roomUsers', ({ room, users }) => {
        outputRoomName(room);
        outputUsers(users);
    });

    socket.on('message', message => {
        outputMessage(message);
    });

    socket.on('viewers', message => {
        $('.viewers').html(message);
        $('.viewer-container').css('display', 'flex')
    });

    chatForm.addEventListener('submit', e => {
        e.preventDefault();
        const msg = e.target.elements.msg.value;
        socket.emit('chatMessage', { username, userimage, msg });
        e.target.elements.msg.value = '';
    });

    function outputMessage(message) {
        insertMessage(message);
    }

    function outputRoomName(room) {
        // 	roomName.innerText = room;
    }

    function outputUsers(users) {
        let list = '';
        var result = {};
        for (var i in users) {
            result[users[i].username] = null;
        }
        result = Object.keys(result);

        for (var key in result) {
            list += `<li>${result[key]}</li>`;
        }

        // userList.innerHTML = list;
    }
}
