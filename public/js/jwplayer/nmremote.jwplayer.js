
window.nmremote = function (e) {
    if (!(this instanceof window.nmremote)) {
        return new window.nmremote(e);
    }

    window.onkeydown = function (e) {
        switch (e.key) {

            case "MediaPlay": //Play
                jwplayer().play();
                break;
            case "MediaStop": // Stop
                jwplayer().stop();
                break;
            case "MediaRewind": // Rewind
                nmplayer.rewindVideo();
                break;
            case "MediaFastForward": // Forward
                nmplayer.forwardVideo();
                break;
            case "MediaTrackPrevious": // Previous
                nmplayer.previous();
                break;
            case "MediaTrackNext": // Next
                nmplayer.next();
                break;
            case "Pause": // Pause
                jwplayer().pause();
                break;
            case "Subtitle": // Subtitle
                nmplayer.cycleSubtitles();
                break;
            case "PageUp": // Prog up

                break;
            case "PageDown": // Prog down

                break;
            case "1":

                break;
            case "2":

                break;
            case "3":

                break;
            case "4":

                break;
            case "5":

                break;
            case "6":

                break;
            case "7":

                break;
            case "8":

                break;
            case "9":

                break;
            case "0":

                break;
            case "ColorF0Red":

                break;
            case "ColorF1Green":

                break;
            case "ColorF2Yellow":
                nmplayer.delaySub();
                break;
            case "ColorF3Blue":
                nmplayer.expediteSub();
                break;

            default:
                // alert(e.key);
                break;
        }
    }
}
