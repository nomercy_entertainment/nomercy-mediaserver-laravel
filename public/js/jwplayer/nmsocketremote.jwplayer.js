
window.nmremote = function (host, user) {
    if (!(this instanceof window.nmremote)) {
        return new window.nmremote(host, user);
    }



    const supportsBackgroundLoading = jwplayer().getEnvironment().Features.backgroundLoading && !(/PlayStation|Chrome\/49/).test(navigator.userAgent);

    if (typeof io !== 'undefined') {

        const port = '2096';
        const socket = io.connect(location.host + ':' + port, {
            reconnection: true,
            reconnectionDelay: 100,
            reconnectionDelayMax: 500,
            reconnectionAttempts: Infinity
        });


        let state;
        let urlQuery = new URLSearchParams(window.location.search);
        let currentItem;
        let duration;
        let time = true;

        window.newTime = 0;

        socket.on('connect', function (e) {

            socket.emit('subscribe', {
                room: user,
            });

            if (typeof jwplayer != "undefined") {

                jwplayer().on("time", function (e) {
                    object = {
                        room: user,
                        position: humanTime(jwplayer().getPosition()),
                        remaining: humanTime(jwplayer().getDuration() - jwplayer().getPosition()),
                        percent: (jwplayer().getPosition() / jwplayer().getDuration()) * 100,
                        currentItem: jwplayer().getPlaylistIndex(),
                        playlist: jwplayer().getPlaylist(),
                        playlistItem: jwplayer().getPlaylistItem(),
                        state: jwplayer().getState(),
                    };
                    socket.emit("player-time", object);
                });

                jwplayer().on("play", (e) => {
                    object = {
                        room: user,
                        position: humanTime(jwplayer().getPosition()),
                        remaining: humanTime(jwplayer().getDuration() - jwplayer().getPosition()),
                        percent: (jwplayer().getPosition() / jwplayer().getDuration()) * 100,
                        currentItem: jwplayer().getPlaylistIndex(),
                        playlist: jwplayer().getPlaylist(),
                        playlistItem: jwplayer().getPlaylistItem(),
                        state: jwplayer().getState(),
                    };
                    socket.emit("player-play", object);
                });
                jwplayer().on("pause", (e) => {
                    socket.emit("player-pause", {
                        room: user,
                    });
                });
                jwplayer().on("playlistItem", (e) => {
                    object = {
                        room: user,
                        position: humanTime(jwplayer().getPosition()),
                        remaining: humanTime(jwplayer().getDuration() - jwplayer().getPosition()),
                        percent: (jwplayer().getPosition() / jwplayer().getDuration()) * 100,
                        currentItem: jwplayer().getPlaylistIndex(),
                        playlist: jwplayer().getPlaylist(),
                        playlistItem: jwplayer().getPlaylistItem(),
                        state: jwplayer().getState(),
                    };
                    socket.emit("player-playlistItem", object);
                });

                socket.on('player-ask', function (client) {
                    object = {
                        room: user,
                        position: humanTime(jwplayer().getPosition()),
                        remaining: humanTime(jwplayer().getDuration() - jwplayer().getPosition()),
                        percent: (jwplayer().getPosition() / jwplayer().getDuration()) * 100,
                        currentItem: jwplayer().getPlaylistIndex(),
                        playlist: jwplayer().getPlaylist(),
                        playlistItem: jwplayer().getPlaylistItem(),
                        state: jwplayer().getState(),
                    };
                    socket.emit('player-answer', object);
                });
                socket.on('player-play', function (e) {
                    jwplayer().play();
                });
                socket.on('player-pause', function (e) {
                    jwplayer().pause();
                });
                socket.on('player-previous', function (e) {
                    nmplayer.previous();
                    state = "paused";
                });
                socket.on('player-next', function (e) {
                    nmplayer.next();
                });
                socket.on('player-setEpisode', function (e) {
                    nmplayer.setEpisode(e.season, e.episode);
                });
                socket.on('player-seek', function (e) {
                    jwplayer().seek(e.time);
                });

                socket.on('player-subtitleOffset', function (e) {
                    console.log(e.offset);
                    nmplayer.subtitleOffset(e.offset);
                });

            }
            else {

                socket.emit('remote-ask', {
                    room: user,
                });

                socket.on('remote-answer', function (e) {

                    console.log(e);
                    nmremote.position(e.position);
                    nmremote.remaining(e.remaining);
                    nmremote.time(e.percent);
                    nmremote.title(e.playlistItem);
                    nmremote.image(e.playlistItem);
                    loadSeasons(e.playlist);
                    currentItem = e.currentItem;
                    duration = e.duration;

                    $("[data-button]").click(function (e) { onClick(e); });
                    if (e.state == "playing") {
                        state = e.state;
                        nmremote.play();
                    }
                    else {
                        state = e.state;
                        nmremote.pause();
                    }
                });

                socket.on('remote-playlist', function (e) {
                    loadSeasons(e);
                });

                socket.on('remote-playlistItem', function (e) {

                    nmremote.title(e.playlistItem);
                    nmremote.image(e.playlistItem);
                    nmremote.time(e.percent);
                    state = e.state;
                    currentItem = e.currentItem;
                    playlistItem = e.playlistItem;
                    if (e.state == "playing") {
                        nmremote.play();
                    }
                    else {
                        nmremote.pause();
                    }
                });

                socket.on('remote-time', function (e) {

                    nmremote.position(e.position);
                    nmremote.remaining(e.remaining);
                    nmremote.time(e.percent);
                    currentItem = e.currentItem;
                    state = e.state;
                });

                socket.on('remote-play', function (e) {

                    nmremote.play();
                    state = "playing";
                });

                socket.on('remote-pause', function (e) {
                    nmremote.pause();
                    state = "paused";
                });

                $("#btn-playback").on('click', function () {
                    nmremote.togglePlayback();
                });

                $("#previous").on('click', function () {
                    nmremote.previous();
                });

                $("#next").on('click', function () {
                    nmremote.next();
                });

                $('#remote-slider').mousedown(function () {
                    time = false;

                }).change(function () {
                    let newValue = this.value;

                    let seek = (duration / 100) * newValue;
                    console.log(seek);
                    nmremote.seek(seek);
                    time = false;
                });

                $('#subtitleOffset').change(function () {
                    console.log(this.value);
                    nmremote.subtitleOffset(this.value);
                });
            }

            this.sock = socket;

            this.togglePlayback = () => {
                if (state == "paused") {
                    nmremote.play();
                    socket.emit('remote-play', {
                        room: user,
                    });
                    playing = true;
                }
                else {
                    nmremote.pause();
                    socket.emit('remote-pause', {
                        room: user,
                    });
                    playing = false;
                }
            };
            this.play = () => {
                $("#btn-playback").removeClass("play");
                $("#btn-playback").addClass("pause");
            };
            this.pause = () => {
                $("#btn-playback").removeClass("pause");
                $("#btn-playback").addClass("play");
            };

            this.subtitleOffset = (e) => {
                socket.emit('remote-subtitleOffset', {
                    room: user,
                    offset: e
                });
            };

            this.previous = () => {
                socket.emit('remote-previous', {
                    room: user,
                });
            };
            this.next = () => {
                socket.emit('remote-next', {
                    room: user,
                });
            };

            this.title = (e) => {
                $('#title').text(`S${e.season}E${e.episode} ${e.title}`);
            };
            this.image = (e) => {
                $('#image').attr('src', e.image);
            };

            this.position = (e) => {
                $('#current-time').text(e);
            };

            this.remaining = (e) => {
                $('#remaining-time').text(e);
            };

            this.time = (e) => {
                if (time) {
                    $('#remote-slider').val(e);
                }
                $('#time').css('width', `${e}%`);
                $('#time-nipple').css('left', `${e}%`);
            };

            this.setEpisode = (s, e) => {
                socket.emit('remote-setEpisode', {
                    room: user,
                    season: s,
                    episode: e
                });
            };

            this.seek = (time) => {
                socket.emit('remote-seek', {
                    room: user,
                    time: time,
                });
            };

            function loadSeasons(playlist) {
                console.log(playlist);
                $(`#season-forward-container`).empty();
                $(`#seasons-container`).empty();

                if (urlQuery.get('type') == null) {
                    let season = 999,
                        style;

                    playlist.forEach((e, i) => {
                        if (playlist.length > 1) {
                            $('#seasons').removeClass('hidden');
                        }
                        if (e.season != season) {
                            season = e.season;
                            $('#season-forward-container').append(`
                                <div data-button="season-forward" data-season="${e.season != 0 ? e.season : 'Specials'}" class="flex flex-col mb-2 overflow-hidden season-forward" style="background:${i % 2 == 0 ? '#25252550' : '#1d1c1c50'}">
                                    <div data-button="season-forward" data-season="${e.season != 0 ? e.season : 'Specials'}" class="flex flex-row overflow-hidden season-forward">
                                        <div data-button="season-forward" data-season="${e.season != 0 ? e.season : 'Specials'}" class="flex flex-col justify-center float-left w-32 mx-auto">
                                            <img data-button="season-forward" data-season="${e.season != 0 ? e.season : 'Specials'}" src="${e.season_image || '.x'}" onerror="this.src='https://cdn.nomercy.tv/img/poster-not-available.png'" alt="" style="display: block;height: 100%;margin: auto; padding: 10%;max-height: 100%;">
                                        </div>
                                        <div data-button="season-forward" data-season="${e.season != 0 ? e.season : 'Specials'}" class="flex flex-col float-left w-10/12 h-full mr-4">
                                            <div data-button="season-forward" data-season="${e.season != 0 ? e.season : 'Specials'}" class="flex flex-col h-4 mx-4 my-4 text-center">
                                                ${e.season != 0 ? ' Season ' + e.season : 'Specials'}
                                            </div>
                                            <div data-button="season-forward" data-season="${e.season != 0 ? e.season : 'Specials'}" class="flex flex-col float-left h-16 mx-4 my-2 overflow-hidden text-xs" style="min-width: 73%;">
                                                ${e.season_overview || ''}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            `);
                        }
                    });
                    season = 999;
                    style = '';
                    playlist.forEach((e, i) => {
                        if (e.season != season) {
                            season = e.season;
                            if (e.season == urlQuery.get('season')) {
                                style = 'active';
                            } else {
                                style = '';
                            }
                            $('#seasons-container').append(`
                                <div id="season-${e.season != 0 ? e.season : 'Specials'}-container" class="season h-full w-full overflow-hidden flex-col ${style}">
                                    <div id="season-${e.season != 0 ? e.season : 'Specials'}-list" data-seasonList="${e.season != 0 ? e.season : 'Specials'}" class="w-full p-3 mb-2 overflow-y-auto season-list" style="overflow-x: hidden;"></div>
                                </div>
                            `);
                        }
                    });
                    playlist.forEach((e, i) => {

                        $(`#season-${e.season != 0 ? e.season : 'Specials'}-list`).append(`
                            <div id="${i}" data-button="video" onclick="nmremote.setEpisode(${e.season || 0},${e.episode || 'Specials'})" class="w-full h-32 p-2 mb-2" style="background:${i % 2 == 0 ? '#25252550' : '#1d1c1c50'}">
                                <div data-button="video" class="w-full p-2 text-sm">
                                S${zeroPad(e.season || 0, 2)}E${zeroPad(e.episode || 0, 2)} ${e.title.replace('S H I E L D', 'S.H.I.E.L.D.')}
                                </div>
                                <div class="flex-row h-20">
                                    <div data-button="video" class="float-left w-3/12 h-full px-2 py-1">
                                        <img data-button="video" src="${e.image != 'Specials' ? e.image.replace('original', 'original') : '.x'}" onerror="this.src='https://cdn.nomercy.tv/img/still-not-available.png'" alt="" style="display: block;margin: auto;max-height: 100%;">
                                    </div>
                                    <div data-button="video" id="" class="float-left w-9/12 p-1 text-xs overview">
                                        ${e.description}
                                    </div>
                                </div>
                            </div>
                        `);
                    });
                }
                else {
                    $(`#seasons-container`).append(`
                        <div data-button="season-back" id="season-1-container" class="season-list-container" style="display:block;width: 100%;margin-top: 50px;">
                            <div data-button="season-back"  id="season-1-button" data-season="1" class="episodes" style="overflow-x: hidden;"></div>
                            <div id="season-1-list" data-seasonList="1" class="episode-list" style="overflow-x: hidden;"></div>
                        </div>
                    `);
                    $(`#seasons-container`).addClass('active');
                    playlist.forEach((e, i) => {
                        $('#season-1-list').append(`
                            <div id="${i}" data-button="video" onclick="nmremote.setEpisode(1,${i || 0})" class="w-full p-2" style="background:${i % 2 == 0 ? '#25252550' : '#1d1c1c50'}">
                                <div data-button="video" class="w-full p-2">
                                    #${i} ${e.title.replace('S H I E L D', 'S.H.I.E.L.D.')}
                                </div>
                                <div class="row w-100">
                                    <div data-button="video" class="float-left w-3/12 p-1">
                                        <img data-button="video" id="" src="${e.image != null ? e.image.replace('original', 'w300') : '.x'}" onerror="this.src='https://cdn.nomercy.tv/img/noimage.thumbnail.jpg'" alt="" style="display: block">
                                    </div>
                                    <div data-button="video" id="" class="float-left w-9/12 p-1 text-sm" style="font-size: 0.8rem;line-height:1rem;font-weight: 500;overflow: hidden;height: 4rem;">
                                        ${e.description}
                                    </div>
                                </div>
                            </div>
                        `);
                    });
                }
            }

            function onClick(e) {
                switch (e.target.dataset.button) {
                    case "back":
                        if (typeof window.nmsync !== 'undefined' && !jwplayer().getPlaylistItem().production && (typeof jwplayer().getPlaylistIndex().length == 'undefined' || jwplayer().getPlaylistIndex() == jwplayer().getPlaylistIndex().length) && (jwplayer().getDuration() - jwplayer().getPosition() < 360)) {
                            nmremote.theEnd();
                        }
                        else {
                            nmremote.goBack();
                        }
                        break;
                    case "close":
                        nmremote.play();
                        hideSeasonsMenu();
                        break;
                    case "quality":
                        toggleQualityMenu();
                        break;
                    case "options":
                        toggleOptionsMenu();
                        break;
                    case "previous":
                        $('.spinner').css('display', 'block');
                        nmremote.previous();
                        break;
                    case "next":
                        $('.spinner').css('display', 'block');
                        nmremote.next();
                        break;
                    case "playback":
                        nmremote.togglePlayback();
                        break;
                    case "fullscreen":
                        toggleFullscreen();
                        break;
                    case "seasons":
                        toggleSeasonsMenu();
                        break;
                    case "season-back":
                        _seasonBack();
                        break;
                    case "season-forward":
                        _seasonForward(e.target.dataset.season || 'Specials');
                        break;
                    case "mute":
                        toggleMute();
                        break;
                    case "slider":
                        break;
                    case "session":
                        changeAspect();
                        break;
                    case "href":
                        window.location = e.target.dataset.href;
                        break;
                    case "subtitle":
                        jwplayer().setCurrentCaptions(e.target.dataset.track);
                        break;
                    case "audio":
                        if (typeof jwplayer_hls_provider != "undefined") {
                            jwplayer().hls.audioTrack = e.target.dataset.track;
                        }
                        else {
                            jwplayer().setCurrentAudioTrack(e.target.dataset.track);
                        }
                        break;
                    default:
                        console.log("no data-button property set");
                        break;
                }
                sleep(2000);
            }

            // $("[data-button]").click(function(e) {onClick(e)});

            function clearToggles() {
                optionsMenuToggle = false;
                audioMenuToggle = false;
                qualityMenuToggle = false;
            }

            function toggleQualityMenu() {
                hideOptionsMenu();
                hideQualityMenu();
                hideSeasonsMenu();

                $('[data-button="level"]').click(function () {
                    qualityMenuToggle = false;
                    $('#qualityMenu').css("display", "none");
                    jwplayer().play();
                });

                if (qualityMenuToggle == false) {
                    showQualityMenu();
                }
                else {
                    hideQualityMenu();
                }
            }
            function showQualityMenu() {
                qualityMenuToggle = true;
                nmremote.pause();
                $('#qualityMenu').css("display", "flex");
            }
            function hideQualityMenu() {
                qualityMenuToggle = false;
                $('#qualityMenu').css("display", "none");
            }


            function toggleOptionsMenu() {
                hideOptionsMenu();
                hideQualityMenu();
                hideSeasonsMenu();

                $('[data-button="audio"],[data-button="subtitle"]').click(function () {
                    optionsMenuToggle = false;
                    $('#qualityMenu').css("display", "none");
                    jwplayer().play();
                });

                if (optionsMenuToggle == false) {
                    showOptionsMenu();
                }
                else {
                    hideOptionsMenu();
                }
            }
            function showOptionsMenu() {
                optionsMenuToggle = true;
                nmremote.pause();
                $('#optionsMenu').css("display", "flex");
                $("#optionsMenu").css('right', '0');
            }
            function hideOptionsMenu() {
                optionsMenuToggle = false;
                $('#optionsMenu').css("display", "none");
                $("#optionsMenu").css('right', '-40vw');
            }


            function toggleSeasonsMenu() {
                hideOptionsMenu();
                hideQualityMenu();
                hideSeasonsMenu();

                _seasonBack();
                let season = urlQuery.get('season') || '01';
                _seasonForward(season);
                $('#season-menu-name').text(`Season ${season != 0 ? season : 'specials'}`);

                scroll_to_div(`#${urlQuery.get('item') || currentItem}`);

                $('[data-button="video"]').click(function () {
                    qualityMenuToggle = false;
                    $('#seasonsMenu').css("display", "none");
                });

                if (optionsMenuToggle == false) {
                    showSeasonsMenu();
                }
                else {
                    hideSeasonsMenu();
                }
            }

            function showSeasonsMenu() {
                seasonsMenuToggle = true;
                nmremote.pause();
                $('#seasonsMenu').css("display", "flex");
                $('#seasonsMenu').css("width", "100%");
                $("#seasonsMenu").css('right', '0');
            }
            function hideSeasonsMenu() {
                seasonsMenuToggle = false;
                $('#seasonsMenu').css("display", "none");
                $('#seasonsMenu').css("width", "0");
                $("#seasonsMenu").css('right', '-40vw');
            }


            function _seasonBack() {
                $('.season').css('display', 'none');
                $('#seasons-container').css('display', 'none');
                $('#season-menu-name').text(``);
                $('[data-button="season-back"]').css("display", "none");
                $("#season-forward-container").css("display", "flex");
            }

            function _seasonForward(season) {
                season = String(zeroPad(season, 2));
                $('.season').css('display', 'none');
                $('#seasons-container').css('display', 'flex');
                $('#seasonsMenu').css('display', 'flex');
                $("#season-forward-container").css("display", "none");
                $('[data-button="season-back"]').css("display", "flex");
                $('#season-menu-name').text(`Season ${season != 0 ? season : 'specials'}`);
                $(`#season-${season != 0 ? season : 'specials'}-container`).css("display", "flex");
            }


            function scroll_to_div(hash) {
                let target = $(hash);
                headerHeight = 200;
                if (target.length) {
                    $(".active>.episode-list").stop().animate({
                        scrollTop: target.offset().top - headerHeight // offsets for fixed header
                    }, "linear");
                }
            }
        });
    }

    function zeroPad(num, places) {
        var zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    }
    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    function humanTime(time) {
        var st;
        ct = parseInt(time); // ct is current time

        if (st != ct) {
            st = ct;
            var hours = parseInt(st / 3600);
            var minutes = parseInt(
                (st % 3600) / 60
            );
            var seconds = parseInt(st % 60);
            if (("" + minutes).length == 1) {
                minutes = "0" + minutes;
            }
            if (("" + seconds).length == 1) {
                seconds = "0" + seconds;
            }
            if (hours != 0) {
                hours = "" + hours + ":";
            } else {
                hours = "";
            }
            current = hours + minutes + ":" + seconds;
        }
        return current.replace("NaN:NaN:NaN", "00:00");
    }

};

