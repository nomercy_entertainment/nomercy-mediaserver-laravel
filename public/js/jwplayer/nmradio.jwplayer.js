window.nmradio = function (e) {
    if (!(this instanceof window.nmradio)) {
        return new window.nmradio(e);
    }
    $("video").ready(() => {
        // $("nav").css("display", "none");
    });

    this.setup;
    this.jwplayer;
    this.syncData;
    let optionsToggle = false,
        countrysToggle = false,
        quality = null,
        lock = false,
        thumb = false,
        timer = null,
        tap = null,
        tapCount = 10,
        isSeeking = false,
        firstError = true,
        current_time,
        offset = 0
        ;
    window.newTime = 0;

    this.setup = (f) => {

        this.jwplayer = f.jwplayerInstance.setup(
            f.jwplayerSetup
        );

        if ((!window.isOpera || !window.isFirefox || !window.isSafari || !window.isIE || !window.isChrome || !window.isEdgeChromium || !window.isBlink)) {
            // jwplayer_hls_provider.attach();
        }

        jwplayer().on("ready", (e) => {

            var rootDiv = document.getElementsByClassName("jw-overlays")[0];
            rootDiv.innerHTML += `
                <div id="overlay" class="video-overlay">
                    <div class="video-controls video-controls-top">
                    </div>
                    <div class="video-controls video-controls-center">
                        <button id="btn-playback-mobile" class="btn-playback play mobile" type="button"></button>
                        <span id="meta"></span>
                    </div>
                    <div class="video-controls video-controls-bottom">
                        <button id="btn-playback" data-button="playback" class="btn-playback play" type="button"></button>
                        <button id="btn-mute-mobile" data-button="mute" class="btn-unmute unmuted" type="button"></button>
                        <div id="previous">
                        <button id="btn-previous" type="button" data-button="previous" class="previous"></button>
                            <div class="button-title"> Previous </div>
                        </div>
                        <div id="next">
                        <button id="btn-next" type="button" data-button="next" class="next"></button>
                            <div class="button-title"> Next </div>
                        </div>
                        <div id="countrys">
                            <button id="btn-countrys" type="button" data-button="countrys" class="countrys">
                                <i class="fas fa-list-ol text-white" data-button="countrys" style="left: 1rem;position: absolute;top: 0.4rem;"></i>
                            </button>
                            <div class="button-title"> countrys </div>
                        </div>
                    </div>
                    <div id="countrysMenu" class="countrysMenu">
                        <i class="fas fa-times text-white" data-button="close" style="right: 2rem;position: absolute;top: 0;padding: 1rem;cursor: unset;"></i>
                        <div id="country-forward-container" class="country-forward-container"></div>
                        <div id="countrys-container" class="countrys-container"></div>
                    </div>
                    <div id="subtitle-window" class="subtitle-window"></div>
                </div>
            `;


            const btn_playback = document.getElementById("btn-playback");

            dynamicControls();
            addEventListeners();
            _countryBack();

            jwplayer().on("autostartNotAllowed", (e) => {
                jwplayer().playlistItem(jwplayer().getPlaylistIndex());
            })
            jwplayer().once('beforePlay', () => {
                if (jwplayer().getPlaylist().length > 1) {
                    $("#btn-countrys").css("display", "flex");
                    loadcountrys();
                    _countryBack();
                    if (new URLSearchParams(window.location.search).get('country') != null && new URLSearchParams(window.location.search).get('channel') != null) {
                        nmradio.setchannel(new URLSearchParams(window.location.search).get('country'), new URLSearchParams(window.location.search).get('channel'))
                    }
                    else {
                        jwplayer().playlistItem(237);
                    }
                }
            });
            jwplayer().on('beforePlay', () => {
                $("#meta").text('');
                document.title = jwplayer().getPlaylistItem().title;
                document.description = jwplayer().getPlaylistItem().description;
                addEventListeners();
                dynamicControls();
                hidecountrys();
                buttonDisplay();
            });
            jwplayer().once("play", () => {
                hasPlayed = true;
            });
            jwplayer().on("play", (e) => {

                var res = "/radio?country=" + jwplayer().getPlaylistItem().country.split(' ').join('-').replace("\'", "") + "&channel=" + jwplayer().getPlaylistItem().title.split(' ').join('-').replace("\'", "");
                window.history.pushState("nmplayer", jwplayer().getPlaylistItem().title, res);

                btn_playback.className = "pause";

                if (jwplayer().getMute()) {
                    $("#btn-mute-mobile").toggleClass("unmuted muted");
                };
                if (new URLSearchParams(window.location.search).get('country') != null) {
                    _countryForward(new URLSearchParams(window.location.search).get('country'));
                }
                else {
                    _countryForward('Netherlands');
                }

            });
            jwplayer().on("pause", (e) => {
                lock = true;
                btn_playback.className = "play";
                showControls();
            });
            jwplayer().on("time", (e) => {
                window.newTime = e.position;
                $.ajax(jwplayer().getPlaylistItem().meta, {
                    cache: false
                }).then(function (data) {
                    $("#meta").text(data.data.song.artist + ' - ' + data.data.song.title)
                });
            });
            jwplayer().on("mute", (e) => {
                if (e.mute) {
                    $("#btn-mute-mobile").removeClass("unmuted");
                    $("#btn-mute-mobile").addClass("muted");
                } else {
                    $("#btn-mute-mobile").removeClass("muted");
                    $("#btn-mute-mobile").addClass("unmuted");
                }
            });
            jwplayer().on("playlistItem", (e) => {
                this._changeSource(e.item.file);
            });
            jwplayer().once("beforeComplete", (e) => {
                this.next();
            });
        });
        // var titleDiv = document.getElementById('title');
        // var artistDiv = document.getElementById('artist');
        jwplayer().on('meta', ({ metadata: { artist, title, url } = defaults }) => {
            // titleDiv.textContent = title;
            // artistDiv.textContent = artist;
            // loadImage(url);
            console.log(artist, title, url);
        });

        jwplayer().once("idle", (e) => {
            handleError(e);
        });
        jwplayer().on("error", (e) => {

            // $('video')[0].src=jwplayer().getPlaylistItem().file;
            // $('video')[0].play();

            // handleError(e);
        });
        jwplayer().on("mediaError", (e) => {
            // handleError(e);
        });
        jwplayer().on("error", (e) => {

            // $('video')[0].src=jwplayer().getPlaylistItem().file;
            // $('video')[0].play();

        });
        const addEventListeners = () => {
            const video = document.querySelector('video');
            const videoControls = document.getElementById("overlay");

            video.addEventListener("mousemove", dynamicControls);
            video.addEventListener("click", onVideoClick);
            videoControls.addEventListener("click", onClick);
        };
        const onClick = (e) => {
            switch (e.target.dataset.button) {
                case "back":
                    this.goBack();
                    break;
                case "close":
                    this.play();
                    break;
                case "quality":
                    this.toggleQuality();
                    break;
                case "options":
                    this.toggleOptions();
                    break;
                case "video":
                    hidecountrys();
                    break;
                case "previous":
                    this.previous();
                    break;
                case "next":
                    this.next();
                    break;
                case "playback":
                    this.togglePlayback();
                    break;
                case "fullscreen":
                    this.toggleFullscreen();
                    break;
                case "countrys":
                    togglecountrys();
                    break;
                case "country-back":
                    _countryBack();
                    break;
                case "country-forward":
                    _countryForward(e.target.dataset.country);
                    break;
                case "mute":
                    this.toggleMute();
                    break;
                case "slider":
                    break;
                case "href":
                    window.location = e.target.dataset.href;
                    break;
                default:
                    console.log("no data-button property set");
                    break;
            }
        };
        const onVideoClick = (e) => {
            if (jwplayer().getState() == "idle") {
                // this.setchannel(new URLSearchParams(window.location.search).get('country'),new URLSearchParams(window.location.search).get('channel'))
            }
        };
        const handleError = (e) => {
            console.log(e);
            if (!window.nmsync) {
                if (firstError) {
                    current_time = jwplayer().hls.media.currentTime;
                    firstError = false;
                }
                this._changeSource(jwplayer().playlistItem().file);
                jwplayer().seek(current_time);
            }
            else {
                nmsync._getTime();
            }
        };
        const loadcountrys = () => {
            document.getElementById("country-forward-container").innerHTML = '';
            document.getElementById("countrys-container").innerHTML = '';
            let country = 999,
                style;
            jwplayer().getPlaylist().forEach((e, i) => {
                if (e.country.split(' ').join('-').replace("\'", "") != country) {
                    // country = e.country;

                    country = e.country.split(' ').join('-').replace("\'", "");
                    document.getElementById("country-forward-container").innerHTML += `
                        <div data-button="country-forward" data-country="${country}" class="country-forward-button">
                            <div data-button="country-forward" data-country="${country}" class="country-text row"> ${country} <div style="font-size: small;margin: inherit;"></div></div>
                            <div data-button="country-forward" data-country="${country}" class="country-forward" style="cursor: none"></div>
                        </div>
                    `;
                }
            });
            country = 999, style;
            jwplayer().getPlaylist().forEach((e, i) => {
                if (e.country.split(' ').join('-').replace("\'", "") != country) {
                    // country = e.country;
                    country = e.country.split(' ').join('-').replace("\'", "");
                    if (e.country == new URLSearchParams(window.location.search).get('country')) {
                        style = 'active';
                    } else {
                        style = '';
                    }
                    document.getElementById("countrys-container").innerHTML += `
                        <div data-button="country-back"  id="country-${country}-container" class="country-list-container ${style}" style="width: 100%;height: 3rem;">
                            <div data-button="country-back"  id="country-${country}-button" data-country="${country}" class="channels" style="overflow-x: hidden;">
                                <button data-button="country-back" class="country-back left-arrow" style=" position: relative;"></button>
                                <div data-button="country-back" class="country-text">${country} </div>
                            </div>
                            <div id="country-${country}-list" data-countryList="${country}" class="channel-list" style="overflow-x: hidden;"></div>
                        </div>
                    `;
                }
            });
            jwplayer().getPlaylist().forEach((e, i) => {
                country = e.country.split(' ').join('-').replace("\'", "");
                document.getElementById(`country-${country}-list`).innerHTML += `
                    <div data-button="video" onclick="nmradio.setchannel('${country}','${e.title.split(' ').join('-').replace("\'", "")}')" class="channel-item col-12 row" data-channel="S01E01" style=" margin: 0px; overflow-x: hidden;">
                        <div data-button="video" class="col-12" style="overflow: hidden;">
                            <div data-button="video" id="" class="channel-title">
                                ${e.title}
                            </div>
                            <div data-button="video" id="" class="channel-overview">
                                ${e.description}
                            </div>
                        </div>
                    </div>
                `;
            });
        };
        const zeroPad = (num, places) => {
            var zero = places - num.toString().length + 1;
            return Array(+(zero > 0 && zero)).join("0") + num;
        };
        const sleep = (ms) => {
            return new Promise(resolve => setTimeout(resolve, ms));
        };
        const _countryBack = () => {
            $("#countrys-container").css("display", "none");
            $(".country-list-container").removeClass("active");
            $("#country-forward-container").css("display", "block");
        };
        const _countryForward = (country) => {
            $("#country-forward-container").css("display", "none");
            $("#countrys-container").css("display", "block");
            $(`#country-${country}-container`).addClass("active");
        };
        const buttonDisplay = () => {
            if (jwplayer().getPlaylist().length > 1) {
                $("#btn-previous").css("display", "flex");
                $("btn#next").css("display", "flex");
            }
            if (jwplayer().getPlaylistIndex() > 0 && jwplayer().getPlaylist().length > 1) {
                $("#btn-previous").css("display", "flex");
                $("button.previous").attr("title", jwplayer().getPlaylist()[jwplayer().getPlaylistIndex() - 1].title);
            } else {
                $("#btn-previous").css("display", "none");
            }
            if (!jwplayer().getPlaylist()[jwplayer().getPlaylistIndex() + 1] && jwplayer().getPlaylist().length >= 1) {
                $("#btn-next").css("display", "none");
            } else {
                $("#btn-next").css("display", "flex");
                $("button.next").attr("title", jwplayer().getPlaylist()[jwplayer().getPlaylistIndex() + 1].title);
            }
            let last;
            for (i = jwplayer().getPlaylistIndex(); i < jwplayer().getPlaylist().length; i++) {
                if (jwplayer().getPlaylist()[i].file !== null) {
                    last = i;
                    this.last = i;
                }
            }
            if (jwplayer().getPlaylistIndex() == last) {
                $("#btn-next").css("display", "none");
            }
            if (window.localStorage !== undefined) {
                if (typeof jwplayer().hls !== 'undefined' && localStorage.getItem('nmradio.quality') && localStorage.getItem('nmradio.quality') == 1 && jwplayer().hls.currentLevel == 0) {
                    // $("#btn-quality").removeClass("HD1080P UHD4K");
                    // $("#btn-quality").removeClass("HD1080P");
                    // $("#btn-quality").addClass("UHD4K");
                    // jwplayer().hls.nextLevel = 1;
                }
            }

        };
        const togglecountrys = () => {
            switch (countrysToggle) {
                case false:
                    showcountrys();
                    break;
                default:
                    hidecountrys();
                    break;
            }
        };
        const showcountrys = () => {
            countrysToggle = true;
            lock = true;
            // $('.video-controls').css('display', 'none');
            $('.subtitle-window').hide();
            $(".countrysMenu").addClass("active");
        };
        const hidecountrys = () => {
            countrysToggle = false;
            lock = false;
            $('.subtitle-window').show();
            $(".countrysMenu").removeClass("active");
            // this.play();
        };
        const dynamicControls = () => {
            showControls();
            clearTimeout(timer);
            if (!lock && !countrysToggle && !optionsToggle) {
                timer = setTimeout(hideControls, 4000);
            }
        };
        const toggleControls = () => {
            if ((!thumb && lock) || optionsToggle || countrysToggle) {
                showControls();
            } else {
                hideControls();
            }
        };
        const showControls = () => {
            // $('.video-controls').css('display', 'flex');
            $(".video-overlay").addClass("active");
            $(".jw-wrapper").addClass("active");
            if (!optionsToggle) {
                $("*").css("cursor", "unset");
            }
        };
        const hideControls = () => {
            $(".video-overlay").removeClass("active");
            $(".jw-wrapper").removeClass("active");
            if (!optionsToggle) {
                $("video").css("cursor", "none");
            }
            setTimeout(() => {
                $("button").each(function (e) {
                    $(this).css("opacity", "1");
                    $(this).removeClass("active");
                });
            }, 500);
        };

        this.setchannel = (country, title) => {
            jwplayer().getPlaylist().forEach((e, index) => {
                if (e.country.split(' ').join('-').replace("\'", "") == country && e.title.split(' ').join('-').replace("\'", "") == title) {
                    jwplayer().playlistItem(index);
                }
            });
        };
        this._changeSource = (source) => {
            $(".subtitle-window").html('');
            const vid = document.querySelector('video');
            if (typeof jwplayer().hls !== "undefined") {
                jwplayer().hls.attachMedia(vid);
                jwplayer().hls.loadSource(source);
                jwplayer().hls.on(Hls.Events.MANIFEST_PARSED, (e) => {
                    this.setSubtitleTrack();
                });
            }
        };
        this.previous = () => {
            if (jwplayer().getPlaylistIndex() === 0) {

            } else {
                jwplayer().playlistPrev()
            }
        };
        this.next = () => {
            jwplayer().playlistNext()
        };
        this.togglePlayback = () => {
            switch (jwplayer().getState()) {
                case 'paused':
                    // this._getTime();
                    sleep(50);
                    this.play();
                    break;
                default:
                    this.pause();
                    break;
            }
        };
        this.play = () => {
            jwplayer().play();
        };
        this.pause = () => {
            jwplayer().pause();
        };
        this.toggleMute = () => {
            if (jwplayer().getMute()) {
                jwplayer().setMute(0);
            } else {
                jwplayer().setMute(1);
            }
        };
        document.onkeydown = function (e) {
            switch (e.keyCode) {
                case 107:
                    offset = offset + 0.1;
                    console.log(offset);
                    break;
                case 109:
                    offset = offset - 0.1;
                    console.log(offset);
                    break;
                case 32:
                    jwplayer().playToggle()
                    break;
                case 37:
                    rewindVideo()
                    break;
                case 39:
                    forwardVideo()
                    break;

                default:
                    break;
            }
        }
    };
}
