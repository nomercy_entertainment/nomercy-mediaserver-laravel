var jwDefaults = {

  hlshtml: true,
  primary: "html5",
  flashplayer: "//ssl.p.jwpcdn.com/player/v/8.12.5/jwplayer.flash.swf",
  stagevideo: true,
  renderCaptionsNatively: false,
  preload: "auto",
  autostart: true,
  repeat: false,
  mute: false,
  controls: false,
  debug: false,
  stretching: "uniform",

  cast: {
  },

  playbackRateControls: true,
  nextUpDisplay: true,
  displaytitle: false,
  displaydescription: false,
  stretching: "uniform",
  width: "100vw",
  height: "100vh",
  aspectratio: "16:9",
  abouttext: "Loving it!",
  captions: {
    color: "#ff0000",
    fontFamily: "Arial",
    fontOpacity: 100,
    edgeStyle: "uniform",
    backgroundColor: "#ffffff",
    windowColor: "#ffffff",
    windowOpacity: 0,
    backgroundOpacity: 0
  }
};

jwplayer.defaults = jwDefaults;
