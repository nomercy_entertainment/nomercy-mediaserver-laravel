window.nmsync = function (e) {
	if (!(this instanceof window.nmsync)) {
		return new window.nmsync(e);
	};

	this.leeway = 1500;

	let urlQuery = new URLSearchParams(window.location.search);

	this._getTime = () => {
		if (e.storage == "server") {

			if (urlQuery.get('type') !== null) {
				postData = {
					special_id: jwplayer().getPlaylistItem().tmdbid,
					type: 'special',
					_token: e.bearer,
				};
			}
			else if (urlQuery.get('season') == null && urlQuery.get('episode') == null) {
				postData = {
					tmdbid: jwplayer().getPlaylistItem().tmdbid,
					_token: e.bearer,
				};
			}
			else {
				postData = {
					unique_id: jwplayer().getPlaylistItem().uid,
					tmdbid: jwplayer().getPlaylistItem().tmdbid,
					_token: e.bearer,
				};
			}

			$.ajax({
				url: `/sync/get`,
				type: 'POST',
				headers: {
					Authorization: 'Bearer ' + e.bearer,
					_token: e.bearer,
				},
				data: postData,
				success: function (data) {
					// console.log({ getTime: data });
					if (urlQuery.get('type') !== null && urlQuery.get('item') !== null) {
						nmplayer.setEpisode(0, urlQuery.get('item'));
						if (jwplayer().getPlaylistItem().uid == data.unique_id) {
							jwplayer().seek(parseInt(data.watched));
						}
					}
					else if (urlQuery.get('season') == null && urlQuery.get('episode') == null) {
						jwplayer().getPlaylist().forEach((e, index) => {
							if (e.uid == data.unique_id) {
								jwplayer().playlistItem(index);
							}
                        });
						setTimeout(() => {
							jwplayer().seek(parseInt(data.watched));
						}, 500);
                    }

					if (data.unique_id && jwplayer().getPlaylistItem().uid == data.unique_id) {
						setTimeout(() => {
							jwplayer().seek(parseInt(data.watched));
						}, 500);
					}
					if (data == '') {
						jwplayer().play();
                    }

                    setTimeout(() => {
                        if (typeof jwplayer_hls_provider == "undefined") {
                            jwplayer().getAudioTracks().forEach((e, index) => {
                                if (e.name != null && e.name.replace(' ', '_') == data.audio) {
                                    jwplayer().setCurrentAudioTrack(index);
                                }
                            });
                            jwplayer().getPlaylistItem().tracks.forEach((e, index) => {
                                if (e.name != null && e.name.replace(' ', '_') == data.subtitle) {
                                    jwplayer().setCurrentCaptions(index);
                                }
                            });
                        }
                    }, 1000);

					window.newTime = 0;
				},
				error: function (data) {
					console.warn({ setTime: data.responseJSON });

					if (urlQuery.get('type') !== null && urlQuery.get('item') !== null) {
						nmplayer.setEpisode(0, urlQuery.get('item'));
					}
					else if (urlQuery.get('season') !== null && urlQuery.get('episode') !== null) {
						nmplayer.setEpisode(urlQuery.get('season'), urlQuery.get('episode'));
					}
					else {
						nmplayer.setEpisode(1, 1);
					}
				},
			});
		}
	};
	this._setTime = () => {
		if (e.storage == "server") {
			let audio = null;
			if (typeof jwplayer_hls_provider != "undefined") {
				audio = jwplayer().hls.audioTracks[jwplayer().hls.audioTrack] ? jwplayer().hls.audioTracks[jwplayer().hls.audioTrack].name : null;
			}
			else if (typeof jwplayer_hls_provider == "undefined") {
				audio = jwplayer().getAudioTracks()[jwplayer().getCurrentAudioTrack()] ? jwplayer().getAudioTracks()[jwplayer().getCurrentAudioTrack()].name.replace(' ', '_') : null;
            }

			$.ajax({
				url: `/sync/set`,
				type: 'POST',
				headers: {
					Authorization: 'Bearer ' + e.bearer,
					_token: e.bearer,
				},
				data: {
					_token: e.bearer,
					special_id: urlQuery.get('type') == 'special' ? urlQuery.get('id') : null,
					unique_id: jwplayer().getPlaylistItem().uid,
					season_id: jwplayer().getPlaylistItem().season_id || null,
					episode_id: jwplayer().getPlaylistItem().episode_id || null,
					tmdbid: urlQuery.get('type') != 'special' ? jwplayer().getPlaylistItem().tmdbid : null,
					watched: jwplayer().getPosition(),
					subtitle: typeof jwplayer().getPlaylistItem().tracks[jwplayer().getCurrentCaptions()].name != 'undefined' ? jwplayer().getPlaylistItem().tracks[jwplayer().getCurrentCaptions()].name.replace(' ', '_') : null,
					audio: audio
				},
				success: function (data) {
					// console.log({ setTime: data });
				},
				error: function (data) {
					console.warn({ setTime: data.responseJSON });
				},
			});
		}
	}

	let firstItem = true;
	jwplayer().on("beforePlay", (e) => {
		if (firstItem) {
			if (urlQuery.get('type') !== null && urlQuery.get('item') !== null) {
				nmplayer.setEpisode(0, urlQuery.get('item'));
				setTimeout(() => {
					this._getTime();
				}, this.leeway);
			}
			else if (urlQuery.get('type') == null && urlQuery.get('season') !== null && urlQuery.get('episode') !== null) {
				nmplayer.setEpisode(urlQuery.get('season'), urlQuery.get('episode'));
				setTimeout(() => {
					this._getTime();
				}, this.leeway);
			}
			else {
				setTimeout(() => {
					this._getTime();
				}, this.leeway);
				// nmplayer.setEpisode(1, 1);
			}
			firstItem = false;
		}
	});

	jwplayer().on("play", (e) => {
		window.newTime = 0;
	});

	jwplayer().on("time", (e) => {
		if (e.position > (window.newTime + 5)) {
			window.newTime = e.position;
			this._setTime()
		}
	});
	jwplayer().on('bufferChange', (e) => {

	});

	jwplayer().on("playlistComplete", (e) => {
		nmsync.theEnd();
	});

	if (typeof window.videoSync != 'undefined' && window.videoSync.socket) {
		jwplayer().on("play", (e) => {
			videoSync.socket.emit('play', {
				username: videoSync.username,
				room: videoSync.room,
				time: jwplayer().getPosition()
			});
		});

		jwplayer().on("pause", (e) => {
			videoSync.socket.emit('pause', {
				username: videoSync.username,
				room: videoSync.room
			});

		});
	}


	this.theEnd = () => {

        if(!jwplayer().getPlaylistItem().production){
            $.ajax({
                type: 'post',
                data: {
                    _token: e.bearer,
                    id: jwplayer().getPlaylistItem().tmdbid,
                },
                url: "/dashboard/content/deletewatched",
                success: function (data) {
                    nmplayer.goBack();
                },
                error: function (data) {
                    alert(`Can't delete the progress.`)
                },
            });
        }
	}

    window.addEventListener('warning', function (e) {
        console.log(e);
        alert(e.source + ', ' + e.message + ', ' + e.lineno + ', ' + e.colno);
    });

    window.addEventListener('error', function (e) {
        console.log(e);
        alert(e.source + ', ' + e.message + ', ' + e.lineno + ', ' + e.colno);
    });
}
