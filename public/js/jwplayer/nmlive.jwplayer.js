window.nmplayer = function (e) {
    if (!(this instanceof window.nmplayer)) {
        return new window.nmplayer(e);
    }

    this.setup;
    this.jwplayer;
    this.syncData;
    let
        audioMenuToggle = false,
        optionsMenuToggle = false,
        qualityMenuToggle = false,
        seasonsMenuToggle = false,
        lock = false,
        thumb = false,
        timer = null,
        tap = null,
        tapCount = 10,
        isSeeking = false,
        firstError = true,
        current_time,
        offset = 0,
        message,
        theTimeout
        ;



    var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    var isFirefox = window.navigator.userAgent.includes('Firefox');
    var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
    var isIE = /*@cc_on!@*/false || !!document.documentMode;
    var isEdge = !isIE && !!window.StyleMedia;
    var isEdgeChromium = window.navigator.userAgent.includes('Edg');
    var isChrome = window.navigator.userAgent.includes('Chrome');
    var isTizen = window.navigator.userAgent.includes('Firefox/47.0');
    var isOmi = window.navigator.userAgent.includes('OMI');
    var isMobile = {
        Android: function () {
            if (navigator.userAgent.match(/Android/i)) {
                return true;
            }
        },
        BlackBerry: function () {
            if (navigator.userAgent.match(/BlackBerry/i)) {
                return true;
            }
        },
        iOS: function () {
            if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
                return true;
            }
        },
        Opera: function () {
            if (navigator.userAgent.match(/Opera Mini/i)) {
                return true;
            }
        },
        Windows: function () {
            if (
                navigator.userAgent.match(/IEMobile/i) ||
                navigator.userAgent.match(/WPDesktop/i)
            ) {
                return true;
            }
        },
        any: function () {
            return (
                isMobile.Android() ||
                isMobile.BlackBerry() ||
                isMobile.iOS() ||
                isMobile.Opera() ||
                isMobile.Windows()
            );
        }
    };

    if (typeof jwplayer_hls_provider != "undefined") {
        jwplayer_hls_provider.attach();
    }

    let urlQuery = new URLSearchParams(window.location.search);


    this.setup = (f) => {

        this.jwplayer = f.jwplayerInstance.setup(
            f.jwplayerSetup

        );

        jwplayer().on("ready", (e) => {
            $(".libassjs-canvas-parent").insertAfter($("video"));
            $('.jwplayer').prepend(`
                <div id="overlay" class="absolute flex flex-col w-full h-full p-0 mx-0 text-white">
                    <div id="overlay-top" class="absolute left-0 flex flex-row w-full h-24 px-8 py-4 md:mr-8 lg:px-16">
                        <button data-button="back" id="back" class="w-16 h-16 p-5 mx-2 text-white back"></button>
                        <button data-button="quality" id="quality" class="hidden w-16 h-16 p-5 mx-2 text-white quality"></button>
                        <button data-button="options" id="options" class="hidden w-16 h-16 p-5 mx-2 text-white options"></button>
                        <button data-button="previous" id="previous" class="w-16 h-16 p-5 mx-2 text-white previous"></button>
                        <button data-button="next" id="next" class="w-16 h-16 p-5 mx-2 text-white next"></button>
                        <button data-button="seasons"  id="seasons" class="hidden w-16 h-16 p-5 mx-2 text-white seasons"></button>
                    </div>
                    <div id="overlay-center" class="flex flex-row w-full h-full p-0 mx-2">
                        <div id="player-message" class="row player-message"></div>
                        <div id="spinner" class="spinner"></div>
                        <div class="hidden nextup z-100" onclick="nmplayer.nextClick()">
                            <div class="bg"></div>
                            <div class="triangle"></div>
                            &nbsp;&nbsp;  next episode
                        </div>
                        <div class="video-rewind-notify rewind notification">
                            <div class="rewind-icon icon">
                                <i class="left-triangle triangle">
                                    <button class="fast-rewind" type="button"></button>
                                    <button class="fast-rewind" type="button"></button>
                                    <button class="fast-rewind" type="button"></button>
                                </i>
                                <span class="rewind">10 seconds</span>
                            </div>
                        </div>
                        <div class="video-forward-notify forward notification">
                            <div class="forward-icon icon">
                                <i class="right-triangle triangle">
                                    <button class="fast-forward" type="button"></button>
                                    <button class="fast-forward" type="button"></button>
                                    <button class="fast-forward" type="button"></button>
                                </i>
                                <span class="forward">10 seconds</span>
                            </div>
                        </div>
                    </div>

                    <div id="overlay-bottom" class="absolute bottom-0 left-0 flex flex-row w-full h-24 px-4 py-2 mb-3 md:mb-0 lg:px-16">
                        <button id="btn-playback" data-button="playback" class="w-16 h-16 p-5 mx-2 text-white pause play" type="button"></button>
                        <button id="btn-mute-mobile" data-button="mute" class="w-16 h-16 p-5 mx-2 text-white unmute unmuted" type="button"></button>
                        <div id="current-time" class="w-24 h-12 pt-3 mr-4 text-center text-white 3xl:pt-2 time current-time" >00:00</div>
                        <div id="remaining-time" class="w-24 h-12 pt-3 ml-auto text-center text-white 3xl:pt-2 time remaining-time" >00:00</div>
                        <button data-button="fullscreen" id="btn-fullscreen" class="w-16 h-16 p-5 mx-2 text-white fullscreen" type="button"></button>
                    </div>

                    <div id="optionsMenu" class="absolute top-0 right-0 flex flex-col h-full p-5 bg-black sm:h-screen bg-opacity-90">
                        <div class="flex-row w-full px-2 py-2">
                        <button data-button="close" class="float-right w-10 h-10 close"></button>
                        </div>
                        <div class="flex flex-row w-full h-full mt-6">
                        <div id="audioMenu" class="flex-col float-left w-6/12 text-3xl font-bold text-center lg:mx-8">
                            Audio
                            <div id="audios" class="flex flex-col w-full h-full mt-4 overflow-x-hidden overflow-y-auto text-base xl:px-4 lg:mt-10 lext-left">
                            </div>
                        </div>
                            <div id="subtitleMenu" class="flex-col float-left w-6/12 text-3xl font-bold text-center lg:mx-8">
                                Subtitles
                                <div id="subtitles" class="flex flex-col w-full h-full mt-4 overflow-x-hidden overflow-y-auto text-base xl:px-4 lg:mt-10 lext-left">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="seasonsMenu" class="absolute top-0 right-0 flex-col w-full h-full p-5 bg-black sm:h-screen bg-opacity-90">
                        <div class="flex flex-row w-full px-2 py-2">
                            <button data-button="season-back" class="w-3/12 h-4 left-arrow" style="height: 45px;"></button>
                            <div id="season-menu-name" class="w-full h-12 py-4 text-2xl text-center float-center text-bold"></div>
                            <button data-button="close" class="w-10 h-10 close"></button>
                        </div>
                        <div id="season-forward-container" class="flex-col hidden w-full h-full px-2 overflow-y-auto season-forward-container"></div>
                        <div id="seasons-container" class="w-full h-full overflow-hidden seasons-container"></div>

                    </div>


                    <div id="qualityMenu" class="absolute flex-col hidden p-5 bg-black bg-opacity-50">
                        <div class="flex flex-row w-full px-2 py-2">
                            <div class="w-full h-12 py-4 text-2xl text-center float-center text-bold"></div>
                            <button data-button="quality" class="w-10 h-10 px-3 bg-gray-800 qual" onclick="$('.qualityMenuItem').hide();$('#qualities').show();">Q</button>
                            <button data-button="aspect" class="w-10 h-10 px-3 bg-gray-800 aspect" onclick="$('.qualityMenuItem').hide();$('#aspects').show();">A</button>
                            <button data-button="close" class="w-10 h-10 px-3 close"></button>
                        </div>

                        <div id="aspects" class="flex-col hidden w-full h-full px-2 overflow-y-auto qualityMenuItem">
                            <div class="flex-row w-full h-8 px-8 py-2 text-right" onclick="nmplayer.changeAspectRatio('contain');$('.activeAspectItem').hide();$('#active_aspect_contain').show()"">
                                <p id="aspect_contain" class="float-right w-5/12 pl-3 text-right">Contain</p>
                                <div id="active_aspect_contain" class="float-left w-2/12 pr-3 activeAspectItem">✔</div>
                            </div>
                            <div class="flex-row w-full h-8 px-8 py-2 text-right" onclick="nmplayer.changeAspectRatio('cover');$('.activeAspectItem').hide();$('#active_aspect_cover').show()"">
                                <p id="aspect_contain" class="float-right w-5/12 pl-3 text-right">Cover</p>
                                <div id="active_aspect_cover" class="hidden float-left w-2/12 pr-3 activeAspectItem">✔</div>
                            </div>
                            <div class="flex-row w-full h-8 px-8 py-2 text-right" onclick="nmplayer.changeAspectRatio('fill');$('.activeAspectItem').hide();$('#active_aspect_fill').show()"">
                                <p id="aspect_contain" class="float-right w-5/12 pl-3 text-right">Fill</p>
                                <div id="active_aspect_fill" class="hidden float-left w-2/12 pr-3 activeAspectItem">✔</div>
                            </div>
                        </div>
                        <div id="qualities" class="flex-col w-full h-full px-2 overflow-y-auto qualityMenuItem"></div>
                    </div>
                    <div id="subtitle-window" class="subtitle-window"></div>
                </div>
            `);
        });

        jwplayer().on("setupError", (e) => {

        });
        jwplayer().on("ready", (e) => {

        });
        jwplayer().on("playlist", (e) => {

        });
        jwplayer().on("playlistItem", (e) => {
            loadQualities();
        });
        jwplayer().on("viewable", (e) => {
            addEventListeners();
        });
        jwplayer().on("captionsList", () => {

        });
        jwplayer().on("audioTracks", (e) => {

        });
        jwplayer().on("cast", (e) => {

        });
        jwplayer().on("displayClick", (e) => {

        });



        jwplayer().on("beforePlay", (e) => {

            buttonDisplay();
            hideQualityMenu();
        });
        jwplayer().on("play", (e) => {

            clearTimeout(theTimeout);
            lock = false;
            clearToggles();
            hideControls();
            $('#spinner').css("display", "none");
            $("#btn-playback").toggleClass("pause", "play");

            $('#remaining-time').text(humanTime(jwplayer().getDuration() - jwplayer().getPosition()));

            $('#btn-live').removeClass('bg-red-700');
            $('#btn-live').addClass('bg-blue-700');

        });
        jwplayer().on("pause", (e) => {
            $("#btn-playback").toggleClass("pause", "play");
            hideControls();
        });
        jwplayer().on("stop", (e) => {
            $("#btn-playback").toggleClass("pause", "play");
            lock = true;
            showControls();
        });

        jwplayer().on("firstFrame", (e) => {
            if (jwplayer().getMute()) {
                $("#btn-mute-mobile").toggleClass("muted", "unmuted");
            };

        });
        jwplayer().on("beforeComplete", (e) => {

        });
        jwplayer().on("complete", (e) => {

        });
        jwplayer().on("playlistComplete", (e) => {

        });

        jwplayer().on("error", (e) => {
            tryStream();
        });
        jwplayer().on("mediaError", (e) => {
            tryStream();
        });

        jwplayer().on("mute", (e) => {
            if (e.mute) {
                $("#btn-mute-mobile").removeClass("unmuted");
                $("#btn-mute-mobile").addClass("muted");
            } else {
                $("#btn-mute-mobile").removeClass("muted");
                $("#btn-mute-mobile").addClass("unmuted");
            }
        });
        jwplayer().on("fullscreen", (e) => {
            $("#btn-fullscreen").toggleClass("fullscreen-not", "fullscreen");
        });
        jwplayer().on("time", (e) => {

            $('.current-time').text(humanTime(e.position));
            $('#btn-live').css('display', 'flex');

            if (jwplayer().getDuration() != 'Infinity') {
                $('#remaining-time').text(humanTime(jwplayer().getDuration() - jwplayer().getPosition()))
                $("#slider>#time").css("width", `${(jwplayer().getPosition() / jwplayer().getDuration()) * 100}%`);
                $('#slider').css('display', 'none');
            }
            else {
                $('#slider').css('display', 'flex');
                $('#remaining-time').text('Live');
                $('#remaining-time').addClass('live');
            }









            if (jwplayer().getCurrentCaptions() == 0 && jwplayer().getPlaylistItem().tracks) {
                $('.subtitle-window').text('');
                $('.subtitle-window').css('display', 'none');
            }
            else {
                getCue(e.position);
            }

            if (jwplayer().getCurrentQuality() == 0 && jwplayer().getQualityLevels().length > 1) {
                $('#auto_label').text(`(${jwplayer().getVisualQuality().level.height}p) `)
            }
            else if (jwplayer().getQualityLevels().length > 1) {
                $('#auto_label').text('');
            }



        });

        jwplayer().on("bufferChange", (e) => {

            let percentage = (jwplayer().getPosition() + jwplayer().getBuffer()) / jwplayer().getDuration() * 100;
            percentage = percentage > 100 ? 100 : percentage;
            $("#slider>#buffer").css("width", percentage + "%");
        });
        jwplayer().on("seek", (e) => {
            $('#spinner').css('display', 'block');
            $(".subtitle-window").html('');
            window.newTime = 0;
            var percentage = (e.offset / jwplayer().getDuration()) * 100;
            $("#slider span").css("width", percentage + "%");
            $('.current-time').text(humanTime(e.offset));
        });
        jwplayer().on("seeked", (e) => {
            $('#spinner').css('display', 'none');
            jwplayer().play();
        });

        jwplayer().on("buffer", (e) => {
            if(e.reason == 'stalled'){
                jwplayer().stop();
                jwplayer().play();
            };
        });

        function tryStream() {

            $('#overlay button').css('display', 'none');
            $('#spinner').css('display', 'none');
            $('#remaining-time').text('Offline');
            $('#btn-live').text('Offline');

            $('#btn-live').removeClass('bg-blue-700');
            $('#btn-live').addClass('bg-red-700');
            $('#current-time').text('');
            jwplayer().stop();

            theTimeout = setInterval(function () {
                $.ajax({
                    url: `/activestreams/${f.jwplayerSetup.name}.txt?${Math.floor(Math.random() * 101000)}`,
                    dataType: "text",
                    success: function (data) {
                        if (data == `live\n`) {
                            console.log(data);
                            $('#spinner').css('display', 'block');
                            $('#remaining-time').text('Live');
                            $('#overlay button').css('display', 'flex');
                            $('#btn-live').removeClass('bg-red-700');
                            $('#btn-live').addClass('bg-blue-700');
                            clearTimeout(theTimeout);
                            jwplayer().play();

                        }
                        else {
                            jwplayer().stop();
                            console.error(data);
                        }
                    },
                    error: function () {
                        jwplayer().stop();
                    }
                });
            }, 5000);

        }

        // Button toggle functionality.

        function clearToggles() {
            lock = false;
            optionsMenuToggle = false;
            audioMenuToggle = false;
            qualityMenuToggle = false;
        }

        function toggleQualityMenu() {
            hideQualityMenu();

            $('[data-button="level"]').click(function () {
                qualityMenuToggle = false;
                $('#qualityMenu').css("display", "none");
                jwplayer().play();
            });

            if (qualityMenuToggle == false) {
                showQualityMenu();
            }
            else {
                hideQualityMenu();
            }
        };
        function showQualityMenu() {
            qualityMenuToggle = true;
            $('#qualityMenu').css("display", "flex");
        }
        function hideQualityMenu() {
            qualityMenuToggle = false;
            $('#qualityMenu').css("display", "none");
        }


        function dynamicControls() {
            showControls();
            clearTimeout(timer);
            if (lock != true && optionsMenuToggle != true && seasonsMenuToggle != true && qualityMenuToggle != true) {
                timer = setTimeout(hideControls, 3000);
            }
        };
        function showControls() {
            $("#overlay").addClass("active");
            $("video").css("cursor", "unset");
        };
        function hideControls() {
            if (lock != true) {
                $("#overlay").removeClass("active");
                $("video").css("cursor", "none");
            }
        };

        function toggleFullscreen() {
            jwplayer().setFullscreen();
            sleep(1500);
        };
        function togglePlayback() {
            switch (jwplayer().getState()) {
                case 'paused':
                    jwplayer().play();
                    break;
                case 'playing':
                    jwplayer().pause();
                    break;
                default:
                    break;
            }
        };
        function toggleMute() {
            if (jwplayer().getMute()) {
                jwplayer().setMute(0);
            } else {
                jwplayer().setMute(1);
            }
        };

        const rewindVideo = () => {
            tapCount = tapCount + 10;
            $(".rewind.notification").css('display', 'flex')
            $("span.rewind").text(`${Math.abs(tapCount)} seconds`);
            clearTimeout(tap);
            tap = setTimeout(function () {
                $(".rewind.notification").css('display', 'none')
                jwplayer().seek(jwplayer().getPosition() - tapCount);
                tapCount = 0;
                isSeeking = false;
            }, 500);
        };
        const forwardVideo = () => {
            tapCount = tapCount + 10;
            $(".forward.notification").css('display', 'flex')
            $("span.forward").text(`${Math.abs(tapCount)} seconds`);
            clearTimeout(tap);
            tap = setTimeout(function () {
                $(".forward.notification").css('display', 'none');
                jwplayer().seek(jwplayer().getPosition() + tapCount);
                tapCount = 0;
                isSeeking = false;
            }, 500);
        };

        function goBack() {
            if (jwplayer().getPlaylistItem().video_type != "movie") {
                location.href = "/channels/";
            }
        };


        function _changeSource(source) {
            $(".subtitle-window").html('');
            const vid = document.querySelector('video');
            if (typeof jwplayer_hls_provider != "undefined") {
                jwplayer().hls.detachMedia()
                jwplayer().hls = new Hls();
                jwplayer().hls.loadSource(source);
                jwplayer().hls.attachMedia(vid);
                jwplayer().hls.on(Hls.Events.MANIFEST_PARSED, (e) => {
                    setSubtitleTrack();
                });
            }
        };


        function buttonDisplay() {
            if (jwplayer().getPlaylist().length > 1) {
                $("#previous").css("display", "flex");
                $("#next").css("display", "flex");
            }
            if (jwplayer().getPlaylistIndex() > 0 && jwplayer().getPlaylist().length > 1) {
                $("#previous").css("display", "flex");
                $("#previous").attr("title", jwplayer().getPlaylist()[jwplayer().getPlaylistIndex() - 1].title);
            } else {
                $("#previous").css("display", "none");
            }
            if (!jwplayer().getPlaylist()[jwplayer().getPlaylistIndex() + 1] && jwplayer().getPlaylist().length >= 1) {
                $("#next").css("display", "none");
            } else {
                $("#next").css("display", "flex");
                $("next").attr("title", jwplayer().getPlaylist()[jwplayer().getPlaylistIndex() + 1].title);
            }
            let last;
            for (i = jwplayer().getPlaylistIndex(); i < jwplayer().getPlaylist().length; i++) {
                if (jwplayer().getPlaylist()[i].file !== null) {
                    last = i;
                    this.last = i;
                }
            }
            if (jwplayer().getPlaylistIndex() == last) {
                $("#next").css("display", "none");
            }

        };




        // Sync or storage functions.


        function setSubtitleTrack() {
            if (typeof localStorage !== 'undefined') {
                jwplayer().getPlaylistItem().tracks.forEach((e, index) => {
                    if (e.name == localStorage.getItem('jwplayer.captionLabel')) {
                        jwplayer().setCurrentCaptions(index);
                    }
                });

            }
        };


        this.previous = () => {
            if (jwplayer().getPlaylistIndex() === 0) {

            } else {
                jwplayer().playlistPrev()
            }
            sleep(2500);
        };
        this.next = () => {
            jwplayer().playlistNext()
            sleep(2500);
        };
        this.setEpisode = (season, episode) => {
            jwplayer().getPlaylist().forEach((e, index) => {
                if (urlQuery.get('type') == null) {
                    if (e.season == season && e.episode == episode) {
                        jwplayer().playlistItem(index);
                    }
                }
                else {
                    if (index == episode) {
                        jwplayer().playlistItem(index);
                    }
                }
            });
        };


        // Tools.

        function scroll_to_div(hash) {
            let target = $(hash);
            headerHeight = 200;
            if (target.length) {
                $(".active>.episode-list").stop().animate({
                    scrollTop: target.offset().top - headerHeight // offsets for fixed header
                }, "linear");
            }
        }
        function humanTime(time) {
            var st;
            ct = parseInt(time); // ct is current time

            if (st != ct) {
                st = ct;
                var hours = parseInt(st / 3600);
                var minutes = parseInt(
                    (st % 3600) / 60
                );
                var seconds = parseInt(st % 60);
                if (("" + minutes).length == 1) {
                    minutes = "0" + minutes;
                }
                if (("" + seconds).length == 1) {
                    seconds = "0" + seconds;
                }
                if (hours != 0) {
                    var hours = "" + hours + ":";
                } else {
                    var hours = "";
                }
                current = hours + minutes + ":" + seconds;
            }
            return current.replace("NaN:NaN:NaN", "00:00");
        };
        function convertToSeconds(hms) {
            var a = hms.split(':');
            return (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
        }

        function zeroPad(num, places) {
            var zero = places - num.toString().length + 1;
            return Array(+(zero > 0 && zero)).join("0") + num;
        };
        function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        };




        // Events.

        function onClick(e) {
            console.log(e);
            switch (e.target.dataset.button) {
                case "back":
                    goBack();
                    break;
                case "close":
                    jwplayer().play();
                    break;
                case "quality":
                    toggleQualityMenu();
                    break;
                case "options":
                    toggleOptionsMenu();
                    break;
                    break;
                case "previous":
                    nmplayer.previous();
                    break;
                case "next":
                    nmplayer.next();
                    break;
                case "playback":
                    togglePlayback();
                    break;
                case "fullscreen":
                    toggleFullscreen();
                    break;
                case "seasons":
                    loadSeasons();
                    toggleSeasonsMenu();
                    break;
                case "season-back":
                    _seasonBack();
                    break;
                case "season-forward":
                    _seasonForward(e.target.dataset.season || 'Specials');
                    break;
                case "mute":
                    toggleMute();
                    break;
                case "slider":
                    break;
                case "session":
                    changeAspect();
                    break;
                case "href":
                    window.location = e.target.dataset.href;
                    break;
                case "subtitle":
                    jwplayer().setCurrentCaptions(e.target.dataset.track);
                    break;
                case "audio":
                    if (typeof jwplayer_hls_provider != "undefined") {
                        jwplayer().hls.audioTrack = e.target.dataset.track;
                    }
                    else {
                        jwplayer().setCurrentAudioTrack(e.target.dataset.track);
                    }
                    break;
                default:
                    console.log("no data-button property set");
                    break;
            }
        };
        function onVideoClick(e) {

            if (audioMenuToggle || optionsMenuToggle || qualityMenuToggle || seasonsMenuToggle || lock) {
                hideQualityMenu();
            }

            // if(!isMobile.any()){
            //     togglePlayback();
            // }
            if (isSeeking) {
                if (e.pageX < Math.floor(window.innerWidth / 3.5)) {
                    rewindVideo();
                }
                if (e.pageX > (window.innerWidth - Math.floor(window.innerWidth / 3.5))) {
                    forwardVideo();
                }
            }
        };
        function onVideoDblclick(e) {
            const video = document.querySelector('video');
            e.preventDefault();
            e.stopPropagation();
            if (e.pageX < Math.floor(window.innerWidth / 3.5)) {
                isSeeking = true;
                rewindVideo();
            }
            if (e.pageX > (window.innerWidth - Math.floor(window.innerWidth / 3.5))) {
                isSeeking = true;
                forwardVideo();
            }
            if (e.pageX > Math.floor(window.innerWidth / 3.5) && e.pageX < (window.innerWidth - Math.floor(window.innerWidth / 3.5))) {
                if (e.pageY < (window.innerHeight - Math.floor(window.innerHeight / 1.4))) {
                    if (video.volume != 1) {
                        video.volume = video.volume + 0.1;
                    }
                }
                if ((e.pageY > (window.innerHeight - Math.floor(window.innerHeight / 1.4)) && e.pageY < Math.floor(window.innerHeight / 1.6)) || !isMobile.any()) {
                    isSeeking = false;
                    toggleFullscreen();
                }
                if (e.pageY > Math.floor(window.innerHeight / 1.6)) {
                    video.volume = video.volume - 0.1;
                }
            }
        };

        function addEventListeners() {
            $("#overlay [data-button]").click(function (e) { onClick(e) });
            $("#overlay").dblclick(function (e) { onVideoDblclick(e) });
            $("#overlay-center").mousemove(function (e) { dynamicControls(e) });
            $("#overlay-center").click(function (e) { onVideoClick(e) });
            document.onkeydown = function (e) {
                switch (e.keyCode) {
                    case 107:
                        offset = offset + 1;
                        clearTimeout(message);
                        $('#player-message').css('display', 'block');
                        $('#player-message').html(`Subtitle delay ${offset * 10}ms`);
                        message = setTimeout(() => {
                            $('#player-message').css('display', 'none');

                        }, 2000);
                        break;
                    case 109:
                        offset = offset - 1;
                        clearTimeout(message);
                        $('#player-message').css('display', 'block');
                        $('#player-message').html(`Subtitle delay ${offset * 10}ms`);
                        message = setTimeout(() => {
                            $('#player-message').css('display', 'none');

                        }, 2000);
                    case 32:
                        jwplayer().playToggle()
                        break;
                    case 37:
                        rewindVideo()
                        break;
                    case 39:
                        forwardVideo()
                        break;

                    default:
                        break;
                }
            }
            screen.orientation.addEventListener("change", function (e) {
                toggleFullscreen();
            }, false);
        };



        // Dynamic builds based on player data.

        function clearLists() {
            $(`#subtitles`).empty();
            $('#audios').empty();
            $('#qualityMenu').empty();
        }


        function loadAudios() {

            $(".activeAudioItem").css("display", "none");
            $('#audios').empty();

            if (typeof jwplayer_hls_provider != "undefined") {

            }
            if (typeof jwplayer_hls_provider == "undefined") {
                jwplayer().on('audioTracks', (e) => {
                    $('#audios').empty();
                    if (jwplayer().getAudioTracks().length > 1) {
                        $('#options').css("display", "flex");
                    }
                    jwplayer().getAudioTracks().forEach((data, index) => {
                        $('#audios').append(`
                            <div class="flex-row w-full h-8 py-2" data-button="audio" data-track="${index}">
                                <p id="audio_${data.name}" class="float-right w-11/12 pl-3 text-left" data-button="audio" data-track="${index}">${data.name}
                                    <div id="active_audio_${data.name}" class="hidden float-left w-1/12 pr-3 activeSubItem" data-button="audio" data-track="${index}">✔</div>
                                </p>
                            </div>
                        `);
                    });
                    setTimeout(() => {
                        $("#active_audio_" + jwplayer().getAudioTracks()[jwplayer().getCurrentAudioTrack()].name).css("display", "block");
                    }, 1500);
                });

                jwplayer().on('audioTrackChanged', (event, data) => {
                    hideOptionsMenu();
                    $(".activeAudioItem").css("display", "none");
                    $("#active_audio_" + jwplayer().getAudioTracks()[jwplayer().getCurrentAudioTrack()].name).css("display", "block");
                    if (typeof localStorage !== 'undefined') {
                        localStorage.setItem("jwplayer.audioLabel", jwplayer().getAudioTracks()[jwplayer().getCurrentAudioTrack()].name);
                    }
                });
            }
        }

        if (typeof jwplayer_hls_provider != "undefined") {

        }
        if (typeof jwplayer_hls_provider == "undefined") {
            jwplayer().on('levels', (e, index) => {

                $('#qualities').empty();

                if (jwplayer().getQualityLevels().length > 1) {
                    $("#quality").css("display", "flex");
                }
                jwplayer().getQualityLevels().forEach((data, i) => {
                    $('#qualities').append(`
                        <div class="flex-row w-full h-8 px-8 py-2 text-right" data-button="level" data-track="${i}" style="background:${i % 2 == 0 ? '#0a090980' : '#000000ba'}" onclick="jwplayer().setCurrentQuality(${i})">
                        <p id="level_${data.label}" class="float-right w-11/12 pl-3 text-right" data-button="level" data-track="${i}">${data.label == 0 ? 'Auto' : data.label}</p>
                            ${i == 0 ? `<div id="auto_label" class="float-right w-11/12 pl-3 text-right"></div>` : `<div id="auto_label"> </div>`}
                            <div id="active_level_${i}" class="hidden float-left w-1/12 pr-3 activeLevelItem" data-button="level" data-level="${i}">✔</div>
                        </div>
                    `);
                });
                setTimeout(() => {
                    $(`#active_level_${jwplayer().getCurrentQuality()}`).css("display", "block");
                }, 500);
            });

            jwplayer().on('levelsChanged', (event, data) => {
                $(".activeLevelItem").css("display", "none");
                $(`#active_level_${jwplayer().getCurrentQuality()}`).css("display", "block");
                jwplayer().play();
            });
        }
        function loadQualities() {

        }

        function getCue(time) {
            $("#subtitle-window").html('');
            if (typeof jwplayer().getPlaylistItem().tracks[jwplayer().getCurrentCaptions()] != 'undefined') {
                let currentTime = time - (offset / 10);
                jwplayer().getPlaylistItem().tracks[jwplayer().getCurrentCaptions()].data.forEach(cue => {
                    if (currentTime >= cue.startTime && currentTime <= cue.endTime) {
                        $(".subtitle-window").html(cue.text);
                    }
                });
            }
        };

    }
}
