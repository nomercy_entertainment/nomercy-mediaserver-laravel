videojs.registerPlugin('nmsync', function () {

    // onChange(player.activePlugins_.nmplayer, () => player.trigger('setupComplete'));

    function episodeData() {
        let playlist = player.playlist()[player.playlist.currentIndex_];
        let audioLabel = player.audioTracks().tracks_[player.getCurrentAudio()] ? player.audioTracks().tracks_[player.getCurrentAudio()].label : null;
        let captionLabel = player.textTracks().tracks_[player.getCurrentCaption()] ? player.textTracks().tracks_[player.getCurrentCaption()].label : null;
        let obj = {
            profile_id: user.id,
            video_id: playlist.uid,
            tmdb_id: playlist.tmdbid,
            type: playlist.video_type,
            audio: audioLabel,
            subtitle: captionLabel,
            time: Math.floor(player.currentTime() || 0),
            _token: player.options_.bearer,
        };
        return obj;
    }

    function load() {
        let playlist = player.playlist()[0];
        let data = {
            tmdb_id: playlist.tmdbid,
            type: playlist.video_type,
            _token: player.options_.bearer,
        };
        switch (player.options_.storage) {
            case 'socket':
                socket.emit('load', data);
                break;
            case 'server':
                $.ajax({
                    url: `/sync/get`,
                    type: 'POST',
                    headers: {
                        Authorization: 'Bearer ' + player.options_.bearer,
                        _token: player.options_.bearer,
                    },
                    data: data,
                    success: function (data) {
                        console.log(typeof data.length == 'undefined');
                        if(typeof data.length == 'undefined'){
                            defaultStart();
                        }
                        else{
                            setEpisode(data, true);
                        }
                    },
                    error: function (data) {
                        console.warn({ getTime: data.responseJSON });
                        defaultStart()
                    },
                });


                break;

            default:
                defaultStart();
                break;
        }
    }

    function getTime() {
        let playlist = player.playlist()[player.playlist.currentIndex_];
        let data = {
            tmdb_id: playlist.tmdbid,
            type: playlist.video_type,
            video_id: playlist.uid,
            _token: player.options_.bearer,
        };
        switch (player.options_.storage) {
            case 'socket':
                socket.emit('getTime', data);
                break;
            case 'server':
                $.ajax({
                    url: `/sync/get`,
                    type: 'POST',
                    headers: {
                        Authorization: 'Bearer ' + player.options_.bearer,
                        _token: player.options_.bearer,
                    },
                    data: data,
                    success: function (data) {
                        // console.log({ getTime: data });
                        setEpisode(data, false);
                    },
                    error: function (data) {
                        console.warn({ getTime: data.responseJSON });
                        defaultStart()
                    },
                });

                break;

            default:

                break;
        }
    }

    function setTime() {
        let data = episodeData();
        switch (player.options_.storage) {
            case 'socket':
                socket.emit('setTime', data);
                break;
            case 'server':
                $.ajax({
                    url: `/sync/set`,
                    type: 'POST',
                    headers: {
                        Authorization: 'Bearer ' + player.options_.bearer,
                        _token: player.options_.bearer,
                    },
                    data: data,
                    success: function (data) {
                        // console.log({ setTime: data });
                    },
                    error: function (data) {
                        console.warn({ setTime: data.responseJSON });
                    },
                });

                break;

            default:

                break;
        }
    }

    function setEpisode(data, change) {
        console.log(data);

        if(change){
            let item = player.playlist().findIndex(p => p.uid == data.video_id);
            player.playlist.currentItem(item);
        }
        setTimeout(() => {
            player.currentTime(data.time);
            player.one('canplay', function () {
                player.setSubtitleTrack(data.subtitle || localStorage.getItem('player.captionLabel'));
                player.setAudioTrack(data.audio || localStorage.getItem('player.audioLabel'));
            });
        }, 500);

    }

    function defaultStart() {
        if (player.playlist().length > 1) {
            player.setEpisode(1, 1);
        } else {
            player.playlist.currentItem(0);
        }
        player.one('canplay', function () {
            player.setSubtitleTrack(localStorage.getItem('player.captionLabel'));
            player.setAudioTrack(localStorage.getItem('player.audioLabel'));
        });
    }

    switch (player.options_.storage) {
        case 'socket':

            break;
        case 'server':

            break;

        default:

            break;
    }

    player.on('setupComplete', function () {
        this.newTime = 0;
        if (player.getParameterByName('season') == null && player.getParameterByName('episode') == null) {
            load();
        }
        else {
            player.setEpisode(player.getParameterByName('season'), player.getParameterByName('episode'));
            setTimeout(() => {
                getTime();
            }, 500);
        }

        switch (player.options_.storage) {
            case 'socket':
                socket.on("load", (data) => {
                    if (data != null) {
                        setEpisode(data, true);
                    }
                    else {
                        defaultStart();
                    }
                });

                socket.on("getTime", (data) => {
                    if (data != null) {
                        setEpisode(data, false);
                    }
                    else {
                        defaultStart();
                    }
                });

                break;
            case 'server':

                break;

            default:

                break;
        }

    });

    player.on('seeked', () => {
        setTime();
    });
    player.on('timeupdate', () => {
        if (player.currentTime() > (player.newTime + 5)) {
            this.newTime = player.currentTime();
            setTime();
        }
    });

    player.on('ended', () => {
        this.newTime = 0;
    });

    player.on('playlistitem', function () {
        this.newTime = 0;
    });
    player.on('play', function () {
        this.newTime = 0;
    });

    this.nmsync_initialized = true;
});

            // localStorage.setItem('player.captionLabel', textTracks.tracks_[currentCaption].label.replace('segment-metadata', 'Off').replace(' ', '_'));
