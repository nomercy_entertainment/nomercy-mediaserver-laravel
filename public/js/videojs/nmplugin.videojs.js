
function onChange(item, cb) {
    if(item == false) {
        window.setTimeout(onchange(item, cb), 100);
    } else {
        cb();
    }
}

videojs.registerPlugin('nmplayer', function () {

    if (!location.pathname.startsWith('/streams')) {
        $("nav").addClass('hidden');
        $("video").append('<div class="libassjs-canvas-parent"></div>');
    }

    // Tools.

    function scroll_to_div(hash) {
        let target = $(hash);
        let headerHeight = 200;
        if (target.length) {
            $(".active>.episode-list").stop().animate({
                scrollTop: target.offset().top - headerHeight // offsets for fixed header
            }, "linear");
        }
    }

    function humanTime(time) {
        time = parseInt(time);
        let hours = parseInt(time / 3600);
        let minutes = parseInt(
            (time % 3600) / 60
        );
        let seconds = parseInt(time % 60);
        if (("" + minutes).length == 1) {
            minutes = "0" + minutes;
        }
        if (("" + seconds).length == 1) {
            seconds = "0" + seconds;
        }
        if (hours != 0) {
            hours = "" + hours + ":";
        } else {
            hours = "";
        }
        current = hours + minutes + ":" + seconds;
        return current.replace("NaN:NaN:NaN", "00:00");
    }

    function convertToSeconds(hms) {
        let a = hms.split(':');
        return (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
    }

    function zeroPad(num, places) {
        let zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    }

    function sleep(ms) {
        return new Promise(function (resolve) { setTimeout(resolve, ms); });
    }

    function loadPlaylist(theUrl) {
        if (typeof player.options_.playlist == 'string') {
            var xhr = new XMLHttpRequest();
            xhr.open("GET", theUrl);
            xhr.setRequestHeader('Accept-Language', player.options_.locale || 'en');
            xhr.send(null);
            xhr.onload = function (e) {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        playlist = JSON.parse(xhr.responseText);
                        setPlaylist(playlist);
                    }
                    else {
                        console.error(xhr.statusText);
                    }
                }
            };
            xhr.onerror = function (e) {
                console.error(xhr.statusText);
            };
        }
        else if (typeof player.options_.playlist == 'object') {
            playlist = player.options_.playlist;
            setPlaylist(playlist);
        }

    }
    function setPlaylist(playlist){
        if (playlist.length == 1) {
            player.playlist(playlist, 0);
        } else {
            player.playlist(playlist, 0);
            player.playlist.autoadvance(1);

        }
        player.trigger('setupComplete');
    }

    this.getParameterByName = (name, url = window.location.href) => {
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    };

    player.lock = false;
    let optionsMenuToggle = false,
        settingsMenuToggle = false,
        seasonsMenuToggle = false,
        timer,
        tap = null,
        tapCount = 10,
        isSeeking = false,
        message,
        currentCaption = -1,
        currentAudio = 0,
        currentQuality = 0,
        leeway = 500,
        remainingTime,
        qualityLevels,
        textTracks,
        audioTracks,
        current,
        next,
        translations,
        // playlist,
        playlistItem,
        interacted = false
        ;

    let isMobile = {
        Android: function () {
            if (navigator.userAgent.match(/Android/i)) {
                return true;
            }
        },
        BlackBerry: function () {
            if (navigator.userAgent.match(/BlackBerry/i)) {
                return true;
            }
        },
        iOS: function () {
            if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
                return true;
            }
        },
        Opera: function () {
            if (navigator.userAgent.match(/Opera Mini/i)) {
                return true;
            }
        },
        Windows: function () {
            if (
                navigator.userAgent.match(/IEMobile/i) ||
                navigator.userAgent.match(/WPDesktop/i)
            ) {
                return true;
            }
        },
        any: function () {
            return (
                isMobile.Android() ||
                isMobile.BlackBerry() ||
                isMobile.iOS() ||
                isMobile.Opera() ||
                isMobile.Windows()
            );
        }
    };

    translations = JSON.parse(player.options_.translations);

    $(`#${player.player_.el().id}`).prepend(`
        <div id="overlay" class="absolute flex-col w-full h-screen p-0 mx-0 text-white">
            <div id="overlay-top" class="absolute left-0 flex flex-row w-full h-20 px-8 py-3 md:mr-8 lg:px-16" data-rows="0">
                <button data-type="ui" data-menu="false" data-row="0" data-col="0" data-selected="false" aria-label="Back" data-button="back" id="back" class=""><span class="back"/></button>
                <button data-type="ui" data-menu="false" data-row="0" data-col="1" data-selected="false" aria-label="Settings" data-button="settings" data-menu="settings" id="setting" class=""><span class="setting"/></button>
                <button data-type="ui" data-menu="false" data-row="0" data-col="2" data-selected="false" aria-label="Seasons" data-button="seasons" data-menu="seasons"  id="seasons" class="hidden "><span class="playlist"/></button>
                <button data-type="ui" data-menu="false" data-row="0" data-col="3" data-selected="false" aria-label="Previous" data-button="previous" id="previous" class="hidden "><span class="previous"/></button>
                <button data-type="ui" data-menu="false" data-row="0" data-col="4" data-selected="false" aria-label="Next" data-button="next" id="next" class="hidden "><span class="next"/></button>
                <button data-type="ui" data-menu="false" data-row="0" data-col="5" data-selected="false" aria-label="Cast" data-button="cast" id="cast" class="hidden"><span class="cast"/></button>
            </div>
            <div id="overlay-center" class="flex flex-row w-full h-full p-0 mx-0">
                <div id="player-message" class="row player-message text-2xl"></div>
                <button aria-label="Play" id="btn-play-mobile" aria-label="Playback" data-button="playback" class="md:hidden" type="button"></button>
                <div id="spinner" class="spinner"></div>
                <div class="hidden nextup z-100" onclick="player.nextClick()">
                    <div class="bg"></div>
                    <div class="triangle"></div>
                    &nbsp;&nbsp;  next episode
                </div>
                <div class="absolute top-0 left-0 hidden w-6/12 h-screen pr-2 py-auto lg:w-3/12 rewind notification">
                    <div class="flex flex-col items-center justify-center w-full h-full pr-2 md:flex-row rewind-icon icon" style="background-color: #ffffff2e;border-radius: 0% 150% 150% 0%/ 100% 100% 100% 100%;">
                        <div class="flex flex-col h-20">
                            <span class="seek-back" type="button"></span>
                            <span class="text-center sm:mx-auto lg:mx-2 rewind text-2xl">10 seconds</span>
                        </div>
                    </div>
                </div>
                <div class="absolute top-0 right-0 hidden w-6/12 h-screen pl-2 py-auto lg:w-3/12 forward notification">
                    <div class="flex flex-col items-center justify-center w-full h-full pl-2 md:flex-row forward-icon icon" style="background-color: #ffffff2e;border-radius: 150% 0% 0% 150%/ 100% 100% 100% 100%;">
                        <div class="flex flex-col h-20">
                            <span class="seek-forward" type="button"></span>
                            <span class="text-center sm:mx-auto lg:mx-2 forward text-2xl">10 seconds</span>
                        </div>
                    </div>
                </div>
            </div>
            <div id="slider-container" data-button="slider pt-6" class="w-11/12 mx-auto">
                <div id="slider" data-button="slider" class="relative left-0 w-full">
                    <div id="slider-pop" data-button="slider-pop" class="hidden">
                        <div id="slider-image" src"#"></div>
                        <div id="slider-time" class="time"></div>
                    </div>
                    <span id="buffer" data-button="buffer"></span>
                    <span id="time" data-button="slider"></span>
                    <span id="time-nipple" class="hidden" data-button="slider"></span>
                </div>
            </div>
            <div id="overlay-bottom" class="absolute bottom-0 left-0 flex flex-row w-full h-24 px-4 pb-2 mb-0 md:px-10 sm:pt-6 2xl:pt-4 md:mb-0 lg:px-12" data-rows="1">
                <button data-type="ui" data-menu="false" data-row="1" data-col="0" data-selected="true" aria-label="Playback" data-button="playback" class="" type="button"><span id="btn-playback" class="play pause"/></button>
                <button data-type="ui" data-menu="false" data-row="1" data-col="1" data-selected="false" aria-label="Mute" data-button="mute" class="" type="button"><span id="btn-mute" class="unmuted muted"/></button>
                <div id="current-time" class="w-24 h-12 pt-3 mr-4 text-lg text-center text-white 3xl:ml-16 3xl:pt-3 time current-time" >00:00</div>
                <div id="remaining-time" class="w-24 h-12 pt-3 pl-4 ml-auto text-lg text-center text-white 3xl:mr-32 3xl:pt-3 time remaining-time" >00:00</div>
                <button data-type="ui" data-menu="false" data-row="1" data-col="2" data-selected="false" aria-label="Full screen" data-button="fullscreen" class="" type="button"><span id="btn-fullscreen" class="fullscreen fullscreen-not"/></button>
            </div>

            <div id="seasonsMenu" class="absolute top-0 right-0 flex-col hidden h-full bg-black sm:h-screen bg-gray-800 border border-gray-700">
                <div class="flex flex-row justify-end w-full px-2 py-1 bg-gray-900 border-lg md:px-4 items-center" data-rows="0"">
                    <button data-type="menu" data-menu="season" data-row="1" data-col="1" data-selected="false" data-button="season-back" class="flex-col w-full p-2 overflow-y-auto text-base" style="height: 45px;"><span id="season-back" class="back"/></button>
                    <div id="season-menu-name" class="flex-col w-full p-2 overflow-y-auto text-base text-2xl text-center float-center text-bold"></div>
                    <button data-type="menu" data-menu="season" data-row="1" data-col="1" data-selected="false" data-button="close" class="flex-col w-full p-2 overflow-y-auto text-base"><span data-button="close" class="close"/></button>
                </div>
                <div id="season-forward-container" class="flex-col hidden w-full h-full px-2 overflow-y-auto season-forward-container"></div>
                <div id="seasons-container" class="w-full h-full overflow-hidden seasons-container"></div>

            </div>

            <div id="settingsMenu" class="absolute flex-col hidden h-64 left-0 ml-8 md:ml-32 top-0 mt-16 overflow-hidden bg-gray-800 bg-opacity-100 border border-gray-700">

                <div class="flex flex-row justify-end w-full px-2 py-1 bg-gray-900 border-lg md:px-4" data-rows="0">
                    <button data-type="menu" data-menu="settings" data-row="0" data-col="0" data-selected="false" id="aspect" data-button="aspect" class=" settingMenuButton opacity-50 hover:opacity-100"><span class="aspect"/></button>
                    <button data-type="menu" data-menu="settings" data-row="0" data-col="1" data-selected="false" id="language" data-button="language" class="hidden  settingMenuButton opacity-50 hover:opacity-100" ><span class="languages"/></button>
                    <button data-type="menu" data-menu="settings" data-row="0" data-col="2" data-selected="false" id="subtitle" data-button="subtitle" class="hidden  settingMenuButton opacity-50 hover:opacity-100" ><span class="subtitles"/></button>
                    <button data-type="menu" data-menu="settings" data-row="0" data-col="3" data-selected="false" id="level" data-button="level" class="hidden  settingMenuButton opacity-50 hover:opacity-100" ><span class="levels"/></button>
                    <button data-type="menu" data-menu="settings" data-row="0" data-col="4" data-selected="false" data-button="close" class="ml-auto hover:opacity-100"><span data-button="close" class="close"/></button>
                </div>

                <div id="aspects" class="flex-col hidden w-full p-2 overflow-y-auto text-base settingMenuItem" data-rows="0">
                    <div data-type="menu-item" data-menu="aspect" data-row="0" data-col="0" data=selected="true" class="flex flex-row h-10 px-4 py-2 opacity-100 cursor-pointer w-12/12 hover:opacity-100" onclick="player.changeAspectRatio('contain');">
                        <div id="active_aspect_contain" class="w-1/12 activeAspectItem">✔</div>
                        <p id="aspect_contain" class="float-left w-11/12 h-6 pr-3 ml-auto text-right">${translations.contain}</p>
                    </div>
                    <div data-type="menu-item" data-menu="aspect" data-row="1" data-col="0" data-selected="false" class="flex flex-row h-10 px-4 py-2  cursor-pointer w-12/12 opacity-50 hover:opacity-100" onclick="player.changeAspectRatio('cover');">
                        <div id="active_aspect_cover" class="hidden w-1/12 activeAspectItem">✔</div>
                        <p id="aspect_cover" class="float-left w-11/12 h-6 pr-3 ml-auto text-right">${translations.cover}</p>
                    </div>
                    <div data-type="menu-item" data-menu="aspect" data-row="2" data-col="0" data-selected="false" class="flex flex-row h-10 px-4 py-2  cursor-pointer w-12/12 opacity-50 hover:opacity-100" onclick="player.changeAspectRatio('fill');">
                        <div id="active_aspect_fill" class="hidden w-1/12 activeAspectItem">✔</div>
                        <p id="aspect_fill" class="float-left w-11/12 h-6 pr-3 ml-auto text-right">${translations.fill}</p>
                    </div>
                </div>
                <div id="qualities" class="flex-col hidden w-full p-2 overflow-y-auto text-base settingMenuItem" data-rows="0"></div>
                <div id="languages" class="flex-col hidden w-full p-2 overflow-y-auto text-base settingMenuItem" data-rows="0"></div>
                <div id="subtitles" class="flex-col hidden w-full p-2 overflow-y-auto text-base settingMenuItem" data-rows="0"></div>
            </div>
            <div id="subtitle-window" class="subtitle-window"></div>
        </div>
    `);

    player.on('ready', function (e) {
        loadPlaylist(player.options_.playlist);

        if (typeof player.nmsync == 'undefined') {

            if (player.getParameterByName('season') !== null && player.getParameterByName('episode') !== null) {
                player.setEpisode(player.getParameterByName('season'), player.getParameterByName('episode'));
            } else if (player.getParameterByName('type') !== null && player.getParameterByName('item') !== null) {
                player.setEpisode(0, player.getParameterByName('item'));
            } else {
                if (playlist.length > 1) {
                    player.setEpisode(1, 1);
                } else {
                    player.setEpisode(0, 0);
                }
            }
            // var promise = player.play();

            // if (promise !== undefined) {
            //     promise.then(function () {
            //         // Autoplay started!
            //     }).catch(function (e) {
            //         $('#btn-play-mobile').removeClass("pause").addClass("play").removeClass('hidden');
            //     });
            // }
        }
    });

    player.on('duringplaylistchange', function () {
        Object.keys(player).forEach(key => {
            player.off(key);
        });

        player.one('playing', function () {
            loadSeasons();
            if(!window.isIOS){
                toggleMute();
            }
        });

        player.on('play', function () {

            player.lock = false;
            $('#slider-pop').addClass('invisible');
            $("#btn-playback").removeClass("play").addClass("pause");
            $('#remaining-time').text(humanTime(player.duration() - player.currentTime()));

            if (player.muted()) {
                $("#btn-mute").removeClass("unmuted");
                $("#btn-mute").addClass("muted");
            } else {
                $("#btn-mute").removeClass("muted");
                $("#btn-mute").addClass("unmuted");
            }

            if(!player.options_.liveui){
                document.title = ` ${playlistItem.show} S${playlistItem.season}E${playlistItem.episode} ${playlistItem.title}`;
                document.description = playlistItem.description;
            }
            else{
                document.title = playlistItem.title;
                document.description = playlistItem.description;
            }

            hideMenus();
            hideControls();

            setTimeout(function () {
                buttonDisplay();
                player.loadPreviewSlider();
            }, leeway);

        });

        player.on('playing', function () {
            $("#time-nipple").addClass('hidden');
            $("#slider-pop").addClass('invisible');

            if (qualityLevels.length < 2 && audioTracks.tracks_.length < 2 && textTracks.tracks_.length < 1) {
                $('.settingMenuButton').removeClass('opacity-100').addClass('opacity-50');
                $('#aspect').removeClass('hidden').removeClass('opacity-50').addClass('opacity-100');
                $('#aspects').removeClass('hidden');
            }

            $('[data-menu="aspect"],[data-menu="quality"],[data-menu="language"],[data-menu="subtitle"]').click(function () {
                optionsMenuToggle = false;
                $('#settingsMenu').addClass('hidden');
                player.play();
            });

            $('.spinner-main,.spinner,#btn-play-mobile').addClass('hidden');
        });
        player.on('pause', function () {
            player.lock = true;
            $('#btn-play-mobile').removeClass('hidden');
            $("#btn-playback").removeClass("pause");
            $("#btn-playback").addClass("play");
            showControls();
        });
        player.on('playlistComplete', function () {
            player.goBack();
        });
        player.on('volumechange', function () {
            if (player.muted()) {
                $("#btn-mute").removeClass("unmuted");
                $("#btn-mute").addClass("muted");
            } else {
                $("#btn-mute").removeClass("muted");
                $("#btn-mute").addClass("unmuted");
            }
        });
        player.on('timeupdate', function () {
            let remaining = player.duration() - player.currentTime();
            if (remaining != Infinity) {
                $("#slider>#time").removeClass('hidden');
                remainingTime = humanTime(remaining);
            } else {
                $("#slider>#time").addClass('hidden');
                remainingTime = 'Live';
            }

            $('.spinner').addClass('hidden');
            $('.current-time').text(humanTime(player.currentTime()));
            $('#remaining-time').text(remainingTime);
            $("#slider>#time").css("width", `${(player.currentTime() / player.duration()) * 100}%`);
            $("#slider #buffer").css("width", `${(player.bufferedEnd() / player.duration()) * 100}%`);

            firstError = true;
        });

        player.on('fullscreenchange', function () {
            $("#btn-fullscreen").toggleClass("fullscreen", "fullscreen-not");
        });

        $("[data-button]").click(function (e) {
            switch (e.target.parentNode.dataset.button) {
                case "back":
                    player.goBack();
                    break;
                case "close":
                    hideMenus();
                    player.play();
                    break;
                case "settings":
                    toggleSettingMenu();
                    break;
                case "options":
                    toggleOptionsMenu();
                    break;
                case "previous":
                    player.previous();
                    break;
                case "next":
                    player.next();
                    break;
                case "playback":
                    player.togglePlayback();
                    break;
                case "fullscreen":
                    player.toggleFullscreen();
                    break;
                case "seasons":
                    toggleSeasonsMenu();
                    break;
                case "season-back":
                    _seasonBack();
                    break;
                case "season-forward":
                    seasonForward();
                    break;
                case "mute":
                    toggleMute();
                    break;
                case "slider":
                    break;
                case "session":
                    break;
                case "language":
                    $('.settingMenuItem').addClass('hidden');
                    $('#languages').removeClass('hidden');
                    $('.settingMenuButton').removeClass('opacity-100').addClass('opacity-50');
                    $('#language').removeClass('opacity-50').addClass('opacity-100');
                    break;
                case "subtitle":
                    $('.settingMenuItem').addClass('hidden');
                    $('#subtitles').removeClass('hidden');
                    $('.settingMenuButton').removeClass('opacity-100').addClass('opacity-50');
                    $('#subtitle').removeClass('opacity-50').addClass('opacity-100');
                    break;
                case "level":
                    $('.settingMenuItem').addClass('hidden');
                    $('#qualities').removeClass('hidden');
                    $('.settingMenuButton').removeClass('opacity-100').addClass('opacity-50');
                    $('#level').removeClass('opacity-50').addClass('opacity-100');
                    break;
                case "aspect":
                    $('.settingMenuItem').addClass('hidden');
                    $('#aspects').removeClass('hidden');
                    $('.settingMenuButton').removeClass('opacity-100').addClass('opacity-50');
                    $('#aspect').removeClass('opacity-50').addClass('opacity-100');
                    break;
                case "href":
                    window.location = e.target.dataset.href;
                    break;
                default:
                    // console.log("no data-button property set");
                    break;
            }
        });

        player.one('playlistitem', function () {
            $("#overlay-center, video").click(function (e) {
                if (window.innerWidth < 900 && !player.isFullscreen_ && interacted == false) {
                    if(!player.options_.liveui){
                        player.requestFullscreen();
                    }
                    if(player.paused()){
                        var promise = player.play();

                        if (promise !== undefined) {
                            promise.then(function () {
                                // Autoplay started!
                            }).catch(function (e) {
                                $('#btn-play-mobile').removeClass("pause").addClass("play").removeClass('hidden');
                            });
                        }
                    }
                    interacted = true;
                }
            });
        });

        $("#overlay-center, video").mousemove(function (e) {
            dynamicControls();
        }).click(function (e) {
            if (!isMobile.any()) {
                player.togglePlayback();
            }
            if (isSeeking) {
                if (e.pageX < Math.floor(window.innerWidth / 3.5)) {
                    player.rewindVideo();
                }
                if (e.pageX > (window.innerWidth - Math.floor(window.innerWidth / 3.5))) {
                    player.forwardVideo();
                }
            }
        }).dblclick(function (e) {
            const video = document.querySelector('video');
            e.preventDefault();
            e.stopPropagation();
            if (e.pageX < Math.floor(window.innerWidth / 3.5)) {
                isSeeking = true;
                player.rewindVideo();
            }
            if (e.pageX > (window.innerWidth - Math.floor(window.innerWidth / 3.5))) {
                isSeeking = true;
                player.forwardVideo();
            }
            if (e.pageX > Math.floor(window.innerWidth / 3.5) && e.pageX < (window.innerWidth - Math.floor(window.innerWidth / 3.5))) {
                if (e.pageY < (window.innerHeight - Math.floor(window.innerHeight / 1.4))) {
                    if (video.volume != 1) {
                        video.volume = video.volume + 0.1;
                    }
                }
                if ((e.pageY > (window.innerHeight - Math.floor(window.innerHeight / 1.4)) && e.pageY < Math.floor(window.innerHeight / 1.6)) || !isMobile.any()) {
                    isSeeking = false;
                    player.toggleFullscreen();
                }
                if (e.pageY > Math.floor(window.innerHeight / 1.6)) {
                    video.volume = video.volume - 0.1;
                }
            }
        });

        player.on('playlistitem', function () {

            playlist = player.playlist();
            playlistItem = player.playlist()[player.playlist.currentIndex_];
            player.loadPreviewSlider();
            // $("#preload-images").attr('src', `${playlistItem.tracks[0].file.replace("previews.vtt", "sprite.webp").replace('chapters.vtt', 'sprite.webp')}`);

            let res;
            if (playlist.length > 1 && playlistItem.video_type != 'movie' && playlistItem.video_type != 'special') {
                res = (player.options_.subroute || '') + "/" + playlistItem.video_type + '/' + playlistItem.tmdbid + "/watch" + "?season=" + zeroPad(playlistItem.season, 2) + "&episode=" + zeroPad(playlistItem.episode, 2);
            } else if (playlist.length > 1) {
                res = (player.options_.subroute || '') + "/" + playlistItem.video_type + '/' + playlistItem.tmdbid + "/watch" + "";
            }
            if (playlist.length > 1 && player.getParameterByName('type') == null) {
                window.history.pushState("player", playlistItem.title, res);
            } else if (playlist.length > 1) {
                res = (player.options_.subroute || '') + "/" + playlistItem.video_type + '/' + playlistItem.tmdbid + "/watch" + "?type=" + player.getParameterByName('type') + "&item=" + player.playlist.currentIndex();
                window.history.pushState("player", playlistItem.title, res);
            }

            qualityLevels = player.qualityLevels();
            textTracks = player.textTracks();
            audioTracks = player.audioTracks();

            player.one('playing', function () {
                updateAudioList();
                updateSubtitleList();
                updateQualityList();
                setSettingsMenu();
            });
        });

        this.nmplayer_initialized = true;
    });


    function hideMenus() {
        hideSettingMenu();
        hideSeasonsMenu();
    }

    function buttonDisplay() {

        if (playlist.length > 1) {
            $("#previous").removeClass('hidden');
            $("#next").removeClass('hidden');
        }

        if (player.playlist.currentIndex() > 0 && playlist.length > 1) {
            $("#previous").removeClass('hidden');
            $("#previous").attr("title", playlist[player.playlist.currentIndex() - 1].title);
        } else {
            $("#previous").addClass('hidden');
        }
        if (!playlist[player.playlist.currentIndex() + 1] && playlist.length >= 1) {
            $("#next").addClass('hidden');
        } else {
            $("#next").removeClass('hidden');
            $("#next").attr("title", playlist[player.playlist.currentIndex() + 1].title);
        }
        let last;
        for (let i = player.playlist.currentIndex(); i < playlist.length; i++) {
            if (playlist[i].file !== null) {
                last = i;
                player.last = i;
            }
        }
        if (player.playlist.currentIndex() == last) {
            $("#next").addClass('hidden');
        }

        if (playlist.length > 1) {
            $('#seasons').removeClass('hidden');
        }

        if (qualityLevels.levels_.length > 1) {
            $('#levels').removeClass('hidden');
        }

    }

    function dynamicControls() {
        showControls();
        clearTimeout(timer);
        timer = setTimeout(hideControls, 5000);
    }
    function showControls() {
        $("#overlay,.video-js").addClass("active");
        $('.video-js').css('cursor', 'unset');
    }
    function hideControls() {
        if (player.lock == false && settingsMenuToggle == false && optionsMenuToggle == false) {
            $("#overlay, .video-js").removeClass("active");
            $('.video-js').css('cursor', 'none');
            $(`[data-type="ui"],[data-type="menu-item"]`).attr('data-selected', 'false');
            $('[data-type="ui"][data-button="playback"]:not(.hidden)').attr('data-selected', 'true');
        }
    }

    function toggleSettingMenu() {
        hideMenus();
        if (settingsMenuToggle == false) {
            showSettingMenu();
        } else {
            hideSettingMenu();
        }
    }
    function showSettingMenu() {
        player.pause();
        settingsMenuToggle = true;
        $('#settingsMenu').removeClass('hidden');
    }
    function hideSettingMenu() {
        settingsMenuToggle = false;
        $('#settingsMenu').addClass('hidden');
    }

    function toggleSeasonsMenu() {
        hideMenus();
        if (optionsMenuToggle == false) {
            showSeasonsMenu();
        } else {
            hideSeasonsMenu();
        }


        _seasonBack();
        let season = player.getParameterByName('season') || '01';
        _seasonForward(season);
        $('#season-menu-name').text(`Season ${season != 0 ? season : 'specials'}`);

        scroll_to_div(`#${player.getParameterByName('item') || player.playlist.currentIndex()}`);

        $('[data-button="video"]').click(function () {
            settingsMenuToggle = false;
            $('#seasonsMenu').addClass('hidden');
        });

    }
    function showSeasonsMenu() {
        seasonsMenuToggle = true;
        player.pause();
        $('#seasonsMenu').removeClass('hidden');
        $("#seasonsMenu").css('right', '0');
        $("#subtitle-window").addClass('hidden');
    }
    function hideSeasonsMenu() {
        seasonsMenuToggle = false;
        $('#seasonsMenu').addClass('hidden');
        $("#seasonsMenu").css('right', '-40vw');
        $("#subtitle-window").removeClass('hidden');
    }

    function toggleMute() {
        player.muted(!player.muted());
    }

    function _seasonBack() {
        $('.season').addClass('hidden');
        $('#seasons-container').addClass('hidden');
        $('#season-menu-name').text('');
        $('[data-button="season-back"]').addClass('hidden');
        $("#season-forward-container").removeClass('hidden');
    }
    function _seasonForward(season) {
        season = String(zeroPad(season, 2));
        $('.season').addClass('hidden');
        $('#seasons-container').removeClass('hidden').addClass('flex');
        $('#seasonsMenu').removeClass('hidden').addClass('flex');
        $("#season-forward-container").addClass('hidden');
        $('[data-button="season-back"]').removeClass('hidden');
        $('#season-menu-name').text(`Season ${season != 0 ? season : 'specials'}`);
        $(`#season-${season != 0 ? season : 'specials'}-container`).removeClass('hidden');
    }


    function setCurrentQuality(index) {
        currentQuality = parseInt(index);

        qualityLevels.levels_.forEach(function (e, i) {
            if (i != currentQuality) {
                qualityLevels[i].enabled = false;
            } else {
                qualityLevels.levels_[currentQuality].enabled = true;
            }
        });

        player.play();

    }
    function setCurrentCaption(index) {
        currentCaption = index;

        textTracks.tracks_.forEach(function (data, index) {
            if (index != currentCaption) {
                textTracks[index].mode = 'hidden';
            } else {
                textTracks[index].mode = 'showing';
            }
        });

        $(".activeSubItem").addClass('hidden');

        if (typeof textTracks.tracks_[currentCaption] != 'undefined') {

            $(".activeSubItem").addClass('hidden').parent();
            $("#active_subtitle_" + textTracks.tracks_[currentCaption].label.replace('segment-metadata', 'Off').replace(' ', '_')).removeClass('hidden').parent().removeClass('opacity-50').addClass('opacity-100');
        }

        if (currentCaption == -1) {
            $(".activeSubItem").addClass('hidden').parent();
            $("#active_subtitle_Off").removeClass('hidden').parent().removeClass('opacity-50').addClass('opacity-100');
        }

        localStorage.setItem('player.captionLabel', textTracks.tracks_[currentCaption] ? textTracks.tracks_[currentCaption].label.replace('segment-metadata', 'Off').replace(' ', '_') : 'Off');

        player.opus();
        player.play();
    }
    function setCurrentAudio(index) {
        currentAudio = parseInt(index);

        audioTracks[currentAudio].enabled = true;

        $(".activeAudioItem").addClass('hidden');
        if (audioTracks.length > 0) {
            $("#active_audio_" + audioTracks.tracks_[currentAudio].label.replace('segment-metadata', 'Off').replace(' ', '_')).removeClass('hidden').parent().removeClass('opacity-50').addClass('opacity-100');
            localStorage.setItem('player.audioLabel', audioTracks.tracks_[currentAudio].label.replace('segment-metadata', 'Off').replace(' ', '_'));
        }

        player.play();
    }



    this.toggleFullscreen = () => {
        if (player.isFullscreen_) {
            player.exitFullscreen();
        } else {
            player.requestFullscreen();
        }
    };
    this.togglePlayback = () => {
        switch (player.paused()) {
            case true:
                player.play();
                break;
            case false:
                player.pause();
                break;
            default:
                break;
        }
    };

    this.loadPreviewSlider = function () {
        let previewTime = [];
        let m;
        if (typeof playlistItem.tracks != 'undefined') {
            $("#slider-image").css({
                'background-image': `url('${playlistItem.tracks[0].file.replace("previews.vtt", "sprite.webp").replace('chapters.vtt', 'sprite.webp')}')`
            });
            $.get(playlistItem.tracks[0].file, function (data) {
                let regex = /(\d{2}:\d{2}:\d{2})\.\d{3}\s-->\s(\d{2}:\d{2}:\d{2})\.\d{3}\nsprite\.webp#(xywh=\d{1,},\d{1,},\d{1,},\d{1,})/gm;
                while ((m = regex.exec(data)) !== null) {
                    if (m.index === regex.lastIndex) {
                        regex.lastIndex++;
                    }
                    previewTime.push({
                        start: m[1],
                        end: m[2],
                        currentTime: m[3]
                    });
                }
            });

            $('#slider').mousemove(function (e) {
                player.lock = true;
                let thisOffset = $(this).offset(),
                    relativeXcurrentTime = (e.pageX - thisOffset.left),
                    sliderTime = (player.duration() / $('#slider').width()) * relativeXcurrentTime,
                    popLeft = relativeXcurrentTime;

                $("#subtitle-window").addClass('hidden');
                $("#time-nipple").removeClass('hidden');
                $("#time-nipple").css('left', relativeXcurrentTime);

                $("#slider-pop").removeClass('invisible');
                $("#slider-pop").css('left', popLeft);
                $("#slider-pop").css('visibility', 'visible');

                $("#slider-time").html(humanTime(sliderTime));

                previewTime.forEach(function (match) {
                    let start = convertToSeconds(match.start),
                        end = convertToSeconds(match.end);
                    if (sliderTime >= start && sliderTime < end) {
                        $("#slider-image").css('background-position', `-${match.currentTime.split('=')[1].split(',')[0]}px -${match.currentTime.split('=')[1].split(',')[1]}px`);
                    }
                });
            }).mouseleave(function (e) {
                $("#subtitle-window").removeClass('hidden');
                $("#slider-pop").addClass('invisible');
                $("#slider-pop").css({
                    visibility: 'hidden'
                });
                $("#time-nipple").addClass('hidden');
                player.lock = false;
            }).click(function (e) {
                player.lock = false;
                let offset = $(this).offset();
                let left = e.pageX - offset.left;
                let totalWidth = $("#slider").width();
                let percentage = left / totalWidth;
                let videoTime = parseInt(player.duration() * percentage);
                player.currentTime(videoTime);
                player.play();
            });

        }
    };
    this.getCurrentCaption = function () {
        return currentCaption;
    };
    this.setCurrentCaption = function (index) {
        setCurrentCaption(index);
    };

    this.getCurrentAudio = function () {
        return currentAudio;
    };
    this.setCurrentAudio = function (index) {
        setCurrentAudio(index);
    };

    this.getCurrentQuality = function () {
        return player.Quality;
    };
    this.setCurrentQuality = function (index) {
        setCurrentQuality(index);
    };

    this.setUpNext = function (e) {
        eggBoiled = e;
    };
    this.nextClick = function () {
        clearTimeout(next);
        $('.nextup').addClass('hidden');
        player.next();
    };
    this.seasonForward = function (s) {
        _seasonForward(s);
    };
    this.goBack = function () {
        player.pause();
        location.href = document.location.protocol + '//' + document.location.hostname + ':' + document.location.port + (player.options_.subroute || '') + playlistItem.backroute;
    };
    this.previous = function () {
        if (player.playlist.currentItem() != 0) {
            player.playlist.previous();
        }
    };
    this.next = function () {
        if (player.playlist.currentItem() != player.playlist.lastIndex()) {
            player.playlist.next();
        }
    };
    this.setEpisode = function (season, episode) {
        $('.spinner').removeClass('hidden');
        playlist.forEach(function (e, index) {
            if (player.getParameterByName('type') == null) {
                if (e.season == season && e.episode == episode) {
                    if (e.sources.length == 0) {
                        player.setEpisode(season, episode + 1);
                    } else {
                        player.playlist.currentItem(index);
                    }
                }
            } else {
                if (index == episode) {
                    player.playlist.currentItem(index);
                }
            }
        });
        player.play();
    };

    this.setSubtitleTrack = function (lang) {
        textTracks.tracks_.forEach((e, index) => {
            if (e.label != null && e.label.replace(' ', '_') == lang) {
                player.setCurrentCaption(index);
            }
        });
    };

    this.setAudioTrack = function (lang) {
        audioTracks.tracks_.forEach((e, index) => {
            if (e.label != null && e.label.replace(' ', '_') == lang) {
                player.setCurrentAudio(index);
            }
        });
    };

    this.cycleSubtitles = function () {

        if (currentCaption < textTracks.length - 1) {
            player.setCurrentCaption(currentCaption + 1);
        } else {
            player.setCurrentCaption(-1);
        }

        let label = typeof textTracks[currentCaption] != 'undefined' ? textTracks[currentCaption].label : translations.off;

        clearTimeout(message);
        $('#player-message').removeClass('hidden');
        $('#player-message').html("Subtitle: " + label);
        message = setTimeout(function () {
            $('#player-message').addClass('hidden');
            $('#player-message').empty();
        }, 2000);
    };

    this.changeAspectRatio = function (val) {
        $('.video-js video').css({
            objectFit: val
        });
        $('.activeAspectItem').hide();
        $(`#active_aspect_${val}`).show().parent().removeClass('opacity-50').addClass('opacity-100');
    };

    this.rewindVideo = function () {
        tapCount = tapCount + 10;
        $(".rewind.notification").removeClass('hidden').addClass('flex');
        $("span.rewind").text(`${Math.abs(tapCount)} seconds`);
        clearTimeout(tap);
        tap = setTimeout(function () {
            $(".rewind.notification").addClass('hidden');
            player.currentTime(player.currentTime() - tapCount);
            tapCount = 0;
            isSeeking = false;
            player.play();
        }, leeway);
    };
    this.forwardVideo = function () {
        tapCount = tapCount + 10;
        $(".forward.notification").removeClass('hidden').addClass('flex');
        $("span.forward").text(`${Math.abs(tapCount)} seconds`);
        clearTimeout(tap);
        tap = setTimeout(function () {
            $(".forward.notification").addClass('hidden');
            player.currentTime(player.currentTime() + tapCount);
            tapCount = 0;
            isSeeking = false;
            player.play();
        }, leeway);
    };

    this.opus = function () {

        if (typeof window.octopusInstance != 'undefined' && typeof window.octopusInstance.video != 'undefined' && window.octopusInstance.video.parentNode != null) {
            window.octopusInstance.dispose();
        }
        $('.libassjs-canvas-parent').remove();

        let subtitleURL = (typeof textTracks.tracks_[player.getCurrentCaption()] != 'undefined' && typeof textTracks.tracks_[player.getCurrentCaption()].src != 'undefined') ? textTracks.tracks_[player.getCurrentCaption()].src : '#';

        options = {
            video: document.querySelector("video"),
            subUrl: subtitleURL,
            lossyRender: true,
            fonts: [
            ],
            debug: false,
            workerUrl: "/js/dist/subtitles-octopus-worker.js"
        };

        if (subtitleURL.endsWith('ass')) {
            window.octopusInstance = new SubtitlesOctopus(options); // You can experiment in console
        }
    };

    function loadSeasons() {
        $(`#season-forward-container`).empty();
        $(`#seasons-container`).empty();

        if (player.getParameterByName('type') == null) {
            let season = 999;
            let style;

            playlist.forEach(function (e, i) {
                if (e.season != season) {
                    season = e.season;
                    let src = `src="${e.season_image || 'https://cdn.nomercy.tv/img/poster-not-available.png'}" onerror="this.src='https://cdn.nomercy.tv/img/poster-not-available.png'"`;

                    $('#season-forward-container').append(`
                    <div onclick="player.seasonForward(${e.season != '00' ? e.season : 'Specials'})" class="flex flex-col mb-2 overflow-hidden season-forward">
                        <div class="flex flex-row overflow-hidden season-forward">
                            <div class="flex flex-col justify-center float-left w-32 mx-auto">
                                <img ${src} alt="" style="display: block;height: 100%;margin: auto; padding: 10%;max-height: 100%;">
                            </div>
                            <div class="flex flex-col float-left w-10/12 h-full mr-4">
                                <div class="flex flex-col h-4 mx-4 mb-2 text-center text-xl">
                                    ${e.season != 0 ? ' Season ' + e.season : 'Specials'}
                                </div>
                                <div class="flex flex-col float-left h-20 mx-4 py-2 overflow-hidden text-xs" style="min-width: 73%;">
                                    ${e.season_overview || ''}
                                </div>
                            </div>

                        </div>
                    </div>
                `);
                }
            });
            season = 999;
            playlist.forEach(function (e, i) {
                if (e.season != season) {
                    season = e.season;
                    if (e.season == player.getParameterByName('season')) {
                        style = 'active';
                    } else {
                        style = '';
                    }
                    $('#seasons-container').append(`
                    <div id="season-${e.season != 0 ? e.season : 'Specials'}-container" class="season h-full w-full overflow-hidden flex-col ${style}">
                        <div id="season-${e.season != 0 ? e.season : 'Specials'}-list" data-seasonList="${e.season != 0 ? e.season : 'Specials'}" class="w-full p-3 mb-2 overflow-y-auto season-list" style="overflow-x: hidden;"></div>
                    </div>
                `);
                }
            });


            playlist.forEach(function (e, i) {

                let src = `${e.image != 'Specials' ? e.image.replace('original', 'w185') : '.x'}"`;

                $(`#season-${e.season != '00' ? e.season : 'Specials'}-list`).append(`
                <div id="${i}" data-button="video" onclick="player.setEpisode(${e.season || 0},${e.episode || 'Specials'})" class="w-full h-32 p-2 mb-2">
                    <div data-button="video" class="w-full md:ml-40 px-2 py-1 text-sm">
                    S${zeroPad(e.season || 0, 2)}E${zeroPad(e.episode || 0, 2)} ${e.title.replace('S H I E L D', 'S.H.I.E.L.D.')}
                    </div>
                    <div class="flex-row h-20">
                        <div data-button="video" class="float-left w-3/12 h-full px-1 pb-1">
                            <img data-button="video" src="${src}" onerror="this.src='https://cdn.nomercy.tv/img/still-not-available.png'" alt="" style="display: block;margin: auto;max-height: 100%;">
                        </div>
                        <div data-button="video" id="" class="float-left w-9/12 p-1 text-xs overview">
                            ${e.description}
                        </div>
                    </div>
                </div>
            `);
            });
        }
        else {
            $(`#seasons-container`).append(`
            <div data-button="season-back" id="season-1-container" class="season-list-container" style="display:block;width: 100%;margin-top: 50px;">
                <div data-button="season-back"  id="season-1-button" data-season="1" class="episodes" style="overflow-x: hidden;"></div>
                <div id="season-1-list" data-seasonList="1" class="episode-list" style="overflow-x: hidden;"></div>
            </div>
        `);
            $(`#seasons-container`).addClass('active');
            playlist.forEach(function (e, i) {
                $('#season-1-list').append(`
                <div id="${i}" data-button="video" onclick="player.setEpisode(1,${i || 0})" class="w-full p-2" style="background:${i % 2 == 0 ? '#0a090980' : '#000000ba'}">
                    <div data-button="video" class="w-full p-2">
                        #${i} ${e.title.replace('S H I E L D', 'S.H.I.E.L.D.')}
                    </div>
                    <div class="row w-100">
                        <div data-button="video" class="float-left w-3/12 p-1">
                            <img data-button="video" id="" src="${e.image != null ? e.image.replace('original', 'w300') : '.x'}" onerror="https://cdn.nomercy.tv/img/noimage.thumbnail.jpg'" alt="" style="display: block">
                        </div>
                        <div data-button="video" id="" class="float-left w-9/12 p-1 text-sm" style="font-size: 0.8rem;line-height:1rem;font-weight: 500;overflow: hidden;height: 7rem;">
                            ${e.description}
                        </div>
                    </div>
                </div>
            `);
            });
        }
    }

    function updateAudioList() {
        $(`#languages`).empty();
        if (audioTracks.tracks_.length > 1) {
            $('#options').removeClass('hidden');
            $('#language').removeClass('hidden').removeClass('opacity-50').addClass('opacity-100');
            $('#languages').removeClass('hidden');
        }
        audioTracks.tracks_.forEach(function (data, index) {
            let label = data.language.replace('SoundHandler', 'eng');
            $('#languages').append(`
                <div data-type="menu-item" data-menu="language" data-row="${index}" data-col="0" class="flex flex-row h-10 px-4 py-2 opacity-100 cursor-pointer w-12/12 hover:opacity-100" onclick="player.setCurrentAudio(${index})">
                    <div id="active_audio_${data.label}" class="${index == 0 ? 'hidden' : ''} w-1/12 activeAudioItem" onclick="player.setCurrentAudio(${index})">✔</div>
                    <p id="audio_${data.label}" class="float-left w-11/12 pr-3 ml-auto text-right" onclick="player.setCurrentAudio(${index})">
                    ${translations[label]}
                    </p>
                </div>
            `);
        });
        $(".activeAudioItem").addClass('hidden');
    }
    function updateQualityList() {
        $(`#qualities`).empty();

        if (qualityLevels.levels_.length > 1) {
            $('#options').removeClass('hidden');
            $('#level').removeClass('hidden');
        }
        qualityLevels.levels_.forEach(function (data, i) {
            $('#qualities').append(`
            <div data-type="menu-item" data-menu="quality" data-row="${i}" data-col="0" class="flex flex-row h-10 px-4 py-2 opacity-100 cursor-pointer w-12/12 hover:opacity-100" onclick="player.setCurrentQuality(${i})">
                <div id="active_quality_${i}" class="${i == 0 ? 'hidden' : ''} w-1/12 activeQualityItem" onclick="player.setCurrentQuality(${i})">✔</div>
                <p id="active_quality_${data.height}" class="float-left w-11/12 pr-3 ml-auto text-right" onclick="player.setCurrentQuality(${i})">
                    ${data.height}
                </p>
            </div>
        `);
        });

        qualityLevels.on('change', function () {
            $(".activeQualityItem").addClass('hidden');
            $("#active_quality_" + qualityLevels[qualityLevels.selectedIndex].height).removeClass('hidden').prev().removeClass('hidden').parent().removeClass('opacity-50').addClass('opacity-100');
        });

        player.setCurrentQuality(player.getCurrentQuality());


    }
    function updateSubtitleList() {
        $(`#subtitles`).empty();

        if (textTracks.tracks_.length > 0 && !location.pathname.startsWith('/streams')) {
            $('#options').removeClass('hidden');
            $('#subtitle').removeClass('hidden');
            $('#subtitles').append(`
            <div data-type="menu-item" data-menu="subtitle" data-row="0" data-col="0" data-selected="false" class="flex flex-row h-10 px-4 py-2 opacity-100 cursor-pointer w-12/12 hover:opacity-100" onclick="player.setCurrentCaption(-1)">
                <div id="active_subtitle_Off" class="w-1/12 activeSubItem" onclick="player.setCurrentCaption(-1)">✔</div>
                <p id="subtitle_Off" class="float-left w-11/12 pr-3 ml-auto text-right" onclick="player.setCurrentCaption(-1)">
                    ${translations.off}
                </p>
            </div>
        `);
        }

        textTracks.tracks_.forEach(function (data, index) {
            data.mode = 'hidden';
            if (!data.label.includes('segment-metadata')) {
                $('#subtitles').append(`
                <div data-type="menu-item" data-menu="subtitle" data-row="${index}" data-col="0" data-selected="false" class="flex flex-row h-10 px-4 py-2 opacity-100 cursor-pointer w-12/12 hover:opacity-100" onclick="player.setCurrentCaption(${index})">
                    <div id="active_subtitle_${data.label.replace('segment-metadata', `${translations.off}`).replace(' ', '_')}" class="hidden w-1/12 activeSubItem" onclick="player.setCurrentCaption(${index})">✔</div>
                    <p id="subtitle_${data.label.replace('segment-metadata', `${translations.off}`).replace(' ', '_')}" class="float-left w-11/12 pr-3 ml-auto text-right" onclick="player.setCurrentCaption(${index})">
                        ${data.label.replace('segment-metadata', `${translations.off}`)}
                    </p>
                </div>
            `);

            }
        });

    }
    function setSettingsMenu() {
        if (!location.pathname.startsWith('/streams')) {
            if (audioTracks.tracks_.length > 1) {
                $("#active_audio_" + audioTracks.tracks_[currentAudio].label).removeClass('hidden').parent().removeClass('opacity-50').addClass('opacity-100');
            }
            else if (textTracks.tracks_.length > 0) {
                $(`#languages`).addClass('hidden');
                $('#subtitle').removeClass('opacity-50').addClass('opacity-100');
                $(`#subtitles`).removeClass('hidden');
            }
            else if (qualityLevels.levels_.length > 1) {
                $(`#languages`).addClass('hidden');
                $('#level').removeClass('opacity-50').addClass('opacity-100');
                $(`#qualities`).removeClass('hidden');

            }
            else {


            }
        }
    }

});
