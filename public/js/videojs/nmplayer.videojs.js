window.nmplayer = function (a) {

    if(typeof $ == 'undefined'){
        document.getElementsByTagName("head")[0].innerHTML += '<script type="text/javascript" src="/js/jquery-3.5.1.min.js"><\/script>';
    }

    if (!(this instanceof window.nmplayer)) {
        return new window.nmplayer(a);
    }

    if(!location.pathname.startsWith('/streams')){
        $("nav").addClass('hidden');
    }

    this.id = a;

    var optionsMenuToggle = false,
        qualityMenuToggle = false,
        seasonsMenuToggle = false,
        timer,
        tap = null,
        tapCount = 10,
        isSeeking = false,
        subOffset = 0,
        message,
        lock = false,
        currentCaption = -1,
        currentAudio = 0,
        currentQuality = 0,
        firstItem = true,
        urlQuery = new URLSearchParams(window.location.search),
        leeway = 500
        ;

    const isMobile = {
        Android: function () {
            if (navigator.userAgent.match(/Android/i)) {
                return true;
            }
        },
        BlackBerry: function () {
            if (navigator.userAgent.match(/BlackBerry/i)) {
                return true;
            }
        },
        iOS: function () {
            if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
                return true;
            }
        },
        Opera: function () {
            if (navigator.userAgent.match(/Opera Mini/i)) {
                return true;
            }
        },
        Windows: function () {
            if (
                navigator.userAgent.match(/IEMobile/i) ||
                navigator.userAgent.match(/WPDesktop/i)
            ) {
                return true;
            }
        },
        any: function () {
            return (
                isMobile.Android() ||
                isMobile.BlackBerry() ||
                isMobile.iOS() ||
                isMobile.Opera() ||
                isMobile.Windows()
            );
        }
    };

    this.setup = (f) => {

        player = videojs(a, f.options);

        if(f.playlist.length == 1){
            player.playlist(f.playlist, 0);
        }

        else {
            player.playlist(f.playlist, -1);
            player.playlist.autoadvance(0);
        }

        player.ready(function() {
            var promise = player.play();

            if (promise !== undefined) {
              promise.then(function() {
                // Autoplay started!
              }).catch(function(e) {
                $('#btn-play-mobile').removeClass("pause").addClass("play").css({display: 'block'});
              });
            }
        });

        player.on("ready", (e) => {
            $("video").append(`<div class="libassjs-canvas-parent"></div>`);
            $(`#${a}`).prepend(`
                <div id="overlay" class="absolute flex-col hidden w-full h-screen p-0 mx-0 text-white">
                    <div id="overlay-top" class="absolute left-0 flex flex-row w-full h-20 px-8 py-3 md:mr-8 lg:px-16">
                        <button data-button="back" id="back" class=""><span class="back"/></button>
                        <button data-button="quality" id="quality" class=""><span class="quality"/></button>
                        <button data-button="seasons"  id="seasons" class="hidden"><span class="playlist"/></button>
                        <button data-button="previous" id="previous" class="hidden"><span class="previous"/></button>
                        <button data-button="next" id="next" class="hidden"><span class="next"/></button>
                    </div>
                    <div id="overlay-center" class="flex flex-row w-full h-full p-0 mx-0">
                        <div id="player-message" class="row player-message"></div>
                        <button id="btn-play-mobile" data-button="playback" class="" type="button"><span class="pause"/></button>
                        <div id="spinner" class="spinner"></div>
                        <div class="hidden nextup z-100" onclick="nmplayer.nextClick()">
                            <div class="bg"></div>
                            <div class="triangle"></div>
                            &nbsp;&nbsp;  next episode
                        </div>
                        <div class="absolute top-0 left-0 hidden w-6/12 h-screen pr-2 py-auto lg:w-3/12 rewind notification">
                            <div class="flex flex-col items-center justify-center w-full h-full pr-2 md:flex-row rewind-icon icon" style="background-color: #ffffff2e;border-radius: 0% 150% 150% 0%/ 100% 100% 100% 100%;">
                                <div class="flex flex-col h-12">
                                    <span class="seek-back" type="button"></span>
                                    <span class="text-center sm:mx-auto lg:mx-2 rewind">10 seconds</span>
                                </div>
                            </div>
                        </div>
                        <div class="absolute top-0 right-0 hidden w-6/12 h-screen pl-2 py-auto lg:w-3/12 forward notification">
                            <div class="flex flex-col items-center justify-center w-full h-full pl-2 md:flex-row forward-icon icon" style="background-color: #ffffff2e;border-radius: 150% 0% 0% 150%/ 100% 100% 100% 100%;">
                                <div class="flex flex-col h-12">
                                    <span class="seek-forward" type="button"></span>
                                    <span class="text-center sm:mx-auto lg:mx-2 forward">10 seconds</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="slider-container" data-button="slider pt-6" class="w-11/12 mx-auto">
                        <div id="slider" data-button="slider" class="relative left-0 w-full">
                            <div id="slider-pop" data-button="slider-pop" class="hidden">
                                <div id="slider-image" src"#"></div>
                                <div id="slider-time" class="time"></div>
                            </div>
                            <span id="buffer" data-button="buffer"></span>
                            <span id="time" data-button="slider"></span>
                            <span id="time-nipple" class="hidden" data-button="slider"></span>
                        </div>
                    </div>
                    <div id="overlay-bottom" class="absolute bottom-0 left-0 flex flex-row w-full h-24 px-4 pb-2 mb-0 md:px-10 sm:pt-6 2xl:pt-4 md:mb-0 lg:px-12">
                        <button data-button="playback" class="" type="button"><span id="btn-playback" class="play pause"/></button>
                        <button data-button="mute" type="button"><span id="btn-mute" class="unmuted muted"/></button>
                        <div id="current-time" class="w-24 h-12 pt-3 mr-4 text-lg text-center text-white 3xl:ml-16 3xl:pt-3 time current-time" >00:00</div>
                        <div id="remaining-time" class="w-24 h-12 pt-3 pl-4 ml-auto text-lg text-center text-white 3xl:mr-32 3xl:pt-3 time remaining-time" >00:00</div>
                        <button data-button="fullscreen" class="" type="button"><span id="btn-fullscreen" class="fullscreen fullscreen-not"/></button>
                    </div>

                    <div id="seasonsMenu" class="absolute top-0 right-0 flex-col hidden h-full bg-black sm:h-screen bg-opacity-90">
                        <div class="flex flex-row justify-end w-full px-2 py-2">
                            <button data-button="season-back" class="w-3/12 h-4 my-2" style="height: 45px;"><span id="season-back" class="back"/></button>
                            <div id="season-menu-name" class="w-full py-3 text-2xl text-center float-center text-bold"></div>
                            <button data-button="close" class="m-2"><span data-button="close" class="close"/></button>
                        </div>
                        <div id="season-forward-container" class="flex-col hidden w-full h-full px-2 overflow-y-auto season-forward-container"></div>
                        <div id="seasons-container" class="w-full h-full overflow-hidden seasons-container"></div>

                    </div>

                    <div id="qualityMenu" class="absolute flex-col hidden h-48 overflow-hidden bg-gray-700 bg-opacity-100 border border-gray-700 md:h-56">

                        <div class="flex flex-row justify-end w-full px-2 py-1 bg-gray-800 border-lg md:px-4">
                            <button id="language" data-button="language" class="hidden opacity-50 qualityMenuButton" ><span class="languages"/></button>
                            <button id="subtitle" data-button="subtitle" class="hidden opacity-50 qualityMenuButton" ><span class="subtitles"/></button>
                            <button id="level" data-button="level" class="hidden opacity-50 qualityMenuButton" ><span class="levels"/></button>
                            <button id="aspect" data-button="aspect" class="opacity-50 qualityMenuButton"><span class="aspect"/></button>
                            <button data-button="close" class="ml-auto"><span data-button="close" class="close"/></button>
                        </div>

                        <div id="aspects" class="flex-col hidden w-full p-2 overflow-y-auto text-base qualityMenuItem">
                            <div class="flex flex-row h-10 px-4 py-2 opacity-100 cursor-pointer w-12/12 hover:opacity-100" onclick="nmplayer.changeAspectRatio('contain');">
                                <div id="active_aspect_contain" class="w-1/12 activeAspectItem">✔</div>
                                <p id="aspect_contain" class="float-left w-11/12 h-6 pr-3 ml-auto text-right">Contain</p>
                            </div>
                            <div class="flex flex-row h-10 px-4 py-2 opacity-50 cursor-pointer w-12/12 hover:opacity-100" onclick="nmplayer.changeAspectRatio('cover');">
                                <div id="active_aspect_cover" class="hidden w-1/12 activeAspectItem">✔</div>
                                <p id="aspect_contain" class="float-left w-11/12 h-6 pr-3 ml-auto text-right">Cover</p>
                            </div>
                            <div class="flex flex-row h-10 px-4 py-2 opacity-50 cursor-pointer w-12/12 hover:opacity-100" onclick="nmplayer.changeAspectRatio('fill');">
                                <div id="active_aspect_fill" class="hidden w-1/12 activeAspectItem">✔</div>
                                <p id="aspect_contain" class="float-left w-11/12 h-6 pr-3 ml-auto text-right">Fill</p>
                            </div>
                        </div>
                        <div id="qualities" class="flex-col hidden w-full p-2 overflow-y-auto text-base qualityMenuItem"></div>
                        <div id="languages" class="flex-col hidden w-full p-2 overflow-y-auto text-base qualityMenuItem"></div>
                        <div id="subtitles" class="flex-col hidden w-full p-2 overflow-y-auto text-base qualityMenuItem"></div>
                    </div>
                    <div id="subtitle-window" class="subtitle-window"></div>
                </div>
            `);
        });
        player.on('ready', e => {
            if (new URLSearchParams(window.location.search).get('season') !== null && new URLSearchParams(window.location.search).get('episode') !== null) {
                nmplayer.setEpisode(new URLSearchParams(window.location.search).get('season'), new URLSearchParams(window.location.search).get('episode'));
            }
            else if (new URLSearchParams(window.location.search).get('type') !== null && new URLSearchParams(window.location.search).get('item') !== null) {
                nmplayer.setEpisode(0, new URLSearchParams(window.location.search).get('item'));
            }
            else {
                nmplayer.setEpisode(1, 1);
            }


            $('.spinner-main').addClass('hidden');
        });

        player.on("play", (e) => {
            lock = false;
            $("#overlay").removeClass("hidden");
            $("#btn-playback").removeClass("play");
            $("#btn-playback").addClass("pause");
            $('.spinner').addClass('hidden');
            $('#btn-play-mobile').css({display: 'none'});
            $("#slider-pop").addClass('hidden');

            $('#remaining-time').text(humanTime(player.duration() - player.currentTime()));
            if (player.muted()) {
                $("#btn-mute").removeClass("unmuted");
                $("#btn-mute").addClass("muted");
            }
            else{
                $("#btn-mute").removeClass("muted");
                $("#btn-mute").addClass("unmuted");
            }
            var res;

            if (player.playlist().length > 1 && player.playlist()[player.playlist.currentItem()].video_type != 'movie' && player.playlist()[player.playlist.currentItem()].video_type != 'special') {
                res = (f.subroute || '') + "/watch?id=" + player.playlist()[player.playlist.currentItem()].tmdbid + "&season=" + zeroPad(player.playlist()[player.playlist.currentItem()].season, 2) + "&episode=" + zeroPad(player.playlist()[player.playlist.currentItem()].episode, 2);
            }
            else if (player.playlist().length > 1) {
                res = (f.subroute || '') + "/watch?id=" + player.playlist()[player.playlist.currentItem()].tmdbid;
            }
            if (player.playlist().length > 1 && new URLSearchParams(window.location.search).get('type') == null) {
                window.history.pushState("nmplayer", player.playlist()[player.playlist.currentItem()].title, res);
            }
            else if(player.playlist().length > 1){
                res = "/watch?id=" + player.playlist()[player.playlist.currentItem()].tmdbid + '&type=' + new URLSearchParams(window.location.search).get('type') + "&item=" + player.playlist.currentIndex();
                window.history.pushState("nmplayer", player.playlist()[player.playlist.currentItem()].title, res);
            }

            document.title = player.playlist()[player.playlist.currentItem()].title;
            document.description = player.playlist()[player.playlist.currentItem()].description;
            window.newTime = 0;

            addEventListeners();
            hideControls();
            hideOptionsMenu();
            hideSeasonsMenu();
            hideQualityMenu();

        });

        player.on("playing", (e) => {
            setTimeout(() => {
                nmplayer.loadPreviewSlider();
                buttonDisplay();
            }, this.leeway);
            setSubtitleTrack();
            setAudioTrack();
        });

        player.on('pause', (e) => {
            lock = true;
            $("#btn-playback").removeClass("pause");
            $("#btn-playback").addClass("play");
            showControls();
        });
        player.onpause = function() {
            alert("The video has been paused");
        };
        player.on("playlistComplete", (e) => {
            nmplayer.goBack();
        });
        player.on('warning', (e) => {
            alert(e.code + ', ' + e.message);
        });
        player.on("error", (e) => {
            if(location.pathname.startsWith('/streams')){
                player.pause();
                player.src('');
            }
            else{
                nmplayer.next();
            }
        });
        player.on("mediaerror", (e) => {
                player.pause();
                player.src('');
        });
        player.on("volumechange", (e) => {
            if (player.muted()) {
                $("#btn-mute").removeClass("unmuted");
                $("#btn-mute").addClass("muted");
            }
            else{
                $("#btn-mute").removeClass("muted");
                $("#btn-mute").addClass("unmuted");
            }
        });
        player.on("timeupdate", (e) => {

            let remaining = player.duration() - player.currentTime();
            if(remaining != Infinity){
                remainingTime = humanTime(remaining);
            }
            else{
                remainingTime = 'Live';
            }

            $('.spinner').addClass('hidden');
            $('.current-time').text(humanTime(player.currentTime()));
            $('#remaining-time').text(remainingTime);
            $("#slider>#time").css("width", `${(player.currentTime() / player.duration()) * 100}%`);
            $("#slider #buffer").css("width", `${(player.bufferedEnd() / player.duration()) * 100}%`);

            firstError = true;
        });

        function clearToggles() {
            optionsMenuToggle = false;
            seasonsMenuToggle = false;
            qualityMenuToggle = false;
            lock = false;
        }

        function toggleQualityMenu() {
            hideOptionsMenu();
            hideQualityMenu();
            hideSeasonsMenu();

            // $('[data-button="level"]').click(function () {
            //     qualityMenuToggle = false;
            //     $('#qualityMenu').addClass('hidden');
            //     player.play();
            // });

            if (qualityMenuToggle == false) {
                showQualityMenu();
            }
            else {
                hideQualityMenu();
            }
        }
        function showQualityMenu() {
            qualityMenuToggle = true;
            // player.pause();
            $('#qualityMenu').removeClass('hidden');
        }
        function hideQualityMenu() {
            qualityMenuToggle = false;
            $('#qualityMenu').addClass('hidden');
        }

        function toggleOptionsMenu() {
            hideQualityMenu();
            hideSeasonsMenu();

            $('[data-button="audio"],[data-button="subtitle"]').click(function () {
                optionsMenuToggle = false;
                $('#qualityMenu').addClass('hidden');
                player.play();
            });

            if (optionsMenuToggle == false) {
                showOptionsMenu();
            }
            else {
                hideOptionsMenu();
            }
        }
        function showOptionsMenu() {
            optionsMenuToggle = true;
            player.pause();
            // $('#optionsMenu').removeClass('hidden');
            $("#optionsMenu").css('right', '0');
            $("#subtitle-window").addClass('hidden');
            $("#active_subtitle_" + textTracks.tracks_[currentCaption].label.replace('segment-metadata', 'Off').replace(' ', '_')).get(0).scrollIntoView({
                block: 'center'
            });
            $("#active_audio_" + audioTracks.tracks_[currentAudio].label.replace('SoundHandler', 'English')).replace(' ', '_').get(0).scrollIntoView({
                block: 'center'
              });
        }
        function hideOptionsMenu() {
            optionsMenuToggle = false;
            $('#optionsMenu').addClass('hidden');
            $("#optionsMenu").css('right', '-40vw');
            $("#subtitle-window").removeClass('hidden');
        }


        function toggleSeasonsMenu() {
            hideOptionsMenu();
            hideQualityMenu();
            hideSeasonsMenu();

            _seasonBack();
            let season = new URLSearchParams(window.location.search).get('season') || '01';
            _seasonForward(season);
            $('#season-menu-name').text(`Season ${season != 0 ? season : 'specials'}`);

            scroll_to_div(`#${new URLSearchParams(window.location.search).get('item') || player.playlist.currentIndex()}`);

            $('[data-button="video"]').click(function () {
                qualityMenuToggle = false;
                $('#seasonsMenu').addClass('hidden');
            });

            if (optionsMenuToggle == false) {
                showSeasonsMenu();
            }
            else {
                hideSeasonsMenu();
            }
        }
        function showSeasonsMenu() {
            seasonsMenuToggle = true;
            player.pause();
            $('#seasonsMenu').removeClass('hidden');
            $("#seasonsMenu").css('right', '0');
            $("#subtitle-window").addClass('hidden');
        }
        function hideSeasonsMenu() {
            seasonsMenuToggle = false;
            $('#seasonsMenu').addClass('hidden');
            $("#seasonsMenu").css('right', '-40vw');
            $("#subtitle-window").removeClass('hidden');
        }


        function dynamicControls() {
            showControls();

            clearTimeout(timer);

            if (lock == false && optionsMenuToggle == false && qualityMenuToggle == false && seasonsMenuToggle == false) {
                timer = setTimeout(hideControls, 5000);
            }
        }
        function showControls() {
            $("#overlay, .video-js").addClass("active");
            $('.video-js').css('cursor', 'unset');
        }
        function hideControls() {
            if (lock == false) {
                $("#overlay, .video-js").removeClass("active");
                $('.video-js').css('cursor', 'none');
            }
        }

        function toggleFullscreen() {
            if(player.isFullscreen_){
                player.exitFullscreen();
            }
            else{
                player.requestFullscreen();
            }

            $("#btn-fullscreen").toggleClass( "fullscreen","fullscreen-not");

        }
        function togglePlayback() {
            switch (player.paused()) {
                case true:
                    player.play();
                    break;
                case false:
                    player.pause();
                    break;
                default:
                    break;
            }
        }
        function toggleMute() {
            player.muted(!player.muted());
        }

        function _seasonBack() {
            $('.season').addClass('hidden');
            $('#seasons-container').addClass('hidden');
            $('#season-menu-name').text(``);
            $('[data-button="season-back"]').addClass('hidden');
            $("#season-forward-container").removeClass('hidden');
        }

        function _seasonForward(season) {
            season = String(zeroPad(season, 2));
            $('.season').addClass('hidden');
            $('#seasons-container').removeClass('hidden').addClass('flex');
            $('#seasonsMenu').removeClass('hidden').addClass('flex');
            $("#season-forward-container").addClass('hidden');
            $('[data-button="season-back"]').removeClass('hidden');
            $('#season-menu-name').text(`Season ${season != 0 ? season : 'specials'}`);
            $(`#season-${season != 0 ? season : 'specials'}-container`).removeClass('hidden');
        }

        function buttonDisplay() {

            if (player.playlist().length > 1) {
                $("#previous").removeClass('hidden');
                $("#next").removeClass('hidden');
            }

            if (player.playlist.currentIndex() > 0 && player.playlist().length > 1) {
                $("#previous").removeClass('hidden');
                $("#previous").attr("title", player.playlist()[player.playlist.currentIndex() - 1].title);
            } else {
                $("#previous").addClass('hidden');
            }
            if (!player.playlist()[player.playlist.currentIndex() + 1] && player.playlist().length >= 1) {
                $("#next").addClass('hidden');
            } else {
                $("#next").removeClass('hidden');
                $("#next").attr("title", player.playlist()[player.playlist.currentIndex() + 1].title);
            }
            let last;
            for (i = player.playlist.currentIndex(); i < player.playlist().length; i++) {
                if (player.playlist()[i].file !== null) {
                    last = i;
                    this.last = i;
                }
            }
            if (player.playlist.currentIndex() == last) {
                $("#next").addClass('hidden');
            }

            if (player.playlist().length > 1) {
                $('#seasons').removeClass('hidden');
            }

            if (player.qualityLevels().length > 1) {
                $('#levels').removeClass('hidden');
            }

        }

        // Sync or storage functions.

        function setSubtitleTrack() {
            if (typeof localStorage !== 'undefined') {
                player.textTracks().tracks_.forEach((e, index) => {
                    if (e.label != null && e.label.replace('segment-metadata', 'Off').replace(' ', '_') == localStorage.getItem('nmplayer.captionLabel')) {
                        setCurrentCaption(index);
                    }
                });
            }
            nmplayer.opus();
        }

        function setAudioTrack() {
            if (typeof localStorage !== 'undefined') {
                player.audioTracks().tracks_.forEach((e, index) => {
                    if (e.label != null && e.label.replace(' ', '_').replace('SoundHandler', 'English') == localStorage.getItem('nmplayer.audioLabel')) {
                        setCurrentAudio(index);
                    }
                });

            }
        }


        function preloadImages() {
            player.playlist().forEach(function (i) {
                i.tracks.forEach(function (t) {
                    if (t.kind == 'thumbnails') {
                        $('.preload-images').attr('src', t.file.replace("previews.vtt", "sprite.webp"));
                    }
                });
            });
        }

        // Tools.

        function scroll_to_div(hash) {
            let target = $(hash);
            headerHeight = 200;
            if (target.length) {
                $(".active>.episode-list").stop().animate({
                    scrollTop: target.offset().top - headerHeight // offsets for fixed header
                }, "linear");
            }
        }
        function humanTime(time) {
            var st;
            ct = parseInt(time); // ct is current time

            if (st != ct) {
                st = ct;
                var hours = parseInt(st / 3600);
                var minutes = parseInt(
                    (st % 3600) / 60
                );
                var seconds = parseInt(st % 60);
                if (("" + minutes).length == 1) {
                    minutes = "0" + minutes;
                }
                if (("" + seconds).length == 1) {
                    seconds = "0" + seconds;
                }
                if (hours != 0) {
                    hours = "" + hours + ":";
                } else {
                    hours = "";
                }
                current = hours + minutes + ":" + seconds;
            }
            return current.replace("NaN:NaN:NaN", "00:00");
        }
        function convertToSeconds(hms) {
            var a = hms.split(':');
            return (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
        }

        function zeroPad(num, places) {
            var zero = places - num.toString().length + 1;
            return Array(+(zero > 0 && zero)).join("0") + num;
        }
        function sleep(ms) {
            return new Promise(resolve => setTimeout(resolve, ms));
        }

        // Events.

        function onClick(e) {
            switch (e.target.parentNode.dataset.button) {
                case "back":
                    if (!player.playlist()[player.playlist.currentItem()].production && (typeof player.playlist.currentIndex().length == 'undefined' || player.playlist.currentIndex() == player.playlist.currentIndex().length) && (player.duration() - player.currentTime() < 600)) {
                        nmplayer.theEnd();
                    }
                    else {
                        nmplayer.goBack();
                    }
                    break;
                case "close":
                    hideControls();
                    hideOptionsMenu();
                    hideSeasonsMenu();
                    hideQualityMenu();
                    hideControls();
                    break;
                case "quality":
                    toggleQualityMenu();
                    break;
                case "options":
                    toggleOptionsMenu();
                    break;
                case "previous":
                    $('.spinner').removeClass('hidden');
                    nmplayer.previous();
                    break;
                case "next":
                    $('.spinner').removeClass('hidden');
                    nmplayer.next();
                    break;
                case "playback":
                    togglePlayback();
                    break;
                case "fullscreen":
                    toggleFullscreen();
                    break;
                case "seasons":
                    toggleSeasonsMenu();
                    break;
                case "season-back":
                    _seasonBack();
                    break;
                case "season-forward":
                    _seasonForward(e.target.dataset.season || 'Specials');
                    break;
                case "mute":
                    toggleMute();
                    break;
                case "slider":
                    break;
                case "session":
                    changeAspect();
                    break;
                case "language":
                    $('#languages').show();
                    $('.qualityMenuItem').hide();$('#languages').show();
                    $('.qualityMenuButton').removeClass('opacity-100').addClass('opacity-50');
                    $('#language').removeClass('opacity-50').addClass('opacity-100');
                    break;
                case "subtitle":
                    $('#subtitles').show();
                    $('.qualityMenuItem').hide();$('#subtitles').show();
                    $('.qualityMenuButton').removeClass('opacity-100').addClass('opacity-50');
                    $('#subtitle').removeClass('opacity-50').addClass('opacity-100');
                    break;
                case "level":
                    $('.qualityMenuItem').hide();
                    $('#qualities').show();
                    $('.qualityMenuButton').removeClass('opacity-100').addClass('opacity-50');
                    $('#level').removeClass('opacity-50').addClass('opacity-100');
                    break;
                case "aspect":
                    $('.qualityMenuItem').hide();
                    $('#aspects').show();
                    $('.qualityMenuButton').removeClass('opacity-100').addClass('opacity-50');
                    $('#aspect').removeClass('opacity-50').addClass('opacity-100');
                    break;
                case "href":
                    window.location = e.target.dataset.href;
                    break;
                default:
                    // console.log("no data-button property set");
                    break;
            }
            sleep(2000);
        }

        function onVideoClick(e) {
            if (!isMobile.any()) {
                togglePlayback();
            }
            if (isSeeking) {
                if (e.pageX < Math.floor(window.innerWidth / 3.5)) {
                    nmplayer.rewindVideo();
                }
                if (e.pageX > (window.innerWidth - Math.floor(window.innerWidth / 3.5))) {
                    nmplayer.forwardVideo();
                }
            }
        }
        function onVideoDblclick(e) {
            const video = document.querySelector('video');
            e.preventDefault();
            e.stopPropagation();
            if (e.pageX < Math.floor(window.innerWidth / 3.5)) {
                isSeeking = true;
                nmplayer.rewindVideo();
            }
            if (e.pageX > (window.innerWidth - Math.floor(window.innerWidth / 3.5))) {
                isSeeking = true;
                nmplayer.forwardVideo();
            }
            if (e.pageX > Math.floor(window.innerWidth / 3.5) && e.pageX < (window.innerWidth - Math.floor(window.innerWidth / 3.5))) {
                if (e.pageY < (window.innerHeight - Math.floor(window.innerHeight / 1.4))) {
                    if (video.volume != 1) {
                        video.volume = video.volume + 0.1;
                    }
                }
                if ((e.pageY > (window.innerHeight - Math.floor(window.innerHeight / 1.4)) && e.pageY < Math.floor(window.innerHeight / 1.6)) || !isMobile.any()) {
                    isSeeking = false;
                    toggleFullscreen();
                }
                if (e.pageY > Math.floor(window.innerHeight / 1.6)) {
                    video.volume = video.volume - 0.1;
                }
            }
        }

        function addEventListeners() {
            removeEventListeners();
            $("[data-button]").click(function (e) { onClick(e); });
            $("#overlay").dblclick(function (e) { onVideoDblclick(e); });
            $("#overlay-center, video").mousemove(function (e) {
                dynamicControls(e);
            }).click(function (e) {
                onVideoClick(e);
            });
            if(player.playlist().length > 1){
                setTimeout(() => {
                    loadSeasons();
                }, this.leeway);
            }

            document.onkeydown = function (e) {
                switch (e.keyCode) {
                    case 107:
                        nmplayer.expediteSub();
                        break;
                    case 109:
                        nmplayer.delaySub();
                        break;
                    case 32:
                        togglePlayback();
                        break;
                    case 37:
                        nmplayer.rewindVideo();
                        break;
                    case 39:
                        nmplayer.forwardVideo();
                        break;

                    default:
                        break;
                }
            };
        }

        function removeEventListeners() {
            $("[data-button]").off();
            $("#overlay").off();
            $("#overlay-center").off();
            $("#overlay-center").off();
        }

        // Dynamic builds based on player data.

        function clearLists() {
            $(`#captions`).empty();
            $(`#subtitles`).empty();
            $('#audios').empty();
            $('#qualityMenu').empty();
        }

        function loadSeasons() {
            $(`#season-forward-container`).empty();
            $(`#seasons-container`).empty();

            if (new URLSearchParams(window.location.search).get('type') == null) {
                let season = 999,
                    style;

                player.playlist().forEach((e, i) => {
                    if (e.season != season) {
                        var src;
                        season = e.season;
                        // if(supportsBackgroundLoading){
                        src = `src="${e.season_image || 'https://cdn.nomercy.tv/img/poster-not-available.png'}" onerror="this.src='https://cdn.nomercy.tv/img/poster-not-available.png'"`;
                        // }
                        $('#season-forward-container').append(`
                                <div onclick="nmplayer.seasonForward(${e.season != '00' ? e.season : 'Specials'})" class="flex flex-col mb-2 overflow-hidden season-forward" style="background:${i % 2 == 0 ? '#0a090980' : '#000000ba'}">
                                    <div class="flex flex-row overflow-hidden season-forward">
                                        <div class="flex flex-col justify-center float-left w-32 mx-auto">
                                            <img ${src} alt="" style="display: block;height: 100%;margin: auto; padding: 10%;max-height: 100%;">
                                        </div>
                                        <div class="flex flex-col float-left w-10/12 h-full mr-4">
                                            <div class="flex flex-col h-4 mx-4 my-4 text-center">
                                                ${e.season != 0 ? ' Season ' + e.season : 'Specials'}
                                            </div>
                                            <div class="flex flex-col float-left h-16 mx-4 my-2 overflow-hidden text-xs" style="min-width: 73%;">
                                                ${e.season_overview || ''}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            `);
                    }
                });
                season = 999;
                style = '';
                player.playlist().forEach((e, i) => {
                    if (e.season != season) {
                        season = e.season;
                        if (e.season == new URLSearchParams(window.location.search).get('season')) {
                            style = 'active';
                        } else {
                            style = '';
                        }
                        $('#seasons-container').append(`
                                <div id="season-${e.season != 0 ? e.season : 'Specials'}-container" class="season h-full w-full overflow-hidden flex-col ${style}">
                                    <div id="season-${e.season != 0 ? e.season : 'Specials'}-list" data-seasonList="${e.season != 0 ? e.season : 'Specials'}" class="w-full p-3 mb-2 overflow-y-auto season-list" style="overflow-x: hidden;"></div>
                                </div>
                            `);
                    }
                });
                player.playlist().forEach((e, i) => {

                    var src;
                    src = `src="${e.image != 'Specials' ? e.image.replace('original', 'original') : '.x'}" onerror="this.src='https://cdn.nomercy.tv/img/still-not-available.png'"`;
                    $(`#season-${e.season != '00' ? e.season : 'Specials'}-list`).append(`
                            <div id="${i}" data-button="video" onclick="nmplayer.setEpisode(${e.season || 0},${e.episode || 'Specials'})" class="w-full h-32 p-2 mb-2" style="background:${i % 2 == 0 ? '#0a090980' : '#000000ba'}">
                                <div data-button="video" class="w-full p-2 text-sm">
                                S${zeroPad(e.season || 0, 2)}E${zeroPad(e.episode || 0, 2)} ${e.title.replace('S H I E L D', 'S.H.I.E.L.D.')}
                                </div>
                                <div class="flex-row h-20">
                                    <div data-button="video" class="float-left w-3/12 h-full px-2 py-1">
                                        <img data-button="video" ${src} alt="" style="display: block;margin: auto;max-height: 100%;">
                                    </div>
                                    <div data-button="video" id="" class="float-left w-9/12 p-1 text-xs overview">
                                        ${e.description}
                                    </div>
                                </div>
                            </div>
                        `);
                });
            }
            else {
                $(`#seasons-container`).append(`
                        <div data-button="season-back" id="season-1-container" class="season-list-container" style="display:block;width: 100%;margin-top: 50px;">
                            <div data-button="season-back"  id="season-1-button" data-season="1" class="episodes" style="overflow-x: hidden;"></div>
                            <div id="season-1-list" data-seasonList="1" class="episode-list" style="overflow-x: hidden;"></div>
                        </div>
                    `);
                $(`#seasons-container`).addClass('active');
                player.playlist().forEach((e, i) => {
                    $('#season-1-list').append(`
                        <div id="${i}" data-button="video" onclick="nmplayer.setEpisode(1,${i || 0})" class="w-full p-2" style="background:${i % 2 == 0 ? '#0a090980' : '#000000ba'}">
                            <div data-button="video" class="w-full p-2">
                                #${i} ${e.title.replace('S H I E L D', 'S.H.I.E.L.D.')}
                            </div>
                            <div class="row w-100">
                                <div data-button="video" class="float-left w-3/12 p-1">
                                    <img data-button="video" id="" src="${e.image != null ? e.image.replace('original', 'w300') : '.x'}" onerror="this.src='https://cdn.nomercy.tv/img/noimage.thumbnail.jpg'" alt="" style="display: block">
                                </div>
                                <div data-button="video" id="" class="float-left w-9/12 p-1 text-sm" style="font-size: 0.8rem;line-height:1rem;font-weight: 500;overflow: hidden;height: 4rem;">
                                    ${e.description}
                                </div>
                            </div>
                        </div>
                    `);
                });
            }
        }

        this.loadPreviewSlider = () => {
            let previewTime = [];
            let m;
            if(typeof player.playlist()[player.playlist.currentItem()].tracks != 'undefined'){
                $("#slider-image").css('background-image', `url('${player.playlist()[player.playlist.currentItem()].tracks[0].file.replace("previews.vtt", "sprite.webp").replace('chapters.vtt', 'sprite.webp')}')`);
                $.get(player.playlist()[player.playlist.currentItem()].tracks[0].file, function (data) {
                    const regex = /(\d{2}:\d{2}:\d{2})\.\d{3}\s-->\s(\d{2}:\d{2}:\d{2})\.\d{3}\nsprite\.webp#(xywh=\d{1,},\d{1,},\d{1,},\d{1,})/gm;
                    while ((m = regex.exec(data)) !== null) {
                        if (m.index === regex.lastIndex) {
                            regex.lastIndex++;
                        }
                        previewTime.push({
                            start: m[1],
                            end: m[2],
                            currentTime: m[3]
                        });
                    }
                });

                $('#slider').mousemove(function (e) {
                    lock = true;
                    let parentOffset = $(this).offset().left,
                        thisOffset = $(this).offset(),
                        relativeXcurrentTime = (e.pageX - thisOffset.left),
                        sliderTime = (player.duration() / $('#slider').width()) * relativeXcurrentTime,
                        popLeft = relativeXcurrentTime;

                    $("#subtitle-window").addClass('hidden');
                    $("#time-nipple").removeClass('hidden');
                    $("#time-nipple").css('left', relativeXcurrentTime);

                    $("#slider-pop").removeClass('hidden').addClass('flex');
                    $("#slider-pop").css('left', popLeft);
                    $("#slider-time").html(humanTime(sliderTime));

                    previewTime.forEach((match) => {
                        let start = convertToSeconds(match.start),
                            end = convertToSeconds(match.end);
                        if (sliderTime >= start && sliderTime < end) {
                            $("#slider-image").css('background-position', `-${match.currentTime.split('=')[1].split(',')[0]}px -${match.currentTime.split('=')[1].split(',')[1]}px`);
                        }
                    });
                }).mouseleave(function (e) {
                    $("#subtitle-window").removeClass('hidden');
                    $("#slider-pop").addClass('hidden');
                    $("#time-nipple").addClass('hidden');
                    lock = false;
                }).click(function (e) {
                    lock = false;
                    let offset = $(this).offset();
                    let left = e.pageX - offset.left;
                    let totalWidth = $("#slider").width();
                    let percentage = left / totalWidth;
                    let videoTime = parseInt(player.duration() * percentage);
                    player.currentTime(videoTime);
                    nmplayer._setTime();
                });
            }
        };

        this.getCurrentCaption = () => {
            return currentCaption;
        };
        this.setCurrentCaption = (index) => {
            setCurrentCaption(index);
        };
        function setCurrentCaption(index){
            currentCaption = index;

            let textTracks = player.textTracks();

            textTracks.tracks_.forEach((data, index) => {
                if(index != currentCaption){
                    textTracks[index].mode = 'hidden';
                }
                else {
                    textTracks[index].mode = 'showing';
                }
            });

            $(".activeSubItem").addClass('hidden').parent().removeClass('opacity-100').addClass('opacity-50');

            if(typeof textTracks.tracks_[currentCaption] != 'undefined'){
                $("#active_subtitle_" + textTracks.tracks_[currentCaption].label.replace('segment-metadata', 'Off').replace(' ', '_')).removeClass('hidden').parent().removeClass('opacity-50').addClass('opacity-100');
                localStorage.setItem('nmplayer.captionLabel', textTracks.tracks_[currentCaption].label.replace('segment-metadata', 'Off').replace(' ', '_'));
            }

            if(currentCaption == -1){
                $("#active_subtitle_Off").removeClass('hidden').parent().removeClass('opacity-50').addClass('opacity-100');
            }

            nmplayer.opus();
            player.play();
        }

        this.getCurrentAudio = () => {
            return currentAudio;
        };
        this.setCurrentAudio = (index) => {
            setCurrentAudio(index);
        };

        function setCurrentAudio(index){
            currentAudio = parseInt(index);

            let audioTracks = player.audioTracks();

            audioTracks[currentAudio].enabled = true;

            audioTracks.on('change', function() {
                $(".activeAudioItem").addClass('hidden').parent().removeClass('opacity-100').addClass('opacity-50');
                if(audioTracks.length > 0){
                    $("#active_audio_" + audioTracks.tracks_[currentAudio].label.replace('segment-metadata', 'Off').replace(' ', '_')).removeClass('hidden').parent().removeClass('opacity-50').addClass('opacity-100');

                    localStorage.setItem('nmplayer.audioLabel', audioTracks.tracks_[currentAudio].label.replace('segment-metadata', 'Off').replace(' ', '_'));
                }
            });
            player.play();
        }

        this.getCurrentQuality = () => {
            return this.Quality;
        };
        this.setCurrentQuality = (index) => {
            setCurrentQuality(index);
        };

        function setCurrentQuality(index){
            currentQuality = parseInt(index);

            let quality = player.qualityLevels();

            quality.levels_.forEach(function(e,i){
                if(i != currentQuality){
                    quality[i].enabled = false;
                }
                else {
                    quality.levels_[currentQuality].enabled = true;
                }
            });

            player.play();

            quality.on('change', function() {
                $(".activeQualityItem").addClass('hidden').parent().removeClass('opacity-100').addClass('opacity-50');
                $("#active_quality_" + quality[quality.selectedIndex].height).removeClass('hidden').prev().removeClass('hidden').parent().removeClass('opacity-50').addClass('opacity-100');
                localStorage.setItem('nmplayer.QualityLabel', quality[quality.selectedIndex].height);
            });
        }

        this.setUpNext = (e) => {
            eggBoiled = e;
        };
        this.nextClick = () => {
            clearTimeout(next);
            $('.nextup').addClass('hidden');
            nmplayer.next();
        };
        this.setSubtitleFont = (param, value, save) => {

            if(value == 'delete'){
                console.log(param, value);
                $("#subtitle-window").css(param, 'unset');
                if(save == true){
                    if (typeof localStorage !== 'undefined') {
                        localStorage.removeItem(`subtitle-font-${param}`);
                    }
                }
            }
            else {
                console.log(param, value);

                $("#subtitle-window").css(param, value);

                if (typeof localStorage !== 'undefined' && save == true) {
                    localStorage.setItem(`subtitle-font-${param}`, value);
                }
            }
        };
        this.seasonForward = (s) => {
            _seasonForward(s);
        };
        this.goBack = () => {
            location.href = document.location.protocol + '//' + document.location.hostname + (f.subroute || '') + player.playlist()[player.playlist.currentItem()].backroute;
        };
        this.previous = () => {
            if (player.playlist.currentItem() === 0) {

            } else {
                player.playlist.previous();
            }
        };
        this.next = () => {
            if (player.playlist.currentItem() != player.playlist.lastIndex()) {
                player.playlist.next();
            } else {

            }
        };
        this.setEpisode = (season, episode) => {
            $('.spinner').removeClass('hidden');
            player.playlist().forEach((e, index) => {
                if (new URLSearchParams(window.location.search).get('type') == null) {
                    if (e.season == season && e.episode == episode) {
                        if(e.sources.length == 0){
                            nmplayer.setEpisode(season, episode + 1);
                        }
                        else{
                            player.playlist.currentItem(index);
                        }
                    }
                }
                else {
                    if (index == episode) {
                        player.playlist.currentItem(index);
                    }
                }
            });
        };
        this.subtitleOffset = (e) => {
            subOffset = parseInt(e);
        };
        this.expediteSub = () => {
            subOffset = subOffset + 1;
            clearTimeout(message);
            $('#player-message').removeClass('hidden');
            $('#player-message').html(`Subtitle delay: ${subOffset * 10}ms`);
            message = setTimeout(() => {
                $('#player-message').addClass('hidden');
                $('#player-message').empty();
            }, 2000);
        };
        this.delaySub = () => {
            subOffset = subOffset - 1;
            clearTimeout(message);
            $('#player-message').removeClass('hidden');
            $('#player-message').html(`Subtitle delay: ${subOffset * 10}ms`);
            message = setTimeout(() => {
                $('#player-message').addClass('hidden');
                $('#player-message').empty();
            }, 2000);
        };

        this.cycleSubtitles = () => {

            // if(player.getthis.nmplayer.currentCaption() < nmplayer.currentCaption.length){
            //     player.setthis.nmplayer.currentCaption(player.getthis.nmplayer.currentCaption() + 1);
            // }
            // else {
            //     player.setthis.nmplayer.currentCaption(0);
            // }

            clearTimeout(message);
            $('#player-message').removeClass('hidden');
            // $('#player-message').html("Subtitle: " + player.getCaptionsList()[player.getcurrentCaption()].label);
            message = setTimeout(() => {
                $('#player-message').addClass('hidden');
                $('#player-message').empty();
            }, 2000);
        };

        this.changeAspectRatio = (val) => {
            $('.video-js video').css({
                objectFit: val
            });
            $('.activeAspectItem').hide().parent().removeClass('opacity-100').addClass('opacity-50');$(`#active_aspect_${val}`).show().parent().removeClass('opacity-50').addClass('opacity-100');
        };

        this.rewindVideo = () => {
            tapCount = tapCount + 10;
            $(".rewind.notification").removeClass('hidden').addClass('flex');
            $("span.rewind").text(`${Math.abs(tapCount)} seconds`);
            clearTimeout(tap);
            tap = setTimeout(function () {
                $(".rewind.notification").addClass('hidden');
                player.currentTime(player.currentTime() - tapCount);
                tapCount = 0;
                isSeeking = false;
                player.play();
            }, this.leeway);
        };
        this.forwardVideo = () => {
            tapCount = tapCount + 10;
            $(".forward.notification").removeClass('hidden').addClass('flex');
            $("span.forward").text(`${Math.abs(tapCount)} seconds`);
            clearTimeout(tap);
            tap = setTimeout(function () {
                $(".forward.notification").addClass('hidden');
                player.currentTime(player.currentTime() + tapCount);
                tapCount = 0;
                isSeeking = false;
                player.play();
            }, this.leeway);
        };

        player.on('loadedmetadata', function() {

            $(`#subtitles`).empty();
            $(`#languages`).empty();
            $(`#qualities`).empty();

            let textTracks = player.textTracks();
            if (textTracks.tracks_.length > 0 && !location.pathname.startsWith('/streams')) {
                $('#options').removeClass('hidden');
                $('#subtitle').removeClass('hidden');
                $('#subtitles').removeClass('hidden');
                $('.qualityMenuButton').removeClass('opacity-100').addClass('opacity-50');
                $('#subtitle').removeClass('opacity-50').addClass('opacity-100');
            }
            if(player.src().endsWith('mp4')){
                $('#subtitles').append(`
                    <div class="flex flex-row h-10 px-4 py-2 opacity-100 cursor-pointer w-12/12 hover:opacity-100" onclick="nmplayer.setCurrentCaption(-1)">
                        <div id="active_subtitle_Off" class="hidden w-1/12 activeSubItem" onclick="nmplayer.setCurrentCaption(-1)">✔</div>
                        <p id="subtitle_Off" class="float-left w-11/12 pr-3 ml-auto text-right" onclick="nmplayer.setCurrentCaption(-1)">
                            Off
                        </p>
                    </div>
                `);
            }
            textTracks.tracks_.forEach((data, index) => {
                data.mode = 'hidden';
                $('#subtitles').append(`
                    <div class="flex flex-row h-10 px-4 py-2 opacity-100 cursor-pointer w-12/12 hover:opacity-100" onclick="nmplayer.setCurrentCaption(${index})">
                        <div id="active_subtitle_${data.label.replace('segment-metadata', 'Off').replace(' ', '_')}" class="${index == 0 ? 'hidden' : ''} w-1/12 activeSubItem" onclick="nmplayer.setCurrentCaption(${index})">✔</div>
                        <p id="subtitle_${data.label.replace('segment-metadata', 'Off').replace(' ', '_')}" class="float-left w-11/12 pr-3 ml-auto text-right" onclick="nmplayer.setCurrentCaption(${index})">
                            ${data.label.replace('segment-metadata', 'Off')}
                        </p>
                    </div>
                `);
            });




            let audioTracks = player.audioTracks();
            if (audioTracks.tracks_.length > 1) {
                $('#options').removeClass('hidden');
                $('#language').removeClass('hidden');
                $('#languages').removeClass('hidden');
                $('.qualityMenuButton').removeClass('opacity-100').addClass('opacity-50');
                $('#language').removeClass('opacity-50').addClass('opacity-100');
            }
            audioTracks.tracks_.forEach((data, index) => {
                $('#languages').append(`
                    <div class="flex flex-row h-10 px-4 py-2 opacity-100 cursor-pointer w-12/12 hover:opacity-100" onclick="nmplayer.setCurrentAudio(${index})">
                        <div id="active_audio_${data.label}" class="${index == 0 ? 'hidden' : ''} w-1/12 activeAudioItem" onclick="nmplayer.setCurrentAudio(${index})">✔</div>
                        <p id="audio_${data.label}" class="float-left w-11/12 pr-3 ml-auto text-right" onclick="nmplayer.setCurrentAudio(${index})">
                        ${data.label.replace('SoundHandler', 'English')}
                        </p>
                    </div>
                `);
            });
            $(".activeAudioItem").addClass('hidden').parent().removeClass('opacity-100').addClass('opacity-50');
            if (audioTracks.tracks_.length > 0) {
                $("#active_audio_" + audioTracks.tracks_[currentAudio].label).removeClass('hidden').parent().removeClass('opacity-50').addClass('opacity-100');
            }

            let qualityLevels = player.qualityLevels();

            if (qualityLevels.length > 1) {
                $('#options').removeClass('hidden');
                $('#levels').removeClass('hidden');
                $('#qualities').removeClass('hidden');
                $('.qualityMenuButton').removeClass('opacity-100').addClass('opacity-50');
                $('#levels').removeClass('opacity-50').addClass('opacity-100');
            }
            qualityLevels.levels_.forEach((data, i) => {
                $('#qualities').append(`
                    <div class="flex flex-row h-10 px-4 py-2 opacity-100 cursor-pointer w-12/12 hover:opacity-100" onclick="nmplayer.setCurrentQuality(${i})">
                        <div id="active_quality_${i}" class="${i == 0 ? 'hidden' : ''} w-1/12 activeQualityItem" onclick="nmplayer.setCurrentQuality(${i})">✔</div>
                        <p id="active_quality_${data.height}" class="float-left w-11/12 pr-3 ml-auto text-right" onclick="nmplayer.setCurrentQuality(${i})">
                            ${data.height}
                        </p>
                    </div>
                `);

            });

            setTimeout(() => {
                if(qualityLevels.length < 2 && audioTracks.tracks_.length < 2 && textTracks.tracks_.length < 1){
                    $('#aspects').removeClass('hidden');
                    $('.qualityMenuButton').removeClass('opacity-100').addClass('opacity-50');
                    $('#aspects').removeClass('opacity-50').addClass('opacity-100');
                }
            }, 1500);

            nmplayer.setCurrentQuality(nmplayer.getCurrentQuality());

        });

        this.opus = () => {

            if (typeof window.octopusInstance != 'undefined' && typeof window.octopusInstance.video != 'undefined' && window.octopusInstance.video.parentNode != null) {
                window.octopusInstance.dispose();
            }
            $('.libassjs-canvas-parent').remove();

            var subtitleURL = (typeof player.textTracks().tracks_[nmplayer.getCurrentCaption()] != 'undefined' && typeof player.textTracks().tracks_[nmplayer.getCurrentCaption()].src != 'undefined') ? player.textTracks().tracks_[nmplayer.getCurrentCaption()].src : '#';

            var options = {
                video: document.querySelector("video"),
                subUrl: subtitleURL,
                lossyRender: true,
                fonts: [
                    "https://cdn.nomercy.tv/fonts/OpenSans-Bold.ttf",
                    "https://cdn.nomercy.tv/fonts/OpenSans-BoldItalic.ttf",
                    "https://cdn.nomercy.tv/fonts/OpenSans-ExtraBold.ttf",
                    "https://cdn.nomercy.tv/fonts/OpenSans-ExtraBoldItalic.ttf",
                    "https://cdn.nomercy.tv/fonts/OpenSans-Italic.ttf",
                    "https://cdn.nomercy.tv/fonts/OpenSans-Light.ttf",
                    "https://cdn.nomercy.tv/fonts/OpenSans-LightItalic.ttf",
                    "https://cdn.nomercy.tv/fonts/OpenSans-Regular.ttf",
                    "https://cdn.nomercy.tv/fonts/OpenSans-Semibold.ttf",
                    "https://cdn.nomercy.tv/fonts/OpenSans-SemiboldItalic.ttf",
                    'https://cdn.nomercy.tv/fonts/times.ttf',
                    'https://cdn.nomercy.tv/fonts/kaiu.ttf',
                ],
                debug: false,
                workerUrl: "/js/dist/subtitles-octopus-worker.js"
            };

            if(subtitleURL.endsWith('ass')){
                window.octopusInstance = new SubtitlesOctopus(options); // You can experiment in console
            }

        };


        this._getTime = () => {


            if (f.storage == "server") {

                if (urlQuery.get('type') !== null) {
                    postData = {
                        special_id: player.playlist()[player.playlist.currentItem()].tmdbid,
                        type: 'special',
                        _token: f.bearer,
                    };
                }
                else if (urlQuery.get('season') == null && urlQuery.get('episode') == null) {
                    postData = {
                        tmdbid: player.playlist()[player.playlist.currentItem()].tmdbid,
                        _token: f.bearer,
                    };
                }
                else {
                    postData = {
                        unique_id: player.playlist()[player.playlist.currentItem()].uid,
                        tmdbid: player.playlist()[player.playlist.currentItem()].tmdbid,
                        _token: f.bearer,
                    };
                }

                $.ajax({
                    url: `/sync/get`,
                    type: 'POST',
                    headers: {
                        Authorization: 'Bearer ' + f.bearer,
                        _token: f.bearer,
                    },
                    data: postData,
                    success: function (data) {
                        // console.log({ getTime: data });
                        _getSet(data);
                    },
                    error: function (data) {
                        console.warn({ setTime: data.responseJSON });

                        if (urlQuery.get('type') !== null && urlQuery.get('item') !== null) {
                            nmplayer.setEpisode(0, urlQuery.get('item'));
                        }
                        else if (urlQuery.get('season') !== null && urlQuery.get('episode') !== null) {
                            nmplayer.setEpisode(urlQuery.get('season'), urlQuery.get('episode'));
                        }
                        else {
                            nmplayer.setEpisode(1, 1);
                        }
                    },
                });
            }
            else {

                let unique_id = player.playlist()[player.playlist.currentItem()].uid;
                let data = JSON.parse(localStorage.getItem(`item-${unique_id}`));
                if(data){
                    _getSet(data);
                }
            }
        };

        function _getSet(data){

            if(Object.keys(data).length == 0){
                if (urlQuery.get('season') != null && urlQuery.get('episode') != null) {
                    nmplayer.setEpisode(urlQuery.get('season'), urlQuery.get('episode'));
                }
                else{
                    nmplayer.setEpisode(1, 1);
                }

                player.pause();
                player.play();
            }

            else if (urlQuery.get('type') !== null && urlQuery.get('item') !== null) {
                nmplayer.setEpisode(0, urlQuery.get('item'));
                if (player.playlist()[player.playlist.currentItem()].uid == data.unique_id) {
                    player.currentTime(parseInt(data.watched));
                }
            }
            else if (urlQuery.get('season') == null && urlQuery.get('episode') == null) {
                player.playlist().forEach((e, index) => {
                    if (e.uid == data.unique_id) {
                        player.playlist.currentItem(index);
                    }
                });
                setTimeout(() => {
                    player.currentTime(parseInt(data.watched));
                }, this.leeway);
            }

            if (data.unique_id && player.playlist()[player.playlist.currentItem()].uid == data.unique_id) {
                setTimeout(() => {
                    player.currentTime(parseInt(data.watched));
                }, this.leeway);
            }

            setTimeout(() => {
                player.audioTracks().tracks_.forEach((e, index) => {
                    if (e.label != null && e.label.replace('SoundHandler', 'English') == data.audio) {
                        nmplayer.setCurrentAudio(index);
                    }
                });
                player.textTracks().tracks_.forEach((e, index) => {
                    if (e.label != null && e.label.replace('segment-metadata', "null") == data.subtitle) {
                        nmplayer.setCurrentCaption(index);
                    }
                });
            }, 1000);

            window.newTime = 0;
        }

        this._setTime = () => {
            if(!location.pathname.startsWith('/streams')){
                let audio = player.audioTracks().tracks_[nmplayer.getCurrentAudio()] ? player.audioTracks().tracks_[nmplayer.getCurrentAudio()].label.replace('SoundHandler', 'English').replace(' ', '_') : null;
                let subtitle = player.textTracks().tracks_[nmplayer.getCurrentCaption()] ? player.textTracks().tracks_[nmplayer.getCurrentCaption()].label.replace('segment-metadata', "Off").replace(' ', '_') : null;

                if (f.storage == "server") {

                    $.ajax({
                        url: `/sync/set`,
                        type: 'POST',
                        headers: {
                            Authorization: 'Bearer ' + f.bearer,
                            _token: f.bearer,
                        },
                        data: {
                            _token: f.bearer,
                            special_id: urlQuery.get('type') == 'special' ? urlQuery.get('id') : null,
                            unique_id: player.playlist()[player.playlist.currentItem()].uid,
                            season_id: player.playlist()[player.playlist.currentItem()].season_id || null,
                            episode_id: player.playlist()[player.playlist.currentItem()].episode_id || null,
                            tmdbid: urlQuery.get('type') != 'special' ? player.playlist()[player.playlist.currentItem()].tmdbid : null,
                            watched: player.currentTime(),
                            subtitle: subtitle,
                            audio: audio
                        },
                        success: function (data) {
                            // console.log({ setTime: data });
                        },
                        error: function (data) {
                            console.warn({ setTime: data.responseJSON });
                        },
                    });
                }
                else {
                    let data = {
                        tmdbid: urlQuery.get('type') != 'special' ? player.playlist()[player.playlist.currentItem()].tmdbid : null,
                        season_id: player.playlist()[player.playlist.currentItem()].season_id || null,
                        episode_id: player.playlist()[player.playlist.currentItem()].episode_id || null,
                        watched: player.currentTime(),
                        special_id: urlQuery.get('type') == 'special' ? urlQuery.get('id') : null,
                        unique_id: player.playlist()[player.playlist.currentItem()].uid,
                        subtitle: subtitle,
                        audio: audio
                    };

                    let unique_id = player.playlist()[player.playlist.currentItem()].uid;
                    localStorage.setItem(`item-${unique_id}`, JSON.stringify(data));

                }
            }
        };

        player.on("loadedmetadata", (e) => {
            if (firstItem) {
                if (urlQuery.get('type') !== null && urlQuery.get('item') !== null) {
                    nmplayer.setEpisode(0, urlQuery.get('item'));
                    setTimeout(() => {
                        this._getTime();
                    }, this.leeway);
                }
                else if (urlQuery.get('type') == null && urlQuery.get('season') !== null && urlQuery.get('episode') !== null) {
                    nmplayer.setEpisode(urlQuery.get('season'), urlQuery.get('episode'));
                    setTimeout(() => {
                        this._getTime();
                    }, this.leeway);
                }
                else {
                    setTimeout(() => {
                        this._getTime();
                    }, this.leeway);
                    nmplayer.setEpisode(1, 1);
                }
                firstItem = false;
            }
            window.newTime = 0;
        });

        player.on("timeupdate", (e) => {
            if (player.currentTime() > (window.newTime + 5)) {
                window.newTime = player.currentTime();
                nmplayer._setTime();
            }
        });

        player.on('ended', function() {
            if (player.playlist.next() === undefined) {
                nmplayer.theEnd();
            }
        });

        this.theEnd = () => {

            if(!player.playlist()[player.playlist.currentItem()].production){
                $.ajax({
                    type: 'post',
                    data: {
                        _token: f.bearer,
                        id: player.playlist()[player.playlist.currentItem()].tmdbid,
                    },
                    url: "/dashboard/content/deletewatched",
                    success: function (data) {
                        nmplayer.goBack();
                    },
                    error: function (data) {
                        alert(`Can't delete the progress.`);
                    },
                });
            }
        };
    };
};
