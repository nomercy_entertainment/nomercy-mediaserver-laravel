videojs.registerPlugin('nmremote', function () {

    document.addEventListener('keydown', function (e) {
        switch (e.key) {

            case "MediaPlay": //Play
                player.play();
                break;
            case "Pause": // Pause
                player.pause();
                break;
            case " ": // Space bar
                player.togglePlayback();
                break;
            case "MediaStop": // Stop
                player.stop();
                break;
            case "MediaRewind": // Rewind
                player.rewindVideo();
                break;
            case "MediaFastForward": // Forward
                player.forwardVideo();
                break;
            case "MediaTrackPrevious": // Previous
                player.previous();
                break;
            case "MediaTrackNext": // Next
                player.next();
                break;
            case "Subtitle": // Subtitle
                player.cycleSubtitles();
                break;
            case "PageUp": // Prog up

                break;
            case "PageDown": // Prog down

                break;
            case "1":

                break;
            case "8":
                down();
                break;
            case "3":

                break;
            case "4":
                left();
                break;
            case "5":
                enter();
                break;
            case "6":
                right();
                break;
            case "7":

                break;
            case "2":
                up();
                break;
            case "9":

                break;
            case "0":

                break;
            case "ColorF0Red": // Red

                break;
            case "ColorF1Green": // Green

                break;
            case "ColorF2Yellow": // Yellow

                break;
            case "ColorF3Blue": // Blue

                break;
            case "ArrowUp":
                // up();
                break;
            case "ArrowRight":
                // right();
                break;
            case "ArrowDown":
                // down();
                break;
            case "ArrowLeft":
                // left();
                break;
            case "Enter":
                enter();
                break;

            default:
                // alert(e.key);
                break;
        }
    }, false);

    this.nmremote_initialized = true;
});
