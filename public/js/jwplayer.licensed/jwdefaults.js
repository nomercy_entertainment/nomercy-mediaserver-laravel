var jwDefaults = {

    "playbackRateControls": true,
    "nextUpDisplay": true,
    // "renderCaptionsNatively": true,
    "stagevideo": true,

    "stagevideo": true,
    "stretching": "uniform",
    "width": "100%",
    "primary": "html5",
    "hlshtml": true,
    "autostart": true,
    "mute": false,
    "preload": "auto",
    "abouttext": "Loving it!",
    "aspectratio": "16:9",
    "autostart": true,
    "controls": true,
    "displaydescription": false,
    "displaytitle": false,
    "flashplayer": "//ssl.p.jwpcdn.com/player/v/8.6.0/jwplayer.flash.swf",
    "mute": false,
    "ph": 1,
    "pid": "Tu6QjlQe",
    "playbackRateControls": false,
    "preload": "auto",
    "repeat": false,
    "stretching": "uniform",
    "height": 260,
    "width": "100%",
    "cast": {
    },
    "skin": {
        "controlbar": {
            "background": "rgba(0,0,0,0)",
            "icons": "#ffffff",
            "iconsActive": "#ff9900",
            "iconsactive": "#ff9900",
            "text": "#F2F2F2"
        },
        "menus": {
            "background": "#333333",
            "text": "rgba(255,255,255,0.8)",
            "textActive": "#E1C100"
        },
        "timeslider": {
            "progress": "#ff9900",
            "rail": "rgba(240,240,240,0.3)"
        },
        "tooltips": {
            "background": "#FFFFFF",
            "text": "#000000"
        }
    },
};

jwplayer.defaults = jwDefaults;
