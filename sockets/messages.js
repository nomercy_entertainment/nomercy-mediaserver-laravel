function formatMessage(username, userimage, text) {
  return {
    username,
    userimage,
    text,
    time: new Date().getTime()
  };
}

module.exports = formatMessage;
