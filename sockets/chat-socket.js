const fs = require('fs');

const formatMessage = require('./messages');
const {
    userJoin,
    getCurrentUser,
    userLeave,
    getRoomUsers
} = require('./user');

const botName = 'NoMercy Bot';


exports = module.exports = function (socket, io) {

    socket.on('joinRoom', ({
        username,
        room
    }) => {
        const user = userJoin(socket.id, username, room);

        socket.join(user.room);
        socket.emit('message', formatMessage(botName, '/img/NoMercy Bot.png', `Hi ${username}.\n Welcome to ${user.room.toLowerCase() != user.username.toLowerCase() ? user.room + "'s" : 'your'} chatroom.`));

        socket.to(user.room).emit('roomUsers', {
            room: user.room,
            users: getRoomUsers(user.room)
        });

        io.to(user.room).emit('viewers', getRoomUsers(user.room).length);
        console.log(username + ' connected to room: ' + room + ' with id: ' + socket.id);
    });

    socket.on('reConnect', ({
        username,
        room
    }) => {
        const user = userJoin(socket.id, username, room);

        socket.join(user.room);

        io.to(user.room).emit('roomUsers', {
            room: user.room,
            users: getRoomUsers(user.room)
        });
        io.to(user.room).emit('viewers', getRoomUsers(user.room).length);
    });

    socket.on('chatMessage', ({
        username,
        userimage,
        msg
    }) => {
        const user = getCurrentUser(socket.id);
        console.log({
            "message": {
                "username": username,
                "userimage": userimage,
                "message": msg,
                "timestamp": new Date().toString(),
            }
        });


        function formatSeconds(seconds) {
            var date = new Date(1970, 0, 1);
            date.setSeconds(seconds);
            return date.toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1");
        }


        function getStreamTime(path) {
            now = new Date();
            fileTime = fs.statSync(path).mtime;

            var timeDiff = now - fileTime;

            timeDiff /= 1000;

            return formatSeconds(Math.round(timeDiff));
        }

        if (msg.startsWith('!')) {

            username = botName;
            msg = msg.replace("!", "").replace(/^\s+|\s+$/g, '');

            let file = './public/activestreams/' + user.room + '.txt';
            uptime = getStreamTime(file);
            let contents = fs.readFileSync(file, 'utf8');


            switch (msg) {
                case 'ping':
                    message = `Pong`;
                    break;
                case 'pong':
                    message = `Ping`;
                    break;
                case 'uptime':
                    if (uptime && uptime != '' && contents != `offline\n`) {
                        message = `I am streaming for ${uptime}`;
                    }
                    else {
                        message = `Sorry, I am not streaming.`;
                    }
                    break;
                case 'help':
                    message = `The available commands are:\n!ping\n!pong\n!uptime`;
                    break;

                default:
                    username = botName;
                    message = `!${msg} is not a valid command, typ !help for the list of commands.`
                    break;
            }
        }
        else {
            message = msg;
            userimage = userimage
            username = username
        }

        io.to(user.room).emit('message', formatMessage(username, userimage, message));
    });

    socket.on('disconnect', () => {
        const user = userLeave(socket.id);

        if (user) {

            io.to(user.room).emit('roomUsers', {
                room: user.room,
                users: getRoomUsers(user.room)
            });
            io.to(user.room).emit('viewers', getRoomUsers(user.room).length);
        }
    });
}
