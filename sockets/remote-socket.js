
exports = module.exports = function (socket, io) {


    socket.on('subscribe', function (data) {
        console.log("User connected: " + data.room + " on socket: " + socket.id);
        socket.join(data.room);
    });

    // socket.on('send message', function(data) {
    //     console.log('sending room post', data.room);
    //     socket.broadcast.to(data.room).emit('conversation private post', {
    //         message: data.message
    //     });
    // });

    socket.on('remote-play', function (data) {
        socket.broadcast.to(data.room).emit('player-play', data);
    });
    socket.on('player-play', function (data) {
        socket.broadcast.to(data.room).emit('remote-play', data);
    });

    socket.on('remote-pause', function (data) {
        socket.broadcast.to(data.room).emit('player-pause', data);
    });
    socket.on('player-pause', function (data) {
        socket.broadcast.to(data.room).emit('remote-pause', data);
    });

    socket.on('remote-time', function (data) {
        socket.broadcast.to(data.room).emit('player-time', data);
    });
    socket.on('player-time', function (data) {
        socket.broadcast.to(data.room).emit('remote-time', data);
    });

    socket.on('remote-playlistItem', function (data) {
        socket.broadcast.to(data.room).emit('player-playlistItem', data);
    });
    socket.on('player-playlistItem', function (data) {
        socket.broadcast.to(data.room).emit('remote-playlistItem', data);
    });

    socket.on('remote-next', function (data) {
        socket.broadcast.to(data.room).emit('player-next', data);
    });
    socket.on('remote-previous', function (data) {
        socket.broadcast.to(data.room).emit('player-previous', data);
    });

    socket.on('remote-setEpisode', function (data) {
        socket.broadcast.to(data.room).emit('player-setEpisode', data);
    });

    socket.on('remote-seek', function (data) {
        socket.broadcast.to(data.room).emit('player-seek', data);
    });

    socket.on('remote-subtitleOffset', function (data) {
        console.log('sending subtitleOffset', data.offset);
        socket.broadcast.to(data.room).emit('player-subtitleOffset', data);
    });


    socket.on('remote-ask', function (data) {
        socket.broadcast.to(data.room).emit('player-ask', data);
    });
    socket.on('player-answer', function (data) {
        socket.broadcast.to(data.room).emit('remote-answer', data);
    });

}
