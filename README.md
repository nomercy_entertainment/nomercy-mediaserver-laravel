<p align="center"><img src="https://nomercy.tv/img/logo-wide-transparent.svg" height="200" width="400" style></p>

# NoMercy Media Server

This Project is build on laravel and alows you to archive your digital Movie and TV Show library. <br>
You can find a quick preview of the UI [here](https://nomercy.tv/preview).

### Quick overview
* Netflix inspired design.
* Automatic playback and resume.
* It uses [TheMovieDB](https://themoviedb.org) to fetch movie and tv show info.
* Utilizes [JW player](https://jwplayer.com) <small style="margin-left:0.5rem;font-weight:600" >Open-source & licenced.</small>
* It encodes your video files to mp4 for single audio and to hls for multi-audio/multi-quality. <small style="margin-left:0.5rem;font-weight:600" >The free version of JW Player is currently unstable for multi-audio encodes</small>

### Prerequisites
* Docker
* Ffmpeg <br> <small style="margin-left:0.5rem;font-weight:600" >* Ffmpeg needs to be compiled with non-free and zscale.</small>
* Youtube-dl

### Quickstart

```bash
git clone --single-branch --branch NoMercyMediaServer https://gitlab.com/nomercy_entertainment/laradocklemp.git NoMercyMediaServer
cd NoMercyMediaServer
cp .env.example .env
```
Edit the .env file with your credentials

```bash
docker-compose up -d
```
The server can be accessed on http://localhost/

If you want to use ssl with Let's Encrypt you need to set the domain and email in the docker .env file and run:
```bash 
docker exec -it nomercymediaserver /usr/bin/letsencrypt-setup
```

### Notes

It is still in pre-release stage <br>
If you like to test it and help me test and debug, send me a message if you need help getting started. <br>

This code is not final and is subjected to change and may break at any time.<br>
Fixes and improvements are welcome.<br>
Questions may be asked at [stoney@nomercy.tv](mailto:stoney@nomercy.tv) or [@Stoney_Eagle](https://twitter.com/stoney_eagle)


## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

If you discover a security vulnerability within NoMercy Media Server, please send an e-mail to Stoney Eagle via [stoney@nomercy.tv](mailto:stoney@nomercy.tv). All security vulnerabilities will be promptly addressed.
