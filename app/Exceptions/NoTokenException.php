<?php

namespace App\Exceptions;

use Exception;

class NoTokenException extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report()
    {
        //
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return response()->json([
            "status" => 'error',
            "status_code" => 200,
            'status_error' => __('status.noToken.error'),
            'status_message' => __('status.noToken.message'),
        ], 401);
    }
}
