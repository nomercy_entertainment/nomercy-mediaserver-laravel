<?php

namespace App\Http\Controllers;

use App\Models\Workers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware('role:user', ['only' => ['index']]);

        $userAgents = [
            'Chrome',
            'Firefox',
            'Android',
        ];

        $userAgent = $request->header('User-Agent');

        if (contains($userAgent, $userAgents)) {
            $this->image = 'poster.webp';
        } else {
            $this->image = 'poster.tiny.jpg';
        }
    }

    public function index()
    {
        $workers = DB::table('jobs_progress')->where('active', true)->get();
        $token = $this->token;

        return view('dashboard.index',  compact('workers', 'token'));
    }

    public function edit()
    {
        return view('dashboard.content.edit');
    }

    public function remove_qued(Request $request)
    {
        Workers::where("id", $request->id)->delete();

        return ['message' => 'Queue item ' . $request->id . ' removed.'];
    }

    public function clear_que(Request $request)
    {
        Workers::where("queue", $request->queue)->delete();

        return ['message' => 'All items from queue: ' . $request->queue . ' have been removed.'];
    }
}
