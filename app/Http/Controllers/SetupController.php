<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Database\LanguageController;
use App\Http\Controllers\Providers\TMDBProvider;
use App\Models\FolderRoots;
use App\Models\Genre;
use App\Models\SupportedLanguages;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class SetupController extends Controller
{
    public $request, $user;

    public function __construct()
    {
    }

    public function index()
    {
        if (User::count() > 1 && (FolderRoots::count() > 0 && SupportedLanguages::count() > 0) && env('APP_URL') != "") {
            return redirect('/');

        }

        $FolderRoots = FolderRoots::get();
        $SupportedLanguages = SupportedLanguages::get();
        $languages = (new LanguageController())->list();
        $languages = collect($languages)->sortBy('language_name');

        $disk = Storage::disk('media');
        $disk->base_path = $disk->getDriver()->getAdapter()->getPathPrefix();

        $base_folder = $disk->directories('/');
        $this->addGenres();

        return view('setup.index', [
            "FolderRoots" => $FolderRoots,
            "SupportedLanguages" => $SupportedLanguages,
            "languages" => $languages,
            "disk" => $disk,
            "base_folder" => $base_folder,
        ]);
    }

    public function base(Request $request){

        $keys = [

            'APP_URL' => $request->APP_URL,
            'WORKER_URL' => $request->APP_URL,
            'WORKER_NAME' => $request->WORKER_NAME,
            'FEATURED' => $request->FEATURED,
            'APP_PRIVACY' => $request->APP_PRIVACY,

            'TVDB_APIKEY' => $request->TVDB_APIKEY,
            'IMDB_TOKEN' => $request->IMDB_TOKEN,
            'OMDB_APIKEY' => $request->OMDB_APIKEY,
            'PEXELS_API_KEY' => $request->PEXELS_API_KEY,

            'JWPLAYER_KEY' => $request->JWPLAYER_KEY,
            'JWPLAYER_VERSION' => $request->JWPLAYER_VERSION ?? '8.17.7',

            'OPENSUBTITLES_USERNAME' => $request->OPENSUBTITLES_USERNAME,
            'OPENSUBTITLES_PASSWORD' => $request->OPENSUBTITLES_PASSWORD,
            'OPENSUBTITLES_DEFAULT_LANGUAGE' => $request->OPENSUBTITLES_DEFAULT_LANGUAGE,
            'OPENSUBTITLES_LANGUAGE' => $request->OPENSUBTITLES_LANGUAGE,
            'OPENSUBTITLES_USERAGENT' => $request->OPENSUBTITLES_USERAGENT ?? 'TemporaryUserAgent',

        ];

        foreach($keys as $key => $value){
            if($value != null){
                put_permanent_env($key, $value);
            }
        }
        return redirect('/');
    }

    public function folders(Request $request)
    {
        $index = 1;
        $array = [];
        $this->disk = Storage::disk('media');
        $this->disk_base_path = $this->disk->getDriver()->getAdapter()->getPathPrefix();
        $folder = $this->disk->directories($request->folder);
        $array['back'] = preg_replace("/(.*)\/.*/", '$1', '/' . $request->folder);
        $array['current_path'] = $request->folder ?? '/';

        foreach ($folder as $folder) {
            $full_path = $this->disk_base_path . $folder;
            if (is_readable($full_path) && is_writable($full_path) && $folder !== '.$EXTEND' && $folder !== '.' && $folder !== '..') {
                if (is_dir($full_path) && $this->disk->directories($folder) != []) {
                    $array['results'][$index]['name'] = preg_replace("/.*\/(.*)/", '$1', '/' . $folder);
                    $array['results'][$index]['path'] = '/' . $folder;
                    $array['results'][$index]['full_path'] = $full_path;
                    $array['results'][$index]['last'] = false;
                } else {
                    $array['results'][$index]['name'] = preg_replace("/.*\/(.*)/", '$1', '/' . $folder);
                    $array['results'][$index]['path'] = '/' . $folder;
                    $array['results'][$index]['full_path'] = $full_path;
                    $array['results'][$index]['last'] = true;
                }
                $index++;
            }
        }

        return $array;
    }

    public function addfolders(Request $request)
    {
        foreach ($request->all() as $key => $item) {
            if ($key != '_token') {
                $id = preg_replace('/\w*-/', '', $key);
                $val = preg_replace('/-\d*/', '', $key);
                if ($item == 'true') {
                    $item = true;
                }
                $array[$id][$val] = $item;
            }
        }

        foreach ($array as $item) {
            FolderRoots::updateOrCreate(['folder' => $item['folder']], $item);
        }
        return redirect('/');
    }

    public function addlanguages(Request $request)
    {

        foreach ($request->all() as $key => $item) {
            if ($key != '_token') {
                foreach ($item as $lang) {
                    $array = json_decode($lang, true);
                    SupportedLanguages::updateOrCreate(['language_code' => $array['language_code']], $array);
                }
            }
        }
        return redirect('/');
    }

    public function addGenres()
    {
        $TMDBProvider = new TMDBProvider();
        $data = $TMDBProvider->genres();

        foreach ($data as $item) {
            Genre::updateOrCreate(["id" => $item['id']], collect($item)->toArray());
        }
    }

}
