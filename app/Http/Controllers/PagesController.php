<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Content\ServerController;
use App\Http\Controllers\Encoder\Video;
use App\Models\Episode;
use App\Models\Genre;
use App\Models\Migration;
use App\Models\Tv;
use App\Models\VideoFile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use NoMercy\OpenSubtitles\App\Client;
use RecursiveDirectoryIterator;
use RecursiveFilterIterator;
use RecursiveIteratorIterator;
use stdClass;
use Tymon\JWTAuth\Facades\JWTAuth;

ini_set('max_execution_time', 0);

class ReadableFilter extends RecursiveFilterIterator{
    public function accept(){
            return $this->current()->isReadable();
    }
}

class PagesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function is_dir_empty($dir)
    {
        if (!is_readable($dir)) return NULL;
        return (count(scandir($dir)) == 2);
    }

    public function test(Request $request)
    {

        if (!Auth::user()->isOwner) {
            abort(403, "You are not allowed here so GTFO!");
        } else {

            // dd( env('APP_URL'));

            // $shows = Tv::where('languages', 'JapanSpanishe')->get();
            // foreach($shows as $show){
            //     $show->update([
            //         'languages' => 'English, Japanese'
            //     ]);
            // }

            // $res = (new ServerController())->get_server_tokens();
            // dd($res);

            // $files = VideoFile::where('file', 'LIKE', '%.mp4.mp4%')->get();
            // foreach($files as $file){
            //     $file->update([
            //         'file' => str_replace('.mp4.mp4', '.mp4', $file->file)
            //     ]);
            // }


            // $id = $request->query('id', 0);

            // $show = Tv::where('id', $id)->select('title')->first()['title'] ?? null;

            // $episodes = Episode::where('show_id', $id)->get()->map(function($episode) {
            //     return $episode->only(['season_number', 'episode_number', 'title']);
            // });
            // $res = [];


            // $str1 = [". ", " ", "!", "?",   "&", "..",   "#", "'", ":", ",", "?", "Ã¯Â¿Â½", "", "(", ")", "-.", "/", "’", "⁄", "\"", "[", "]", "Æ", "`", '+.'];
            // $str2 = [".",  ".",  "",  "", "and",  ".", "%23",  "", "",  "",  "",       "",  "",  "",  "",   "", ".",  "", ".",   "",  "",  "", 'AE', ''];

            // foreach($episodes as $episode){
            //     $res = str_replace(' ', '.', $show) . '.S' . zeropad($episode['season_number'], 2)  . 'E' . zeropad($episode['episode_number'], 2) . '.' . str_replace($str1, $str2, $episode['title']);
            //     if(!file_exists('/mnt/windows/d/Anime/Download/Dragon.Ball.Kai.(2009)/'. $res . '.mkv')){
            //         print $res . '<br>';

            //     }
            // }



            // $path = [];

            // $iterator = new RecursiveIteratorIterator(new ReadableFilter(new RecursiveDirectoryIterator('/mnt/media/Anime/Download/')));
            // foreach ($iterator as $file) {
            //     if ($file->isDir()) continue;
            //     // if($file->getExtension() == "mkv" || $file->getExtension() == "mp4" || $file->getExtension() == "avi"){
            //     if(contains(mime_content_type($file->getPathname()), array('video'))){
            //         $path[] = $file->getPathname();
            //     }
            // }

            // // Storage::put('everything.json', json_encode($path) );

            // dd($path);









            // $base_url = "http://192.168.2.100:8081";
            // $folder = 'Anime/Download/Heaven\'s Lost Property (2009)';

            // $show = Http::baseUrl($base_url)->get("/api/6d4ed1778ab9b92749f2d6f1f795444e/?cmd=show&indexerid=118101")->json()['data'];
            // $data = Http::baseUrl($base_url)->get("/api/6d4ed1778ab9b92749f2d6f1f795444e/?cmd=show.seasons&indexerid=118101&profile=1")->json()['data'];

            // $files = Storage::disk('media')->allFiles($folder);

            // // dd($files);
            // $search = [];
            // $result = [];

            // foreach ($data as $season) {
            //     foreach ($season as $episode) {
            //         $search[] = [
            //             "season" => zeropad($episode['season'], 2),
            //             "episode" => zeropad($episode['episode'], 2),
            //             "name" => $episode['name'],
            //             "show" => $show['show_name'],
            //             "fileOut" => "/mnt/media/" . $folder . "/" . $show['show_name'] . ".S" . zeropad($episode['season'], 2) . "E" . zeropad($episode['episode'], 2),
            //         ];
            //     }
            // }

            // // dd($search);
            // $re = '/\s(?<absolute>\d{4})\s-\s(?<title>[\w\sé,\'!?\-\.]*)\s/i';
            // $re = '/-\s\d*\s-\s(?<title>[\w\sé,\'!?\-\.°]*).\w{3,4}/m';
            // $re = '/(-\s\d*\s-\s)?(\s-\s)(?<title>[\w\sé,\'!?\-\.]*)\.\w{3,4}/m';

            // $str1 = ['!'];
            // $str2 = [''];
            // $str3 = [',', '&', 'vs.', "'", ''];
            // $str4 = ['', 'and', 'versus', ''];

            // $str5 = ['-'];
            // $str6 = [' '];

            // $str7 = [" Pokémon BW Adventures In Unova", "Pokémon BW Adventures In Unova "];
            // $str8 = [""];

            // $str9 = [',', '?'];
            // $str10 = [''];

            // foreach ($files as $key => $file) {
            //     preg_match($re, $file, $match);
            //     // dump($match);
            //     if ($match) {

            //         foreach ($search as $key2 => $sr) {

            //             if (strtolower($sr['name']) == strtolower($match['title'])) {
            //                 $result[] = array_merge(
            //                     [
            //                         "fileIn" => "/mnt/media/" . $file,
            //                         "name" => $match['title'],
            //                         "ext" => ".mkv",
            //                     ],
            //                     $sr
            //                 );

            //                 unset($search[$key2]);
            //                 unset($files[$key]);
            //             } else if (str_replace($str1, $str2, strtolower($sr['name'])) == strtolower($match['title'])) {
            //                 $result[] = array_merge(
            //                     [
            //                         "fileIn" => "/mnt/media/" . $file,
            //                         "name" => $match['title'],
            //                         "ext" => ".mkv",
            //                     ],
            //                     $sr
            //                 );

            //                 unset($search[$key2]);
            //                 unset($files[$key]);
            //             } else if (strtolower($sr['name']) == str_replace($str1, $str2, strtolower($match['title']))) {
            //                 $result[] = array_merge(
            //                     [
            //                         "fileIn" => "/mnt/media/" . $file,
            //                         "name" => $match['title'],
            //                         "ext" => ".mkv",
            //                     ],
            //                     $sr
            //                 );

            //                 unset($search[$key2]);
            //                 unset($files[$key]);
            //             } else if (str_replace($str3, $str4, strtolower($sr['name'])) == strtolower($match['title'])) {
            //                 $result[] = array_merge(
            //                     [
            //                         "fileIn" => "/mnt/media/" . $file,
            //                         "name" => $match['title'],
            //                         "ext" => ".mkv",
            //                     ],
            //                     $sr
            //                 );

            //                 unset($search[$key2]);
            //                 unset($files[$key]);
            //             } else if (strtolower($sr['name']) == str_replace($str3, $str4, strtolower($match['title']))) {
            //                 $result[] = array_merge(
            //                     [
            //                         "fileIn" => "/mnt/media/" . $file,
            //                         "name" => $match['title'],
            //                         "ext" => ".mkv",
            //                     ],
            //                     $sr
            //                 );

            //                 unset($search[$key2]);
            //                 unset($files[$key]);
            //             } else if (str_replace($str5, $str6, strtolower($sr['name'])) == strtolower($match['title'])) {
            //                 $result[] = array_merge(
            //                     [
            //                         "fileIn" => "/mnt/media/" . $file,
            //                         "name" => $match['title'],
            //                         "ext" => ".mkv",
            //                     ],
            //                     $sr
            //                 );

            //                 unset($search[$key2]);
            //                 unset($files[$key]);
            //             } else if (strtolower($sr['name']) == str_replace($str5, $str6, strtolower($match['title']))) {
            //                 $result[] = array_merge(
            //                     [
            //                         "fileIn" => "/mnt/media/" . $file,
            //                         "name" => $match['title'],
            //                         "ext" => ".mkv",
            //                     ],
            //                     $sr
            //                 );

            //                 unset($search[$key2]);
            //                 unset($files[$key]);
            //             } else if (str_replace($str7, $str8, strtolower($sr['name'])) == strtolower($match['title'])) {
            //                 $result[] = array_merge(
            //                     [
            //                         "fileIn" => "/mnt/media/" . $file,
            //                         "name" => $match['title'],
            //                         "ext" => ".mkv",
            //                     ],
            //                     $sr
            //                 );

            //                 unset($search[$key2]);
            //                 unset($files[$key]);
            //             } else if ($sr['name'] == str_replace($str7, $str8, $match['title'])) {
            //                 $result[] = array_merge(
            //                     [
            //                         "fileIn" => "/mnt/media/" . $file,
            //                         "name" => $match['title'],
            //                         "ext" => ".mkv",
            //                     ],
            //                     $sr
            //                 );

            //                 unset($search[$key2]);
            //                 unset($files[$key]);
            //             } else if (str_replace($str9, $str10, strtolower($sr['name'])) == strtolower($match['title'])) {
            //                 $result[] = array_merge(
            //                     [
            //                         "fileIn" => "/mnt/media/" . $file,
            //                         "name" => $match['title'],
            //                         "ext" => ".mkv",
            //                     ],
            //                     $sr
            //                 );

            //                 unset($search[$key2]);
            //                 unset($files[$key]);
            //             } else if ($sr['name'] == str_replace($str9, $str10, $match['title'])) {
            //                 $result[] = array_merge(
            //                     [
            //                         "fileIn" => "/mnt/media/" . $file,
            //                         "name" => $match['title'],
            //                         "ext" => ".mkv",
            //                     ],
            //                     $sr
            //                 );

            //                 unset($search[$key2]);
            //                 unset($files[$key]);
            //             }
            //         }
            //     }
            // }

            // // dd($result);
            // $cmd_arr = [];
            // foreach ($result as $renamable) {
            //     $cmd = "mv \"" . $renamable['fileIn'] . "\" \"" . $renamable['fileOut'] . $renamable['ext'] . "\"";
            //     $cmd_arr[] = $cmd;
            //     shell_exec($cmd);
            // }
            // // dd($result);
            // dd($cmd_arr);

            // // $query = "Head for a New Adventure";

            // // $episodes = Episode::where('show_id', '60572')
            // //     ->where('title', 'like', '%' . $query .'%')
            // //     ->get();
            // // dd($episodes);


        }
    }
}
