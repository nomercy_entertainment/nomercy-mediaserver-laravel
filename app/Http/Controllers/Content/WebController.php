<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Player\PlayerController;
use App\Models\Genre;
use App\Models\Movie;
use App\Models\Tv;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use PDO;
use Tymon\JWTAuth\Facades\JWTAuth;

class WebController extends Controller
{
    public $id, $type;

    public function  __construct(Request $request)
    {
        $this->middleware('auth');
        $this->id = $request->id;
        $this->item = $request->query('item');
        $this->room = $request->query('room');
        $this->season = $request->season;
        $this->episode = $request->episode;
        $this->type = $request->query('type', null);
    }

    public function index()
    {

        $DataController = new DataController();
        $continue = $DataController->continue()['continue'];
        $data = $DataController->index()['data'];
        $data = collect($data)->unique('id');

        // $external = (new ServerController())->get_request('/');

        // $items = (new ServerController())->get_request('/items?ids=' . str_replace(['[', ']'], '', collect($continue["items"])->flatten()->toJson()));

        // $continue = array_merge($continue, $items["data"] ?? []);

        // $data = array_merge($data, $external['data'] ?? []);

        $arr = [];

        $collection = collect($data)->map(function ($collection) {
            if($collection['collection'] != '' || $collection['collection'] != null){
                return collect($collection)->only(['collection'])->all();
            }
        })->unique('collection')->whereNotNull('collection')->flatten()->toArray();

        foreach($collection as $name){
            $arr[$name] = collect($data)->filter(function ($data) use ($name) {
                if (isset($data['collection'])) {
                    return false !== strstr($data['collection'], $name);
                }
            })->unique('id')->sortBy("title_sort");
        }

        foreach (Genre::get()->where('iso_639_1', '==',  app()->getLocale()) as $genre) {
            $arr[$genre['name']] = collect($data)->filter(function ($data) use ($genre) {
                if (isset($data['genres']) && $data['genres'] != []) {
                    return false !== stristr(collect($data['genres'])->pluck('name')->implode(', '), $genre['name']);
                }
            })->unique('id')->shuffle()->take(36);
        }

        if(env('FEATURED') == 'last'){
            $last = collect($data)->sortByDesc('created_at')->first();
        }
        else {
            $last = collect($data)->shuffle()->first();
        }

        return view('pages.index')
            ->with([
                'data' => $arr,
                'last' => $last,
                'continue' => $continue
            ]);
    }

    public function anime()
    {
        $DataController = new DataController();
        $data = $DataController->anime();

        // $this->ServerController = new ServerController();
        // $external = $this->ServerController->get_request('/anime');

        // $data = array_merge($data, $external['data'] ?? []);

        array_multisort(array_column($data, 'title_sort'),  SORT_ASC, array_column($data, 'have_episodes'), SORT_DESC, $data);

        $data = collect($data)->unique('id')->sort(function ($a, $b) {
            return $a['title_sort'] <=> $b['title_sort'];
        });

        return view('pages.media')
            ->with(['data' => $data]);
    }

    public function movies()
    {

        $DataController = new DataController();
        $data = $DataController->movies();

        // $this->ServerController = new ServerController();
        // $external = $this->ServerController->get_request('/movies');

        // $data = array_merge($data, $external['data'] ?? []);

        $data = collect($data)->unique('id')->sort(function ($a, $b) {
            return $a['title_sort'] <=> $b['title_sort'];
        });

        return view('pages.media')
            ->with(['data' => $data]);
    }

    public function tv()
    {
        $DataController = new DataController();
        $data = $DataController->tv();

        // $this->ServerController = new ServerController();
        // $external = $this->ServerController->get_request('/tv');

        // $data = array_merge($data, $external['data'] ?? []);

        array_multisort(array_column($data, 'title_sort'),  SORT_ASC, array_column($data, 'have_episodes'), SORT_DESC, $data);

        $data = collect($data)->unique('id')->sort(function ($a, $b) {
            return $a['title_sort'] <=> $b['title_sort'];
        });

        return view('pages.media')
            ->with(['data' => $data]);
    }

    public function specials()
    {
        $DataController = new DataController();
        $data = $DataController->specials();

        // $this->ServerController = new ServerController();
        // $external = $this->ServerController->get_request('/specials');

        // $data = array_merge($data, $external['data'] ?? []);

        $data = collect($data)->unique('id')->sort(function ($a, $b) {
            return $a['title_sort'] <=> $b['title_sort'];
        });

        return view('pages.media')
            ->with(['data' => $data]);
    }

    public function collections()
    {
        $DataController = new DataController();
        $data = $DataController->collections();

        // $this->ServerController = new ServerController();
        // $external = $this->ServerController->get_request('/collections');

        // $data = array_merge($data, $external['data'] ?? []);

        $data = collect($data)->unique('id')->sort(function ($a, $b) {
            return $a['title_sort'] <=> $b['title_sort'];
        });

        return view('pages.media')
            ->with(['data' => $data]);
    }

    public function collection()
    {
        $DataController = new DataController();
        $data = $DataController->collection($this->id);

        // $this->ServerController = new ServerController();
        // $external = $this->ServerController->get_request('/collection?id=' . $this->id);

        // $data = array_merge($data, $external['data'] ?? []);

        $data = collect($data)->unique('id')->sort(function ($a, $b) {
            return $a['release_date'] <=>  $b['release_date'];
        });

        return view('pages.media')
            ->with(['data' => $data]);
    }

    public function genres()
    {
        $DataController = new DataController();
        $data = $DataController->genres();

        return view('pages.media')
            ->with(['data' => $data]);
    }

    public function info()
    {

        $languages = languages();
        $data = (new DataController())->info($this->id)['data'];
        // dd($data);

        $user = auth()->user();
        $token = JWTAuth::fromUser($user);
        $JWPLAYER_KEY = env('JWPLAYER_KEY', Http::withToken($token)->acceptJson()->withoutVerifying()->get('https://nomercy.tv/api/keys')->json()['JW_KEY'] ?? '');
            // dd($data);
        return view('pages.info')
            ->with([
                "languages" => $languages,
                "id" => $this->id,
                'data' => $data,
                'JWPLAYER_KEY' => $JWPLAYER_KEY,
            ]);
    }

    public function tv_info()
    {

        $languages = languages();
        $data = (new DataController())->tv_info($this->id)['data'];

        $user = auth()->user();
        $token = JWTAuth::fromUser($user);
        $JWPLAYER_KEY = env('JWPLAYER_KEY', Http::withToken($token)->acceptJson()->withoutVerifying()->get('https://nomercy.tv/api/keys')->json()['JW_KEY'] ?? '');
        return view('pages.info')
            ->with([
                "languages" => $languages,
                "id" => $this->id,
                'data' => $data,
                'JWPLAYER_KEY' => $JWPLAYER_KEY,
            ]);
    }

    public function movie_info()
    {

        $languages = languages();
        $data = (new DataController())->movie_info($this->id)['data'];

        $user = auth()->user();
        $token = JWTAuth::fromUser($user);
        $JWPLAYER_KEY = env('JWPLAYER_KEY', Http::withToken($token)->acceptJson()->withoutVerifying()->get('https://nomercy.tv/api/keys')->json()['JW_KEY'] ?? '');
        return view('pages.info')
            ->with([
                "languages" => $languages,
                "id" => $this->id,
                'data' => $data,
                'JWPLAYER_KEY' => $JWPLAYER_KEY,
            ]);
    }

    public function watch_videojs($id)
    {

        $request = (object) [
            'id' => $id
        ];
        $data = (new PlayerController($request))->playlist($id);

        return view('pages.watch_videojsplugin')
            ->with(
                [
                    'bearer' => auth('api')->tokenById(auth()->user()->id),
                    "data" => $data,
                    'id' => $this->id,
                    'user' => Auth::user()->name,
                    'room' =>  generateRandomString(20)
                ]
            );
    }

    public function watch_jwplayer()
    {
        $user = auth()->user();
        $token = JWTAuth::fromUser($user);
        $JWPLAYER_KEY = env('JWPLAYER_KEY', Http::withToken($token)->withoutVerifying()->get('https://nomercy.tv/api/keys')->json()['JW_KEY'] ?? '');
        $data = [];

        return view('pages.watch_jwplayer')
            ->with(
                [
                    'JWPLAYER_KEY' => $JWPLAYER_KEY,
                    "data" => $data,
                    'id' => $this->id,
                    'user' => Auth::user()->name,
                    'room' =>  generateRandomString(20)
                ]
            );
    }


    public function remote()
    {
        $user = auth()->user();
        $token = JWTAuth::fromUser($user);
        $JWPLAYER_KEY = env('JWPLAYER_KEY', Http::withToken($token)->get('https://nomercy.tv/api/keys')->json()['JW_KEY'] ?? '');
        $data = [];

        return view('pages.remote')
            ->with(
                [
                    'JWPLAYER_KEY' => $JWPLAYER_KEY,
                    "data" => $data,
                    'id' => $this->id,
                    'user' => Auth::user()->name,
                    'room' =>  generateRandomString(20)
                ]
            );
    }
}
