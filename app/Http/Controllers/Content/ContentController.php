<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Providers\TMDBProvider;
use App\Jobs\AddContentJob;
use App\Models\Collection;
use App\Models\Media;
use App\Models\Movie;
use App\Models\Special;
use App\Models\Tv;
use App\Models\UserVideo;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class ContentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $anime = Tv::where('type', 'anime')->get();
        $collection = Collection::get();
        $movie = Movie::get();
        $tv = Tv::where('type', 'tv')->get();
        $special = Special::get();
        return view('dashboard.content.index', compact('anime', 'collection', 'movie', 'tv', 'special'));
    }

    public function add_existing()
    {
        if (Auth::user()) {
            $user = Auth::user();
            $token = JWTAuth::fromUser($user);
        } else {
            $token = null;
        }
        return view('dashboard.content.existing')
            ->with('token', $token);
    }


    public function add()
    {
        if (Auth::user()) {
            $user = Auth::user();
            $token = JWTAuth::fromUser($user);
        } else {
            $token = null;
        }
        return view('dashboard.create')
            ->with('token', $token);
    }

    public function add_new(Request $request)
    {
        $ProcessEncodeCommand = (new AddContentJob($request->id, $request->type));
        dispatch($ProcessEncodeCommand)->onQueue('file');

        return ['message' => $request->type . ' with id: ' . $request->id . ' Has been queued'];
    }

    public function find_media(Request $request)
    {
        $ProcessEncodeCommand = (new AddContentJob(null, 'all', $request->all()['direction']));
        dispatch($ProcessEncodeCommand)->onQueue('queue');
        return ['message' => 'All items are queued for refreshing'];
    }

    public function searchresult(Request $request)
    {
        if ($request->ajax()) {
            $output = "";
            $items = [];
            $results = [];
            $TMDBProvider = new TMDBProvider();

            $str1 = ['-', '.'];
            $str2 = [' ', ' '];
            $search = str_replace($str1, $str2, $request->search);

            $results[] = $TMDBProvider->search('tv', $search, $request->year ?? 1, 0, $request->language ?? 'en')->toArray();
            $results[] = $TMDBProvider->search('movie', $search, $request->year ?? 1, 0, $request->language ?? 'en')->toArray();

            $odd = true;

            foreach ($results as $result) {
                usort($result, function ($a, $b) {
                    if (isset($a['title'])) {
                        return $a['title'] <=> $b['title'];
                    }
                    if (isset($a['name'])) {
                        return $a['name'] <=> $b['name'];
                    }
                });
                foreach ($result as $media) {
                    $items[] = $media;
                }
            }

            $items = collect($items);

            if ($items != []) {
                foreach ($items as $key => $item) {
                    if (isset($item['title'])) {
                        $title = $item['title'];
                    } else {
                        $title = $item['name'];
                    }
                    if (isset($item['release_date'])) {
                        $year = Carbon::createFromDate($item['release_date'])->format('Y');
                        $type = "movie";
                    }
                    if (isset($item['first_air_date'])) {
                        $year = Carbon::createFromDate($item['first_air_date'])->format('Y');
                        $type = "tv";
                    }
                    if (isset($item['poster_path'])) {
                        $poster = 'https://image.tmdb.org/t/p/original/' . $item['poster_path'];
                    } else {
                        $poster = '/img/poster-not-available.webp';
                    }

                    if ($key % 2 == 0) {
                        $class = 'md:pr-2';
                    } else {
                        $class = 'md:pl-2';
                    }

                    if (Auth::user()) {
                        $addButton = '
                            <button onclick="
                                var post = {
                                    id: ' . $item['id'] . ',
                                    type: \'' . $type . '\'
                                };
                                post._token = \'' . csrf_token() .  '\';
                                $.ajax({
                                    type: \'post\',
                                    url: \'' . route("add_new") . '\',
                                    data: post,
                                    success: function () {
                                        $(\'#'. $item['id'] .'\').prepend(`
                                            <div class=\'bg-green-100 border-t-4 border-green-500 rounded-b text-green-900 px-4 py-3 shadow-md\' role=\'alert\'>
                                                <div class=\'flex\'>
                                                    <div class=\'py-1 float-left\'>
                                                        <svg class=\'fill-current h-6 w-6 text-teal-500 mr-4\' xmlns=\'http://www.w3.org/2000/svg\' viewBox=\'0 0 20 20\'><path d=\'M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z\'></path></svg>
                                                    </div>
                                                    <p class=\'font-bold\'>' .  __('ui.success') . '</p>
                                                    <p class=\'text-sm pl-3\'>' . str_replace("'", "\'", $title) . '</p>
                                                    <button type=\'button\' class=\'close float-right ml-auto px-4\' data-dismiss=\'alert\' onclick=\'$(this).parent().parent().remove()\'>×</button>
                                                </div>
                                            </div>
                                        `);
                                    },
                                    error: function () {
                                        $(\'#'. $item['id'] .'\').prepend(`

                                            <div class=\'bg-red-100 border-t-4 border-red-500 rounded-b text-red-900 px-4 py-3 shadow-md\' role=\'alert\'>
                                                <div class=\'flex\'>
                                                    <div class=\'py-1 float-left\'>
                                                        <svg class=\'fill-current h-6 w-6 text-teal-500 mr-4\' xmlns=\'http://www.w3.org/2000/svg\' viewBox=\'0 0 20 20\'><path d=\'M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM9 11V9h2v6H9v-4zm0-6h2v2H9V5z\'></path></svg>
                                                    </div>
                                                    <p class=\'font-bold\'>' .  __('ui.error') . '</p>
                                                    <p class=\'text-sm pl-3\'>' . str_replace("'", "\'", $title) . '</p>
                                                    <button type=\'button\' class=\'close float-right ml-auto px-4\' data-dismiss=\'alert\' onclick=\'$(this).parent().parent().remove()\'>×</button>
                                                </div>
                                            </div>
                                        `);
                                    }
                                });
                                return false;
                            " class="px-4 pt-1 mt-3 bg-purple-600">Add</button>
                        ';
                    } else {
                        $addButton = '';
                    }

                    $output .= '
                        <div class="flex flex-row float-left h-64 my-2 bg-gray-800 sm:w-full lg:w-6/12 ' .  $class . '" style="background-clip: content-box;">
                            <div class="flex flex-col justify-center float-left mx-3 overflow-hidden lg:mx-8">
                                <img class="w-32 mx-auto" src="' . $poster . '" alt="image" style="min-width: 70px;">
                                ' . $addButton . '
                            </div>
                            <div id="' . $item['id'] . '" class="float-left w-full pr-8 my-8">
                                <div class="flex flex-row text-xl font-bold">
                                    <h6 class="">' . $title . '</h6>
                                    <small class="ml-auto mr-8 text-white">' . $year . '</small>
                                </div>
                                <p class="mb-1"></p>
                                <div class="h-32 pr-4 overflow-y-auto text-xs">
                                    ' . $item['overview'] . '
                                </div>
                            </div>
                        </div>
                    ';
                }
                return Response($output);
            } else {
                return abort(404, 'Nothing found for: ' . $search);
            }
        }
    }
    public function create_special()
    {
        if (Auth::user()) {
            $user = Auth::user();
            $token = JWTAuth::fromUser($user);
        } else {
            $token = null;
        }
        return view('dashboard.content.create_special')
            ->with('token', $token);
    }

    public function search_specials(Request $request)
    {
        if ($request->ajax()) {
            $output = "";
            $query = Tv::query();
            $columns = ['name', 'homepage', 'overview'];

            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $request->search . '%');
            }
            $tv = $query->orderBy('title_sort', 'asc')
                ->with('cast')
                ->with('persons')
                ->with('cast')
                ->with('crew')
                ->with('genres')
                ->with('creators')
                ->with('networks')
                ->with('companies')
                ->with('seasons')
                ->with('seasons.episodes')
                ->with('seasons.episodes.video_file')
                ->get();


            $query = Movie::query();
            $columns = ['title', 'homepage', 'overview'];
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', '%' . $request->search . '%');
            }
            $movies = $query->orderBy('title_sort', 'asc')
                ->with('cast')
                ->with('crew')
                ->with('genres')
                ->with('companies')
                ->with('video_file')
                ->get();

            $output .= '
                <div class="float-left p-0 mt-5 col-lg-6 col-sm-12 p-lg-2">
                    <div class="list-group">
                        ';
            foreach ($tv as $data) {
                if (isset($image) && file_exists(public_path("/img/tv/" . $data->id . "/" . $data->id . "." . $this->image))) {
                    $imageUrl = "/img/tv/" . $data->id . "/" . $data->id . "." . $this->image;
                } else {
                    $imageUrl = $data['poster'];
                }
                $year = Carbon::createFromDate($data->first_air_date)->format('Y');
                $overview = substr($data->overview, 0, 400);

                $output .= '
                                <div class="mb-2 list-group-item list-group-item-action flex-column align-items-start bg-dark text-light">
                                    <div class="row">
                                        <div class="float-left my-1 col-4 col-lg-1">
                                            <img class="mb-1 img-fluid" src="' . $imageUrl . '" alt="image">
                                        </div>
                                        <div class="float-left py-2 col-8 col-lg-10">
                                            <div class="d-flex w-100 justify-content-between">
                                                <h6 class="mb-1">' . $data->name  . '</h6>
                                                <small class="text-muted">' . $year . ' </small>
                                            </div>
                                            <p class="mb-2 d-flex w-100 justify-content-between" style="font-size:11px;text-align: justify;">
                                                    ' . $overview . '
                                            </p>
                                        </div>
                                    </div>
                            ';

                foreach ($data->seasons as $season) {
                    if ($season->season_number < 1) {
                        $style = 'display:none;';
                    } else {
                        $style = '';
                    }
                    $output .= '
                                    <div class="row" style=" ' . $style . ' ">
                                        <div class="float-left py-2 col-4 col-lg-2 ml-lg-4">
                                            <div class="float-left px-1 ml-4 col-10">
                                                <div class="form-inline row">
                                                    <span class="mr-2" style="font-size:11px">S' . $season->season_number . ' </span>
                                                    <input type="text" class="mt-2 mb-2 form-control mr-sm-2" style="width:60%" name="season:' . $season->id . '" placeholder="order">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="float-left py-2 col-8 col-lg-9">
                            ';
                    foreach ($season->episodes as $episode) {
                        $output .= '
                                            <div class="float-left px-1 col-6 col-lg-3">
                                                <div class="ml-1 form-inline row">
                                                    <span class="mx-1" style="font-size:11px">S' . $season->season_number . 'E' . $episode->episode_number . ' </span>
                                                    <input type="text" class="mt-2 mb-2 form-control mr-sm-2" style="width:50%" name="episode:' . $episode->id . '" placeholder="order">
                                                </div>
                                            </div>
                                ';
                    }
                    $output .= '
                                        </div>
                                        <hr>
                                    </div>
                                ';
                }

                $output .= '
                                </div>
                    ';
            }
            $output .= '
                    </div>
                </div>
                <div class="float-right p-0 mt-5 col-xl-6 col-sm-12 p-lg-2">
                    <div class="list-group">
                    ';
            foreach ($movies as $data) {
                if (isset($image) && file_exists(public_path("/img/tv/" . $data->id . "/" . $data->id . "." . $this->image))) {
                    $imageUrl = "/img/tv/" . $data->id . "/" . $data->id . "." . $this->image;
                } else {
                    $imageUrl = $data['poster'];
                }

                $year = Carbon::createFromDate($data->release_date)->format('Y');
                $overview = substr($data->overview, 0, 200);

                $output .= '
                            <div class="mb-2 list-group-item list-group-item-action flex-column align-items-start bg-dark text-light">
                                <div class="row">
                                    <div class="float-left col-4 col-lg-1">
                                            <img class="img-fluid" src="' . $imageUrl . '" alt="image">
                                        </div>
                                    <div class="float-left col-8 col-lg-10">
                                        <div class="d-flex w-100 justify-content-between">
                                            <h6 class="mb-1">' . $data->title . '</h6>
                                            <small class="text-muted"> ' . $year . ' </small>
                                        </div>
                                        <p class="mb-1">
                                            <div class="mb-2 d-flex w-100 justify-content-between" style="font-size:11px;text-align: justify;">
                                                ' . $overview . '
                                            </div>
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="float-left p-0 px-1 col-6 col-lg-3">
                                        <div class="form-inline">
                                            <input type="text" class="my-2 ml-3 mr-4 form-control mr-sm-2 w-50" name="movie:' .  $data->id . '" placeholder="order">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ';
            }
            $output .= '
                    </div>
                </div>
                ';
            return Response($output);
        }
    }

    public function post_special(Request $request)
    {
        foreach ($request->all() as $key => $value) {

            $nameSort = addslashes(str_replace(["The ", "An ", "A ", "\""], [""], $request->name));
            $special = Special::updateOrCreate(["name" => $request->name], ["title_sort" => $nameSort]);
            isset($request->poster) ? Special::updateOrCreate(["name" => $request->name], ["poster" => $request->poster]) : null;
            isset($request->overview) ? Special::updateOrCreate(["name" => $request->name], ["overview" => $request->overview]) : null;
            isset($request->backdrop) ? Special::updateOrCreate(["name" => $request->name], ["backdrop" => $request->backdrop]) : null;

            if ($key != "_token" && $key != "name" && $key != "poster" && $key != "backdrop" && $key != "overview" && $value != null) {
                $keys = explode(":", $key);
                Media::updateOrCreate([$keys[0] . "_id" => $keys[1]], [
                    "type" => $keys[0],
                    "order" => $value,
                    "special_id" => $special->id
                ])->special();
            }
        }
        return redirect()->back();
    }

    public function deletewatched(Request $request)
    {

        $delete = UserVideo::where('user_id', Auth::user()->id)
            ->where('tmdb_id', $request->id)
            ->delete();

        return response()->json([
            "message" => "Progress deleted from history"
        ]);
    }
}
