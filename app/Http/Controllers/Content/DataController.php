<?php

namespace App\Http\Controllers\Content;

use Request;
use App\Models\Tv;
use Carbon\Carbon;
use App\Models\Movie;
use App\Models\Special;
use App\Models\UserVideo;
use App\Models\Collection;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Facades\JWTAuth;

class DataController extends Controller
{
    public $origin;

    public function __construct()
    {
        $this->origin = env('APP_URL');
        $this->lang = Lang::getLocale();

        // $this->token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9hdXRoLm5vbWVyY3kudHZcL2FwaVwvbG9naW4iLCJpYXQiOjE2MDk0MTA5ODcsImV4cCI6MTYxMjAwMjk4NywibmJmIjoxNjA5NDEwOTg3LCJqdGkiOiJ5ZG1CMlRoWkNVa29hNmU3Iiwic3ViIjoiMjBjMTI0NjMtZTM1Yy00ZWM3LTkyZWMtOGExOTUwYTk5YmE2IiwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyJ9.lDN3g6Q_VTtLfJUn1BQn1OSbZ5blyk_6xUH7vIdfGss";
    }

    public function continue()
    {

        $continue = [];

        $array = UserVideo::where('user_id', auth()->user()->id)
            ->orderBy('updated_at', 'desc')
            ->select(['tmdb_id'])
            ->get()
            ->unique('tmdb_id')
            ->flatten()
            ->toArray();

        $continue_tv = Tv::select(['id', 'title', 'title_sort', 'poster', 'backdrop', 'type', 'have_episodes', 'number_of_episodes', 'first_air_date', 'overview', 'collection'])->whereIn('id', $array)
            ->orderBy('updated_at', 'desc')
            ->get();

        $continue_tv = collect($continue_tv)->map(function ($items) {
            if (count($items->toArray()) > 0) {
                return collect($items)->merge([
                    'origin' => $this->origin,
                    'type' => 'tv',
                ]);
            } else {
                return [];
            }
        })->toArray();


        $continue_movie = Movie::select(['id', 'title', 'title_sort', 'poster', 'backdrop', 'type', 'release_date', 'overview', 'collection'])->whereIn('id', $array)
            ->orderBy('updated_at', 'desc')
            ->get();

        $continue_movie = collect($continue_movie)->map(function ($items) {
            if (count($items->toArray()) > 0) {
                return collect($items)->merge([
                    'origin' => $this->origin,
                    'type' => 'movie',
                ]);
            } else {
                return [];
            }
        })->toArray();

        $continue = array_merge($continue_movie, $continue_tv);

        return [
            "continue" => $continue,
            "items" => collect($array),
        ];

    }

    public function index()
    {

        $tv = Tv::select(['id', 'title', 'title_sort', 'poster', 'backdrop', 'type', 'have_episodes', 'number_of_episodes', 'first_air_date', 'overview', 'collection', 'trailer'])
            ->where('have_episodes', '>', '0')
            ->with('genres')
            ->with('trailers')
            ->get();


        $tv = collect($tv)->map(function ($items) {
            return collect($items)->merge([
                // 'trailer' => $items['trailer'],
                'poster' => str_replace('original', 'w300', $items['poster']),
                'origin' => $this->origin,
            ]);
        })->toArray();

        $movie = Movie::select(['id', 'title', 'title_sort', 'poster', 'backdrop', 'type', 'release_date', 'overview', 'collection', 'trailer'])
            ->with('genres')
            ->with('trailers')
            ->get();

        $movie = collect($movie)->map(function ($items) {
            if (count($items->toArray()) > 0) {
                return collect($items)->merge([
                    // 'trailer' => $items['trailer'],
                    'poster' => str_replace('original', 'w300', $items['poster']),
                    'origin' => $this->origin,
                ]);
            } else {
                return [];
            }
        })->toArray();

        $data = array_merge($movie, $tv);

        // $continue = $this->continue()['continue'];

        return [
            "data" => $data,
            // "continue" => $continue
        ];
    }

    public function anime()
    {
        $data = Tv::select(['id', 'title', 'title_sort', 'poster', 'backdrop', 'type', 'have_episodes', 'number_of_episodes'])
            ->where('type', 'anime')
            ->where('have_episodes', '>', '0')
            ->get();

        $data = collect($data)->map(function ($items) {
            if (count($items->toArray()) > 0) {
                return collect($items)->merge([
                    'origin' => $this->origin,
                    'poster' => str_replace('original', 'w300', $items['poster']),
                    'image_type' => 'tv',
                ]);
            } else {
                return [];
            }
        })->toArray();

        return $data;
    }

    public function movies()
    {
        $data = Movie::select(['id', 'title', 'title_sort', 'poster', 'backdrop', 'type'])
            ->get();

        $data = collect($data)->map(function ($items) {
            if (count($items->toArray()) > 0) {
                return collect($items)->merge([
                    'origin' => $this->origin,
                    'poster' => str_replace('original', 'w300', $items['poster']),
                    'image_type' => 'movie',
                ]);
            } else {
                return [];
            }
        })->toArray();

        return $data;
    }

    public function tv()
    {
        $data = Tv::select(['id', 'title', 'title_sort', 'poster', 'type', 'have_episodes', 'number_of_episodes'])
            ->where('type', 'tv')
            ->where('have_episodes', '>', '0')
            ->get();

        $data = collect($data)->map(function ($items) {
            if (count($items->toArray()) > 0) {
                return collect($items)->merge([
                    'origin' => $this->origin,
                    'poster' => str_replace('original', 'w300', $items['poster']),
                    'image_type' => 'tv',
                ]);
            } else {
                return [];
            }
        })->toArray();

        return $data;
    }

    public function specials()
    {
        $data = Special::select(['id', 'name', 'title_sort', 'poster'])
            ->orderBy('title_sort', 'asc')
            ->get();

        $data = collect($data)->map(function ($items) {
            if (count($items->toArray()) > 0) {
                return collect($items)->merge([
                    'origin' => $this->origin,
                    'image_type' => 'special',
                ]);
            } else {
                return [];
            }
        })->toArray();

        return ['data' => $data];
    }

    public function collections()
    {
        $data = Collection::select(['id', 'title', 'title_sort', 'poster', 'parts'])
            ->get();

        $data = collect($data)->map(function ($items) {
            if ($items->count() > 0) {
                return collect($items)->merge([
                    'origin' => $this->origin,
                    'type' => 'collection',
                    'image_type' => 'collection',
                ]);
            } else {
                return [];
            }
        })->toArray();

        return $data;
    }

    public function collection($id)
    {
        $data = Collection::select(['id', 'title', 'title_sort', 'poster'])
            ->where('id', $id)
            ->with('movies')
            ->first();

        if ($data != null && count($data['movies']) > 0) {
            $data = collect($data['movies'])->map(function ($items) {
                return collect($items)->merge([
                    'origin' => $this->origin,
                    'image_type' => 'movie',
                ]);
            })->toArray();
        }

        return $data ?? [];
    }

    public function genres()
    {
        $data = [];

        return [
            "data" => $data,
            "type" => 'genre'
        ];
    }

    public function info($id)
    {

        $this->data = Tv::where('id', $id)
            ->with('seasons.episodes')
            ->with('media')
            ->with('seasons.media')
            ->with('companies')
            ->with('genres')
            // ->with('supportedLanguages')
            ->with(['seasons.episodes.video_file' => function ($query) {
                $query->select('episode_id', 'duration');
            }])
            ->with(['translation' => function ($query) {
                $query->where('iso_639_1', $this->lang);
            }])
            ->with(['seasons.translation' => function ($query) {
                $query->where('iso_639_1', $this->lang);
            }])
            ->with(['seasons.episodes.translation' => function ($query) {
                $query->where('iso_639_1', $this->lang);
            }])
            ->first();


            $this->columns = Schema::getColumnListing('video_file');


        if ($this->data == null) {
            $this->data = Movie::where('id', $id)
                ->with('video_file')
                ->with('media')
                ->with('companies')
                ->with(['translation' => function ($query) {
                    $query->where('iso_639_1', $this->lang);
                }])
                ->first();
        }

        // if (isset($this->data['seasons'])) {

            //     $this->data->duration = $this->data->duration;
            //     $this->data->overview = str_replace('"', "\\\"", $this->data['overview']);

            //     $this->languages = [];;

            //     $this->data = collect($this->data)->merge([
            //         'id' => $this->data['id'],
            //         'origin' => $this->origin,
            //         'image_type' => 'tv',
            //         'year' => Carbon::parse($this->data['date'] ?? $this->data['first_air_date'])->format('Y'),
            //         'directors' => collect($this->data->crew)->map(function ($crew) {
            //             $directors = [];
            //             if ($crew['job']  == 'Director' || $crew['job'] == 'Directing') {
            //                 $director = $crew;
            //                 return $director;
            //             } else if (strpos($crew['job'], 'Director') == true) {
            //                 $director = $crew;
            //                 return $director;
            //             }
            //             return $directors;
            //         })->flatten()->unique()->take(5),
            //         'lead_actors' => collect($this->data->cast)->map(function ($lead_actors) {
            //             return collect($lead_actors)->only('character');
            //         })->take(5),
            //         'cast' => collect($this->data->cast),
            //         'crew' => collect($this->data->crew),
            //         'posters' => collect($this->data->media)->where('type', 'poster'),
            //         'backdrops' => collect($this->data->media)->where('type', 'backdrop'),
            //         'videos' => collect($this->data->media)->where('type', '<>', 'poster')->where('type', '<>', 'backdrop'),
            //         'audio' => collect($this->data->audio)->toArray(),
            //         'trailers' => collect($this->data->media)->map(function ($media) {
            //             return collect($media->where('type', 'Trailer'))->whereNotNull('key');
            //         })->flatten()->unique(),
            //         'seasons' => collect($this->data->seasons)->map(function ($season) {
            //             return collect($season)
            //                 ->only(['air_date', 'id', 'episode_count', 'name', 'overview', 'poster', 'season_number', 'date', 'tvdb_id', 'episodes']);
            //         }),

            //     ])->merge(collect($this->data['translated'])->except('title'))->toArray();

            //     $seasons = collect($this->data['seasons'])->sortBy('season_number');

            //     return [
            //         "data" => $this->data,
            //         "seasons" => $seasons
            //     ];
        // }


        if ($this->data != null) {

            $data = collect($this->data)->merge([
                'origin' => $this->origin,
                'image_type' => $this->type ?? 'movie',
                // 'year' => Carbon::parse($this->data['first_air_date'])->format('Y'),
                'year' => $this->data['year'],
                'directors' => collect($this->data->crew)->map(function ($crew) {
                    $directors = [];
                    if ($crew['job']  == 'Director' || $crew['job'] == 'Directing') {
                        $directors[] = $crew;
                        // return $director;
                    } else if (strpos($crew['job'], 'Director') == true) {
                        $directors[] = $crew;
                        // return $director;
                    }
                    return $directors;
                })->flatten()->unique()->take(5),
                'lead_actors' => collect($this->data->cast)->map(function ($lead_actors) {
                    return collect($lead_actors)->except('known_for');
                })->take(5),
                'cast' => collect($this->data->cast)->map(function ($lead_actors) {
                    return collect($lead_actors)->except('known_for');
                }),
                'crew' => collect($this->data->crew),
                'posters' => collect($this->data->media)->where('type', 'poster'),
                'backdrops' => collect($this->data->media)->where('type', 'backdrop'),
                'videos' => collect($this->data->media)->where('type', '<>', 'poster')->where('type', '<>', 'backdrop'),
                'audio' => collect($this->data->audio)->toArray(),
                'trailers' => collect($this->data->media)->map(function ($media) {
                    return collect($media->where('type', 'Trailer'))->whereNotNull('key');
                }),
                'title' => str_replace('"', '\'', $this->data['title']),
                'overview' => str_replace('"', '\'', $this->data['overview']),
                'genres' => explode(', ', $this->data['genres']),
                'recommendations' => [],

            ])->merge(collect($this->data['translated'])->except(['title']));

            return [
                "data" => $data,
                "seasons" => null
            ];
        }
        else {

            // $ServerController = new ServerController();
            // $data = $ServerController->get_request('/info?id=' . $id);

            if (!isset($data['data']) || $data['data'] == []) {
                $data = Http::acceptJson()
                ->withHeaders([
                    'Accept-Language' => app()->getLocale(),
                ])
                ->get('https://api.nomercy.tv/tv/'.$id)
                ->json();

                if($data['status'] == 'error'){
                    abort(404, __('ui.itemnotexist'));
                }
                else if($data['status'] == 'ok'){
                    return [
                        "data" => $data['data'],
                        "seasons" => $data['seasons'] ?? null
                    ];
                }

            }
        }
    }

    public function tv_info($id)
    {

        $this->data = Tv::where('id', $id)
            ->with('seasons.episodes')
            ->with('media')
            ->with('seasons.media')
            ->with('companies')
            ->with('genres')
            ->with(['seasons.episodes.video_file' => function ($query) {
                $query->select('episode_id', 'duration');
            }])
            ->with(['translation' => function ($query) {
                $query->where('iso_639_1', $this->lang);
            }])
            ->with(['seasons.translation' => function ($query) {
                $query->where('iso_639_1', $this->lang);
            }])
            ->with(['seasons.episodes.translation' => function ($query) {
                $query->where('iso_639_1', $this->lang);
            }])

            ->first();

        $this->columns = Schema::getColumnListing('video_file');

        if ($this->data != null) {

            $data = collect($this->data)->merge([
                'origin' => $this->origin,
                'image_type' => $this->type ?? 'movie',
                'year' => Carbon::parse($this->data['year'])->format('Y'),
                'directors' => collect($this->data->crew)->map(function ($crew) {
                    $directors = [];
                    if ($crew['job']  == 'Director' || $crew['job'] == 'Directing') {
                        $directors[] = $crew;
                        // return $director;
                    } else if (strpos($crew['job'], 'Director') == true) {
                        $directors[] = $crew;
                        // return $director;
                    }
                    return $directors;
                })->flatten()->unique()->take(5),
                'lead_actors' => collect($this->data->cast)->map(function ($lead_actors) {
                    return collect($lead_actors)->except('known_for');
                })->take(5),
                'cast' => collect($this->data->cast)->map(function ($lead_actors) {
                    return collect($lead_actors)->except('known_for');
                }),
                'crew' => collect($this->data->crew),
                'posters' => collect($this->data->posters),
                'backdrops' => collect($this->data->backdrops),
                // 'videos' => collect($this->data->media)->where('type', '<>', 'poster')->where('type', '<>', 'backdrop'),
                'videos' => [],
                'audio' => collect($this->data->audio)->toArray(),
                'trailers' => collect($this->data->media)->map(function ($media) {
                    return collect($media->where('type', 'Trailer'))->whereNotNull('key');
                }),
                // 'trailers' => [],
                'duration' => $this->data['duration'],
                'genres' => explode(', ', $this->data['genres']),
                'seasons' => collect($this->data->seasons),
                'recommendations' => Http::acceptJson()
                    ->withHeaders([
                        'Accept-Language' => app()->getLocale(),
                    ])
                    ->get('https://api.nomercy.tv/tv/'.$id. '/recommendations')
                ->json()['data'] ?? [],

            ])->merge(collect($this->data['translated'])->except(['title']));

            return [
                "data" => $data,
                "seasons" => null
            ];
        }
        else if($user = auth('api')->tokenById(auth()->user())){

            $data = Http::withToken($user->id)
                ->acceptJson()
                ->withHeaders([
                    'Accept-Language' => app()->getLocale(),
                ])
                ->get('https://api.nomercy.tv/tv/'.$id)
                ->json();

            if($data['status'] == 'error'){
                abort(404, __('ui.itemnotexist'));
            }
            else if($data['status'] == 'ok'){
                return [
                    "data" => $data['data'],
                    "seasons" => $data['seasons'] ?? null
                ];
            }
        }
    }
    public function movie_info($id)
    {
        $this->data = Movie::where('id', $id)
            ->with('media')
            ->with('companies')
            // ->with('trailer')
            ->with(['video_file' => function ($query) {
                $query->select('movie_id', 'duration');
            }])

            ->first();


            $this->columns = Schema::getColumnListing('video_file');

        if ($this->data != null) {

            $data = collect($this->data)->merge([
                'origin' => $this->origin,
                'image_type' => $this->type ?? 'movie',
                'year' => Carbon::parse($this->data['release_date'])->format('Y'),

                'release_date' => Carbon::createFromDate($this->data['release_date'])->format(__('Y')),
                'directors' => collect($this->data->crew)->map(function ($crew) {
                    $directors = [];
                    if ($crew['job']  == 'Director' || $crew['job'] == 'Directing') {
                        $directors[] = $crew;
                        // return $director;
                    } else if (strpos($crew['job'], 'Director') == true) {
                        $directors[] = $crew;
                        // return $director;
                    }
                    return $directors;
                })->flatten()->unique()->take(5),
                'lead_actors' => collect($this->data->cast)->map(function ($lead_actors) {
                    return collect($lead_actors)->except('known_for');
                })->take(5),
                'cast' => collect($this->data->cast)->map(function ($lead_actors) {
                    return collect($lead_actors)->except('known_for');
                }),
                'crew' => collect($this->data->crew),
                'posters' => collect($this->data->media)->where('type', 'poster'),
                'backdrops' => collect($this->data->media)->where('type', 'backdrop'),
                'videos' => collect($this->data->media)->where('type', '<>', 'poster')->where('type', '<>', 'backdrop'),
                'audio' => collect($this->data->audio)->toArray(),
                'trailers' => collect($this->data->media)->where('type', 'Trailer'),
                'duration' => $this->data['duration'],
                'title' => str_replace('"', '\'', $this->data['title']),
                'overview' => str_replace('"', '\'', $this->data['overview']),
                'genres' => explode(', ', $this->data['genres']),
                'recommendations' => Http::acceptJson()
                    ->withHeaders([
                        'Accept-Language' => app()->getLocale(),
                    ])
                    ->get('https://api.nomercy.tv/movie/'.$id. '/recommendations')
                ->json()['data'] ?? [],

            ])->merge(collect($this->data['translated'])->except(['title']));

            return [
                "data" => $data
            ];
        }
        else {

            $data = Http::withToken(auth('api')->tokenById(auth()->user()->id))
                ->acceptJson()
                ->withHeaders([
                    'Accept-Language' => app()->getLocale(),
                ])
                ->get('https://api.nomercy.tv/movie/'.$id)
                ->json();

            if($data['status'] == 'error'){
                abort(404, __('ui.itemnotexist'));
            }
            else if($data['status'] == 'ok'){

                return [
                    "data" => $data['data'],
                    "seasons" => $data['seasons'] ?? null
                ];
            }
        }
    }

    public function items($array)
    {
        $tv = Tv::select(['id', 'title', 'title_sort', 'poster', 'backdrop', 'type', 'have_episodes', 'number_of_episodes', 'genres', 'first_air_date', 'overview', 'collection'])->whereIn('id', $array)
            ->get();

        $tv = collect($tv)->map(function ($items) {
            if (count($items->toArray()) > 0) {
                return collect($items)->merge([
                    'origin' => $this->origin,
                ]);
            } else {
                return [];
            }
        })->toArray();

        $movie = Movie::select(['id', 'title', 'title_sort', 'poster', 'backdrop', 'type', 'genres', 'date', 'overview', 'collection'])->whereIn('id', $array)
            ->get();

        $movie = collect($movie)->map(function ($items) {
            if (count($items->toArray()) > 0) {
                return collect($items)->merge([
                    'origin' => $this->origin,
                ]);
            } else {
                return [];
            }
        })->toArray();

        $data = array_merge($movie, $tv);

        return $data;

    }
}
