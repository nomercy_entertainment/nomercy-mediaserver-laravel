<?php

namespace App\Http\Controllers\Content;

use App\Models\Tv;
use App\Models\Genre;
use App\Models\Movie;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Player\PlayerController;

class ApiController extends Controller
{
    public $id, $type;

    public function  __construct(Request $request)
    {

        // $this->middleware('api');
        $this->id = $request->query('id');
        $this->item = $request->query('item');
        $this->room = $request->query('room');
        $this->season = $request->query('season', null);
        $this->episode = $request->query('episode', null);
        $this->type = $request->query('type', null);
        $this->origin = env('APP_URL');
        // $this->ServerController = new ServerController();
    }

    public function items(Request $request)
    {
        $array = explode(',', $request->query('ids'));

        $data = (new DataController())->items($array);

        return response()->json([
            'data' => $data
        ]);
    }

    public function index()
    {

        $DataController = (new DataController())->index();
        $data = $DataController["data"];
        // $continue = $DataController["continue"];

        $arr = [];

        $collection = collect($data)->map(function ($collection) {
            if($collection['collection'] != '' || $collection['collection'] != null){
                return collect($collection)->only(['collection'])->all();
            }
        })->unique('collection')->whereNotNull('collection')->flatten()->toArray();

        foreach($collection as $name){
            $arr[$name] = collect($data)->filter(function ($data) use ($name) {
                if (isset($data['collection'])) {
                    return false !== strstr($data['collection'], $name);
                }
            })->unique('id')->sortBy("title_sort");
        }

        foreach (Genre::get()->where('iso_639_1', '==',  app()->getLocale()) as $genre) {
            $arr[$genre['name']] = collect($data)->filter(function ($data) use ($genre) {
                if (isset($data['genres']) && $data['genres'] != []) {
                    return false !== stristr(collect($data['genres'])->pluck('name')->implode(', '), $genre['name']);
                }
            })->unique('id')->shuffle()->take(32);
        }

        if(env('FEATURED') == 'last'){
            $last = collect($data)->sortByDesc('created_at')->first();
        }
        else {
            $last = collect($data)->shuffle()->first();
        }

        return response()->json([
            'data' => $arr,
            'last' => $last,
            // 'continue' => $continue
        ]);
    }

    public function anime(Request $request)
    {

        $DataController = new DataController();
        $data = $DataController->anime();


        $data = collect($data)->unique('id')->sort(function ($a, $b) {
            return $a['title_sort'] <=> $b['title_sort'];
        })->values();

        return response()->json(['data' => $data]);
    }

    public function movies()
    {

        $DataController = new DataController();
        $data = $DataController->movies();

        $data = collect($data)->unique('id')->sort(function ($a, $b) {
            return $a['title_sort'] <=> $b['title_sort'];
        })->values();

        return response()->json(['data' => $data]);
    }

    public function tv()
    {
        $DataController = new DataController();
        $data = $DataController->tv();

        $data = collect($data)->unique('id')->sort(function ($a, $b) {
            return $a['title_sort'] <=> $b['title_sort'];
        })->values();

        return response()->json(['data' => $data]);
    }

    public function specials()
    {
        $DataController = new DataController();
        $data = $DataController->specials();

        $data = collect($data)->unique('id')->sort(function ($a, $b) {
            return $a['title_sort'] <=> $b['title_sort'];
        })->values();

        return response()->json(['data' => $data]);
    }

    public function collections()
    {
        $DataController = new DataController();
        $data = $DataController->collections();

        $data = collect($data)->unique('id')->sort(function ($a, $b) {
            return $a['title_sort'] <=> $b['title_sort'];
        })->values();

        return response()->json(['data' => $data]);
    }

    public function collection($id)
    {
        $DataController = new DataController();
        $data = $DataController->collection($id);

        $data = collect($data)->unique('id')->sort(function ($a, $b) {
            return $a['release_date'] <=>  $b['release_date'];
        })->values();

        return response()->json(['data' => $data]);
    }

    public function genres()
    {
        $DataController = new DataController();
        $data = $DataController->genres();

        return response()->json(['data' => $data]);
    }

    public function tv_info($id)
    {
        $DataController = new DataController();
        $data = $DataController->tv_info($id);

        return response()->json($data);
    }
    public function tv_watch($id)
    {
        $PlayerController = new PlayerController();
        $data = $PlayerController->playlist($id);

        return response()->json($data);
    }

    public function movie_info($id)
    {
        $DataController = new DataController();
        $data = $DataController->movie_info($id);

        return response()->json($data);
    }
    public function movie_watch($id)
    {
        $PlayerController = new PlayerController();
        $data = $PlayerController->playlist($id);

        return response()->json($data);
    }

    public function watch($id)
    {
        $JWPLAYER_KEY = env('JWPLAYER_KEY');

        $data = (new PlayerController())->playlist($id);

        return response()->json(
            [
                'JWPLAYER_KEY' => $JWPLAYER_KEY,
                "data" => $data,
                'id' => $this->id,
                'user' => Auth::user()->name,
                'room' =>  generateRandomString(20)
            ]
        );
    }
}
