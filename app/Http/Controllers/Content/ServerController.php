<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use App\Models\Server;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class ServerController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function get_request($uri)
    {
        $response = [];
        $servers = Server::where('enabled', true)->get();
        foreach ($servers as $server) {
            if ($server->name != env('WORKER_NAME')) {
                if(Carbon::now("UTC")->lt($server->created_at)){
                    $this->get_server_tokens();
                }
                try {

                    $response = collect($response)->mergeRecursive(Http::withToken($server->access_token)
                        ->withoutVerifying()
                        ->timeout(15)
                        ->acceptJson()
                        ->get("https://". $server->location  . '/api' . $uri)
                        ->json()
                    );
                    if($response == null){
                        $response = collect($response)->mergeRecursive(Http::withToken($server->access_token)
                            ->withoutVerifying()
                            ->timeout(15)
                            ->acceptJson()
                            ->get("http://". $server->location  . '/api' . $uri)
                            ->json()
                        );
                    }

                } catch (\Throwable $th) {
                    $response = [];
                }
            }
        }
        return $response ?? ['data' => []];
    }

    public function post_request($uri, $data)
    {

        $response = [];
        $servers = Server::where('enabled', true)->get();
        if($servers->count() == 0){
            $this->get_server_tokens();
            $servers = Server::where('enabled', true)->get();
        }
        foreach ($servers as $server) {
            if ($server->name != env('WORKER_NAME')) {
                if(Carbon::now("UTC")->gte($server->created_at)){
                    $this->get_server_tokens();
                }

                $token = $server->access_token;
                try {
                    $response = Http::withToken($token)
                        ->withoutVerifying()
                        ->timeout(15)
                        ->post("https://". $server->location  . '/api' . $uri, $data)
                        ->json()
                        ;
                    if($response == null){
                        $response = Http::withToken($token)
                            ->withoutVerifying()
                            ->timeout(15)
                            ->post("http://". $server->location  . '/api' . $uri, $data)
                            ->json();
                    }

                    if (isset($response['message']) && $response['message'] == 'unauthenticated') {
                        dd('Unauthenticated on external server');
                    }
                } catch (\Throwable $th) {
                    $response = [];
                }
            }
        }

        // dd($response);
        return $response ?? ['data' => []];
    }

    public function get_server_tokens(){
        try {
            $user = auth()->user();

            if($user->isOwner == 1){

                $token = JWTAuth::fromUser($user);
                $response = http::withToken($token)
                    ->retry(2, 3)
                    ->post("https://nomercy.tv/api/auth/server_tokens")->json();

                    if(isset($response['servers'])){
                        Server::truncate();

                        foreach($response['servers'] as $server){
                            if($server['name'] != env("WORKER_NAME")){
                                Server::updateOrCreate(["name" => $server['name']], $server);
                            }
                        }
                    }
                return $response;
            }
        } catch (\Throwable $th) {
            // abort(400, "Something went wrong, Refresh the page");
        }

    }

}
