<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use App\Models\Episode;
use App\Models\Movie;
use App\Models\Tv;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SearchController extends Controller
{

    public function search(Request $request)
    {
        $this->middleware('auth');
        if ($request->ajax()) {
            $year = null;
            $output = "";
            $items = [];

            $results[] = Tv::where('title', 'like', '%' . $request->search . '%')
                ->orWhere('homepage', 'like', '%' . $request->search . '%')
                ->orWhere('overview', 'like', '%' . $request->search . '%')
                ->get();

            $results[] = Movie::where('title', 'like', '%' . $request->search . '%')
                ->orWhere('homepage', 'like', '%' . $request->search . '%')
                ->orWhere('overview', 'like', '%' . $request->search . '%')
                ->with('video_file')
                ->get();


            foreach ($results as $result) {
                foreach ($result as $media) {
                    if ((isset($media->video_file) && $media->video_file->file != null) || $media->type == 'tv') {
                        $items[] = $media;
                    }
                }
            }

            usort($items, function ($a, $b) {
                return $a['title_sort'] <=> $b['title_sort'];
            });


            $results2[] = Episode::where('title', 'like', '%' . $request->search . '%')
                ->orWhere('overview', 'like', '%' . $request->search . '%')
                ->with('tv')
                ->with('video_file')
                ->orderBy('title')
                ->get();

            foreach ($results2 as $result) {
                foreach ($result as $media) {
                    if (isset($media->video_file) && $media->video_file->file != null) {
                        $items[] = $media;
                    }
                }
            }

            if ($items) {
                foreach ($items as $item) {

                    $id = $item->id;
                    $poster = $item->poster;

                    if (isset($item->title)) {
                        $title = $item->title;
                        $poster = $item->poster;
                        $episode =  '&season=' . $item->season_number . '&episode=' . $item->episode_number . '';
                    }
                    if (isset($item->show_id)) {
                        $title = $item->video_file->title;
                        $episode =  '&season=' . $item->season_number . '&episode=' . $item->episode_number . '';
                        $poster = $item->still;
                        $id = $item->show_id;
                    } else {
                        $episode = '';
                        $poster = '/img/noimage.poster.jpg';
                    }


                    if (isset($item->date)) {
                        $year = Carbon::createFromDate($item->date)->format('Y');
                    }
                    if (isset($item->first_air_date)) {
                        $year = Carbon::createFromDate($item->first_air_date)->format('Y');
                    }

                    if (!isset($item->poster) && (!isset($item->still) || $item->poster == '')) {
                        $poster = '/img/noimage.thumbnail.jpg';
                    }
                    if (isset($item->poster) && !isset($item->still)) {
                        $poster = $item->poster;
                    }
                    if (isset($item->still_path)) {
                        $poster = env('APP_URL') . '/assets/tv' . $item->still_path;
                    }

                    $page = $episode == '' ? 'info' : 'watch';

                    $output .= '<a href="/' . $page . '?id=' . $id . $episode . '" class="w-full bg-gray-800 hover:no-underline">
                    <div class="flex flex-row w-full h-40 p-2 bg-gray-800">
                        <div class="flex flex-col justify-center w-2/12 h-full">
                            <img class="w-24 mx-auto" src="' . $poster . '" alt="image">
                        </div>
                        <div class="flex flex-col justify-center w-full h-full">
                            <div class="w-full h-6 p-2 text-white">
                                <h6 class="float-left w-10/12 mb-1 text-xs text-left">' . $title . '</h6>
                                <small class="float-right w-2/12 mb-1 text-xs text-left">' . $year . '</small>
                            </div>
                            <div class="h-32 p-2 overflow-y-auto text-xs text-left" style="font-size:10px;white-space: normal;">
                                ' . $item->overview . '
                            </div>
                        </div>
                    </div>
                </a>';
                }
            }
            return $output;
        }
    }
}
