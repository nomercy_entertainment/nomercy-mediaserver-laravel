<?php

namespace App\Http\Controllers\Content;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LiveStreamController extends Controller
{
    public function watch(Request $request)
    {
        $id = 1;
        $JWPLAYER_KEY = env('JWPLAYER_KEY');

        $streamer_image = 'https://www.gravatar.com/avatar/d34d156085c5ebf680bb24fe595c694b';

        $username = Auth::user()->preferred_username ?? 'guest';

        return view('pages.live_videojs', compact('id', 'streamer_image', 'username', 'JWPLAYER_KEY'));
    }
}
