<?php

namespace App\Http\Controllers\Player;

use App\Http\Controllers\Content\ServerController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Player\VideoStream;
use App\Models\Episode;
use App\Models\Movie;
use App\Models\Season;
use App\Models\Special;
use App\Models\SupportedLanguages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;

class PlayerController extends Controller
{
    private $id, $type, $lang_code, $lang_name, $file;

    public function __construct()
    {
        $this->id = request()->query('id');
        $this->season = request()->query('season', null);
        $this->episode = request()->query('episode', null);
        $this->type = request()->query('type', null);
        $this->file = request()->query('file', null);

        $this->lang_code = [];
        $this->lang_name = [];
        $this->lang_country = [];
        foreach (SupportedLanguages::select('language_code', 'language_name', 'country_code')->orderBy('audio_order', 'asc')->get() as $audio_code) {
            $this->lang_code[] = $audio_code['language_code'];
            $this->lang_name[] = $audio_code['language_name'];
            $this->lang_country[] = $audio_code['country_code'];
        }
    }

    public function replace_code_name($string)
    {
        return str_replace($this->lang_code, $this->lang_name, $string);
    }

    public function playlist($id)
    {
        $this->id = $id;
        $video = [];

        if (!isset($this->type)) {

            $array = Season::where('tv_id', $id)
                ->where('season_number', '>', 0)
                ->orderBy('season_number', 'asc')
                ->with('tv')
                ->with('episodes.video_file')
                ->get();

            if (count($array) > 0) {
                foreach ($array as $season) {
                    foreach ($season->episodes->sortBy('episode_number') as $episodes) {
                        $video[] = collect($episodes)->merge([
                            "origin" => env("APP_URL")
                        ]);
                    }
                }

                return $this->create_array(collect($video)->toArray(), $array);
            }


            if (count($array) == 0) {
                $array = Movie::where('id', $this->id)
                    ->with('video_file')
                    ->get();

                if (isset($array[0]->video_file)) {
                    return $this->create_array($array,  $array);
                }
            }
            if ($array == null || count($array) == 0) {
                $ServerController = new ServerController();
                $video = $ServerController->get_request('/playlist?id=' . $this->id);
                return $video;
            }
        }
        elseif (isset($this->type) && $this->type == "special") {

            $array = Special::where('id', $this->id)
                ->with('media')
                ->first();

            foreach ($array->media as $media) {
                if ($media->type == 'movie') {
                    $video[] = Movie::where('id', $media->movie_id)
                        ->with('video_file')
                        ->first();
                }
                if ($media->type == 'season') {
                    $seasons = Season::where('id', $media->season_id)
                        ->with('episodes.video_file')
                        ->first();
                    foreach ($seasons["episodes"] as $episode) {
                        $video[] = $episode;
                    }
                }
                if ($media->type == 'episode') {
                    $video[] = Episode::where('id', $media->episode_id)
                        ->with('video_file')
                        ->first();
                }
            }
        }
    }

    public function create_array($video, $main)
    {
        $video = collect($video);

        $json_array = [];
        $index = 1;

        $sub_type = [
            "full",
            "sign",
            "sdh"
        ];
        $type = null;

        $json_data = array();

        foreach ($video as $data) {

            $data = (object) $data;

            $data->video_file = (object) $data->video_file;

            $production = false;

            if (isset($main[0]->tv->type)) {
                $type = $main[0]->tv->type;
            }
            else{
                $type = 'movie';
            }

            if (isset($main[0]->tv->title)) {
                if($index != count($video)){
                    $backroute = '/' . $type . '#scroll_' . str_replace(['A ', 'An ', 'The ', '"'], '', $main[0]->tv->title)[0];
                }
                else {
                    $backroute = '/';
                }
                $name = $main[0]->tv->title;
                $name = str_replace(['The ', 'A ', 'An '], '', $name);
                $production = $main[0]->tv->in_production == 0 ? false : true;
            } else {
                if($index != count($video)){
                $backroute = '/movies#scroll_' . str_replace(['A ', 'An ', 'The ', '"'], '', $data->title)[0];
            }
            else {
                $backroute = '/';
            }
                $name = $data->title;
                $name = str_replace(['The ', 'A ', 'An '], '', $name);
            }

            if (isset($data->still) && $data->still != null) {
                $image = $data->still;
            } else if (isset($data->backdrop_path) && $data->backdrop_path != null) {
                $image = '/assets/' . ($type == 'movie' ? 'movie' : 'tv' ) . str_replace('.jpg', '.original.jpg', $data->backdrop_path);
            } else {
                $image = '';
            }
            // dd($main);

            $json_array['id'] = (int) $data->id;
            $json_array['show'] = $name ?? '';
            $json_array['title'] = $data->title ?? '';
            $json_array['description'] = $data->overview;
            $json_array['image'] = $image;
            $json_array['poster'] = $image;

            $json_array['backroute'] = $backroute;
            $json_array['season_image'] =  isset($main[$data->season_number - 1]) && $main[$data->season_number - 1]->poster_path != null ? img_url($main[$data->season_number - 1]->poster_path) : $main[$data->season_number - 1]['poster'] ?? null;
            // $json_array['season_image'] =  $main[$data->season_number - 1]['poster'];
            $json_array['season_overview'] = isset($data->season_number) && isset($main[$data->season_number - 1]) ? $main[$data->season_number - 1]->overview  : '';

            $json_array['date'] = $data->date  ?? $data->date ?? '';
            $json_array['video_type'] = $type;
            $json_array['tmdbid'] = (int) $this->id ?? '';
            $json_array['season'] = $data->season_number ?? '';
            $json_array['episode'] = $data->episode_number ?? '';
            $json_array['production'] = $production;
            $json_array['origin'] = $data->origin;

            $json_array['uid'] = (int) isset($data->show_id) ? ($data->show_id ?? '') . ($data->season_id ?? '') . ($data->video_file->episode_id ?? '') : $data->id;

            $json_array['season_id'] = $data->season_id;
            $json_array['episode_id'] = isset($data->video_file->episode_id) ? $data->video_file->episode_id : null;

            $json_array['vote_average'] = $data->vote_average;
            $json_array['vote_count'] = $data->vote_count;

            $tracks = [];
            $tracks2 = [];
            $json_array['sources'] = [];
            $json_array['file'] = [];
            $json_array['tracks'] = [];
            $json_array['textTracks'] = [];

            if (isset($data->video_file->file) && file_exists(str_replace('/Media', env("MEDIA_ROOT"), $data->video_file->file))) {
                $json_array['file'] = str_replace(env('MEDIA_ROOT'), '/Media', $data->video_file->file);

                $json_array['sources'][] = [
                    'src' => str_replace(env('MEDIA_ROOT'), '/Media', $data->video_file->file),
                    'type' => contains($data->video_file->file, array('m3u8')) ? 'application/x-mpegURL' : 'video/mp4'
                    // 'type' => contains($data->video_file->file, array('m3u8')) ? 'application/vnd.apple.mpegurl' : 'video/mp4'
                ];

                $tracks[] = [
                    "file" => $data->video_file->previews,
                    "kind" => "thumbnails"
                ];
                $tracks[] = [
                    "file" => $data->video_file->chapters,
                    "kind" => "chapters"
                ];

                $re = '/(?<folder>.*)\/(?<file>.*)\.(?<ext>(mp4|m3u8))/';
                preg_match($re, str_replace('/Media', env("MEDIA_ROOT"), $data->video_file->file), $match);


                // $tracks2[] = [
                //     "label" => "Off",
                //     "src" => null,
                //     "srclang" => null,
                //     "kind" => "subtitles"
                // ];

                $subs = Storage::disk('root')->files($match['folder'] . '/subtitles');
                foreach($subs as $sub){
                    $re = '/(?<folder>.*)\/(?<file>.*)\.(?<lang>(\w{3}))\.(?<type>(full|sign|sdh))\.(?<ext>(vtt|ass))/m';
                    preg_match($re, $sub, $match);
                    if(isset($match['type']) && isset($match['lang'])){

                        $tracks[] = [
                            "file" => str_replace(env("MEDIA_ROOT"), '/Media', '/' . $sub),
                            "name" => $match['type'] != 'full' ? $this->replace_code_name($match['lang']) . ' ' . $match['type'] : $this->replace_code_name($match['lang']),
                            "language" => $match['lang'],
                            "srclang" => $match['lang'],
                            "kind" => "captions"
                        ];

                        $tracks2[] = [
                            "label" => $match['type'] != 'full' ? $this->replace_code_name($match['lang']) . ' ' . $match['type'] : $this->replace_code_name($match['lang']),
                            "src" => str_replace(env("MEDIA_ROOT"), '/Media', '/' . $sub),
                            "srclang" => $match['type'] != 'full' ? $this->replace_code_name($match['lang']) . '_' . $match['type'] : $this->replace_code_name($match['lang']),
                            "kind" => "subtitles"
                        ];
                    }
                }


                $json_array['tracks'] = $tracks;
                $json_array['textTracks'] = $tracks2;
            }

            array_push($json_data, $json_array);
            $index++;
        }

        return $json_data;
    }

    public function video()
    {
        $video_path = '/mnt/media/' . $this->file;
        $stream = new VideoStream($video_path);
        $stream->start();
    }

    public function movie_watch($id)
    {
        $this->id = $id;
        return $this->playlist($id);
    }

    public function tv_watch($id)
    {
        $this->id = $id;
        return $this->playlist($id);
    }
}
