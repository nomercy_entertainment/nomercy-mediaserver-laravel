<?php

namespace App\Http\Controllers\Player;

use App\Http\Controllers\Controller;
use App\Models\UserVideo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserVideoController extends Controller
{
    public function index()
    {

        $user = User::with('user_video')
            ->find(Auth::id());

        $data = UserVideo::where('user_id', $user->id)
            ->get();

        return response()->json($data);
    }

    public function set(Request $request)
    {

        $user = User::find(Auth::id());

        $user_video = UserVideo::updateOrCreate(
            [
                "user_id" => $user->id,
                "video_id" => $request->video_id,
            ],
            [
                "tmdb_id" => $request->tmdb_id,
                "season_id" => $request->season_id,
                "episode_id" => $request->episode_id,
                "time" => $request->time,
                "subtitle" => $request->subtitle,
                "audio" => $request->audio,
                "special_id" => $request->special_id,
            ]
        );

        return response()->json($user_video);
    }

    public function get(Request $request)
    {
        $user = User::find(Auth::id());

        if (isset($request->video_id) && !isset($request->type)) {
            $data = UserVideo::where('video_id', $request->video_id)
                ->where('user_id', $user->id)
                ->first();
            return response()->json($data);
        }
        else if (isset($request->type) && $request->type == 'special') {

            $data = UserVideo::where('special_id', $request->special_id)
                ->where('user_id', $user->id)
                ->orderBy('updated_at', 'DESC')
                ->first();
            return response()->json($data);
        } else {
            $data = UserVideo::where('tmdb_id', $request->tmdb_id)
                ->where('user_id', $user->id)
                ->orderBy('updated_at', 'DESC')
                ->first();
            return response()->json($data);
        }
    }
}
