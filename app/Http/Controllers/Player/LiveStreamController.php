<?php

namespace App\Http\Controllers\Player;

use App\Http\Controllers\Controller;
use App\User;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Tymon\JWTAuth\Facades\JWTAuth;

class LiveStreamController extends Controller
{
    public function watch(Request $request)
    {
        $datetime = new DateTime('tomorrow');

        $user = User::where('isOwner', 1)->first();
        $token = JWTAuth::fromUser($user);
        $JWPLAYER_KEY = env('JWPLAYER_KEY', Http::withToken($token)->withoutVerifying()->get('https://nomercy.tv/api/keys')->json()['JW_KEY']);

        $id = generateRandomString(5);
        $username = Auth::user()->name ?? 'guest';

        $curPageName = explode('/', $_SERVER['REQUEST_URI'])[2];
        $streamer = User::where('name', $curPageName)->with('stream')->first();

        if($streamer == null || $streamer->stream == null){
            abort(404);
        }
        else{
            $userimage = 'https://www.gravatar.com/avatar/' . md5($streamer['email'] ?? '') . '?d=wavatar&r=r';
            $streamer->stream->image = 'https://www.gravatar.com/avatar/' . md5($streamer['email'] ?? '') . '?d=wavatar&r=r';
        }

        return view('pages.live_videojs', compact('id', 'streamer', 'username', 'userimage', 'JWPLAYER_KEY'));

    }

    public function obsninja()
    {
        return view('pages.obsninja');
    }

}
