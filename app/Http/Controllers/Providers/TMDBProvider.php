<?php

namespace App\Http\Controllers\Providers;

use App\User;
use Carbon\Carbon;
use App\Models\Genre;
use App\Models\Language;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

ini_set('max_execution_time', 0);

class TMDBProvider extends Controller
{
    private $token;
    public $languages;

    public function __construct()
    {
        $this->token = env('TMDB_TOKEN');
        $this->languages = languages();
    }
    public function movie($id)
    {

        $this->id = $id;
        dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Fetching data from themoviedb.org for show: ' . $id);
        $movie = Http::withToken($this->token)
            ->get('https://api.themoviedb.org/3/movie/' . $id . '?append_to_response=credits,videos,images,external_ids,translations,alternative_titles,changes')
            ->json();

        isset($movie['title']) ?dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Movie info found' . PHP_EOL . 'Movie: ' . $movie['title']) : exit('Nothig found for this id!');

        // dd($movie);

        $videos = collect($movie['videos']['results'])->map(function($video) {
            return collect($video)->merge([
                'tv_id' => $this->id,
                'type' => $video['type'],
            ])->except('id');
        });


        $list = collect($movie)->merge([
            'poster' => $movie['poster_path'] ? $movie['poster_path'] : '',
            'backdrop' => $movie['backdrop_path'] ? $movie['backdrop_path'] : '',

            'belongs_to_collection' => $movie['belongs_to_collection'] ? collect($movie['belongs_to_collection'])->merge([
                'poster' => $movie['belongs_to_collection']['poster_path']
                    ? $movie['belongs_to_collection']['poster_path']
                    : '',
                'backdrop' => $movie['belongs_to_collection']['backdrop_path']
                    ? $movie['belongs_to_collection']['backdrop_path']
                    : '',
                "title_sort" => titleSort($movie['title']),
                "title" => $movie['title'],
                "parts" => count(Http::withToken($this->token)
                    ->get('https://api.themoviedb.org/3/collection/' . $movie['belongs_to_collection']['id'] . '')
                    ->json()['parts']),
            ])->except(['name','poster_path', 'backdrop_path']) : collect([]),

            'cast' => collect($movie['credits']['cast'])->map(function ($cast) {
                return collect($cast)->merge([
                    'profile_path' => $cast['profile_path']
                        ? $cast['profile_path']
                        : '',
                ])->except([]);
            }),
            'crew' => collect($movie['credits']['crew'])->map(function ($crew) {
                return collect($crew)->merge([
                    'profile_path' => $crew['profile_path']
                        ? $crew['profile_path']
                        : '',
                ])->except([]);
            }),
            // 'release_date' => Carbon::parse($movie['release_date'])->format('M d, Y'),
            'duration' => $movie['runtime'],
            'genres' => collect($movie['genres']),
            'backdrops' => collect($movie['images']['backdrops'])->map(function ($backdrops) {
                return collect($backdrops)->merge([
                    'key' => null,
                    'file_path' => $backdrops['file_path']
                        ? $backdrops['file_path']
                        : '',
                    'type' => 'backdrop',
                    'vote_average' => $backdrops['vote_average'] * 10 . '%',
                ])->except(['aspect_ratio']);
            }),
            'videos' => $videos,
            'posters' => collect($movie['images']['posters'])->map(function ($posters) {
                return collect($posters)->merge([
                    'key' => null,
                    'file_path' => $posters['file_path']
                        ? $posters['file_path']
                        : '',
                    'type' => 'poster',
                    'vote_average' => $posters['vote_average'] * 10 . '%',
                ])->except(['aspect_ratio']);
            }),
            'production_companies' => collect($movie['production_companies'])->map(function ($production_companies) {
                return collect($production_companies)->merge([
                    'logo_path' => $production_companies['logo_path']
                        ? $production_companies['logo_path']
                        : '',
                ])->except([]);
            }),
            'title_sort' => addslashes(str_replace(["The ", "An ", "A ", "\""], [""], $movie['title'])),
            'languages' => replace_country_name($this->languages, collect($movie['spoken_languages'])->pluck('iso_639_1')->flatten()->implode(', ')),
            'original_language' => $movie['original_language'],
            'external_ids' => collect($movie['external_ids']),
            'translations' => collect($movie['translations']['translations'])->map(function ($translations) {
                return collect($translations)->merge([
                    'movie_id' => $this->id,
                ])->merge($translations['data'])->except(['data']);
            }),
            'type' => 'movie',
            'vote_average' => $movie['vote_average'] * 10 . '%',
            'alternative_titles' => collect($movie['alternative_titles']['titles'])->map(function ($alternative_titles) {
                return collect($alternative_titles)->merge([
                    'movie_id' => $this->id,
                ])->except(['type']);
            }),

        ])->except(['credits', 'adult', 'budget', 'video', 'production_countries', 'spoken_languages', 'images', 'runtime', 'revenue', 'changes', 'backdrop_path', 'poster_path']);

        $items = $list->all();
        ksort($items);

        foreach ($items as $key => $item) {
            if (is_object($item)) {
                $array['relations'][$key] = $item;
            } else {
                $array['database'][$key] = $item;
            }
        }

        return $array;
    }

    public function tv($id)
    {

        $this->id = $id;
        dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Fetching data from themoviedb.org for show: ' . $id);
        $tvshow = Http::withToken($this->token)
            ->get('https://api.themoviedb.org/3/tv/' . $id . '?append_to_response=credits,videos,images,external_ids,content_ratings,translations,alternative_titles,changes')
            ->json();

        isset($tvshow['name']) ?dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Show info found' . PHP_EOL . 'Show: ' . $tvshow['name']) : exit('Nothig found for this id!');

        $this->tvshow = $tvshow;


        $videos = collect($tvshow['videos']['results'])->map(function($video) {
            return collect($video)->merge([
                'tv_id' => $this->id,
                'type' => $video['type'],
            ])->except('id');
        });


        $list = collect($tvshow)->merge([
            'poster' => $tvshow['poster_path'] ?? '',
            'backdrop' => $tvshow['backdrop_path'] ?? '',

            'cast' => collect($tvshow['credits']['cast'])->map(function ($cast) {
                return collect($cast)->merge([
                    'profile_path' => $cast['profile_path']
                        ? $cast['profile_path']
                        : '',
                ])->except([]);
            }),
            'crew' => collect($tvshow['credits']['crew'])->map(function ($crew) {
                return collect($crew)->merge([
                    'profile_path' => $crew['profile_path']
                        ? $crew['profile_path']
                        : '',
                ])->except([]);
            }),
            'backdrops' => collect($tvshow['images']['backdrops'])->map(function ($images) {
                return collect($images)->merge([
                    'tv_id' => $this->id
                ]);
            }),
            'posters' => collect($tvshow['images']['posters'])->map(function ($images) {
                return collect($images)->merge([
                    'tv_id' => $this->id
                ]);
            }),
            // 'first_air_date' => Carbon::parse($tvshow['first_air_date'])->format('M d, Y'),
            // 'last_air_date' => Carbon::parse($tvshow['first_air_date'])->format('M d, Y'),
            'runtime' => (int) collect($tvshow['episode_run_time'])->flatten()->implode(', '),
            'genres' => collect($tvshow['genres']),
            'languages' => replace_country_name($this->languages, collect($tvshow['languages'])->flatten()->implode(', ')),
            'last_episode_to_air' => $tvshow['last_episode_to_air']['id'] ?? null,
            'next_episode_to_air' => $tvshow['next_episode_to_air']['id'] ?? null,
            'networks' => collect($tvshow['networks'])->map(function ($networks) {
                return collect($networks)->merge([
                    'logo_path' => $networks['logo_path']
                        ? $networks['logo_path']
                        : '',
                ])->except([]);
            }),
            'seasons' => collect($tvshow['seasons'])->map(function ($seasons) {
                return collect($seasons)->merge($this->season($this->id, $seasons['season_number']))->except(['air_date']);
            }),
            'original_title' => $tvshow['original_name'],
            'original_language' => $tvshow['original_language'],
            'origin_country' => collect($tvshow['origin_country'])->flatten()->implode(', '),
            'production_companies' => collect($tvshow['production_companies'])->map(function ($production_companies) {
                return collect($production_companies)->merge([
                    'logo_path' => $production_companies['logo_path']
                        ? $production_companies['logo_path']
                        : '',
                ])->except([]);
            }),
            'title_sort' => addslashes(str_replace(["The ", "An ", "A ", "\""], [""], $tvshow['name'])),

            'overview' => str_replace('"', '\\"', $tvshow['overview']),
            'title' => str_replace('"', '\\"', $tvshow['name']),

            'content_ratings' => $tvshow['content_ratings']['results'],
            'translations' => collect($tvshow['translations']['translations'])->map(function ($translations) {
                return collect($translations)->merge([
                    'tv_id' => $this->id,
                    'name' => str_replace('"', '\\"', $translations['name']),
                ])->merge($translations['data'])->except(['data']);;
            }),
            'alternative_titles' => collect($tvshow['alternative_titles']['results']),
            'videos' => $videos,
            'vote_average' => $tvshow['vote_average'] * 10 . '%',
            'created_by' => collect($tvshow['created_by'])->map(function ($created_by) {
                return collect($created_by)->merge([
                    'profile_path' => $created_by['profile_path']
                        ? $created_by['profile_path']
                        : '',
                ])->except([]);
            }),
            'languages' => replace_country_name($this->languages, collect($tvshow['spoken_languages'])->pluck('iso_639_1')->flatten()->implode(', ')),

            'external_ids' => collect($tvshow['external_ids']),
            'alternative_titles' => collect($tvshow['alternative_titles']['results']),
            'content_ratings' => collect($tvshow['content_ratings']),
        ])->except(['credits', 'name', 'original_name', 'episode_run_time', 'spoken_languages', 'images', 'changes', 'type', 'poster_path', 'backdrop_path']);

        $items = $list->all();
        ksort($items);

        foreach ($items as $key => $item) {
            if (is_object($item)) {
                $array['relations'][$key] = $item->toArray();
            } else {
                $array['database'][$key] = $item;
            }
        }

        dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Fetching data complete for show: ' . $id);
        return $array;
    }

    public function season($id, $season)
    {
        $season = Http::withToken($this->token)
            ->get('https://api.themoviedb.org/3/tv/' . $id . '/season/' . $season . '?append_to_response=credits,images,external_ids,content_ratings,translations,alternative_titles,changes')
            ->json();

        $this->season_id = $season['id'];

        $array = collect($season)->merge([
            'poster' => $season['poster_path'] ? $season['poster_path'] : '',

            'episodes' =>  collect($season['episodes'])->map(function ($episodes) {
                return collect($episodes)->merge($this->episode($this->id, $episodes['season_number'], $episodes['episode_number']))->except(['air_date', 'name']);
            }),
            'cast' => collect($season['credits']['cast'])->map(function ($cast) {
                return collect($cast)->merge([
                    'profile_path' => $cast['profile_path']
                        ? $cast['profile_path']
                        : '',
                ])->except([]);
            }),
            'crew' => collect($season['credits']['crew'])->map(function ($crew) {
                return collect($crew)->merge([
                    'profile_path' => $crew['profile_path']
                        ? $crew['profile_path']
                        : '',
                ])->except([]);
            }),
            'posters' => collect($season['images']['posters'])->map(function ($images) {
                return collect($images)->merge([
                    'file_path' => $images['file_path']
                        ? $images['file_path']
                        : '',
                    'vote_average' => $images['vote_average'] * 10 . '%',
                    'iso_639_1' => $images['iso_639_1'],
                    'type' => 'poster',
                ])->except(['aspect_ratio']);
            }),
            'date' => Carbon::parse($season['air_date'])->format('M d, Y'),

            'translations' => collect($season['translations']['translations'] ?? [])->map(function ($translations) {
                return collect($translations)->merge([
                    'season_id' => $this->season_id,
                    'name' => str_replace('"', '\\"', $translations['name']),
                ])->merge($translations['data'])->except(['data']);
            }),
            'season_number' => zeropad($season['season_number']),

            'overview' => str_replace('"', '\\"', $season['overview']),
            'name' => str_replace('"', '\\"', $season['name']),

        ])->merge($season['external_ids'])->except(['air_date', 'credits', 'external_ids', 'images', 'changes', 'poster_path']);

        $items = $array->all();
        ksort($items);

       dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Found season: ' . $season['season_number'] . ' for show: ' . $this->tvshow['name']);
        return collect($items);
    }

    public function episode($id, $season, $episode)
    {

        $episode = Http::withToken($this->token)
            ->get('https://api.themoviedb.org/3/tv/' . $id . '/season/' . $season . '/episode/' . $episode . '?append_to_response=credits,images,external_ids,content_ratings,translations,alternative_titles,changes')
            ->json();

        $this->episode_id = $episode['id'] ?? null;

        $array = collect($episode)->merge([
            'overview' => str_replace('"', '\\"', $episode['overview'] ?? ''),
            'title' => str_replace('"', '\\"', $episode['name']),

            'still' => $episode['still_path'] ?? '',
            'vote_average' => isset($episode['vote_average']) ? $episode['vote_average'] * 10 . '%' : null,
            'stills' => collect(isset($episode['images']) ? $episode['images']['stills'] : [])->map(function ($still_path) {
                return collect($still_path)->merge([
                    'file_path' => $still_path['file_path']
                        ? $still_path['file_path']
                        : '',
                    'type' => 'still',
                ])->except(['aspect_ratio']);
            }),
            'cast' => collect($episode['credits']['cast'])->map(function ($cast) {
                return collect($cast)->merge([
                    'profile_path' => $cast['profile_path']
                        ? $cast['profile_path']
                        : '',
                ])->except([]);
            }),
            'crew' => collect($episode['credits']['crew'])->map(function ($crew) {
                return collect($crew)->merge([
                    'profile_path' => $crew['profile_path']
                        ? $crew['profile_path']
                        : '',
                ])->except(['adult', 'known_for', 'known_for_department']);
            }),
            'guest_stars' => collect($episode['credits']['guest_stars'])->map(function ($guest_stars) {
                return collect($guest_stars)->merge([
                    'profile_path' => $guest_stars['profile_path']
                        ? $guest_stars['profile_path']
                        : '',
                ])->except([]);
            }),
            'date' => Carbon::parse($episode['air_date'])->format('M d, Y'),
            'translations' => collect($episode['translations']['translations'])->map(function ($translations) {
                return collect($translations)->merge([
                    'episode_id' => $this->episode_id,
                    'name' => str_replace('"', '\\"', $translations['name']),
                ])->merge($translations['data'])->except(['data']);
            }),
            'season_number' => zeropad($episode['season_number']),
            'episode_number' => zeropad($episode['episode_number']),
        ])->merge($episode['external_ids'])->except(['credits', 'air_date', 'credits', 'images', 'external_ids', 'name', 'changes', 'still_path', 'poster_path']);

        $items = $array->all();
        ksort($items);

        //dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Merging episode: ' . $episode['name'] . ' for season: ' . $season);

        return collect($items);
    }


    public function actor($actor)
    {
        return collect($actor)->merge([
            // 'birthday' => Carbon::parse($actor['birthday'])->format('M d, Y'),
            'age' => Carbon::parse($actor['birthday'])->age,
            'profile_path' => $actor['profile_path']
                ? $actor['profile_path']
                : '',
        ])->only([
            'birthday', 'age', 'profile_path', 'name', 'id', 'homepage', 'place_of_birth', 'biography'
        ]);
    }

    public function social($social)
    {
        return collect($social)->merge([
            'twitter' => $social['twitter_id'] ? 'https://twitter.com/' . $social['twitter_id'] : null,
            'facebook' => $social['facebook_id'] ? 'https://facebook.com/' . $social['facebook_id'] : null,
            'instagram' => $social['instagram_id'] ? 'https://instagram.com/' . $social['instagram_id'] : null,
        ])->only([
            'facebook', 'instagram', 'twitter',
        ]);
    }

    public function knownForMovies($credits)
    {
        $castMovies = collect($credits)->get('cast');

        return collect($castMovies)->sortByDesc('popularity')->take(5)->map(function ($movie) {
            if (isset($movie['title'])) {
                $title = $movie['title'];
            } elseif (isset($movie['name'])) {
                $title = $movie['name'];
            } else {
                $title = 'Untitled';
            }

            return collect($movie)->merge([
                'poster_path' => $movie['poster_path']
                    ? $movie['poster_path']
                    : '',
                'title' => $title,
                'linkToPage' => $movie['media_type'] === 'movie' ? route('movies.show', $movie['id']) : route('tv.show', $movie['id'])
            ])->only([
                'poster_path', 'title', 'id', 'media_type', 'linkToPage',
            ]);
        });
    }

    public function credits($credits)
    {
        $castMovies = collect($credits)->get('cast');

        return collect($castMovies)->map(function ($movie) {
            if (isset($movie['release_date'])) {
                $releaseDate = $movie['release_date'];
            } elseif (isset($movie['first_air_date'])) {
                $releaseDate = $movie['first_air_date'];
            } else {
                $releaseDate = '';
            }

            if (isset($movie['title'])) {
                $title = $movie['title'];
            } elseif (isset($movie['name'])) {
                $title = $movie['name'];
            } else {
                $title = 'Untitled';
            }

            return collect($movie)->merge([
                'release_date' => $releaseDate,
                // 'release_year' => isset($releaseDate) ? Carbon::parse($releaseDate)->format('Y') : 'Future',
                'title' => $title,
                'character' => isset($movie['character']) ? $movie['character'] : '',
                'linkToPage' => $movie['media_type'] === 'movie' ? route('movies.show', $movie['id']) : route('tv.show', $movie['id']),
            ])->only([
                'release_date', 'release_year', 'title', 'character', 'linkToPage',
            ]);
        })->sortByDesc('release_date');
    }

    public function languages()
    {
        $languages = Http::withToken($this->token)
            ->get('https://api.themoviedb.org/3/configuration/languages')
            ->json();

        Language::upsert($languages, 'iso_639_1');

        return collect($languages)->toArray();
    }

    public function genres()
    {
        $file = '/cache/genres.json';
        if(Genre::count() == 0 || !Storage::exists($file)){

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Fetching genres from themoviedb.org');

           $this->genres = [];
           $allGenres = [];
           foreach(languages() as $this->lang){

               $tv = Http::withToken($this->token)
                   ->get('https://api.themoviedb.org/3/genre/tv/list?language=' .  $this->lang['iso_639_1'])
                   ->json()['genres'];

               $movie = Http::withToken($this->token)
                   ->get('https://api.themoviedb.org/3/genre/movie/list?language=' .  $this->lang['iso_639_1'])
                   ->json()['genres'];

               $allGenres[] = collect(array_merge_recursive($tv, $movie))->map(function($language) {
                   return collect($language)->merge([
                       'iso_639_1' => $this->lang['iso_639_1'],
                       'genre_id' => $language['id']
                   ])->except('id');
               })->toArray();
           }

           foreach($allGenres as $genres){
               foreach($genres as $genre){
                   if($genre['name'] != null){
                       $this->genres[] = $genre;
                   }
               }
           }

           Storage::put($file, json_encode($this->genres));
       }
       else {

           $this->genres = json_decode(Storage::get($file), true);

       }

       return $this->genres;
    }

    public function search($type, $query, $year, $results = 1, $language = 'en')
    {

        $search = $type == 'movie' ?
            Http::withToken($this->token)->acceptJson()
            ->get('https://api.themoviedb.org/3/search/' . $type . '?query=' . $query . '&primary_release_year=' . $year)
            ->json() :
            Http::withToken($this->token)->acceptJson()
            ->get('https://api.themoviedb.org/3/search/' . $type . '?query=' . $query . '&first_air_date_year=' . $year)
            ->json();

        if (isset($search['errors'])) {
            abort(404, $type . ' with query: ' . $query . ' ' . $year . ' resulted in: ' . $search['errors'][0]);
        }

        $array = collect($search['results'])->map(function ($results) {
            return collect($results)->merge([
                'id' => $results['id'],
                'title' => $results['title'] ?? $results['name'],
            ])->only(['id', 'title', 'overview', 'poster_path', 'release_date', 'first_air_date']);
        });

        if ($results == 1) {
            $array = $array->first();
        } elseif ($results > 1) {
            $array = $array->take($results);
        }

        // $items = $array->all();
        // ksort($items);

        return collect($array);
    }
}
