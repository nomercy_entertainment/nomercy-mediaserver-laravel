<?php

namespace App\Http\Controllers\Database;

use PDOException;
use App\Models\Tv;
use Carbon\Carbon;
use App\Models\Cast;
use App\Models\Crew;
use App\Models\Genre;
use App\Models\Media;
use App\Models\Movie;
use App\Models\Season;
use App\Models\Company;
use App\Models\Creator;
use App\Models\Episode;
use App\Models\Network;
use App\Models\GuestStar;
use App\Models\Collection;
use App\Models\Translation;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Encoder\FileController;
use App\Http\Controllers\Providers\TMDBProvider;

ini_set('max_execution_time', 0);

class TMDBController extends Controller
{

    public function __construct()
    {
        $TMDBProvider = new TMDBProvider();
        $data = $TMDBProvider->genres();

        Genre::upsert($data, 'iso_639_1');

    }

    public function search_movie($name, $year, $results = 1)
    {

        $Qname = str_replace(['.'], ['+'], $name);

        if ($Qname != '') {

            $TMDBProvider = new TMDBProvider();
            $result = $TMDBProvider->search('movie', $Qname, $year, $results);

            if (!isset($result['id']) || $result['id'] == null || $result['id'] == '') {
                dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'No result found for query: ' . $Qname . '+' . $year);
                return 1;
            }
            dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Found result: ' .  $result['title'] . '  with TMDB ID: ' . $result['id']);

            $this->add_movie($result['id'], 'out');
        }
    }



    public function add_movie($id, $direction = 'out', $forced = false)
    {

        $movie = Movie::where('id', $id)->first();
        $images = [];
        $files = [];
        $FileController = new FileController();

        // if (isset($movie->updated_at) && (Carbon::create($movie->updated_at->toDateTimeString())->isToday() || Carbon::create($movie->updated_at->toDateTimeString())->isYesterday()) && $forced == false) {
        //     dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Nothing to update, Loading from database');
        //     $files[] = [$movie, 'movie', $direction];
        // } else {

            dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Downloading info for movie: ' . $id);

            $TMDBProvider = new TMDBProvider();
            $data = $TMDBProvider->movie($id);
            $this->movie_id = $id;

            $modelMovie = Movie::find($id);

            $database = collect($data['database'])->only(
                "adult",
                "backdrop",
                "budget",
                "duration",
                "homepage",
                "id",
                "imdb_id",
                "original_language",
                "original_title",
                "overview",
                "popularity",
                "poster",
                "release_date",
                "revenue",
                "status",
                "title",
                "title_sort",
                "trailer",
                "video",
                "vote_average",
            )
            ->toArray();

            Movie::updateOrCreate(["id" => $database['id']], array_merge($database, collect($data['relations']["external_ids"])->toArray()));

            $files[] = [$data['database'], 'movie', $direction];

            if(isset($data['relations']['genres'])){
                $modelGenres = Genre::whereIn('genre_id', collect($data['relations']['genres'])->pluck('id')->unique())->get();
                $modelMovie->genres()->sync($modelGenres);
            }

            if (isset($data['relations']["belongs_to_collection"]['id'])) {
                dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding collections to the database');

                $dl_images[] = [$data['relations']["belongs_to_collection"]['poster'], 'collection', 'original'];
                $dl_images[] = [$data['relations']["belongs_to_collection"]['poster'], 'collection', 'w300'];
                Collection::updateOrCreate(["id" => $data['relations']["belongs_to_collection"]['id']], collect($data['relations']["belongs_to_collection"])->toArray())->movies()->syncWithoutDetaching([$id]);
            };
            if (isset($data['database']['backdrop'])) {
                $dl_images[] = [$data['database']['backdrop'], 'movie', 'original'];
            }
            if (isset($data['database']['poster'])) {
                $dl_images[] = [$data['database']['poster'], 'movie', 'original'];
                $dl_images[] = [$data['database']['poster'], 'movie', 'w300'];
            };

            foreach ($data['relations']["videos"] as $item) {
                if ($item['type'] == 'Trailer') {
                    dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Downloading First trailer from youtube: ' . $item['key'] . '');
                    $cmd = youtubeDl() . " -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' --quiet --merge-output-format mp4 '" . $item['key'] . "' -o  '/var/www/html/public/assets/trailer/movie/' . $id . '.trailer.mp4'";
                    exec($cmd, $output);
                }
                Movie::updateOrCreate(["id" => $data['database']['id']], ["trailer" => $item['key']]);
                if (file_exists("/var/www/html/public/assets/trailer/movie/' . $id . '.trailer.mp4")) {
                    break;
                }
            }

            foreach ($data['relations']["videos"] as $item) {
                if (!file_exists('/var/www/html/public/assets/trailer/movie/' . $id . '.trailer.mp4')) {
                    dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Downloading backup trailer from youtube: ' . $item['key'] . ' type: ' . $item['type']);
                    $cmd = youtubeDl() . " -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' --quiet --merge-output-format mp4 '" . $item['key'] . "' -o  '/var/www/html/public/assets/trailer/movie/' . $id . '.trailer.mp4'";
                    exec($cmd, $output);
                    Movie::updateOrCreate(["id" => $data['database']['id']], ["trailer" => $item['key']]);
                    break;
                }
                dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding key: ' . $item['key'] . ' type: ' . $item['type'] .  ' to the database');
                Media::updateOrCreate(["key" => $item['key']], collect($item)->toArray())->movie()->syncWithoutDetaching([$id]);
            }

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding companies to the database');
            foreach ($data['relations']["production_companies"] as $item) {
                Company::updateOrCreate(["id" => $item['id']], collect($item)->toArray())->movie()->syncWithoutDetaching([$id]);
            }

            foreach ($data['relations']["alternative_titles"] as $item) {
                // TODO: maybe
            }

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding movie backsrops to the database');
            foreach ($data['relations']["backdrops"] as $item) {
                Media::updateOrCreate(["file_path" => $item['file_path']], collect($item)->toArray())->movie()->syncWithoutDetaching([$id]);
            }



            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding movie cast profiles to the database');
            foreach ($data['relations']["cast"] as $item) {
                Cast::updateOrCreate(["id" => $item['id']], collect($item)->toArray())->movie()->syncWithoutDetaching([$id]);
                if ($item['profile_path'] != null) {
                    $dl_images[] = [$item['profile_path'], 'profile', 'w300'];
                }
            }

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding movie crew profiles to the database');
            foreach ($data['relations']["crew"] as $item) {
                Crew::updateOrCreate(["id" => $item['id']], collect($item)->toArray())->movie()->syncWithoutDetaching([$id]);

                if ($item['profile_path'] != null) {
                    $dl_images[] = [$item['profile_path'], 'profile', 'w300'];
                }
            }

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding movie posters to the database');
            foreach ($data['relations']["posters"] as $item) {
                Media::updateOrCreate(["file_path" => $item['file_path']], collect($item)->toArray())->movie()->syncWithoutDetaching([$id]);
            }

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding movie translations to the database');
            foreach ($data['relations']["translations"] as $item) {
                Translation::updateOrCreate(["iso_3166_1" => $item['iso_3166_1'], "movie_id" => $item['movie_id']], collect($item)->toArray())->movie()->syncWithoutDetaching([$id]);
            }

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Downloading images and trailers');

            foreach ($images as $image) {

                download_tmdb($image[0], $image[1], $image[2]);
            }
        // }

        foreach ($files as $file) {
            $FileController->get_movie_file($file[0], $file[1], $file[2]);
            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Added Movie: ' .  $file[0]['title'] . ' to the database');
        }

        dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Done!');
    }


    public function search_tv($name, $year, $results = 1)
    {

        $Qname = str_replace(['.'], ['+'], $name);

        if ($Qname != '') {

            $TMDBProvider = new TMDBProvider();
            $result = $TMDBProvider->search('tv', $Qname, $year, $results);

            if (!isset($result['id']) || $result['id'] == null || $result['id'] == '') {
                dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'No result found for query: ' . $Qname . '+' . $year);
                return 1;
            }
            dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Found result: ' .  $result['title'] . '  with TMDB ID: ' . $result['id']);
            $this->add_tv($result['id'], 'out');
        }
    }


    public function add_tv($id, $direction = 'out', $forced = false)
    {

        $FileController = new FileController();
        $tv = Tv::where('id', $id)->first();
        $dl_images = [];
        $dl_files = [];
        $db_casts = [];
        $db_casts['seasons'] = [];
        $db_casts['episodes'] = [];
        $db_crews = [];
        $db_crews['seasons'] = [];
        $db_crews['episodes'] = [];
        $db_semediasasons[] = [];
        $db_medias = [];
        $db_medias['seasons'] = [];
        $db_medias['episodes'] = [];
        $db_medias['youtube'] = [];
        $db_gueststars[] = [];
        $db_seasons = [];
        $db_trans = [];
        $db_trans['seasons'] = [];
        $db_trans['episodes'] = [];

        if (isset($tv->updated_at) && (Carbon::create($tv->updated_at->toDateTimeString())->isToday() || Carbon::create($tv->updated_at->toDateTimeString())->isYesterday()) && $forced == false) {
            dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Nothing to update, Loading from database');

            foreach (Episode::where('show_id', $id)->get() as $episode) {
                $dl_files[] = [$tv, $direction, $episode['id']];
            }
        } else {

            dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Downloading info for show: ' . $id);

            $TMDBProvider = new TMDBProvider();
            $data = $TMDBProvider->tv($id);
            $this->tv_id = $id;

            if (isset($data['database']['backdrop'])) {
                $dl_images[] = [$data['database']['backdrop'], 'tv', 'original'];
            }
            if (isset($data['database']['poster'])) {

                $dl_images[] = [$data['database']['poster'], 'tv', 'original'];
                $dl_images[] = [$data['database']['poster'], 'tv', 'w300'];
            };

            unset($data['database']["production_countries"]);




            Tv::updateOrCreate(["id" => $data['database']['id']], array_merge($data['database'], collect($data['relations']["external_ids"])->toArray()));

            $modelTv = Tv::where('id', $id)->first();

            dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Added Show: ' .  $data['database']['title'] . ' to the database');


            if(isset($data['relations']['genres'])){
                $modelGenres = Genre::whereIn('genre_id', $data['relations']['genres'])->get()->pluck('id')->unique();
                $modelTv->genres()->sync($modelGenres);
            }

            foreach ($data['relations']["videos"] as $item) {
                if ($item['type'] == 'Trailer') {
                    dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Downloading First trailer from youtube: ' . $item['key'] . '');
                    $cmd = youtubeDl() . " -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' --quiet --merge-output-format mp4 '" . $item['key'] . "' -o  '/var/www/html/public/assets/trailer/' . $id . '.trailer.%(ext)s'";
                    exec($cmd, $output);
                }
                if (file_exists("/var/www/html/public/assets/trailer/tv/' . $id . '.trailer.mp4")) {
                    Tv::updateOrCreate(["id" => $data['database']['id']], ["trailer" => $item['key']]);
                    break;
                }
            }


            foreach ($data['relations']["videos"] as $item) {
                if (!file_exists('/var/www/html/public/assets/trailer/tv/' . $id . '.trailer.mp4')) {
                    dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Downloading backup trailer from youtube: ' . $item['key'] . ' type: ' . $item['type']);
                    $cmd = youtubeDl() . " -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' --quiet --merge-output-format mp4 '" . $item['key'] . "' -o  '/var/www/html/public/assets/trailer/tv/" . $id . ".trailer.%(ext)s'";
                    exec($cmd, $output);
                    dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   .
                    collect($output)->merge($output)->except([3, 4, 5, 6]));
                    Tv::updateOrCreate(["id" => $data['database']['id']], ["trailer" => $item['key']]);
                    break;
                }
                $medias['youtube'][] = [collect($item)->toArray(), $id];
            }

            foreach ($data['relations']["backdrops"] as $item) {
                $db_medias['tv'][] = [collect($item)->toArray(), $id];
            }

            foreach ($data['relations']["posters"] as $item) {
                $db_medias['tv'][] = [collect($item)->toArray(), $id];
            }

            foreach ($data['relations']["production_companies"] as $item) {

            }

            foreach ($data['relations']['seasons'] as $season) {

                if (isset($season['poster'])) {
                    $dl_images[] = [$season['poster'], 'tv', 'original'];
                    $dl_images[] = [$season['poster'], 'tv', 'w300'];
                }

                foreach ($season["posters"] as $item) {
                    // $db_medias['seasons'][] = [collect($item)->toArray(), $season['id']];

                    $db_medias['seasons'][] = $item;
                }
                unset($season["posters"]);

                foreach ($season["cast"] as $item) {
                    // $db_casts['seasons'][] = [collect($item)->toArray(), $season['id']];
                    $db_casts['seasons'][] = $item;
                    $dl_images[] = [$item['profile_path'], 'profile', 'w300'];
                }
                unset($season["cast"]);

                foreach ($season["crew"] as $item) {
                    // $db_crews['seasons'][] = [collect($item)->toArray(), $season['id']];
                    $db_crews['seasons'][] = $item;
                    if ($item['profile_path'] != null) {
                        $dl_images[] = [$item['profile_path'], 'profile', 'w300'];
                    }
                }
                unset($season["crew"]);

                if (isset($season['translations'])) {
                    foreach ($season['translations'] as $item) {
                        // $db_trans['seasons'][] = [$translations, $season['id']];
                        $db_trans['seasons'][] = array_merge($item, [
                            'name' => str_replace('"', '\\"', $item['name']),
                        ]);

                    }
                    unset($season['translations']);
                }

                foreach ($season['episodes'] as $episode) {

                    foreach ($episode['crew'] as $item) {
                        // $db_crews['episodes'][] = [$item, $episode['id']];
                        $db_crews['episodes'][] = $item;
                    }
                    unset($episode['crew']);

                    if (isset($episode['guest_stars'])) {
                        foreach ($episode['guest_stars'] as $item) {
                            // $db_gueststars['episodes'][] = [$item, $episode['id']];
                            $db_gueststars['episodes'][] = $item;
                        }
                        unset($episode['guest_stars']);
                    }
                    foreach ($episode['cast'] as $item) {
                        // $db_casts['episodes'][] = [collect($item)->toArray(), $episode['id']];
                        $db_casts['episodes'][] = $item;
                    }
                    unset($episode['cast']);

                    unset($episode['external_ids']);

                    foreach ($episode['translations'] as $item) {
                        try {
                            // $db_trans['episodes'][] = [$item, $episode['id']];
                            $db_trans['episodes'][] = array_merge($item, [
                                'name' => str_replace('"', '\\"', $item['name']),
                            ]);
                        } catch (PDOException $e) {
                        }
                    }
                    unset($episode['translations']);

                    if (isset($episode['still_path']) && $episode['still_path'] != '') {
                        $dl_images[] = [$episode['still_path'], 'tv', 'w300'];

                        foreach ($episode["stills"] as $item) {
                            // $db_medias['episodes'][] = [collect($item)->toArray(), $episode['id']];
                            $db_medias['episodes'][] = $item;
                        }
                    }
                    unset($episode["stills"]);

                    $db_episodes[] = array_merge(collect($episode)->except('still_path')->toArray(), [
                        "season_id" => $season['id'],
                        "show_id" => $id,
                    ]);

                    $dl_files[] = [$data['database'], $direction, $episode['id']];
                }
                unset($season['episodes']);

                $db_seasons[] = array_merge(collect($season)->except('poster_path')->toArray(), ["tv_id" => $this->tv_id]);

            }


            dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding tv data to the database');

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding ' . count($db_medias['youtube']) . '  tv Youtube items to the database');
            foreach ($db_medias['youtube'] as $media) {
                Media::updateOrCreate(["key" => $media[0]['key']], $media[0])->tv()->syncWithoutDetaching([$media[1]]);
            }

            if (isset($data['relations']['created_by'])) {
                dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding creator profiles to the database');
                foreach ($data['relations']["created_by"] as $item) {
                    Creator::updateOrCreate(["id" => $item['id']], $item)->tv()->syncWithoutDetaching([$id]);
                    if ($item['profile_path'] != null) {
                        $dl_images[] = [$item['profile_path'], 'profile', 'w300'];
                    }
                }
            }

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding tv company logos to the database');
            foreach ($data['relations']["production_companies"] as $item) {
                Company::updateOrCreate(["id" => $item['id']], collect($item)->toArray())->tv()->syncWithoutDetaching([$id]);
            }

            foreach ($data['relations']["alternative_titles"] as $item) {
                // TODO: maybe
            }

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding tv cast profiles to the database');
            foreach ($data['relations']["cast"] as $item) {
                Cast::updateOrCreate(["id" => $item['id']], collect($item)->toArray())->tv()->syncWithoutDetaching([$id]);
                if ($item['profile_path'] != null) {
                    $dl_images[] = [$item['profile_path'], 'profile', 'w300'];
                }
            }
            unset($data['relations']["cast"]);

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding tv crew profiles to the database');
            foreach ($data['relations']["crew"] as $item) {
                Crew::updateOrCreate(["id" => $item['id']], collect($item)->toArray())->tv()->syncWithoutDetaching([$id]);
                if ($item['profile_path'] != null) {
                    $dl_images[] = [$item['profile_path'], 'profile', 'w300'];
                }
            }
            unset($data['relations']["crew"]);

            foreach ($data['relations']["content_ratings"] as $item) {
            }
            foreach ($data['relations']["created_by"] as $item) {
            }

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding tv networks to the database');
            foreach ($data['relations']["networks"] as $item) {
                Network::updateOrCreate(["id" => $item['id']], collect($item)->toArray())->tv()->syncWithoutDetaching([$id]);
            }

            if (isset($data['relations']['translations'])) {
                dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding tv translations to the database');
                foreach ($data['relations']['translations'] as $translations) {
                    Translation::updateOrCreate(["tv_id" => $translations['tv_id'], "iso_3166_1" => $translations['iso_3166_1']], $translations)->tv()->syncWithoutDetaching([$id]);
                }
                unset($data['relations']['translations']);
            }





            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding ' . count($db_trans['seasons']) . '  season translations to the database');
            // foreach ($db_trans['seasons'] as $translation) {
            //     Translation::updateOrCreate(["season_id" => $translation[0]['season_id'], "iso_3166_1" => $translation[0]['iso_3166_1']], $translation[0])->season()->syncWithoutDetaching([$translation[1]]);
            // }
            foreach(array_chunk($db_trans['seasons'], 1000) as $transaction){
                Translation::upsert((array) $transaction, "season_id");
            }

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding ' . count($db_medias['seasons']) . '  season medias to the database');
            // foreach ($db_medias['seasons'] as $media) {
            //     Media::updateOrCreate(["file_path" => $media[0]['file_path']], $media[0])->season()->syncWithoutDetaching([$media[1]]);
            // }
            foreach(array_chunk($db_medias['seasons'], 1000) as $transaction){
                Media::upsert((array) $transaction, "file_path");
            }

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding  ' . count($db_seasons) . ' seasons to the database');
            // foreach ($db_seasons as $season) {
            //     Season::updateOrCreate(["id" => $season['id']], $season)->tv();
            // }
            foreach(array_chunk($db_seasons, 1000) as $transaction){
                Season::upsert((array) $transaction, "id");
            }

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding  ' . count($db_casts['seasons']) . ' season casts to the database');
            // foreach ($db_casts['seasons'] as $cast) {
            //     Cast::updateOrCreate(["id" => $cast[0]['id']], $cast[0])->season()->syncWithoutDetaching([$cast[1]]);
            // }
            foreach(array_chunk($db_casts['seasons'], 1000) as $transaction){
                Cast::upsert((array) $transaction, "id");
            }

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding ' . count($db_crews['seasons']) . '  season crews to the database');
            // foreach ($db_crews['seasons'] as $crew) {
            //     Crew::updateOrCreate(["id" => $crew[0]['id']], $crew[0])->season()->syncWithoutDetaching([$crew[1]]);
            // }
            foreach(array_chunk($db_crews['seasons'], 1000) as $transaction){
                Crew::upsert((array) $transaction, "id");
            }



            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding ' . count($db_episodes) . '  episodes to the database');
            // foreach ($db_episodes as $episode) {
            //     Episode::updateOrCreate(["id" => $episode[0]['id']], $episode[0])->season();
            // }

            foreach(array_chunk($db_episodes, 1000) as $transaction){

                // dump($transaction);
                Episode::upsert((array) $transaction, "id");
            }

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding ' . count($db_casts['episodes']) . '  episode casts to the database');
            // foreach ($db_casts['episodes'] as $cast) {
            //     Cast::updateOrCreate(["id" => $cast[0]['id']], $cast[0])->episode()->syncWithoutDetaching([$cast[1]]);
            // }
            foreach(array_chunk($db_casts['episodes'], 1000) as $transaction){
                Cast::upsert((array) $transaction, "id");
            }

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding ' . count($db_crews['episodes']) . '  episode crews to the database');
            // foreach ($db_crews['episodes'] as $crew) {
            //     Crew::updateOrCreate(["id" => $crew[0]['id']], $crew[0])->episode()->syncWithoutDetaching([$crew[1]]);
            // }
            foreach(array_chunk($db_crews['episodes'], 1000) as $transaction){
                Crew::upsert((array) $transaction, "id");
            }

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding ' . count($db_medias['episodes']) . '  episode medias to the database');
            // foreach ($db_medias['episodes'] as $media) {
            //     Media::updateOrCreate(["file_path" => $media[0]['file_path']], $media[0])->episode()->syncWithoutDetaching([$media[1]]);
            // }
            foreach(array_chunk($db_medias['episodes'], 1000) as $transaction){
                Media::upsert((array) $transaction, "file_path");
            }


            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding ' . count($db_trans['episodes']) . '  episode translations to the database');

            foreach(array_chunk($db_trans['episodes'], 1000) as $transaction){
                Translation::upsert((array) $transaction, "episode_id");
            }


            // dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Adding ' . count($db_gueststars['episodes']) . '  episode gueststars to the database');
            // foreach ($db_gueststars['episodes'] as $cast) {
            //     GuestStar::updateOrCreate(["cast_id" => $cast[0]['id']], $cast[0])->episode()->syncWithoutDetaching([$cast[1]]);
            // }


            dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Downloading ' . count($dl_images) . ' images and trailers');
            foreach ($dl_images as $image) {
                download_tmdb($image[0], $image[1], $image[2]);
            }


        }

        dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'processing  ' . count($dl_files) . ' episodes');
        // foreach ($dl_files as $file) {
        //     $FileController->get_tv_file($file[0], $file[1], $file[2]);
        // }


        $data = Tv::where('id', $id)->with('seasons.episodes.video_file')->first();
        $languages = [];
        $count = 0;
        if (isset($data['seasons'])) {
            foreach ($data['seasons'] as $season) {
                if ($season['season_number'] > 0) {
                    foreach ($season['episodes'] as $episode) {
                        if ($episode['video_file'] != null && isset($episode['video_file']) && $episode['video_file']['duration'] != null) {
                            $count++;
                            foreach (explode(',', $episode['video_file']['languages']) as $lang) {
                                $languages[] = $lang;
                            }
                        }
                    }
                }
            }
        }
        $languages = array_unique($languages);

        dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'show has ' . $count . ' episodes and ' . count($languages) . ' language(s)');
        Tv::updateOrCreate(['id' => $id], [
            'have_episodes' => $count,
            'languages' => implode(', ', $languages),
            "updated_at" => now()
        ]);

        $sql = "DELETE FROM `failed_jobs` WHERE  `exception` like '%MaxAttemptsExceededException%'";
        DB::statement($sql);
    }
}
