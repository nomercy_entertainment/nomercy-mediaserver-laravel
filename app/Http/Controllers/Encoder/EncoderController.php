<?php

namespace App\Http\Controllers\Encoder;

use App\Models\Episode;
use App\Http\Controllers\Controller;
use App\Models\Movie;
use App\Models\Tv;
use App\Models\VideoFile;

class EncoderController extends Controller
{

    public $fileIn, $outputFolder, $fileName, $db_index_number, $type, $imdbid, $dbtype;

    public function __construct($fileIn, $outputFolder, $fileName, $db_index_number, $type, $dbtype)
    {

        $this->fileIn = $fileIn;
        $this->outputFolder = $outputFolder;
        $this->fileName = $fileName;
        $this->db_index_number = $db_index_number;
        // $this->imdbid = $imdbid ?? null;
        $this->type = $type;
        $this->dbtype = $dbtype;
    }

    public function encode_job()
    {

        $ffmpeg = new Ffmpeg($this->fileIn, $this->outputFolder, $this->fileName, $this->db_index_number, $this->type, $this->dbtype);
        $ffmpeg->encode2();

        $VideoFile = VideoFile::updateOrCreate(["id" => $this->db_index_number], $ffmpeg->database());


        if ($this->type != 'movie') {
            $episode = Episode::where('id', $this->db_index_number)->first();
            $episode->video_file != '' ? $episode->video_file->update($VideoFile->toArray()) : null;

            $data = Tv::where('id', $episode['show_id'])
                ->with('seasons.episodes.video_file')
                ->first();
            if($data != null){

                $count = 0;
                if (isset($data['seasons'])) {
                    foreach ($data['seasons'] as $season) {
                        if ($season['season_number'] > 0) {
                            foreach ($season['episodes'] as $episode) {
                                if ($episode['video_file'] != null) {
                                    $count++;
                                }
                            }
                        }
                    }
                }

                $data->update([
                    'type' => $this->dbtype,
                    'have_episodes' => $count
                ]);
            }

        } else {
            $movie = Movie::where('id', $this->db_index_number)->first();
            $movie->video_file->update($VideoFile->toArray());
        }

    }
}
