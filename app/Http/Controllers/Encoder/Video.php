<?php

namespace App\Http\Controllers\Encoder;

use App\Models\FolderRoots;

class Video
{
    public $ffprobe, $supported_audio, $quality_array, $fileIn, $crop;

    public function __construct($ffprobe, $supported_audio, $fileIn, $crop)
    {
        $this->ffprobe = $ffprobe;
        $this->crop = $crop;
        $this->fileIn = $fileIn;

        $this->audio_order = $supported_audio->audio_lang_code;

        // $re = '/(?<folder>\/.*\/).*?(\(\d*\)|\d{4}).*?(\(\d*\)|\d{4}).*/m';
        // preg_match($re, $this->fileIn, $matches);
        // $database_folder = $matches['folder'];

        // dump( $database_folder);
        // $hq = FolderRoots::select('hq')->where('folder', $database_folder)->first()['hq'];
        // // dump('DEBUG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'hq for this file is: ' . $hq);


        $show_regex = '/(?<folder>^[\w\/]*\/[\w\/\.]*\/[\w\/\.]*)\//';
        preg_match($show_regex, $this->fileIn, $matcheOut);
        $folder = $matcheOut['folder'];

        $hq = FolderRoots::select('hq')->where('folder', 'like',  $folder . '%')->first()['hq'] ?? 0;
        $firstStream = collect($this->ffprobe->streams)->where('codec_type', 'video')->first();

        if ($hq && isset($firstStream['width']) && $firstStream['width'] > 2000) {
            $quality_array[] = [
                "width" => collect($this->ffprobe->streams)->where('codec_type', 'video')->first()['width'],
                "height" => collect($this->ffprobe->streams)->where('codec_type', 'video')->first()['height'],
                "rate" => 25000,
                "crf" => 18,
                "tag" => "UHD4K"
            ];
            $quality_array[] = [
                "width" => 1920,
                "height" => 1080,
                "rate" => 8000,
                "crf" => 18,
                "tag" => "HD1080P"
            ];
            // $quality_array[] = [
            //     "width" => 1280,
            //     "height" => 720,
            //     "rate" => 3000,
            //     "crf" => 23,
            //     "tag" => "HD720P"
            // ];
            // $quality_array[] = [
            //     "width" => 480,
            //     "height" => 360,
            //     "rate" => 1500,
            //     "crf" => 28,
            //     "tag" => "SD480P"
            //     // ];
        }
        else if (!$hq && isset($firstStream['width']) && $firstStream['width'] > 2000) {
            $quality_array[] = [
                "width" => 1920,
                "height" => 1080,
                "rate" => 25000,
                "crf" => 18,
                "tag" => "HD1080P"
            ];
            // $quality_array[] = [
            //     "width" => 1280,
            //     "height" => 720,
            //     "rate" => 3000,
            //     "crf" => 23,
            //     "tag" => "HD720P"
            // ];
            // $quality_array[] = [
            //     "width" => 480,
            //     "height" => 360,
            //     "rate" => 1500,
            //     "crf" => 28,
            //     "tag" => "SD480P"
            //     // ];
        }
        else if(isset($firstStream['width'])){
            $quality_array[] = [
                "width" => collect($this->ffprobe->streams)->where('codec_type', 'video')->first()['width'],
                "height" => collect($this->ffprobe->streams)->where('codec_type', 'video')->first()['height'],
                "rate" => 8000,
                "crf" => 23,
                "tag" => "HD1080P"
            ];
            // $quality_array[] = [
            //     "width" => "1280",
            //     "height" => "720",
            //     "rate" => "3000",
            //     "crf" => "23",
            // ];
            // $quality_array[] = [
            //     "width" => "480",
            //     "height" => "360",
            //     "rate" => "1500",
            //     "crf" => "28",
            // ];
        }

        $this->quality_array = $quality_array ?? [];

        $this->video_streams = $this->streams();
    }

    public function streams()
    {

        $video_streams = [];
        foreach ($this->ffprobe->streams as $stream) {
            if ($stream['codec_type'] == "video" && $stream['codec_name'] !== "png" && $stream['codec_name'] !== "mjpeg") {
                $stream = (object) $stream;
                if (isset($stream->tags)) {
                    $stream->tags = (object) $stream->tags;
                } else {
                    $stream->tags = (object) [];
                }
                foreach ($this->quality_array as $quality) {

                    if ($stream->width < 870 && $stream->height < 490) {
                        $tag = "SDDVD";
                    } else if ($stream->width < 1290 && $stream->height < 730) {
                        $tag = "HD720P";
                    } else if ($stream->width < 1930 && $stream->height < 1090) {
                        $tag = "HD1080P";
                    } else if ($stream->width < 3870 && $stream->height < 2170) {
                        $tag = "UHD4K";
                    } else if ($stream->width < 7690 && $stream->height < 4330) {
                        $tag = "UHD8K";
                    } else {
                        $tag = null;
                    }

                    if (isset($stream->color_space) && $stream->color_space == 'bt2020nc') {
                        $hdr = true;
                    } else {
                        $hdr = false;
                    }

                    $bandwidth = 520929;
                    if (isset($this->ffprobe->format->size) && isset($this->ffprobe->format->duration)) {
                        $bandwidth = (int) (floor($this->ffprobe->format->duration) * $quality['rate']) / 8;
                    }

                    if ($stream->width >= (int) $quality['width'] || $stream->width > 2000) {
                        $array = (object)  [
                            "hdr" => (bool) $hdr,
                            "quality" => $tag,
                            "bandwidth" => $bandwidth,
                            "width" => (int) $quality['width'],
                            "height" => (int) $quality['height'],
                            "rate" => $quality['rate'],
                            "crf" => $quality['crf'],
                            "crop" => $this->crop,
                        ];

                        $video_streams[] = Utils::format_video_stream($stream, $array);
                    }
                    else {
                        $array = (object)  [
                            "hdr" => (bool) $hdr,
                            "quality" => $tag,
                            "bandwidth" => $bandwidth,
                            "width" => (int) $stream->width,
                            "height" => (int) $stream->height,
                            "rate" => $stream->bit_rate ?? null,
                            "crf" => 23,
                            "crop" => $this->crop,
                        ];
                        $video_streams[] = Utils::format_video_stream($stream, $array);
                        continue 2;
                    }
                }
            }
        }

        return $video_streams;
    }

    public function count()
    {
        return collect($this->video_streams)->count();
    }

    public function qualities()
    {
        $qualityDB = [];
        foreach ($this->video_streams as $stream) {
            // dump($stream->width ,$stream->height);
            if ($stream->width < 870 && $stream->height < 490) {
                $quality = "SDDVD";
            } else if ($stream->width < 1290 && $stream->height < 730) {
                $quality = "HD720P";
            } else if ($stream->width < 1930 && $stream->height < 1090) {
                $quality = "HD1080P";
            } else if ($stream->width < 3870 && $stream->height < 2170) {
                $quality = "UHD4K";
            } else if ($stream->width < 7690 && $stream->height < 4330) {
                $quality = "UHD8K";
            } else {
                $quality = $stream->width;
            }

            $qualityDB[] = $quality;
        }

        return $qualityDB;
    }

    public function duration()
    {
        if (isset($this->ffprobe->format->duration)) {
            $duration_out = gmdate("H:i:s", $this->ffprobe->format->duration);
        } else {
            $duration_out =  9999;
        }
        return $duration_out;
    }

    public function get()
    {
        $array = [
            "count" => $this->count(),
            "streams" => $this->streams(),
            "duration" => $this->duration(),
            "qualities" => $this->qualities(),
        ];

        return $array;
    }
}
