<?php

namespace App\Http\Controllers\Encoder;

class Chapter
{
    public $ffprobe, $audio_streams;
    public $supported_audio, $lang_code, $lang_name, $lang_country;

    public function __construct($ffprobe, $supported_audio)
    {
        $this->ffprobe = (object) $ffprobe;
        $ffprobe->chapters = (object) $ffprobe->chapters;

        $this->chapters = $this->chapters();
    }

    public function chapters()
    {
        $chapters = [];
        foreach ($this->ffprobe->chapters as $chapter) {
            $chapters[] = Utils::format_chapters((object) $chapter);
        }

        return $chapters;
    }

    public function count()
    {
        $count = 0;
        foreach ($this->ffprobe->chapters as $chapter) {
            $count++;
        }
        return $count;
    }

    public function create_chapter_file()
    {
        $chapter_content = [];
        if (!empty($this->ffprobe->chapters)) {
            $echoid = 1;
            $chapterId = 1;
            $chapter_content[] = "WEBVTT";
            $chapter_content[] = "\n";
            $chapter_content[] = "\n";
            $echoid++;
            foreach ($this->chapters as $chapter) {
                $chapter_start_time = $chapter->start_time;
                $chapter_end_time = $chapter->end_time;
                $chapter_title = $chapter->title;
                $chapter_content[] = "Chapter " . $chapterId;
                $chapter_content[] = "\n";
                $echoid++;
                $chapterId++;
                $chapter_content[] = gmdate('H:i:s', $chapter_start_time) . " --> " . gmdate('H:i:s', $chapter_end_time);
                $chapter_content[] = "\n";
                $echoid++;
                $chapter_content[] = $chapter_title;
                $chapter_content[] = "\n";
                $echoid++;
            }
        }

        $response = implode('', $chapter_content);
        return $response;
    }

    public function get()
    {
        $array = [
            "count" => $this->count(),
            "chapters" => (object) $this->chapters,
            "chapter_file" => $this->create_chapter_file(),
        ];

        return $array;
    }
}
