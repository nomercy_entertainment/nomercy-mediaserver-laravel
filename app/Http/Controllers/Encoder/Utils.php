<?php

namespace App\Http\Controllers\Encoder;

class Utils
{
    public static function contains($haystack, $needle, $offset = 0)
    {
        if (!is_array($needle)) $needle = array($needle);
        foreach ($needle as $query) {
            if (strpos($haystack, $query, $offset) !== false) return true; // stop on first true result
        }
        return false;
    }

    public static function fileNameContains($IMDB_id, $folder, $string)
    {
        $files = scandir($folder);
        $response = array();

        foreach ($files as $file) {

            if (Utils::contains(basename($file), array($string)) !== false) {

                $file = $folder . $file;

                $json = file_get_contents($file);
                $array = json_decode($json, true);

                if ($array['imdbID'] == $IMDB_id) {

                    $response['file'] = str_replace($string, ".mkv", $file);
                    $response['Title'] = $array['Title'];
                    return $response;
                } else {
                    //
                }
            } else {
                //
            }
        }
    }

    public static function format_video_stream($stream, object $quality)
    {
        $lut = 'zscale=tin=smpte2084:min=bt2020nc:pin=bt2020:rin=tv:t=smpte2084:m=bt2020nc:p=bt2020:r=tv,zscale=t=linear:npl=100,format=gbrpf32le,zscale=p=bt709,tonemap=tonemap=hable:desat=0,zscale=t=bt709:m=bt709:r=tv,eq=saturation=0.85';
        // $lut = "zscale=t=linear:npl=100,format=gbrpf32le,zscale=p=bt709,tonemap=tonemap=hable:desat=0,zscale=t=bt709:m=bt709:r=tv,format=yuv420p";
        // $lut = "lut3d=" . storage_path("app/YouTube_HDRtoSDR.cube") . ",scale=-1:-1:out_color_matrix=bt709";
        return (object) [
            "index" => $stream->index,
            "codec_name" => $stream->codec_name,
            "profile" => $stream->profile ?? null,
            "display_aspect_ratio" => $stream->display_aspect_ratio ?? null,
            "pix_fmt" => $stream->pix_fmt ?? null,
            "level" => $stream->level ?? null,
            "chroma_location" => $stream->chroma_location ?? null,
            "field_order" => $stream->field_order ?? null,
            "refs" => $stream->refs ?? null,
            "is_avc" => $stream->is_avc ?? null,
            "hdr" => $quality->hdr ? $lut : '',
            // "hdr" => $quality->hdr ? 'zscale=t=linear:npl=100,format=gbrpf32le,zscale=p=bt709,tonemap=tonemap=hable:desat=0,zscale=t=bt709:m=bt709:r=tv' : '',

            "width" => $quality->width ?? $stream->width,
            "height" => $quality->height ?? $stream->height,
            "quality" => $quality->quality,
            "rate" => $quality->rate,
            "bandwidth" => $quality->bandwidth,
            "crf" => $quality->crf != null ? '-crf ' . $quality->crf : '',
            "crop" => count($quality->crop) > 0 ? $quality->crop[0] : '',
        ];
    }

    public static function format_audio_stream($stream)
    {
        return (object) [
            "index" => $stream->index,
            "codec_name" => $stream->codec_name,
            "profile" => $stream->profile ?? null,
            "channels" => $stream->channels ?? null,
            "channel_layout" => $stream->channel_layout ?? null,
            "language" => (isset($stream->tags['title']) && $stream->tags['title'] == 'Castilian') ? 'cas' : $stream->tags['language'] ?? 'und',
            "title" => $stream->tags['title'] ?? 'English',
        ];
    }

    public static function format_subtitle_stream($stream, $type)
    {

        $title = $stream->tags->title ?? null;
        return (object) [
            "index" => $stream->index,
            "codec_name" => $stream->codec_name,
            "language" => Utils::contains($title, ['Castilian', 'castilian']) !== false ? 'cas' : $stream->tags->language ?? null,
            "title" => $title,
            "type" => $type,
            "extension" => $stream->codec_name == 'subrip' ? 'vtt' : 'ass',
        ];
    }

    public static function format_chapters(object $stream)
    {
        return (object) [
            "id" => $stream->id,
            "start" => $stream->start,
            "start_time" => $stream->start_time,
            "end" => $stream->end,
            "end_time" => $stream->end_time,
            "title" => $stream->tags['title'],
        ];
    }

}
