<?php

namespace App\Http\Controllers\Encoder;

use App\Models\Episode;
use App\Models\FolderRoots;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Database\TMDBController;
use App\Jobs\AddContentJob;
use App\Jobs\QueueFixJobs;
use App\Jobs\SearchContentJob;
use App\Models\Movie;
use App\Models\Tv;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public $id, $folder_locations, $filter, $data;

    public function __construct()
    {

        $this->filter = ["video", "audio_", "thumb", "chapters", "previews", ".webp", ".vtt", "*.ts", "*.aac", "*.srt", "*.jpg", "*.ass", "*.nfo", "*.db", "*.log", "*.cue", "*.flac", "*.mp3", "*.plexignore", "lastwatch", "tbn"];
    }

    public function get_movie_files($direction = 'out')
    {

        $folder_locations = [];
        foreach (FolderRoots::where('enabled', '1')->where('type', 'movie')->orderBy('type', 'asc')->get() as $folder) {
            if (is_dir($folder['folder'])) {
                $folder_locations[] = $folder;
            }
        }

        $file = 'movie_files_' . $direction . '.json';
        if (Storage::disk('local')->exists($file) == true && (time() - (count(json_decode(Storage::disk('local')->get($file), true) ?? [])) * 50) < Storage::disk('local')->lastModified($file) && $direction != 'in') {
            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Loading file list from cache');
            $files = json_decode(Storage::disk('local')->get($file), true);
        } else {

            if ($direction != 'in') {
                dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Indexing Movie ' . $direction . ' file list, This may take a long time so make a cup of tea or grab a beer');
            }
            $filelist = new FileList();
            $filelist->filter($this->filter);


            $FolderRoots = collect($folder_locations)->where('direction', $direction)->all();
            $files = $filelist->scan($FolderRoots);
            $files = array_unique($files);
            Storage::disk('local')->put($file, json_encode($files));
        }

        return $files;
    }

    public function get_movie_file($data, $type, $direction = 'out')
    {

        $folder_locations = [];
        foreach (FolderRoots::where('enabled', '1')->where('type', 'movie')->orderBy('type', 'asc')->get() as $folder) {
            if (is_dir($folder['folder'])) {
                $folder_locations[] = $folder;
            }
        }

        $movie = Movie::where('id', $data['id'])->first();

        $NameController = new NameController($movie);
        $file = $NameController->make_movie_file_name();
        $file_name = $file['name'];

        $FolderRoots = collect($folder_locations)->where('direction', 'out')->first();
        if ($FolderRoots == '') {
            $FolderRoots = collect($folder_locations)->first();
        }

        $outputFolder = $FolderRoots['folder'] . '/' . $file['folder'] . '/';
        $type = $FolderRoots['type'];
        $imdbid = $movie->imdb_id;
        // $files = $this->get_movie_files($direction);

        $files = array_merge($this->get_movie_files('in'), $this->get_movie_files('out'));


        $str1 = [". ", " ", "!", "?",   "&", "..",   "#", "'", ":", ",", "?", "Ã¯Â¿Â½", "", "(", ")", "-.", "/", "’", "⁄", "\"", "[", "]", "Æ"];
        $str2 = [".",  ".",  "",  "", "and",  ".", "%23",  "", "",  "",  "",       "",  "",  "",  "",   "", ".",  "", ".",   "",  "",  "", 'AE'];

        $regex_title = str_replace($str1, $str2, $file['title']);
        $regex_title = str_replace([".", '-'], ["[\.\s]", '(-)?'], $regex_title);

        // $movie_regex = '/.*' . $regex_title . '[\.\s](\()?' . $file['year'] . '(\))?.*([^\d]).*\.(?<ext>mkv|mp4|avi|mov|m3u8)/mi';
        $movie_regex = '/.*' . $regex_title . '[\.\s](\()?' . $file['year'] . '(\))?.*([^\d])?(?<ext>\.mkv|\.mp4|\.avi|\.mov|\.m3u8)/mi';
        dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . "Movie regex: " . $movie_regex);

        $filesIn = [];

        foreach ($files as $file) {

            preg_match($movie_regex, $file, $matches);

            if ($matches) {
                $filesIn[] = $matches[0];
                break 1;
            }
        }

        foreach ($filesIn as $file) {

            if (Utils::contains($file, ['audio_', 'video_']) == false) {
                $fileIn = $file;
            }
        }
        if (isset($fileIn) && file_exists($fileIn)) {
            dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . "Added file to the queue: " . $fileIn);
            $EncoderController = new EncoderController($fileIn, $outputFolder, $file_name, $data['id'], 'movie', $imdbid, 'movie');
            $EncoderController->encode_job();
        }
    }

    public function get_tv_files($direction = 'out', $folder = '')
    {

        $file = 'tv_files_' . $direction . '.json';
        if (Storage::disk('local')->exists($file) == true && (time() - (count(json_decode(Storage::disk('local')->get($file), true) ?? [])) * 50) < Storage::disk('local')->lastModified($file) && $direction != 'in') {
            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Loading file list from cache');
            $files = json_decode(Storage::disk('local')->get($file), true);
        } else {

            $folder_locations = [];
            foreach (FolderRoots::where('enabled', '1')->where('type', '<>', 'movie')->orderBy('type', 'asc')->where('direction', $direction)->get() as $folder) {
                if (is_dir($folder['folder'])) {
                    $folder_locations[] = $folder;
                }
            }
            if ($direction != 'in') {
                dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Indexing TV ' . $direction . ' file list, This may take a long time so make a cup of tea or grab a beer');
            }
            $filelist = new FileList();
            $filelist->filter($this->filter);

            $files = $filelist->scan($folder_locations);
            $files = array_unique($files);
            Storage::disk('local')->put($file, json_encode($files));
        }

        return $files;
    }

    public function get_tv_file($data, $direction = 'out', $id = null)
    {

        $tv = Tv::where('id', $data['id'])->first();
        $episode = Episode::where('id', $id)->first();

        $NameController = new NameController($data);
        $file = $NameController->make_tv_file_name($episode);
        $file_name =  $file['name'];
        $folder =  $file['folder'];
        $episode_folder =  $file['episode_folder'];
        $title =  $file['title'];
        $Season =  $file['Season'];
        $Episode =  $file['Episode'];
        $series_name = $file['series_name'];
        $episode_name = $episode['title'];

        $folder_locations = [];
        foreach (FolderRoots::where('enabled', '1')->where('direction', 'out')->get() as $f) {
            if (is_dir($f['folder'] .  '/' . $folder)) {
                $folder_locations[] = $f;
            }
        }

        $FolderRoot = collect($folder_locations)->first();

        $str1 = [". ", " ", "!", "?",   "&", "..",   "#", "'", ":", ",", "?", "Ã¯Â¿Â½", "", "(", ")", "-.", "/", "’", "⁄", "\"", "[", "]", "Æ", "`", '+.'];
        $str2 = [".",  ".",  "",  "", "and",  ".", "%23",  "", "",  "",  "",       "",  "",  "",  "",   "", ".",  "", ".",   "",  "",  "", 'AE', ''];
        $str3 = [".",  ".",  "!", "", "and",  ".", "%23",  "", "",  "",  "",       "",  "",  "",  "",   "", ".",  "", ".",   "",  "",  "", 'AE', ''];

        $series_name = str_replace("..", ".", $series_name);
        $series_folder = str_replace($str1, $str3, $series_name);
        $series_name = str_replace($str1, $str2, $series_name);

        $regex_series_name = str_replace([".", ' '], "[\.\s]", $series_name);
        $regex_series_folder = str_replace([".", ' '], "[\.\s]", $series_folder);

        $regex_show_folder = $regex_series_folder . '[!]?[\.\s]?[\(]\d{4}[\)]';


        // $regex_episode = $regex_series_name . '[\.\s]?(\s\-\s)?' . 'S' . $Season . 'E' . $Episode;
        $regex_episode = $regex_show_folder . '\/.*' . $regex_series_name . '[\.\s]?(\s\-\s)?.*' . 'S' . $Season . 'E' . $Episode;
        // $regex_episode = $regex_series_name . '[\.\s]?(\s\-\s)?' .zeropad($Episode,3);

        $episode_regex = '/.*\/.*' . $regex_episode . '.*\.(?<ext>m3u8|mp4|mkv|mov|avi)/i';

        dump('');
        dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Episode Regex: ' . $episode_regex);

        $files = array_merge($this->get_tv_files('in'), $this->get_tv_files('out'));

        $filesIn = [];
        $fileIn = null;

        foreach ($files as $file) {

            preg_match($episode_regex, $file, $matches);

            if ($matches) {
                $filesIn[] = $matches[0];
                break 1;
            }
        }

        foreach ($filesIn as $file) {

            $outputFolder = null;

            if (Utils::contains($file, ['audio_', 'video_']) == false) {
                dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . "Video file: " . $file);

                if (Utils::contains($file, array('Anime')) !== false) {
                    $FolderRoot = FolderRoots::where('enabled', '1')->where('type', 'anime')->where('direction', 'out')->first();
                    $outputFolder = $FolderRoot['folder'] . '/' .  $folder . '/' .  $episode_folder . '/';
                } else if (Utils::contains($file, array('TV.Show', 'Marvels', 'HQ', 'High.Quality')) !== false) {
                    $FolderRoot = FolderRoots::where('enabled', '1')->where('type', 'tv')->where('direction', 'out')->first();
                    $outputFolder = $FolderRoot['folder'] . '/' .  $folder . '/' .  $episode_folder . '/';
                } else if ($FolderRoot == null) {
                    dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . "Can't find a destination for this show, you need to add a folder with the name: \"" . $folder . "\"");
                    return 1;
                }

                // dump($file, $outputFolder, $file_name);
                dump($FolderRoot['type']);
                if ($outputFolder != null) {

                    $EncoderController = new EncoderController($file, $outputFolder, $file_name, $episode->id, 'episode', $tv['imdb_id'], $FolderRoot['type']);
                    $EncoderController->encode_job();
                }
            }
        }


        if (isset($fileIn) && $fileIn != null &&  $fileIn != '') {
        }
    }

    public function find_media($direction = 'out', $reverse = false)
    {

        $file = 'all_files_' . $direction . '.json';


        if (Storage::disk('local')->exists($file) == true && (time() - (count(json_decode(Storage::disk('local')->get($file), true) ?? [])) * 50) < Storage::disk('local')->lastModified($file) && $direction != 'in') {
            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Loading file list from cache');
            $files = json_decode(Storage::disk('local')->get($file), true);
            // dump($files);
        } else {

            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Refreshing file list');

            $filelist = new FileList();
            $filelist->filter($this->filter);

            $folder_locations = [];
            foreach (FolderRoots::where('enabled', '1')->where('direction', $direction)->orderBy('type', 'asc')->get() as $folder) {
                if (is_dir($folder['folder'])) {
                    $folder_locations[] = $folder;
                }
            }

            $files = $filelist->scan($folder_locations, 0);

            Storage::disk('local')->put($file, json_encode($files));
        }
        if ($reverse) {
            $files = array_reverse($files);
        }

        foreach ($files as $file) {

            $re = '/(?<root_folder>.*)\/(?<title>.*)[\s\.]\((?<year>\d{4})\)/';
            preg_match($re, $file, $match);

            if ($match && $match['root_folder'] != null && $match['root_folder'] != '' && $match['title'] != '' && $match['year'] != '') {
                $type = FolderRoots::where('folder', $match['root_folder'])->first();

                if (isset($type['type'])) {
                    if ($type['type'] == 'movie') {
                        $SearchContentJob = (new SearchContentJob('movie', $match['title'], $match['year']));
                        dispatch($SearchContentJob)->onQueue('file');
                    } else if ($type != '') {
                        $SearchContentJob = (new SearchContentJob('tv', $match['title'], $match['year']));
                        dispatch($SearchContentJob)->onQueue('file');
                    }
                }
            }
        }
    }

    public function fix_audio_format()
    {
        $QueueFixJobs = (new QueueFixJobs('fix_audio_format'));
        dispatch($QueueFixJobs->onQueue('file'));
        return response()->json(['message' => 'ok'], 200);
    }

    public function check_files_for_porper_codec()
    {
        $QueueFixJobs = (new QueueFixJobs('check_files_for_porper_codec'));
        dispatch($QueueFixJobs->onQueue('file'));
        return response()->json(['message' => 'ok'], 200);
    }

    public function convert_playlist_to_valid()
    {
        $QueueFixJobs = (new QueueFixJobs('convert_playlist_to_valid'));
        dispatch($QueueFixJobs->onQueue('file'));
        return response()->json(['message' => 'ok'], 200);
    }

    public function test()
    {
        $QueueFixJobs = (new QueueFixJobs('test'));
        dispatch($QueueFixJobs->onQueue('file'));
        return response()->json(['message' => 'ok'], 200);
    }

    public function fix_broken_playlists($folder = '')
    {
        $QueueFixJobs = (new QueueFixJobs('fix_broken_playlists', $folder));
        dispatch($QueueFixJobs->onQueue('file'));
        return response()->json(['message' => 'ok'], 200);
    }

    public function check_double_videos($folder = '')
    {
        $QueueFixJobs = (new QueueFixJobs('check_double_videos', $folder));
        dispatch($QueueFixJobs->onQueue('file'));
        return response()->json(['message' => 'ok'], 200);
    }

    public function Low_quality_episodes($id, Request $request)
    {

        $result = [];
        $episodes = Episode::where('tv_id', $id)
            ->with('video_file')
            ->with('tv')
            ->orderBy('season_number')
            ->orderBy('episode_number')
            ->get();

        foreach ($episodes as $episode) {
            if (isset($episode->video_file)) {
                if ($request->get('q') == '1080' && $episode->video_file['quality'] != "['HD1080P']") {
                    if ($episode->video_file['quality'] == "['SDDVD']" || $episode->video_file['quality'] == "['HD720P']") {
                        $result[] = [
                            'name' => $episode['name'],
                            'season_number' => $episode->season_number,
                            'episode_number' => $episode->episode_number,
                            'quality' => str_replace(['[', ']', "'"], '', $episode->video_file['quality'])
                        ];
                    }
                } elseif ($request->get('q') == '720' && $episode->video_file['quality'] != "['HD1080P']" && $episode->video_file['quality'] != "['HD7200P']") {
                    if ($episode->video_file['quality'] == "['SDDVD']") {
                        $result[] = [
                            'name' => $episode['name'],
                            'season_number' => $episode->season_number,
                            'episode_number' => $episode->episode_number,
                            'quality' => str_replace(['[', ']', "'"], '', $episode->video_file['quality'])
                        ];
                    }
                }
            }
        }
        $results[] = [
            'Name' => $episode->tv->name,
            'Low quality' => $result,

        ];

        return response()->json($results);
    }

    public function missing_episodes($tmdbid)
    {
        $result = [];
        $results = [];

        $show = Tv::where('id', $tmdbid)->first();

        $episodes = Episode::where('show_id', $tmdbid)
            ->where('season_number', '>', '00')
            ->orderBy('season_number', 'asc')
            ->orderBy('episode_number', 'asc')
            ->with('video_file')
            ->get();

        foreach ($episodes as $episode) {
            if ($episode->video_file == null || !file_exists(str_replace('/Media', env('MEDIA_ROOT'), $episode->video_file['file']))) {
                $result[] = [
                    'name' => $episode['title'],
                    'season_number' => $episode['season_number'],
                    'episode_number' => $episode['episode_number']
                ];
            }
        }
        if($show == null){
            return response()->json(['message' => 'Hosted on other server']);
        }

        if (count($result) > 0) {
            $results[] = [
                'Name' => $show['title'],
                'Missing' => count($result),
                'Episodes' => $result
            ];
        } else {
            $results[] = [
                'Name' => $show['title'],
                'Missing' => count($result),
            ];
        }

        return response()->json($results);
    }
/*
    public function all_missing_episodes()
    {
        $result = [];
        $results = [];
        $shows = Tv::limit(300)->get();

        foreach ($shows as $show) {
            $Client = new Client\TV($show->tmdbid);
            $tvdbid = $Client->index()['external_ids']['tvdb_id'];

            $client = new \GuzzleHttp\Client(
                [
                    'http_errors' => false,
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Accept' => 'application/json',
                    ],
                    'query' => [
                        'cmd' => 'show',
                        'indexerid' => $tvdbid,
                    ],
                ]
            );
            $response = $client->request('get', env('SR_HOST') . 'api/' . env('SR_APIKEY'));
            $data = json_decode($response->getBody()->getContents(), true);

            if ($data['message'] == "Show not found") {
                $sr_show_exists = false;

                $client = new \GuzzleHttp\Client(
                    [
                        'http_errors' => false,
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                        ],
                        'query' => [
                            'cmd' => 'show.addnew',
                            'indexerid' => $tvdbid,
                            'tvdbid' => $tvdbid,
                            'initial' => `['fullhdtv', 'fullhdwebdl','fullhdbluray', 'udh4ktv', 'uhd4kbluray', 'udh4kwebdl', 'udh8ktv', 'uhd8kbluray', 'udh8kwebdl']`,
                            'tvdbid' => $tvdbid,
                            'location' =>  "/" . $show['type'],
                            'add_show_year' => 'true',
                            'flatten_folders' => 'true',
                            'anime' => $show['type'] == 'anime' ? true : false,
                        ],
                    ]
                );
                $response = $client->request('get', env('SR_HOST') . 'api/' . env('SR_APIKEY'));
                $data = json_decode($response->getBody()->getContents(), true);
                // sleep(30);

                // if($data['result'] == "success"){
                //     $sr_show_exists = true;
                // }
            }
            // else {
            //     $sr_show_exists = true;
            // }
        }
        foreach ($shows as $show) {

            $episodes = Episode::where('tv_id', $show->tmdbid)
                ->where('season_number', '!=', '00')
                ->orderBy('season_number', 'asc')
                ->orderBy('episode_number', 'asc')
                ->with('video_file')
                ->get();

            $episodeArray = '';
            $id = 2;
            $counter = 0;

            foreach ($episodes as $episode) {

                if ($episode->video_file['file'] == null) {
                    $counter++;
                }
            }

            foreach ($episodes as $episode) {

                if ($episode->video_file['file'] == null) {

                    $client = new \GuzzleHttp\Client(
                        [
                            'http_errors' => false,
                            'headers' => [
                                'Content-Type' => 'application/json',
                                'Accept' => 'application/json',
                            ],
                            'query' => [
                                'cmd' => 'episode',
                                'indexerid' => $tvdbid,
                                'season' =>  $episode['season_number'],
                                'episode' => $episode['episode_number'],
                            ],
                        ]
                    );

                    $response = $client->request('get', env('SR_HOST') . 'api/' . env('SR_APIKEY'));
                    $data = json_decode($response->getBody()->getContents(), true);

                    if ($sr_show_exists && $data['data']["status"] == "Skipped") {

                        $client = new \GuzzleHttp\Client(
                            [
                                'http_errors' => false,
                                'headers' => [
                                    'Content-Type' => 'application/json',
                                    'Accept' => 'application/json',
                                ],
                                'query' => [
                                    'cmd' => 'episode.setstatus',
                                    'status' => 'wanted',
                                    'indexerid' => $tvdbid,
                                    'season' =>  $episode['season_number'],
                                    'episode' => $episode['episode_number'],
                                ],
                            ]
                        );

                        $response = $client->request('get', env('SR_HOST') . 'api/' . env('SR_APIKEY'));
                        $data = json_decode($response->getBody()->getContents(), true);
                        $searching_episode = true;


                        $episodeArray .= (int) $episode['season_number'] . "x" . (int) $episode['episode_number'];
                        if ($id < $counter) {
                            $episodeArray .= '|';
                        }
                        $id++;
                    }
                    $setStatusURL[] = env('SR_HOST') . 'home/searchEpisode?show=' . $tvdbid . '&season=' . (int) $episode['season_number'] . '&episode=' . (int) $episode['episode_number'] . '&downCurQuality=0';
                } else {
                    $searching_episode = false;
                }
                $result[] = [
                    'searching' => $searching_episode,
                    'season' => $episode['season_number'],
                    'episode' => $episode['episode_number'],
                ];
            }
        }


        if (count($result) > 0) {
            $results[] = [
                'tvdbid' => $tvdbid,
                'Name' => $show['name'],
                'result' => $data['result'],
                'message' => $data['message'],
                'data' => $result,
            ];
        } else {
            $results[] = [
                'tvdbid' => $tvdbid,
                'Name' => $show['name'],
                'result' => $data['result'],
                'message' => 'No episodes missing',

            ];
        }

        return response()->json([
            'setStatusURL' => $setStatusURL,
            'results' => $results
        ]);
    }
*/
}

