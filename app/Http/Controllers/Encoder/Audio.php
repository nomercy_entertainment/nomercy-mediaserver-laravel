<?php

namespace App\Http\Controllers\Encoder;

class Audio
{
    public $ffprobe, $audio_streams;
    public $supported_audio, $lang_code, $lang_name, $lang_country;

    public function __construct($ffprobe, $supported_audio)
    {
        $this->ffprobe = $ffprobe;
        $this->audio_order = $supported_audio->audio_lang_code;

        $this->lang_code = $supported_audio->lang_code;
        $this->lang_name = $supported_audio->lang_name;
        $this->lang_country = $supported_audio->lang_country;

        $this->audio_streams = $this->streams();
    }

    public function streams()
    {
        $audio_streams = [];
        foreach ($this->ffprobe->streams as $stream) {
            if ($stream['codec_type'] == "audio") {
                $stream = (object) $stream;
                $audio_streams[] = $stream;
            }
        }
        return (object) $audio_streams;
    }

    public function order_streams_by_language()
    {
        $ordered = [];
        $not_ordered = [];
        foreach ($this->audio_order as $lang) {
            foreach ($this->audio_streams as $stream) {
                if (isset($stream->tags['language']) && $stream->tags['language'] == $lang) {
                    if (!isset(${$lang})) {
                        $ordered[$stream->index] = Utils::format_audio_stream($stream);
                        if ((isset($stream->tags->title) && $stream->tags->title != "Castilian") || !isset(${$lang})) {
                            ${$lang} = true;
                        }
                    }
                } else if (isset($stream->tags['language']) && $stream->tags['language'] == 'und') {
                    $not_ordered[$stream->index] = Utils::format_audio_stream($stream);
                    continue 2;
                } else if (!isset($stream->tags['language'])) {
                    $stream->tags['language'] = 'und';
                    $not_ordered[$stream->index] = Utils::format_audio_stream($stream);
                    continue 2;
                }
            }
        }

        if(count($ordered) == 0){
            return $not_ordered;
        }

        return $ordered;
    }

    public function count()
    {
        $count = 0;
        foreach ($this->audio_order as $lang) {
            foreach ($this->audio_streams as $stream) {
                if (isset($stream->tags['language']) && $stream->tags['language'] == $lang) {
                    if (!isset(${$lang})) {
                        $count++;
                        if ((isset($stream->tags->title) && $stream->tags->title != "Castilian") || !isset(${$lang})) {
                            ${$lang} = true;
                            continue 1;
                        }
                    }
                }
                else if (isset($stream->tags['language']) && ($stream->tags['language'] == 'und')) {
                    $count = 1;
                }
            }
        }
        if($count == 0){
            $count = 1;
        }

        return $count;
    }

    public function languages()
    {
        $all_audio_languages = [];
        foreach ($this->order_streams_by_language() as $stream) {
            if (isset($stream->language)) {
                if (isset($stream->title) && $stream->title == "Castilian") {
                    // $all_audio_languages[] = "Castilian";
                    $all_audio_languages[] = "cas";
                } else {
                    // $all_audio_languages[] = str_replace($this->lang_code, $this->lang_name, $stream->language);
                    $all_audio_languages[] = $stream->language;
                }
            } else if (isset($stream->language) && $stream->title == "und") {
                $all_audio_languages[] = 'und';
                continue 1;
            } else {
                $all_audio_languages[] = 'und';
                continue 1;
            }
        }

        $all_audio_languages = array_unique($all_audio_languages);
        asort($all_audio_languages);

        return array_values($all_audio_languages);
    }

    public function get()
    {
        $array = [
            "count" => $this->count(),
            "languages" => $this->languages(),
            "streams" => $this->order_streams_by_language(),
        ];

        return $array;
    }
}
