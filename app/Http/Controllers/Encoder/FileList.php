<?php

namespace App\Http\Controllers\Encoder;

use DirectoryIterator;
use FilesystemIterator;
use RecursiveDirectoryIterator;

class FileList
{
    private $extention, $filter;

    public function filter(array $filters)
    {
        $f = ' -not -path "*/video*" -not -path "*/audio*"  -not -path "*/subtitles"  -not -path "*/thumbs"  -not -name "*thumb*"  ';
        foreach ($filters as $file) {
            $f .= ' -not -name "*' . $file . '*" ';
        }
        $this->filter = $f;
    }

    public function getDirContents($dir, &$results = array())
    {
        $files = scandir($dir);

        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = $path;
            } else if ($value != "." && $value != "..") {
                $this->getDirContents($path, $results);
                $results[] = $path;
            }
        }

        return $results;
    }

    public function scan($directory, $recusion = 1)
    {
        $retval = [];
        $files = [];

        if (!is_array($directory)) {
            $directory = [$directory];
        }

        foreach ($directory as $dir) {
            $dir = $dir['folder'];
            if (is_dir($dir)) {
                if ($recusion) {
                    foreach (new RecursiveDirectoryIterator($dir, FilesystemIterator::SKIP_DOTS) as $item) {
                        if ($item->isDir()) {
                            foreach (new RecursiveDirectoryIterator($item, FilesystemIterator::SKIP_DOTS) as $item2) {
                                if (Utils::contains($item2->getRealPath(), array('mkv', 'mp4', 'm3u8', 'avi', 'webm', 'ogv', 'mov')) !== false) {
                                    $files[] = $item2->getRealPath();
                                }
                            }
                            if (Utils::contains($item->getRealPath(), array('mkv', 'mp4', 'm3u8', 'avi', 'webm', 'ogv', 'mov')) !== false) {
                                $files[] = $item->getRealPath();
                            }
                        } else {
                            if (Utils::contains($item->getRealPath(), array('mkv', 'mp4', 'm3u8', 'avi', 'webm', 'ogv', 'mov')) !== false) {
                                $files[] = $item->getRealPath();
                            }
                        }
                    }

                    foreach ($this->getDirContents($dir) as $item) {
                        if (Utils::contains($item, array('mkv', 'mp4', 'm3u8', 'avi', 'webm', 'ogv', 'mov')) !== false) {
                            $files[] = $item;
                        }
                    }
                } else {
                    foreach (new DirectoryIterator($dir) as $item) {
                        if (!$item->isDot() && ($item->isDir() || $item->isFile())) {
                            $files[] = $item->getRealPath();
                        }
                    }
                }
            }
        }
        $files = array_unique($files);
        sort($files);
        // dump($files);
        return $files;
    }

}
