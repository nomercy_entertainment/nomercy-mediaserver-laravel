<?php

namespace App\Http\Controllers\Encoder;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Progress extends Controller
{

    public function convertToSeconds($time)
    {
        $str_time = preg_replace("/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $time);
        sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
        $time_seconds = $hours * 3600 + $minutes * 60 + $seconds;
        return $time_seconds;
    }

    public function progress()
    {
        $files = Storage::disk('public')->files('working/active');
        $array = [];

        foreach ($files as $file) {

            $re = '/(?<id>\d*).json$/m';
            preg_match($re, $file, $matches);
            $id = $matches['id'];

            if (Storage::disk('public')->exists('working/encoding/' . $id . '.txt') && Storage::disk('public')->exists('working/active/' . $id . '.json')) {
                $progressFile = Storage::disk('public')->get('working/encoding/' . $id . '.txt');
                $queueFile = json_decode(Storage::disk('public')->get('working/active/' . $id . '.json'), true);

                // $speedRegex = '/.*speed=(\d*.\d*.)(?!.*\d.\d)x.*([\S\s])$/';
                // $currentRegex = '/.*time=(\d{2}:\d{2}:\d{2})\.\d* bitrate.*$/';
                $speedRegex = '/speed=(\d*.\d*.)(?!.*\d.\d)x/s';
                $currentRegex = '/.*out_time=(\d{2}:\d{2}:\d{2})\.\d*/s';
                $speed = null;
                $currentTime = null;
                $duration = $queueFile['duration'];
                $remaining = null;
                $remainingHMS = null;

                preg_match($speedRegex, $progressFile, $speedMatch);
                if ($speedMatch && isset($speedMatch[1])) {
                    $speed = $speedMatch[1];
                }
                preg_match($currentRegex, $progressFile, $currentTimeMatch);
                if ($currentTimeMatch && isset($currentTimeMatch[1])) {
                    $currentTime = $currentTimeMatch[1];
                }

                if ($currentTime != null  && $duration != null && $speed != null) {
                    $remaining = floor((($this->convertToSeconds($duration) + 0) - $this->convertToSeconds($currentTime)) /  $speed);
                }

                if ($remaining != null) {
                    $remainingHMS = sprintf('%02d:%02d:%02d:%02d', ($remaining / 86400), (($remaining - 86400) / 3600), ($remaining / 60 % 60), $remaining % 60);
                }

                if ($currentTime != null  && $duration != null) {
                    $percent = floor(($this->convertToSeconds($currentTime) / $this->convertToSeconds($duration)) * 100);
                }

                $days = $remaining >= 86400 ? sprintf("%02d", floor($remaining / 86400)) . 'd' : '';

                $hours = floor(($remaining - (86400 * floor($remaining / 86400))) / 3600) >= 1 ? sprintf("%02d", floor(($remaining  - (86400 * floor($remaining / 86400)))  / 3600)) . 'h' : '';

                $minutes = (floor($remaining / 60 % 60) >= 1 || floor($remaining / 3600) == 1) ? sprintf("%02d", floor($remaining / 60 % 60)) . 'm' : '';
                $seconds = ($minutes | $hours | $days) ? sprintf("%02d", floor($remaining % 60)) . 's' : '';

                $imgNumber = sprintf("%04d", floor(($this->convertToSeconds($currentTime) - 10) / 10));
                $image = '';
                if (file_exists(str_replace('/Media', env('MEDIA_ROOT'), str_replace("*", $imgNumber, $queueFile['img'] ?? null)))) {
                    $image = env('WORKER_URL') . str_replace("*", $imgNumber, $queueFile['img'] ?? null);
                }

                $re = '/(?<dir>.*\/)/m';

                // preg_match($re, $queueFile['img'], $match);

                // $images = Storage::disk('media')->files($match['dir']);
                // return $images;

                $array[] = [
                    "workerName" => env('WORKER_NAME'),
                    "workerURL" => env('WORKER_URL'),
                    "external" => env('EXTERNAL', false),
                    "title" => preg_replace('/.*\//', '', $queueFile['title']),
                    "log" => $queueFile['log'],
                    "img" => str_replace(env('MEDIA_ROOT'), '/Media', $image),
                    "speed" => $speed,
                    "duration" => $this->convertToSeconds($duration),
                    "durationHMS" => $duration,
                    "currentTime" => $this->convertToSeconds($currentTime),
                    "currentTimeHMS" => $currentTime,
                    "remaining" => $remaining,
                    "remainingDHMS" => $remainingHMS,
                    "percent" => $percent ?? null,
                    "days" => $days,
                    "hours" => $hours,
                    "minutes" => $minutes,
                    "seconds" => $seconds,
                ];
            } else {
                return [
                    "workerName" => env('WORKER_NAME'),
                    "disabled" => "true"
                ];
            }
        }
        if (count($files) > 0) {
            return $array;
        } else {
            return [
                "workerName" => env('WORKER_NAME'),
                "disabled" => "true"
            ];
        }
    }

    public function queue($limit = 30)
    {
        if ($limit == 'NaN') {
            $limit = 30;
        }
        $output = [];
        $queue = DB::table('jobs')
            ->where('queue', 'like', '%encoder')
            ->where('attempts', '0')
            ->limit($limit)
            ->get();

        $count = DB::table('jobs')
            ->where('queue', 'like', '%encoder')
            ->where('attempts', '0')
            ->count();

        // dd($queue);
        foreach ($queue as $item) {

            $jsonpayload = json_decode($item->payload);
            $payload = unserialize($jsonpayload->data->command);

            if (isset($payload->fileIn) && isset($payload->encode_log[0])) {
                $file = $payload->fileIn . ' -> ' . implode(', ', $payload->encode_log[0]);
            } else if (isset($payload->fileName) && isset($payload->encode_log[0])) {
                $file = $payload->fileName . ' -> ' . implode(', ', $payload->encode_log[0]);
            } else if (isset($payload->title) && isset($payload->encode_log[0])) {
                $file = $payload->title . ' -> ' . implode(', ', $payload->encode_log[0]);
            } else {
                $file = $payload->verify ?? '';
            }

            $output[] = [
                "id" => $item->id,
                "file" => $file,
                "workerName" => env('WORKER_NAME'),
                "serie" => preg_replace('/.*\//', '', $file),
                "tmdbid" => $payload->tmdbid ?? null,
                "mediatype" => $payload->dbtype ?? '',
                "queue" => $item->queue,
            ];
        }
        return [
            "output" => $output,
            "count" => $count,
        ];
    }
}
