<?php

namespace App\Http\Controllers\Encoder;

use DateTime;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class NameController extends Controller
{
    public $data;

    public function __construct($data)
    {

        $this->data = $data;
    }


    public function make_movie_file_name()
    {
        $year = Carbon::createFromDate($this->data['release_date'])->format('Y');

        $title = $this->data['title'];
        $str1 = [". ", ", ", ": ", "; ", " ! ", "!  ", "! ", "  ", " ", "!", "?", "/", "\\",   "&", "..", ".....", "....", "...", "'", "’", "?", "Ã¯Â¿Â½", "-.", ".(1)", ".(2)", ".(3)", ".(4)", "(", ")", "â€™", "*", "&#39;", ",", "\"", "'", ":"];
        $str2 = [".",   ".",  ".",  ".",   ".",   ".",  ".",  ".", ".",  "",  "", ".",   "", "and",   "",      "",     "",    "",  "",  "",  "",       "",   "",     "",     "",     "",     "",   "", "",    "", "-",     "'",  "",   "",  "", "."];
        $title = str_replace($str1, $str2, $title);


        $fileName = $title . ".(" . $year . ").NoMercy";
        $folder = $title . ".(" . $year . ")";

        return [
            "name" => $fileName,
            "folder" => $folder,
            "title" => $title,
            "year" => $year,
        ];
    }

    public function make_tv_file_name($episode)
    {
        $year = Carbon::createFromDate($this->data['first_air_date'])->format('Y');
        $Season = sprintf("%02d", $episode['season_number']);
        $Episode = sprintf("%02d", $episode['episode_number']);

        $series_name = $this->data['title'];

        $str1 = [". ", " ", "?",   "&", "....", "...", "..",   "#", "'", ":", ",", "?", "Ã¯Â¿Â½", ";", "(", ")", "-.", "/", "⁄", "\"", "[", "]"];
        $str2 = [".",  ".",  "", "and",    ".",   ".",  ".", "%23",  "", "",  "",  "",       "",   "",  "",  "",   "", ".", ".",   "",  "",  ""];
        $SeriesName = preg_replace("/\.$/", "", str_replace($str1, $str2, $series_name));

        $str1 = [". ", " ", "!", "?",   "&", "....", "...", "..",   "#", "'", ":", ",", "?", "Ã¯Â¿Â½", ";", "", "(", ")", "-.", "/", "’", "⁄", "\"", "`"];
        $str2 = [".",  ".",  "",  "", "and",    ".",   ".",  ".", "%23",  "", "",  "",  "",       "",   "", "",  "",  "",   "", ".",  "", ".",   "", ""];
        $SeriesName2 = preg_replace("/\.$/", "", str_replace($str1, $str2, $series_name));

        // $SeriesName2 = preg_replace("/\.*/", '.', $SeriesName2);

        $title = rtrim($episode['title']);
        $str1 = [". ", ", ", ": ", "; ", " ! ", "!  ", "! ", "  ", " ", "!", "?", "/", "\\",   "&", "..", ".....", "....", "...", "'", "’", "?", "Ã¯Â¿Â½", "-.", ".(1)", ".(2)", ".(3)", ".(4)", "(", ")", "â€™", "*", "&#39;", ",", "\"", "'", ":", "`"];
        $str2 = [".",   ".",  ".",  ".",   ".",   ".",  ".",  ".", ".",  "",  "", ".",   "", "and",   "",      "",     "",    "",  "",  "",  "",       "",   "",     "",     "",     "",     "",   "", "",    "", "-",     "'",  "",   "",  "", ".", ""];
        $title = str_replace($str1, $str2, $title);

        $fileName = $SeriesName2 . ".S" . $Season . "E" . $Episode . "." . $title . ".NoMercy";
        $episodeFolder = $SeriesName2 . ".S" . $Season . "E" . $Episode;

        $folder = $SeriesName . ".(" . $year . ")";

        return [
            "folder" => $folder,
            "episode_folder" => $episodeFolder,
            "series_name" => $series_name,
            "name" => $fileName,
            "title" => $title,
            "year" => $year,
            "Season" => $Season,
            "Episode" => $Episode,
        ];
    }
}
