<?php

namespace App\Http\Controllers\Encoder;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MigrationController extends Controller
{

    public function is_dir_empty($dir)
    {
        if (!is_readable($dir)) return NULL;
        return (count(scandir($dir)) == 2);
    }

    public function rename_video()
    {
        $FileController = new FileController();

        $files = $FileController->get_movie_files();
        $files = array_merge($FileController->get_tv_files(), $files);
        $files = array_reverse($files);

        foreach ($files as $file) {

            if (contains($file, ['video.m3u8']) != false) {
                $folder = str_replace('video.m3u8', '', $file);
                $newfolder = str_replace('/video/video.m3u8', '/video_1920x1080/', $file);

                if (is_dir($folder) && (!is_dir($newfolder) || (is_dir($newfolder) && $this->is_dir_empty($newfolder)))) {

                    $file = $newfolder . 'video.m3u8';
                    $newfile = $newfolder . 'video_1920x1080.m3u8';

                    dump('renaming: ' . $folder . ' to ' . $newfolder);

                    rename($folder, $newfolder);
                    $cmd2 = 'mv "' . $file . '"  "' . $newfile . '"';
                    shell_exec($cmd2);
                } else {
                    dump('file: ' . $file . ' already processed');
                }
            }
        }
    }
}
