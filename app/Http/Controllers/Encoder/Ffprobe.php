<?php

namespace App\Http\Controllers\Encoder;

class Ffprobe
{
    public $fileIn, $ffprobe, $supported_audio, $video, $audio, $subtitle, $chapters;

    public function __construct($fileIn, $supported_audio)
    {
        $this->supported_audio = (object) $supported_audio;

        $command = 'ffprobe -v quiet -show_format -show_streams -show_chapters -print_format json "' . str_replace(['$'], ['\$'], $fileIn) . '"';
        $this->ffprobe = json_decode(shell_exec($command), true);

        if (!isset($this->ffprobe['streams']) || count($this->ffprobe['streams']) == 0 || $this->ffprobe == null) {
            dump("Input file does not exist: " . $fileIn . ' ' . PHP_EOL);
            return 1;
        }

        $command2 = 'ffmpeg -ss 240 -i "' . $fileIn . '" -max_muxing_queue_size 999 -vframes 10 -vf cropdetect -f null -  2>&1 ';
        $crop_data = shell_exec($command2);
        $re = '/crop=\d{3,4}:\d{3,4}:\d{1,3}:\d{1,3}/m';
        preg_match($re, $crop_data, $crop);
        // $crop = [];

        $this->ffprobe = (object) $this->ffprobe;
        $this->ffprobe->streams = (object) $this->ffprobe->streams;
        $this->ffprobe->chapters = (object) $this->ffprobe->chapters;
        $this->ffprobe->format = (object) $this->ffprobe->format;
        $this->video = (object) (new Video((object) $this->ffprobe, $this->supported_audio, $fileIn, $crop))->get();
        $this->audio = (object) (new Audio((object) $this->ffprobe, $this->supported_audio))->get();
        $this->subtitle = (object) (new Subtitle((object) $this->ffprobe, $this->supported_audio))->get();
        $this->chapters = (new Chapter($this->ffprobe, $this->supported_audio))->get();
    }

    public function get()
    {
        $array = [
            "video" => (object) $this->video,
            "audio" => (object) $this->audio,
            "subtitle" => (object) $this->subtitle,
            "chapters" => (object) $this->chapters,
        ];

        return (object) $array;
    }
}
