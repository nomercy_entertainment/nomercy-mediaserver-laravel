<?php

namespace App\Http\Controllers\Encoder;

use Illuminate\Support\Facades\Storage;

class VerifyMeta
{
    public static function verify_playlist($showFolder, $tag, $delete = false)
    {
        $directoryLocation = str_replace(env("MEDIA_ROOT") . "/", "", $showFolder);
        $directorysDL = Storage::disk('media')->directories($directoryLocation);

        foreach ($directorysDL as $dir) {
            $filesDL = Storage::disk('media')->files($dir);
            foreach ($filesDL as $file) {

                $reg = '/(?<folder>.*)\//m';
                preg_match($reg, $file, $matches);


                if ((strpos($file, '-0.ts') !== false || strpos($file, '-0000.ts') !== false) && strpos($file, 'video') !== false) {

                    $command = 'ffprobe -v quiet -show_format -show_streams -show_chapters -print_format json "' . str_replace(['$'], ['\$'], env("MEDIA_ROOT") . "/" . $file) . '"';
                    $level = json_decode(shell_exec($command), true)['streams'][0]['level'] ?? 0;
                    if($level == 40){

                    }
                    else if($level > 40){
                        if ($delete == true ) {
                            Storage::disk('media')->deleteDirectory($dir);
                            // dump('Deleting: ' .  $dir);
                            return $file . " Needs re-encoding, Deleting directory";
                        } else {
                            // dump('Not deleting: ' . $dir . ' because there is no source file');
                            return $file . " Needs re-encoding, Not deleting because there is no source file";
                        }
                    }
                }

                if (strpos($file, $tag . '.m3u8') !== false) {
                    $file_content = Storage::disk('media')->get($file);
                    $re = '/(?<file>(' . $tag . '-\d{1,5}\.\w{2,3}))\n#EXT-X-ENDLIST/';

                    preg_match($re, $file_content, $matcheOut);
                    if ($matcheOut) {
                        $playlist_file = str_replace("\n", "", $dir . "/" . $matcheOut['file']);
                        if (Storage::disk('media')->exists($playlist_file)) {
                            return $file . " Is correctly processed";
                        }
                        else if ($delete == true) {
                            Storage::disk('media')->deleteDirectory($dir);
                            // dump('Deleting: ' .  $dir);
                            return $file . " Is incomplete, Deleting directory";
                        }
                        else {
                            dump('Not deleting: ' . $dir . ' because there is no source file');
                            return $file . " Is fautly, Not deleting: ' . $file . ' because there is no source file";
                        }
                    }
                    else if ($delete == true) {
                        Storage::disk('media')->deleteDirectory($dir);
                        // dump('Deleting: ' .  $dir);
                        return $file . " Is fautly, Deleting directory";
                    } else {
                        // dump('Not deleting: ' . $dir . ' because there is no source file');
                        return $file . " Is fautly, Not deleting because there is no source file";
                    }
                }
            }
        }
    }

    public static function verify_mp4($file, $delete)
    {
        $input_ffprobe = json_decode(shell_exec("ffprobe -v quiet -show_format -show_streams -show_chapters -print_format json '" . addslashes($file) . "'"), true);
        $file2 = str_replace(env("MEDIA_ROOT") . "/", "", $file);

        if (!empty($input_ffprobe) && isset($input_ffprobe['format']['duration']) && isset($input_ffprobe['streams']) && count($input_ffprobe['streams']) > 0) {
            return $file2 . " Is correctly processed";
        } else if ($delete == true) {
            Storage::disk('media')->delete($file2);
            return $file2 . " Is fautly, Deleting it";
        } else {
            // dump('Not deleting: ' . $file2 . ' because there is no source file');
            return $file2 . " Is fautly, Not deleting because there is no source file";
        }

        return $file2 . "File does not exist";
    }
}
