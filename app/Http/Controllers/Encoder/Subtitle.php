<?php

namespace App\Http\Controllers\Encoder;

class Subtitle
{
    public $ffprobe, $audio_streams;
    public $supported_audio, $lang_code, $lang_name, $lang_country;

    public function __construct($ffprobe, $supported_audio)
    {
        $this->ffprobe = $ffprobe;
        $this->audio_order = $supported_audio->audio_lang_code;

        $this->lang_code = $supported_audio->lang_code;
        $this->lang_name = $supported_audio->lang_name;
        $this->lang_country = $supported_audio->lang_country;

        $this->subtitle_streams = $this->streams();
    }

    public function streams()
    {

        $subtitle_streams = [];
        foreach ($this->ffprobe->streams as $stream) {
            if ($stream['codec_type'] == "subtitle" && $stream['codec_name'] != 'hdmv_pgs_subtitle') {
                $stream = (object) $stream;
                $stream->tags = isset($stream->tags) ? (object) $stream->tags : null;
                $subtitle_streams[] = $stream;
            }
        }
        return $subtitle_streams;
    }

    public function order_streams_by_language()
    {
        $ordered = [];
        $not_ordered = [];

        foreach ($this->audio_order as $lang) {
            foreach ($this->subtitle_streams as $stream) {

                if (isset($stream->tags->title)) {

                    if (Utils::contains($stream->tags->title, ['full', 'Full', 'FULL']) !== false) {
                        $type = 'full';
                    } elseif (Utils::contains($stream->tags->title, ['sign', 'Sign', 'SIGN', 'Forced']) !== false) {
                        $type = 'sign';
                    } elseif (Utils::contains($stream->tags->title, ['sdh', 'Sdh', 'SDH']) !== false) {
                        $type = 'sdh';
                    } else {
                        $type = 'full';
                    }
                } else {
                    $type = 'full';
                }

                if (isset($stream->tags->language) && $stream->tags->language == $lang) {
                    if (!isset(${$lang . $type})) {
                        $ordered[] = Utils::format_subtitle_stream($stream, $type);
                        if (isset($stream->tags->title) && $stream->tags->title != "Castilian") {
                            ${$lang . $type} = true;
                        }
                    }
                } else {
                    $not_ordered[] = Utils::format_subtitle_stream($stream, $type);
                }
            }
        }

        return $ordered ?? $not_ordered;
    }

    public function count()
    {
        $count = 0;
        foreach ($this->audio_order as $lang) {
            foreach ($this->subtitle_streams as $stream) {
                if (isset($stream->tags->language) && $stream->tags->language == $lang) {
                    if (!isset(${$lang})) {
                        $count++;
                        if (isset($stream->tags->language) && $stream->tags->language != 'spa') {
                            ${$lang} = true;
                        }
                    }
                } else {
                    $count = count($this->subtitle_streams);
                }
            }
        }

        return $count;
    }

    public function languages()
    {
        $all_subtitle_languages = [];
        foreach ($this->subtitle_streams as $stream) {
            if (isset($stream->tags->language)) {
                if (isset($stream->title) && $stream->title == "Castilian") {
                    // $all_subtitle_languages[] = "Castilian";
                    $all_subtitle_languages[] = "cas";
                } else {
                    // $all_subtitle_languages[] = str_replace($this->lang_code, $this->lang_name, $stream->tags->language);
                    $all_subtitle_languages[] = $stream->tags->language;
                }
            } else {
                $all_subtitle_languages[] = 'und';
            }
        }
        $all_subtitle_languages = array_unique($all_subtitle_languages);
        asort($all_subtitle_languages);

        return array_values($all_subtitle_languages);
    }

    public function get()
    {
        $array = [
            "count" => $this->count(),
            "languages" => $this->languages(),
            "streams" => $this->order_streams_by_language(),
        ];

        return $array;
    }
}
