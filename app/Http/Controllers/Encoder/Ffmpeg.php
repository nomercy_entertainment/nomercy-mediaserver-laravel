<?php

namespace App\Http\Controllers\Encoder;

set_time_limit(0);

use App\Models\FolderRoots;
use App\Jobs\ProcessEncodeCommand;
use App\Models\SupportedLanguages;
use App\Models\VideoFile;
use DirectoryIterator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use NoMercy\OpenSubtitles\App\Client;
use NoMercy\OpenSubtitles\App\Hash;

// use NoMercy\OpenSubtitles\App\Client;
// use NoMercy\OpenSubtitles\App\Hash;

class Ffmpeg
{
    public $supported_audio, $ffprobe, $all_subtitle_languages, $db_index_number, $imdbid, $dbtype;
    public $outputFolder, $fileName, $fileName2, $encode_log, $split, $video, $audio, $hq, $defaults, $segment, $duration, $qualities, $languages, $hdr;
    public $index, $delete, $input, $firstAudio, $firstVideo;


    public function __construct($fileIn, $outputFolder, $fileName, $db_index_number, $type, $dbtype)
    {

        $this->fileIn = $fileIn;
        $this->outputFolder = $outputFolder;
        // $this->imdbid = str_replace('tt', '', $imdbid);

        $this->type = $type;
        $this->dbtype = $dbtype;

        $this->fileName = substr(preg_replace('/\.\./', '.', rtrim($fileName, ".")), 0, 100);

        $this->db_index_number = $db_index_number;
        $this->languages = languages();

        if (!is_dir($this->outputFolder)) {
            mkdir($this->outputFolder, 0777, true);
        }

        $supported_audio = [];
        foreach (SupportedLanguages::select('language_code', 'language_name', 'country_code')->orderBy('audio_order', 'asc')->get() as $audio_code) {
            $supported_audio["audio_lang_code"][] = $audio_code->language_code;
            $supported_audio["audio_lang_name"][] = $audio_code->language_name;
            $supported_audio["audio_lang_country"][] = $audio_code->country_code;
        }

        foreach (SupportedLanguages::select('language_code', 'language_name', 'country_code')->orderBy('language_name', 'asc')->get() as $code) {
            $supported_audio["lang_code"][] = $code->language_code;
            $supported_audio["lang_name"][] = $code->language_name;
            $supported_audio["lang_country"][] = $code->country_code;
        }

        $supported_audio = (object) $supported_audio;
        $this->lang_code = $supported_audio->lang_code;
        $this->lang_name = $supported_audio->lang_name;
        $this->lang_country = $supported_audio->lang_country;

        $this->ffprobe = (object) (new Ffprobe($this->fileIn, $supported_audio))->get();

        if ($this->ffprobe == [] || $this->ffprobe == '' || $this->ffprobe == null || !isset($this->ffprobe->video->streams) || count($this->ffprobe->video->streams) == 0) {
           dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . "Input file does not exist: " . $fileIn);
            return 1;
        }

        $this->firstVideo = collect($this->ffprobe->video->streams)->first();
        $this->firstAudio = collect($this->ffprobe->audio->streams)->first();

        $re = '/(?<root>[\/\w]*\/).*/m';
        preg_match($re, $this->fileIn, $matches);
        $this->hq = FolderRoots::where('folder', $matches['root'])->first()->hq ?? 0;

        $segment = [
            '-async 1',
            '-muxdelay 0',
            '-f segment',
            '-segment_time 10',
            '-hls_time 10',
            '-segment_format mpegts',
            '-segment_list_type m3u8',
            '-segment_list_size 0',
            '-segment_time_delta 0.1',
        ];

        $this->segment = implode(' ', $segment);

        $defaults = [
            // '-report',
            '-profile:v baseline -level 3.0',
            '-pix_fmt yuv420p',
            '-tune film',
            '-sc_threshold 0',
            '-force_key_frames "expr:if(isnan(prev_forced_t),gte(t,10),gte(t,prev_forced_t+10))"',
            '-x264opts "keyint=48:min-keyint=48:no-scenecut"',
            '-movflags +faststart',
            '-max_muxing_queue_size 9999',
        ];
        $this->defaults = implode(' ', $defaults);

        $this->encode_log = [];

        $hdr = collect($this->ffprobe->video->streams)->first()->hdr ?? '';
        $this->hdr = $hdr != '' ? true : false;

        $this->split = false;
        $this->index = 0;

        $this->delete = false;

        if (Utils::contains($this->fileIn, array('mkv')) !== false && Utils::contains($this->fileIn, array('/Download')) === true) {
            $this->delete = true;
        }

        $this->encode_log = [];
        $this->encode_log['verify'] = [];
    }

    public function create_video_command2()
    {

        $list = [];
        $array = [];
        $item = [];

        $video = $this->ffprobe->video;
        $audio = $this->ffprobe->audio;

        if (!isset($audio->streams) && !isset($video->streams)) {
           dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'No streams to process this file: ' . $this->fileIn);

            return [
                "response" => [],
            ];
        }

        if (isset($audio->streams) && isset(collect($audio->streams)->first()->channels)) {
            if (collect($audio->streams)->first()->channels > 6) {
                $ac = '-ac 6 -strict 1';
            } else {
                $ac = '-ac ' . collect($audio->streams)->first()->channels . ' -strict 1';
            }
        } else {
            $ac = '';
        }

        $hdr = collect($video->streams)->first()->hdr ?? '';

        $crop = (isset($this->firstVideo->crop) && $this->firstVideo->crop != '') ? $this->firstVideo->crop : '';

        if (isset($this->firstVideo->width) && $this->firstVideo->width < 2000 &&  $audio->count == 1 && $video->count == 1 && !Storage::disk('media')->exists(str_replace('/Media', env('MEDIA_ROOT'), $this->outputFolder) . '/multiaudio.txt')) {
            $this->split = false;
        } else {
            $this->split = true;
        }


        $this->fileName2 = $this->fileName;

        $oldNotFound = true;
        $re = '/(?<folder>.*\/)(?<fileName>(?<episode>.*\.S\d{2,3}E\d{2,3})(?<rest>.*))\.mp4/m';

        $folders = Storage::disk('root')->files($this->outputFolder);
        foreach($folders as $folder){
            preg_match($re, $folder, $match);
            if($match){
                $oldNotFound = false;

                $this->fileName2 = $match['fileName'];
               dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'fileName2: ' . $this->fileName2);
            }
        }


        foreach ($video->streams as $stream) {
            $vf = '';
            if ($crop != '' && $hdr != '') {
                $vf =  '-vf ' . $stream->crop . ',scale=' . $stream->width . ':' . $stream->height . ',' . $hdr;
                $this->encode_log[1][] = 'HDR to SDR, scaling and cropping: ' . $stream->crop;
            } else if ($crop == '' && $hdr != '') {
                $vf =  '-vf scale=' . $stream->width . ':' . $stream->height . ',' . $hdr;
                $this->encode_log[1][] = 'HDR to SDR and scaling';
            } else if (($crop == '' && $hdr == '') || $video->count > 1) {
                $vf = ' -vf scale=' . $stream->width . ':' . $stream->height;
                $this->encode_log[1][] = 'Scaling';
            }

            $bv = '';
            if(isset($stream->rate)){
                $bv = ' -b:v ' . $stream->rate;
            }

            if ($this->split) {

                $dir = $this->outputFolder . 'video_' . $stream->width . 'x' . $stream->height;

                if (file_exists($this->outputFolder . 'video/video.m3u8') && !file_exists($dir . '/video_' . $stream->width . 'x' . $stream->height . '.m3u8') && $stream->width < 2000 && $stream->width > 360) {
                    shell_exec('mv ' . $this->outputFolder . 'video/* ' . $dir);
                    shell_exec('mv ' . $this->outputFolder . 'video/video.m3u8 ' . $dir . '/video_' . $stream->width . 'x' . $stream->height . '.m3u8');
                }

                if (file_exists($dir . '/video_' . $stream->width . 'x' . $stream->height . '.m3u8')) {
                    $this->encode_log['verify'][] = VerifyMeta::verify_playlist($this->outputFolder, 'video_' . $stream->width . 'x' . $stream->height, $this->delete);
                }

                if (!is_dir($dir)) {
                    mkdir($dir, 0777, true);
                }

                $item = '';

                if (!file_exists($dir . '/video_' . $stream->width . 'x' . $stream->height . '.m3u8') && $oldNotFound) {
                    $first_audio = collect($audio->streams)->isEmpty() != true ? $this->firstAudio->language : 'und';
                    $lang_title =  str_replace($this->lang_code, $this->lang_name, collect($audio->streams)->isEmpty() != true ? $this->firstAudio->language : 'Unknown');

                    $array = [
                        '-c:v libx264',
                        '-map 0:' . $this->firstVideo->index ?? ':v:0',
                        $bv,
                        $stream->crf,
                        $vf,
                        $this->defaults,
                        '-c:a aac',
                        $ac,
                        count(collect($audio->streams)) > 0 ?  '-map 0:' . $this->firstAudio->index : '-map a:0',
                        '-metadata:s:a:0 language="' . $first_audio . '"',
                        '-metadata:s:a:0 title="' . $lang_title . '"',

                        $this->segment,
                        '-segment_list "' . $this->outputFolder . 'video_' . $stream->width . 'x' . $stream->height . '/video_' . $stream->width . 'x' . $stream->height . '.m3u8"',
                        '"' . $this->outputFolder . 'video_' . $stream->width . 'x' . $stream->height . '/video_' . $stream->width . 'x' . $stream->height . '-%04d.ts"',
                    ];
                    $item = implode(' ', $array);

                    $this->encode_log[$this->index][] = 'video_' . $stream->width . 'x' . $stream->height . ': 0:' . $this->firstVideo->index ?? '0' . '-0:' . $this->firstAudio->index ?? '0' . ': ' . $this->firstAudio->language;
                }
            }
            else {

                $dir = $this->outputFolder;
                if (!is_dir($dir)) {
                    mkdir($dir, 0777, true);
                } else if (file_exists($this->outputFolder . $this->fileName .  ".mp4")) {
                    $this->encode_log['verify'][] = VerifyMeta::verify_mp4($this->outputFolder . $this->fileName .  ".mp4", $this->delete);
                }

                if (!file_exists($this->outputFolder . $this->fileName .  ".mp4") && $oldNotFound) {

                    $first_audio = collect($audio->streams)->isEmpty() != true ? $this->firstAudio->language : 'und';
                    $lang_title =  str_replace($this->lang_code, $this->lang_name, collect($audio->streams)->isEmpty() != true ? $this->firstAudio->language : 'Unknown');

                    $array = [
                        '-c:v libx264',
                        '-map 0:' . $this->firstVideo->index ?? 'a:0',
                        $bv,
                        $stream->crf,
                        $vf,
                        $this->defaults,
                        '-c:a aac',
                        $ac,
                        count(collect($audio->streams)) > 0 ?  '-map 0:' . $this->firstAudio->index : '-map a:0',
                        '-metadata:s:a:0 language="' . $first_audio . '"',
                        '-metadata:s:a:0 title="' . $lang_title . '"',

                        '"' . $this->outputFolder .  $this->fileName . '.mp4"',
                    ];
                    $item = implode(' ', $array);

                    $this->encode_log[0][] = 'video_' . $stream->width . 'x' . $stream->height . ': 0:' . $this->firstVideo->index ?? '0' . '-0:' . $this->firstAudio->index . ': ' . $this->firstAudio->language;
                }
            }

            if ($item != '') {
                $list[] = $item;
            }
            $hdr = '';
            $crop = '';
        }

        return [
            "response" => $list,
        ];
    }

    public function create_audio_command()
    {
        $response = [];
        $array = [];
        $audio = $this->ffprobe->audio;
        $id = 0;

        if ($this->split && (isset($this->ffprobe->audio->streams) && count($this->ffprobe->audio->streams) > 1) || (isset($this->ffprobe->video->streams) && count($this->ffprobe->video->streams) > 1) || ($this->hdr == true && $this->ffprobe->video->count == 1 && $this->ffprobe->audio->count == 1)) {
            foreach ($audio->streams as $stream) {

                $dir = $this->outputFolder . 'audio_' . $stream->language;

                if (!is_dir($dir)) {
                    mkdir($dir, 0777, true);
                } else if (file_exists($dir . "/audio_" . $stream->language . ".m3u8")) {
                    $this->encode_log['verify'][] = VerifyMeta::verify_playlist($this->outputFolder, "audio_" .  $stream->language, $this->delete);
                }

                $ac = $stream->channels > 6 ? '-ac 6 -strict 1' : '-ac ' . $stream->channels . ' -strict 1';

                if (!file_exists($dir . "/audio_" . $stream->language . ".m3u8")) {
                    $array = [
                        '-c:a aac',
                        '-map 0:' . $stream->index,
                        $ac,
                        $this->defaults,
                        '-metadata:s:a:' . $id . ' language="' . $stream->language . '"',
                        '-metadata:s:a:' . $id . ' title="' . str_replace($this->lang_code, $this->lang_name, $stream->language) . '"',
                        $this->segment,
                        '-segment_list "' . $this->outputFolder . 'audio_' . $stream->language . '/audio_' . $stream->language . '.m3u8"',
                        '"' . $this->outputFolder . 'audio_' . $stream->language . '/audio_' . $stream->language . '-%04d.ts"',
                    ];
                    $id++;
                    $this->encode_log[0][] = 'A:0:' . $stream->index . ':' . $stream->language;
                    $response[] = implode(' ', $array);
                }
            }
        }
        return $response;
    }

    public function create_subtitle_command()
    {
        $response = [];
        $subtitle = $this->ffprobe->subtitle;
        $id = 0;

        $dir = $this->outputFolder . '/subtitles';
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }


        $columns = Schema::getColumnListing('video_file');
        $re = '/^sub_.*/i';

        $this->all_subtitle_languages = [];

        foreach ($columns as $key => $column) {
            preg_match($re, $column, $matches);
            if ($matches) {
                $this->all_subtitle_languages = array_merge($this->all_subtitle_languages,  [$column => '']);
            }
        }

        $this->dump[] = $this->all_subtitle_languages;

        if (isset($subtitle->streams)) {

            foreach ($subtitle->streams as $stream) {
                if (!file_exists($dir . '/' . $this->fileName . '.' . $stream->language . '.' . $stream->type . '.' . $stream->extension)) {

                    $array = [
                        '-map 0:' . $stream->index,
                        '-metadata:s:s:' . $id . ' language="' . $stream->language . '"',
                        '-metadata:s:s:' . $id . ' title="'    . str_replace($this->lang_code, $this->lang_name, $stream->language) . '"',
                        '"' . $this->outputFolder . 'subtitles/' . $this->fileName . '.' . $stream->language . '.' . $stream->type . '.' . $stream->extension . '"',
                    ];

                    $id++;
                    $response[] = implode(' ', $array);
                    $this->encode_log[0][] = 'S:0:' . $stream->index . ':' . $stream->language;
                }
            }
            shell_exec('cd "'  . public_path("fonts/") . '" && ffmpeg -dump_attachment:t "" -i "' . $this->fileIn . '" -y  -hide_banner -loglevel panic');
        };

        return $response;
    }

    public function create_manifest()
    {

        $m3u8_content = [];
        $m3u8_content[] = "#EXTM3U\n";
        $m3u8_content[] = "\n";
        $DEFAULT = "YES";

        foreach ($this->ffprobe->audio->streams as $stream) {

            $m3u8_content[] = '#EXT-X-MEDIA:TYPE=AUDIO,GROUP-ID="stereo",LANGUAGE="' . $stream->language . '",NAME="' . str_replace($this->lang_code, $this->lang_name, $stream->language) . '",DEFAULT=' . $DEFAULT . ',AUTOSELECT=YES,URI="audio_' . $stream->language . '/audio_' . $stream->language . '.m3u8"' . "\n";
            $DEFAULT = "NO";
        }

        // dd($this->ffprobe->video->streams);
        foreach ($this->ffprobe->video->streams as $stream) {

            $m3u8_content[] = "\n";
            $m3u8_content[] = '#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=' . $stream->bandwidth . ',CODECS="avc1.4d4015,mp4a.40.2",AUDIO="stereo",RESOLUTION=' . $stream->width . 'x' . $stream->height . "\n";
            $m3u8_content[] = "video_" . $stream->width . 'x' . $stream->height . "/video_" . $stream->width . 'x' . $stream->height . ".m3u8\n";
        }

        $response = implode('', $m3u8_content);
        if (count($this->ffprobe->audio->streams) > 1 || count($this->ffprobe->video->streams) > 1 || ($this->hdr == true && $this->ffprobe->video->count == 1 && $this->ffprobe->audio->count == 1)) {
            file_put_contents($this->outputFolder .   $this->fileName .  ".m3u8", $response, FILE_USE_INCLUDE_PATH);
        }
    }

    public function create_chapter_file()
    {
        $chapters = $this->ffprobe->chapters;
        $chaptersFile = $this->outputFolder . "chapters.vtt";

        if (isset($chapters->count) && $chapters->count > 0 && (!file_exists($chaptersFile) || (file_exists($chaptersFile) && filesize($chaptersFile) == 0))) {
            file_put_contents($chaptersFile, $chapters->chapter_file, FILE_USE_INCLUDE_PATH);
            $this->dump[] = "Making chapter file";
        }
    }

    public function create_thumbnails_command()
    {
        $array = [];
        $response = [];
        $dir = $this->outputFolder . 'thumbs';
        if (!is_dir($dir) && !file_exists($this->outputFolder . 'sprite.webp')) {
            mkdir($dir, 0777, true);
        }

        if (!file_exists($this->outputFolder . 'sprite.webp')) {
            $array = [
                '-c:v mjpeg',
                '-vf fps=fps=1/10',
                '-ss 2',
                '-s 144x81',
                '"' . $this->outputFolder . 'thumbs/thumb-%04d.jpg"',
            ];

            $this->encode_log[$this->index][] = 'previews';

            $response[] = implode(' ', $array);
        }

        return $response;
    }

    public function get_existing_subtitles($file)
    {
        // $dir = $this->outputFolder . '/subtitles';
        // if (!is_dir($dir)) {
        //     mkdir($dir, 0777, true);
        // }

        // $re = '/(?<lang>\w{3}).(?<type>\w{3,4}).(?<ext>\w{3})$/m';

        // $files = [];
        // foreach(new DirectoryIterator($dir) as $item) {
        //     if (!$item->isDot() && ($item->isDir() || $item->isFile())) {
        //         preg_match($re, $item->getRealPath(), $matches);

        //         if(isset($matches["lang"])){
        //             $files['sub_' . $matches["lang"] . '_' . $matches["type"]] = str_replace(env('MEDIA_ROOT'), '/Media', $item->getRealPath());

        //             $subLangTag = 'sub_' . $matches["lang"] . '_' . $matches["type"];
        //             if (isset($subLangTag) && $subLangTag != '') {
        //                 if (!Schema::hasColumn('video_file', $subLangTag)) {
        //                     $sql = "ALTER TABLE `video_file` ADD COLUMN `$subLangTag` MEDIUMTEXT NULL";
        //                     DB::statement($sql);
        //                 }
        //             }
        //         }
        //     }
        // }

        // $files = array_merge($this->all_subtitle_languages ?? [], $files);

        // return $files;
//////////////////////////////////////////////////////
        // $sub_type = [
        //     "full",
        //     "sign",
        //     "sdh"
        // ];
        // $array = [];

        // $re = '/(?<folder>.*)\/(?<file>.*)\.(?<ext>(mp4|m3u8))/';
        // preg_match($re, $file, $match);

        // foreach ($sub_type as $type) {
        //     foreach ($this->lang_code as $lang) {
        //         $file = $match['folder'] . '/subtitles/' . $match['file'] . '.' . $lang . '.' . $type;
        //         if (file_exists($file . '.vtt')) {
        //             $file =  $file . '.vtt';
        //         }
        //         if (file_exists($file . '.ass')) {
        //             $file =  $file . '.ass';
        //         }

        //         if (file_exists($file)) {
        //             $array[] = replace_code_name($lang);
        //         }
        //     }
        // }

///////////////////////

        $array = [];

        $re = '/(?<folder>.*)\/(?<file>.*)\.(?<ext>(mp4|m3u8))/';
        preg_match($re, $file, $match);

        $subs = Storage::disk('root')->files($match['folder'] . '/subtitles');
        foreach($subs as $sub){
            $re = '/(?<folder>.*)\/(?<file>.*)\.(?<lang>(\w{3}))\.(?<type>(full|sign|sdh))\.(?<ext>(vtt|ass))/m';
            preg_match($re, $sub, $match);
            if(isset($match['type']) && isset($match['lang'])){
                // $array[] = replace_code_name($this->languages, $match['lang']);
                $array[] = $match['lang'];
            }
        }

        return array_unique($array);
    }

    public function download_subtitles()
    {
        if(env('OPENSUBTITLES_USERNAME') != "" && env('OPENSUBTITLES_PASSWORD') != "" &&  strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN' ){
            $hashing = new Hash();
            $client = new Client();

            $moviehash = contains($this->fileIn, array('.m3u8')) !== true ? $hashing->OpenSubtitlesHash($this->fileIn) : null;

            $reg = '/.*\/(?<filename>(.*S(?<season>\d{2})E(?<episode>\d{2}))?.*)/';
            preg_match($reg, $this->fileIn, $matches);

            $reg = '/.*\/(?<title>[\w\.]*).*[\.\s]S(?<season>\d{2})E(?<episode>\d{2})/';
            preg_match($reg, $this->fileIn, $matches2);

            $reg = '/.*\/(?<title>[\w\.]*)\./';
            preg_match($reg, $this->fileIn, $matches3);

            foreach (array(env('OPENSUBTITLES_LANGUAGE', '')) as $sublanguageid) {
                $response = [];
                $subtitle = null;

                $subtitleOutName = $this->outputFolder . "subtitles/" . $this->fileName . "." . $sublanguageid . ".full.vtt";
                if (!file_exists($subtitleOutName)) {

                    $response = $client->searchSubtitles([[
                        'sublanguageid' => $sublanguageid,
                        'imdbid' => $this->imdbid ?? null,
                        'moviehash' => $moviehash,
                        'MovieByteSize' => filesize($this->fileIn)
                    ]])->toArray();

                    if (isset($this->imdbid) && $this->imdbid != null) {

                        if (isset($response['data']) && count($response['data']) == 0) {
                            $response = $client->searchSubtitles([[
                                'sublanguageid' => $sublanguageid,
                                'imdbid' => $this->imdbid,
                                'season' => $matches['season'] ?? null,
                                'episode' => $matches['episode'] ?? null
                            ]])->toArray();
                        }

                        if (isset($response['data']) && count($response['data']) == 0) {
                            $response = $client->searchSubtitles([[
                                'sublanguageid' => $sublanguageid,
                                'imdbid' => $this->imdbid ?? null,
                                'query' => $matches['filename'],
                                'season' => $matches['season'] ?? null,
                                'episode' => $matches['episode'] ?? null
                            ]])->toArray();
                        }
                    }

                    if (isset($response['data']) && count($response['data']) == 0) {
                        if ($matches2) {
                            $response = $client->searchSubtitles([[
                                'sublanguageid' => $sublanguageid,
                                'query' => str_replace(".", " ", $matches2['title']),
                                'season' => $matches2['season'],
                                'episode' => $matches2['episode']
                            ]])->toArray();
                        }
                        else if ($matches3) {
                            $response = $client->searchSubtitles([[
                                'sublanguageid' => $sublanguageid,
                                'query' => str_replace(".", " ", $matches3['title'])
                            ]])->toArray();
                        }
                    }

                    if (isset($response['data']) && isset($response['data'][0]) && isset($response['data'][0]['SubDownloadLink'])) {

                        $subtitle = str_replace("http://", "https://", $response['data'][0]);
                        $subtitleName = public_path("working/" . str_replace(".gz", "", basename($subtitle['SubDownloadLink'])) . '.vtt');
                        $client->downloadSubtitle($subtitle, $subtitleName);

                        if (!file_exists($subtitleOutName) && file_exists($subtitleName)) {

                            $cmd = 'mv "' . $subtitleName . '" "' . $subtitleOutName . '"';
                            dump('DEBUG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . $cmd);
                            shell_exec($cmd);
                            if (file_exists($subtitleOutName)) {
                                dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '  , "subtitle: " . str_replace($this->lang_code, $this->lang_name , $sublanguageid) . " downloaded");
                            }
                            else{
                                dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '  , "subtitle: Sonething went wrong with: " . str_replace($this->lang_code, $this->lang_name , $sublanguageid));
                            }

                        } elseif (file_exists($subtitleName . ".vtt") || file_exists($subtitleName . ".ass")) {
                            dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . "subtitle already exists..");
                        }
                    }
                    else {
                        dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '  , "No " . str_replace($this->lang_code, $this->lang_name , $sublanguageid) . " subtitle found");
                    }
                }
                else {
                    dump('VERBOSE: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '  . str_replace($this->lang_code, $this->lang_name , $sublanguageid)  . " subtitle already exists..");
                }
            }
        }
    }

    public function database()
    {
        $VideoFile = VideoFile::where('episode_id', $this->db_index_number)->first();
        if ($VideoFile == '') {
            $VideoFile = VideoFile::where('movie_id', $this->db_index_number)->first();
        }

        if (isset($VideoFile['VideoFile']) && $VideoFile == null && Utils::contains($this->fileIn, array('NoMercy')) === false) {
            $reg = '/.*\/(?<filename>.*)/m';
            preg_match($reg, $this->fileIn, $fileInMatch);
            $VideoFile = $fileInMatch['filename'];
        }

        if ($this->dbtype != 'movie') {
        }

        if (isset($this->ffprobe->audio->count) && isset($this->ffprobe->video->count)) {

            $ext = $this->ffprobe->audio->count > 1 || $this->ffprobe->video->count > 1  ? '.m3u8' : '.mp4';
            if (file_exists($this->outputFolder . $this->fileName .  ".m3u8")) {
                $ext = '.m3u8';
            } else if (file_exists($this->outputFolder . $this->fileName2 .  ".mp4")) {
                $ext = '.mp4';
            }
        } else {
           dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'file: ' . $this->fileName . ' needs moderation');
           $ext = '.m3u8';
        }

        $command = 'ffprobe -v quiet -show_format -show_streams -show_chapters -print_format json "' . str_replace(['$'], ['\$'], $this->outputFolder .  $this->fileName2 . $ext) . '"';
        $ffprobe = json_decode(shell_exec($command), true);

        if(isset($ffprobe) && isset($ffprobe['format']) && isset($ffprobe['format']['duration'])){
            $duration =  gmdate("H:i:s", $ffprobe['format']['duration']);
        }
        else{
            $duration = null;
        }

        $array = [
            "type"              => $this->dbtype,
            "episode_id"        => $this->dbtype != "movie" ? $this->db_index_number : null,
            "movie_id"          => $this->dbtype == "movie" ? $this->db_index_number : null,
            "title"             => preg_replace(['/([^A-Z])\./', '/\.(S\d)/', '/NoMercy/'], ['${1} ', '. ${1}', ''], $this->fileName),
            "duration"          => $duration,
            "quality"           => implode(',', $this->ffprobe->video->qualities ?? []),
            "file"              => str_replace(env('MEDIA_ROOT'), '/Media', $this->outputFolder) .  $this->fileName2 . $ext,
            "subtitles"         => implode(',', $this->get_existing_subtitles($this->outputFolder .  $this->fileName . $ext)),
            "languages"         => str_replace('JapanSpanishe', 'English, Japanese', implode(',', $this->ffprobe->audio->languages ?? [])),
            "chapters"          => str_replace(env('MEDIA_ROOT'), '/Media', $this->outputFolder) . "chapters.vtt",
            "previews"          => str_replace(env('MEDIA_ROOT'), '/Media', $this->outputFolder) . "previews.vtt",
            "updated_at" => now(),
        ];

        return $array;
    }

    public function encode2()
    {

        $first = true;
        $exec = [];
        $execute = [];

        $this->create_chapter_file();
        // $this->download_subtitles();
        $video_command = $this->create_video_command2();
        $audio_command = $this->create_audio_command();
        $thumbnail_command = $this->create_thumbnails_command();
        $subtitle_command = $this->create_subtitle_command();

        dump($this->encode_log['verify'] ?? []);

        if (
            (isset($this->ffprobe->audio->count) && $this->ffprobe->audio->count > 1) || (isset($this->ffprobe->video->count) && $this->ffprobe->video->count > 1)
        ) {
            $this->create_manifest();
        }

        if (!isset($this->firstVideo->width)) {
           dump('LOG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . "Can't process this file");
            return 1;
        }

        $this->newFile = $this->outputFolder . 'video_' .
            $this->firstVideo->width . 'x' .
            $this->firstVideo->height . '/video_' .
            $this->firstVideo->width . 'x' .
            $this->firstVideo->height . '.m3u8';

        $this->input = implode(' ', [
            '-i "' . $this->fileIn . '"',
            '-muxdelay 0',
            // '-itsoffset 2.5 -i "' . $this->fileIn . '"',
        ]);

        $this->input2 = '-i "' . $this->newFile . '"';


        foreach ($video_command['response'] as $command) {
            if (!$this->split && is_array($command) && contains(implode(' ', $command), array('bt2020')) == false) {
                $exec[0][] = $this->input;
                $exec[0][] = $command;
            }
            else {
                if ($first == true || ( is_array($command) && contains(implode(' ', $command), array('bt2020')) !== false)) {
                    $exec[0][] = $this->input;
                    $exec[0][] = $command;
                } else {
                    $exec[1][] = $this->input2;
                    $exec[1][] = $command;
                }
            }

            if ($first == true) {
                if ($this->split) {
                    $exec[0][] = implode(' ', $audio_command);
                }
                $exec[0][] = implode(' ', $subtitle_command);
                $exec[0][] = implode(' ', $thumbnail_command);
            } else {
                $exec[1][] = implode(' ', $thumbnail_command);
            }

            $first = false;
        }

        if (count($video_command['response']) == 0 && (count($thumbnail_command) > 0 || count($subtitle_command) > 0 || count($audio_command) > 0)) {

            if (count($audio_command) != [] || count($subtitle_command) != [] || count($thumbnail_command) != []) {
                $exec[0][] = $this->input;
            }
            if (count($audio_command) != []) {
                $exec[0][] = implode(' ', $audio_command);
            }

            if (count($subtitle_command) != []) {
                $exec[0][] = implode(' ', $subtitle_command);
            }
            if (count($thumbnail_command) != []) {
                $exec[0][] = implode(' ', $thumbnail_command);
            }
        }

        if (isset($exec[0])) {

            foreach ($exec[0] as $key => $item) {
                if ($item == "" || $item == " " || $item == [] || !isset($item)) {
                    unset($exec[0][$key]);
                }
            }

            if (count($exec[0]) > 1) {
                $array1 = [
                    preg_replace('/\s{2,}/', ' ', implode(' ', $exec[0])),
                    ' -y  2>&1 | tee',
                    public_path('/working/encoding/' . $this->db_index_number . '.txt')
                ];
                $execute[0] = implode(' ', $array1);
            } else {
                unset($exec[0]);
            }
        }

        if (isset($exec[1])) {
            foreach ($exec[1] as $key => $item) {
                if ($item == '' || $item == ' ' || $item == []) {
                    unset($exec[1][$key]);
                }
            }

            if (count($exec[1]) > 1) {
                $array1 = [
                    preg_replace('/\s{2,}/', ' ', implode(' ', $exec[1])),
                    ' -y  2>&1 | tee',
                    public_path('/working/encoding/' . $this->db_index_number . '.txt')
                ];
                $execute[1] = implode(' ', $array1);
            } else {
                unset($exec[1]);
            }
        }

        if (count($execute) > 0) {
            dump($execute);

            $priority = 'High';
            $ProcessEncodeCommand = (new ProcessEncodeCommand($execute, $this->outputFolder, $this->fileName, $this->ffprobe, $this->encode_log, $this->split, $this->db_index_number));
            dispatch($ProcessEncodeCommand->onQueue('encoder'));
        }
    }
}
