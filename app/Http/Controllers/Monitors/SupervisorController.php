<?php

namespace App\Http\Controllers\Monitors;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SupervisorController extends Controller
{


    public function supervisor()
    {
        $files = Storage::disk('root')->files('/var/www/log/supervisor');

        return view('dashboard.supervisor', compact('files'));
    }

    public function clear_log()
    {
        $files = Storage::disk('root')->files('/var/www/log/supervisor');
        foreach ($files as $file) {
            Storage::disk('root')->put('/' . $file, '');
        }
        return response()->json(['message' => 'logs are cleared'], 202);
    }

    public function tail(Request $request)
    {
        $response = [];
        $offset = null;
        $id = 0;
        // if ($request->ajax()) {
            $files = Storage::disk('root')->files('/var/www/log/supervisor');
            foreach ($files as $file) {
                $handle = fopen('/' . $file, 'r');
                if (!isset($offset)) {
                    $data = substr(stream_get_contents($handle, -1, $offset), -200000);

                    //file
                    if ($file == 'var/www/log/supervisor/laravel-queue_00.log') {
                        $re = '/.*ProcessEncodeCommand.*\n([\w":\s\.\/\)\(\]\[\-|&>=+%*,@\'`]*)\n/i';
                        preg_match($re, $data, $match1);
                        if ($match1) {
                            $data = str_replace($match1[1], '', $data);
                        }
                        $data = str_replace('"ffmpeg', '<div class="ml-8">"ffmpeg', $data);
                        $data = str_replace('.txt"', '.txt"</div>', $data);
                    }

                    $data = str_replace('""', "", $data);

                    $re = '/(?<array>\d{1,}\s=>\s".*"\n*)/m';
                    preg_match_all($re, $data, $match6);
                    if ($match6) {
                        foreach ($match6[1] as $match) {
                            $data = str_replace($match, '<div class="mt-1 ml-2">' . $match . '</div>', $data);
                        }
                    }

                    // $data = preg_replace('/(\[.*\].*)/', '/<div class="text-dark"> $1 <\/div> <\/div>/', $data);

                    $response[$id] = [
                        "file" => $file,
                        "data" => nl2br($data)
                    ];
                } else {
                    fseek($handle, 0, SEEK_END);
                    $offset = ftell($handle);
                }
                $id++;
            }
        // }
        // dd($response);
        return response()->json($response);
    }
}
