<?php

namespace App\Http\Controllers\Monitors;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Encoder\Progress;
use App\Models\Workers;
use App\User;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Throwable;
use Tymon\JWTAuth\JWTAuth;

class ProgressController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function workers()
    {
        $encoders = DB::table('jobs_progress')->where('active', true)->get();
        $user = User::find(Auth::user())->first();
        $token = Auth::guard('api')->tokenById($user->id);

        return view('dashboard.queue')
            ->with('token', $token)
            ->with('workers', $encoders);
    }

    public function queue()
    {
        $encoders = DB::table('progress')->where('active', true)->get();

        $user = User::find(Auth::user())->first();
        $token = Auth::guard('api')->tokenById($user->id);

        return view('dashboard.queue')
            ->with('token', $token)
            ->with('workers', $encoders);
    }


    public function queue_list(Request $request)
    {
        if ($request->ajax()) {

            $ProgressController = (new Progress());
            $queue = $ProgressController->queue($request->limit);

            return $queue;
        }
    }

    public function progress_list(Request $request)
    {;

        $workers = [];
        $user = User::find(Auth::user())->first();
        $token = Auth::guard('api')->tokenById($user->id);

        foreach (Workers::where('active', true)->get() as $worker) {

            if ($worker->workerName != env('WORKER_NAME')) {
                try {
                        $workers[] = Http::withToken($token)
                            ->withoutVerifying()
                            ->timeout(5)
                            ->post($worker->workerIp . '/api/progress')
                            ->json()
                        ;

                } catch (Throwable $e) {
                    $workers[] = [
                        "workerName" => $worker->workerName,
                        "message" => "Failed to connect to: " . $worker->workerName . " at: " . $worker->workerIp,
                    ];
                }
            } else {
                $ProgressController = (new Progress());
                $workers[] = $ProgressController->progress();
            }
        }
        return response($workers);
    }

    public function cpuusage(Request $request)
    {
        // if ($request->ajax()) {

        $workers = [];
        $user = Auth::user();
        $token = Auth::guard('api')->tokenById($user->id);

        foreach (Workers::where('active', 1)->get() as $worker) {
            if ($worker->workerName == env('WORKER_NAME')) {
                $workers[] = SystemInfoController::cpuUtilisation();
            } else {
                try {
                    $workers[] = Http::withToken($token)
                        ->withoutVerifying()
                        ->timeout(15)
                        ->post($worker->workerIp . '/api/system/utilisation')
                        ->json()
                        ;
                } catch (GuzzleException $e) {
                    $workers[] = [
                        "workerName" => $worker->workerName,
                        "message" => "Failed to connect to: " . $worker->workerName . " at: " . $worker->workerIp,
                    ];
                }
            }
        }
        return Response($workers);
        // }
    }
}
