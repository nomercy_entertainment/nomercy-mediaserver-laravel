<?php

namespace App\Http\Controllers\Monitors;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use NoMercy\AppSettings\Setting\AppSettings;

class SettingsController extends Controller
{
    public function encoder()
    {
        $settingsUI = [];

        return view('encoder::settings.encoder')
            ->with('settingsPage', 'encoder')
            ->with('settingsUI', $settingsUI);
    }
}
