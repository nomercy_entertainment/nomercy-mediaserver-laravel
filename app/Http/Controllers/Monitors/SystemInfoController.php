<?php

namespace App\Http\Controllers\Monitors;

use App\Http\Controllers\Controller;

class SystemInfoController extends Controller
{
    public static function getCpuInfo()
    {
        if (PHP_OS == "WINNT") {

            // $windowsCpuData = shell_exec('wmic cpu list full /format:list');
        } else {
            $filename = '/proc/cpuinfo';
            $linuxCpuFile = file_get_contents($filename);
            if (strlen($linuxCpuFile) > 0) {
                if (substr($linuxCpuFile, -1) != "\n") {
                    $linuxCpuFile .= "\n";
                }
                $re = '/processor\s*:\s(?<processor>\d)\nvendor_id\s*:\s(?<vendor>.*)\ncpu family.*:\s(?<cpu_family>\d{1,3})\nmodel.*:\s(?<model>.*)\nmodel name\s*:\s(?<model_name>.*)\nstepping\s*:\s(?<stepping>.*)\nmicrocode\s*:\s(?<microcode>.*)\ncpu MHz\s*:\s(?<cpu_mhz>.*)\ncache size\s*:\s(?<cache_size>.*)\nphysical id\s*:\s(?<physical_id>.*)\nsiblings\s*:\s(?<siblings>.*)\ncore id\s*:\s(?<core_id>.*)\ncpu cores\s*:\s(?<cpu_cores>.*)\napicid\s*:\s(?<apicid>.*)\ninitial apicid\s*:\s(?<initial_apicid>.*)\nfpu\s*:\s(?<fpu>.*)\nfpu_exception\s*:\s(?<fpu_exeption>.*)\ncpuid level\s*:\s(?<cpuid_level>.*)\nwp\s*:\s(?<wp>.*)\nflags\s*:\s(?<flags>.*)\nbugs\s*:\s(?<bugs>.*)\nbogomips\s*:\s(?<bogomips>.*)\n(TLB size\s*:\s(?<tlb_size>.*)\n)?clflush size\s*:\s(?<cflush_size>.*)\ncache_alignment\s*:\s(?<cache_alignment>.*)\naddress sizes\s*:\s(?<address_sizes>.*)\npower management\s*:\s(?<power_management>.*)/m';
                preg_match($re, $linuxCpuFile, $matches);
            }
        }
        $cpu = [];
        if ($matches) {
            $processor = $matches['processor'];
            foreach ($matches as $key => $value) {
                if (!Is_Numeric($key)) {
                    if ($key == "flags" || $key == "bugs") {
                        $cpu[$key] = explode(" ", $value);
                    } elseif ($key == "address_sizes") {
                        $cpu[$key] = explode(", ", $value);
                    } else {
                        $cpu[$key] = preg_replace("/\s{2,}/", " ", $value);
                    }
                }
            }
        }
        return $cpu;
    }
    public static function getMemoryInfo()
    {

        $filename = '/proc/meminfo';
        $linuxMemoryFile = file_get_contents($filename);
        $mem = [];

        if (strlen($linuxMemoryFile) > 0) {
            if (substr($linuxMemoryFile, -1) != "\n") {
                $linuxMemoryFile .= "\n";
            }

            $re = '/MemTotal:\s*(?<MemTotal>\d*).*\nMemFree:\s*(?<MemFree>\d*).*\nMemAvailable:\s*(?<MemAvailable>\d*).*\nBuffers:\s*(?<Buffers>\d*).*\nCached:\s*(?<Cached>\d*).*\nSwapCached:\s*(?<SwapCached>\d*).*\nActive:\s*(?<Active>\d*).*\nInactive:\s*(?<Inactive>\d*).*\nActive\(anon\):\s*(?<Active_anon>\d*).*\nInactive\(anon\):\s*(?<Inactive_anon>\d*).*\nActive\(file\):\s*(?<Active_file>\d*).*\nInactive\(file\):\s*(?<Inactive_file>\d*).*\nUnevictable:\s*(?<Unevictable>\d*).*\nMlocked:\s*(?<Mlocked>\d*).*\nSwapTotal:\s*(?<SwapTotal>\d*).*\nSwapFree:\s*(?<SwapFree>\d*).*\nDirty:\s*(?<Dirty>\d*).*\nWriteback:\s*(?<Writeback>\d*).*\nAnonPages:\s*(?<AnonPages>\d*).*\nMapped:\s*(?<Mapped>\d*).*\nShmem:\s*(?<Shmem>\d*).*\nSlab:\s*(?<Slab>\d*).*\nSReclaimable:\s*(?<SReclaimable>\d*).*\nSUnreclaim:\s*(?<SUnreclaim>\d*).*\nKernelStack:\s*(?<KernelStack>\d*).*\nPageTables:\s*(?<PageTables>\d*).*\nNFS_Unstable:\s*(?<NFS_Unstable>\d*).*\nBounce:\s*(?<Bounce>\d*).*\nWritebackTmp:\s*(?<WritebackTmp>\d*).*\nCommitLimit:\s*(?<CommitLimit>\d*).*\nCommitted_AS:\s*(?<Committed_AS>\d*).*\nVmallocTotal:\s*(?<VmallocTotal>\d*).*\nVmallocUsed:\s*(?<VmallocUsed>\d*).*\nVmallocChunk:\s*(?<VmallocChunk>\d*).*\nHardwareCorrupted:\s*(?<HardwareCorrupted>\d*).*\nAnonHugePages:\s*(?<AnonHugePages>\d*).*\nShmemHugePages:\s*(?<ShmemHugePages>\d*).*\nShmemPmdMapped:\s*(?<ShmemPmdMapped>\d*).*\nCmaTotal:\s*(?<CmaTotal>\d*).*\nCmaFree:\s*(?<CmaFree>\d*).*\nHugePages_Total:\s*(?<HugePages_Total>.*)\nHugePages_Free:\s*(?<HugePages_Free>.*)\nHugePages_Rsvd:\s*(?<HugePages_Rsvd>.*)\nHugePages_Surp:\s*(?<HugePages_Surp>.*)\nHugepagesize:\s*(?<Hugepagesize>\d*).*\nDirectMap4k:\s*(?<DirectMap4k>\d*).*\nDirectMap2M:\s*(?<DirectMap2M>\d*).*/m';

            preg_match($re, $linuxMemoryFile, $matches);

            $id = 0;
            foreach ($matches as $key => $value) {
                if (!Is_Numeric($key)) {
                    if ($key == "flags" || $key == "bugs") {
                        $mem[$key] = explode(" ", $value);
                    } elseif ($key == "address_sizes") {
                        $mem[$key] = explode(", ", $value);
                    } else {
                        $mem[$key] = preg_replace("/\s{2,}/", " ", $value);
                    }
                }
            }
            $id++;
        }
        return $mem;
    }

    public static function cpuUtilisation()
    {

        $linuxCpuCommand = shell_exec('mpstat -P ALL 1 1');

        $re = '/Average: \s*(?<core>(all|\d{1,2}))\s*(?<usr>\d{1,2}\.\d{1,2})\s*(?<nice>\d{1,2}\.\d{1,2})\s*(?<sys>\d{1,2}\.\d{1,2})\s*(?<iowait>\d{1,2}\.\d{1,2})\s*(?<irq>\d{1,2}\.\d{1,2})\s*(?<soft>\d{1,2}\.\d{1,2})\s*(?<steal>\d{1,2}\.\d{1,2})\s*(?<guest>\d{1,2}\.\d{1,2})\s*(?<gnice>\d{1,2}\.\d{1,2})\s*(?<idle>\d{1,2}\.\d{1,2})/m';
        preg_match_all($re, $linuxCpuCommand, $matches, PREG_SET_ORDER, 0);
        $cpu = [];
        if ($matches) {
            $id = 0;
            foreach ($matches as $match) {
                $core = $match['core'];
                foreach ($match as $key => $value) {
                    if (!Is_Numeric($key)) {
                        $cpu[$core][$key] = preg_replace("/\s{2,}/", " ", $value);
                    }
                }
                $id++;
            }
        } else {
            $array = [
                "workerName" => env('WORKER_NAME'),
                "Message" => "Can not run shell commands."
            ];
        }

        $array = [
            "workerName" => env('WORKER_NAME'),
            "cpuName" => SystemInfoController::getCpuInfo()['model_name'],
            'cpu' => $cpu
        ];
        return $array;
    }
}
