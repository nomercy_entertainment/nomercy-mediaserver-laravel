<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);

        $response = Http::acceptJson()->post('https://auth.nomercy.tv/api/login', [
            "email" => $credentials['email'],
            "password" => $credentials['password'],
            "app_id" => env('APP_ID'),
            "app_name" => env('WORKER_NAME'),
            "location" => env('APP_URL'),
            "ip" => $request->ip(),
        ])->json();

        if(isset($response['message'])){
            dd($response['message']);
        }
        if (isset($response['error'])) {

            dd($response);
            return redirect(route('login'))->with('error', $response['error']);
        }

        if ($response == false) {
            return redirect(route('login'))->with('error', __('error.500'));
        }

        // dd($response);

        if (!Auth::attempt($credentials)) {

            if (!User::first()) {
                $user = User::create([
                    'id' => $response['data']['user']['id'],
                    'name' => $response['data']['user']['name'],
                    'email' => $credentials['data']['user']['email'],
                    'api_token' => $response['data']['user']['api_token'],
                    'password' => Hash::make($credentials['password']),
                    'isOwner' => true,
                ]);
                Auth::attempt($credentials);
                return redirect('/');
            }
            else if (env('APP_PRIVACY', 'private') == 'public') {
                $user = User::create([
                    'id' => $response['data']['user']['id'],
                    'name' => $response['data']['user']['name'],
                    'email' => $response['data']['user']['email'],
                    'password' => Hash::make($credentials['password']),
                    'isOwner' => false,
                ]);

                Auth::attempt($credentials);
                return redirect('/');
            }

            abort(401, __('error.401'));
        }
        else {
            Auth::attempt($credentials);
            return redirect('/');
        }
    }
}
