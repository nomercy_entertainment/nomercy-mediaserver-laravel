<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

class JWTController extends Controller
{
     /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
        $this->guard = 'api';
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => $validator->errors()
            ], 422);
        }

        if (!$token = auth($this->guard)->attempt($validator->validated())) {
           return response()->json([
               'error' => [
                'any' => [
                   __('auth.failed')
                ]]
           ], 422);
        }

        $user = auth($this->guard)->user();

        $token = JWTAuth::fromUser($user);

        return $this->createNewToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth($this->guard)->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth($this->guard)->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register()
    {
        return ["message" => "not yet implemented"];
    }
    public function refresh()
    {
        return $this->respondWithToken(auth($this->guard)->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth($this->guard)->factory()->getTTL() * 60
        ]);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile() {

        $user = $this->userFilter(auth($this->guard)->user()->id );

        return response()->json($user);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createNewToken($token)
    {
        return response()->json([
            'data' => [
                'token' => [
                    'access_token' => $token,
                    'token_type' => 'bearer',
                    'exp' => auth($this->guard)->factory()->getTTL() * 60,
                    'iat' => Carbon::now('UTC')->timestamp,
                    'expires_at' => Carbon::now('UTC')->addMinutes(auth($this->guard)->factory()->getTTL())->format('Y-m-d H:i:s'),
                    'issued_at' => Carbon::now('UTC')->format('Y-m-d H:i:s'),
                ],
                'user' => $this->userFilter(auth($this->guard)->user()->id )
            ]
        ]);
    }

    public function userFilter($id)
    {
        $user = User::where('id', $id )->first();
        return collect($user)->merge([
            'user' => [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'api_token' => $user->api_token,

            ],
        ])->only('user')['user'];

    }
}
