<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile()
    {
        $user = Auth::user();

        JWTAuth::getJWTProvider()->setSecret(env('APP_ID'));
        $token = Auth::guard('api')->tokenById($user->id);

        $external = Http::withToken($token)->post('https://nomercy.tv/api/auth/me')->json();


        return view('auth.profile')->with('user', $user);
    }
}
