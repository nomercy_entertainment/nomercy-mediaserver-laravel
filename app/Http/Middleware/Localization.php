<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\App;
use Closure;
use Illuminate\Support\Facades\Lang;

class Localization
{
    public function handle($request, Closure $next)
    {
        $languages = collect($request->headers->get("accept-language") ?? 'en')->first();

        $re = '/[a-z]{2}/im';
        preg_match($re, $languages, $matches);

        App::setlocale(strtolower($matches[0]));

        return $next($request);
    }
}
