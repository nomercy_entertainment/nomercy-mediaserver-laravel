<?php

namespace App\Http\Middleware;

use App\Http\Controllers\SetupController;
use App\Models\FolderRoots;
use App\Models\SupportedLanguages;
use App\User;
use Closure;

class InitialSetup
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (User::count() > 1 && (FolderRoots::count() > 0 && SupportedLanguages::count() > 0) && env('APP_URL') != "") {
            return $next($request);
        }

        return redirect('/setup');
    }
}
