<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Exceptions\NoTokenException;
use App\Exceptions\UnauthorizedException;

class ApiToken
{
    private $api_token;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $this->api_token = $request->query('api_token');
        $exept = [
            'api/login',
            'api/register',
        ];

        if(contains($request->path(), $exept) == false){
            $this->mutateSecretToUser();
        }

        return $next($request);
    }

    private function tokenFromHeader()
    {
        $header = request()->headers->get('authorization');
        if($header != ''){
            $token = trim(str_ireplace('bearer', '', $header));
            return $token;
        }
    }


    private function getPayload($token)
    {
        if($token == null){
            throw new NoTokenException;
        }
        else{
            $payload = json_decode(
                base64_decode(
                    str_replace('_', '/', str_replace('-','+',explode('.', $token)[1] ?? null))
                )
            );
            return $payload;

        }
    }

    private function mutateSecretToUser($token = null)
    {

        $token = $this->tokenFromHeader() ?? $token;


        if($token == null){
            $user = User::where('api_token', $this->api_token)->first();
        }
        else{
            $id = is_object($this->getPayload($token)) ? $this->getPayload($token)->sub : null;
            $user = User::where('id', $id)->first();
        }

        if(!is_object($user)){
            throw new UnauthorizedException;
        }

        JWTAuth::getJWTProvider()->setSecret($user->jwt_key);

    }
}
