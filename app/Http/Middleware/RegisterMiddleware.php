<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RegisterMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $user = Auth::user();

        if ($user->servers == null) {
            return redirect('auth/continue');
        }

        if (Auth::guard($guard)->check()) {
            return redirect()->intended('/home');
        }

        return $next($request);
    }
}
