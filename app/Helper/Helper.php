<?php

use Archive7z\Archive7z;
use App\Models\SupportedLanguages;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Storage;

function languages()
{

    $lang_code = [];
    $lang_name = [];
    $lang_country = [];
    foreach (SupportedLanguages::select('language_code', 'language_name', 'country_code', 'flag')
        ->orderBy('audio_order', 'asc')->get() as $audio_code) {
        $lang_code[] = $audio_code['language_code'];
        $lang_name[] = $audio_code['language_name'];
        $lang_country[] = $audio_code['country_code'];
        $lang_flag[] = $audio_code['flag'];
    }

    return [
        "lang_code" => $lang_code,
        "lang_name" => $lang_name,
        "lang_country" => $lang_country,
        "lang_flag" => $lang_flag,
    ];
}

function translations(){
    $lang = app()->getLocale();
    // \Illuminate\Support\Facades\Cache::forget('lang_'.$lang.'.js');
    $strings = \Illuminate\Support\Facades\Cache::rememberForever('lang_'.$lang.'.js', function () use($lang) {
        $files = [
            resource_path('lang/' . $lang . '/languages.php'),
        ];
        $strings = [];

        foreach ($files as $file) {
            $name = basename($file, '.php');
            $strings[] = require $file;
        }

        return $strings[0];
    });
    return json_encode($strings);
}

function replace_country_name(array $arr, $string)
{
    return str_replace($arr["lang_country"], $arr["lang_name"], $string);
}

function replace_code_name(array $arr, $string)
{
    return str_replace($arr["lang_code"], $arr["lang_name"], $string);
}


function replace_name_code(array $arr, $string)
{
    return str_replace($arr["lang_name"], $arr["lang_code"], $string);
}

function replace_name_country(array $arr, $string)
{
    return str_replace($arr["lang_name"], $arr["lang_country"], $string);
}

function replace_name_flag(array $arr, $string)
{
    return base64_decode(str_replace($arr["lang_name"], $arr["lang_flag"], $string));
}

function poster_width()
{
    return 'w300';
}
function backdrop_width()
{
    return 'original';
}

function generateRandomString($length = 20)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function titleSort($title)
{
    return preg_replace('/(\s:.*)/', '', ucfirst(addslashes(str_replace(["The ", "An ", "A ", "\""], [""], $title))));
}

function zeropad($num, $amount = 2)
{
    return sprintf("%0" . $amount . "d", $num);
}

function img_url($img, $size = 'original')
{
    return 'https://image.tmdb.org/t/p/' . $size . $img;
}

function download_tmdb($path, $type, $size = 'original')
{
    if (!empty($path) && !empty($type)) {

        if ($size == 'original') {
            $location = str_replace('.jpg', '.original.jpg', $path);
        } else {
            $location = $path;
            $location2 = str_replace('.jpg', '.webp', $path);
        }

        Storage::disk('public')->put('assets/' . $type . '/' . $location, @file_get_contents(img_url($path, $size), 0));
        if (isset($location2)) {
            convert_image('/var/www/html/public/assets/' . $type . '/' . $location, '/var/www/html/public/assets/' . $type . '/' . $location2, 'y');
        }

    }
}

function convert_image($file_in, $file_out, $replace)
{
    if (file_exists($file_in)) {
        exec(ffmpeg()." -i " . $file_in . "  " . $file_out . " -" . $replace . " 2> /dev/null ");
    }
}


function contains($haystack, $needle, $offset = 0)
{
    if (!is_array($needle)) $needle = array($needle);
    foreach ($needle as $query) {
        if (strpos($haystack, $query, $offset) !== false) return true; // stop on first true result
    }
    return false;
}


function asset_tag()
{
    return generateRandomString(7);
}

function put_permanent_env($key, $value)
{
    $protected_keys = config('nomercy.protected.keys.env');
    if(contains($key, $protected_keys)){
        return;
    }

    $path = app()->environmentFilePath();

    $escaped = preg_quote('='.env($key), '/');

    file_put_contents($path, preg_replace(
        "/^{$key}{$escaped}/m",
        "{$key}={$value}",
        file_get_contents($path)
    ));
}

function os(){
    switch (PHP_OS) {
        case 'CYGWIN_NT-5.1':
            return 'windows';
            break;

        case 'Darwin':
                return 'macos';
            break;

        case 'FreeBSD':
            return 'bsd';
            break;

        case 'HP-UX':
            return 'ux';
            break;

        case 'IRIX64':
            return 'irix';
            break;

        case 'Linux':
            return 'linux';
            break;

        case 'NetBSD':
            return 'bsd';
            break;

        case 'WIN32':
            return 'windows';
            break;

        case 'WINNT':
            return 'windows';
            break;

        case 'Windows':
            return 'windows';
            break;

        default:
            return strtoupper(substr(PHP_OS, 0, 3));
            break;
    }
}

function download_ffmpeg_for_linux(){

    $ffmpeg = 'ffmpeg/linux/ffmpeg';
    $ffprobe = 'ffmpeg/linux/ffprobe';

    if(!Storage::exists($ffmpeg) || !Storage::exists($ffprobe) ){

        $url = 'https://johnvansickle.com/ffmpeg/builds/ffmpeg-git-amd64-static.tar.xz';

        $link = explode('/', $url);
        $fileName = $link[count($link) -1];
        $file = storage_path('app/tmp/'. $fileName);
        $folder = storage_path('app/ffmpeg/linux/');
        system('mkdir -p ' . $folder);

        if(!Storage::exists($file)){
            copy($url, $file);
        }

        $tar_exec = "tar -x -C " . $folder . " --strip-components 1 -f " . $file . " --wildcards '*/ffmpeg' --wildcards '*/ffprobe'";
        shell_exec($tar_exec);
        system("rm -rf ".escapeshellarg($file));
    }
}

function download_ffmpeg_for_macos(){

    $ffmpeg = 'ffmpeg/macos/ffmpeg';
    $ffprobe = 'ffmpeg/macos/ffprobe';

    if(!Storage::exists($ffmpeg)){

        $url = 'https://evermeet.cx/ffmpeg/getrelease/ffmpeg/zip';

        $link = explode('/', $url);
        $fileName = $link[count($link) -1];
        $file = storage_path('app/tmp/'. $fileName);
        $folder = storage_path('app/ffmpeg/macos/');
        system('mkdir -p ' . $folder);

        if(!Storage::exists($file)){
            copy($url, $file);
        }

        $unzip_exec = "unzip -o -d " . $folder . " -j " . $file . " ffmpeg";
        shell_exec($unzip_exec);
        system("rm -rf ".escapeshellarg($file));
    }

    if(!Storage::exists($ffprobe) ){

        $url = 'https://evermeet.cx/ffmpeg/getrelease/ffprobe/zip';

        $link = explode('/', $url);
        $fileName = $link[count($link) -1];
        $file = storage_path('app/tmp/'. $fileName);
        $folder = storage_path('app/ffmpeg/macos/');
        system('mkdir -p ' . $folder);

        if(!Storage::exists($file)){
            copy($url, $file);
        }

        $unzip_exec = "unzip -o -d " . $folder . " -j " . $file . " ffprobe";
        shell_exec($unzip_exec);
        system("rm -rf ".escapeshellarg($file));
    }
}

function download_ffmpeg_for_windows(){

    $ffmpeg = '/ffmpeg/windows/ffmpeg.exe';
    $ffprobe = '/ffmpeg/windows/ffprobe.exe';

    if(!Storage::exists($ffmpeg) || !Storage::exists($ffprobe) ){

        $url = 'https://www.gyan.dev/ffmpeg/builds/ffmpeg-release-full.7z';

        $link = explode('/', $url);
        $fileName = $link[count($link) -1];
        $file = storage_path('app/tmp/'. $fileName);
        $folder = storage_path('app/ffmpeg/windows/');
        system('mkdir -p ' . $folder);

        if(!Storage::exists($file)){
            copy($url, $file);
        }

        $obj = new Archive7z($file);
        if (!$obj->isValid()) {
            throw new \RuntimeException('Incorrect archive');
        }
        $del = '';
        foreach ($obj->getEntries() as $entry) {
            if (contains($path = $entry->getPath(), array('ffmpeg.exe', 'ffprobe.exe') )) {
                $orig = $folder . $path;
                $fileName = explode('/', $orig)[count(explode('/', $orig)) -1];
                $fileName2 = explode('/', $orig)[count(explode('/', $orig)) -2];
                $new = $folder . $fileName;

                if(!file_exists($new)){
                    $entry->extractTo($folder);
                    rename($orig, $new);
                }

                $del = $folder . str_replace([$folder, $fileName2 . '/' . $fileName], '', $orig);
            }
        }
        system("rm -rf ".escapeshellarg($del));
        system("rm -rf ".escapeshellarg($file));
    }
}

function check_binaries(){
    $os = os();

    system('mkdir -p ' . storage_path('app/tmp'));

    if ($os === 'linux' ){
        if(!file_exists(storage_path('app/ffmpeg/linux/ffmpeg')) || !file_exists(storage_path('app/ffmpeg/linux/ffprobe'))) {
            download_ffmpeg_for_linux();
        }
        if( !file_exists(storage_path('app/youtube-dl/linux/youtube-dl'))) {
            system('mkdir -p ' . storage_path('app/youtube-dl/linux/'));
            copy('https://yt-dl.org/downloads/latest/youtube-dl', storage_path('app/youtube-dl/linux/youtube-dl'));
            shell_exec('chmod +x ' . storage_path('app/youtube-dl/linux/youtube-dl'));
            shell_exec(storage_path('app/youtube-dl/linux/youtube-dl -U'));
        }
        return 'linux';
    }

    else if ($os === 'windows' ){
        if(!file_exists(storage_path('app/ffmpeg/windows/ffmpeg.exe')) || !file_exists(storage_path('app/ffmpeg/windows/ffprobe.exe'))) {
            download_ffmpeg_for_windows();
        }
        if(!file_exists(storage_path('app/youtube-dl/windows/youtube-dl.exe'))) {
            system('mkdir -p ' . storage_path('app/youtube-dl/windows/'));
            copy('https://yt-dl.org/downloads/latest/youtube-dl.exe', storage_path('app/youtube-dl/windows/youtube-dl.exe'));
        }
        return 'windows';
    }

    else if ($os === 'macos' ){
        if(!file_exists(storage_path('app/ffmpeg/macos/ffmpeg')) || !file_exists(storage_path('app/ffmpeg/macos/ffprobe'))) {
            download_ffmpeg_for_macos();
        }
        if( !file_exists(storage_path('app/youtube-dl/linux/youtube-dl'))) {
            system('mkdir -p ' . storage_path('app/youtube-dl/linux/'));
            copy('https://yt-dl.org/downloads/latest/youtube-dl', storage_path('app/youtube-dl/linux/youtube-dl'));
            shell_exec('chmod +x ' . storage_path('app/youtube-dl/linux/youtube-dl'));
        }
        return 'macos';
    }

}

function youtubeDl(){
    if (check_binaries() != 'windows'){
        return storage_path('app/youtube-dl/linux/youtube-dl');
    }
    else {
        return storage_path('app/youtube-dl/windows/youtube-dl.exe');
    }
}

function ffmpeg(){
    $os = check_binaries();

    if ($os === 'linux' ){
        return storage_path('app/ffmpeg/linux/ffmpeg');
    }
    else if ($os === 'windows' ){
        return storage_path('app/ffmpeg/windows/ffmpeg.exe');
    }
    else {
        return storage_path('app/ffmpeg/macos/ffmpeg');
    }
}

function ffprobe(){
    $os = check_binaries();

    if ($os === 'linux' ){
        return storage_path('app/ffmpeg/linux/ffprobe');
    }
    else if ($os === 'windows' ){
        return storage_path('app/ffmpeg/windows/ffprobe.exe');
    }
    else {
        return storage_path('app/ffmpeg/macos/ffprobe');
    }
}


function themeColor()
{
    return  Cookie::get('themeColor') ?? '#9a1dce';
}

function platform(){

    $user_agent = request()->header('User-Agent');
    $platform = 'Unknown';

    if (preg_match('/linux/i', $user_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $user_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $user_agent)) {
        $platform = 'windows';
    }
    return strtolower($platform);

}

function browser(){

    $user_agent = request()->header('User-Agent');
    if(preg_match('/PlayStation|Chrome\/49/i',$user_agent))
    {
        $ub = "Playstation";
    }
    elseif(preg_match('/Tiezen/i',$user_agent))
    {
        $ub = "Tiezen";
    }
    elseif(preg_match('/MSIE/i',$user_agent) && !preg_match('/Opera/i',$user_agent))
    {
        $ub = "MSIE";
    }
    elseif(preg_match('/Firefox/i',$user_agent))
    {
        $ub = "Firefox";
    }
    elseif(preg_match('/Chrome/i',$user_agent))
    {
        $ub = "Chrome";
    }
    elseif(preg_match('/Safari/i',$user_agent))
    {
        $ub = "Safari";
    }
    elseif(preg_match('/Opera/i',$user_agent))
    {
        $ub = "Opera";
    }
    elseif(preg_match('/Netscape/i',$user_agent))
    {
        $ub = "Netscape";
    }

    return strtolower($ub);
}

function brand(){

    $user_agent = request()->header('User-Agent');
    $bname = 'Unknown';
    if(preg_match('/PlayStation|Chrome\/49/i',$user_agent))
    {
        $bname = 'Sony';
    }
    elseif(preg_match('/Tiezen/i',$user_agent))
    {
        $bname = 'Samsung';
    }
    elseif(preg_match('/MSIE/i',$user_agent) && !preg_match('/Opera/i',$user_agent))
    {
        $bname = 'Internet Explorer';
    }
    elseif(preg_match('/Firefox/i',$user_agent))
    {
        $bname = 'Mozilla Firefox';
    }
    elseif(preg_match('/Chrome/i',$user_agent))
    {
        $bname = 'Google Chrome';
    }
    elseif(preg_match('/Safari/i',$user_agent))
    {
        $bname = 'Apple Safari';
    }
    elseif(preg_match('/Opera/i',$user_agent))
    {
        $bname = 'Opera';
    }
    elseif(preg_match('/Netscape/i',$user_agent))
    {
        $bname = 'Netscape';
    }

    return $bname;
}
