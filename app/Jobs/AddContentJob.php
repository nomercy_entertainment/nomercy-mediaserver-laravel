<?php

namespace App\Jobs;

use App\Http\Controllers\Database\TMDBController;
use App\Http\Controllers\Encoder\FileController;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddContentJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $id, $type, $direction, $FileController;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id, $type, $direction = 'in')
    {
        $this->id = $id;
        $this->type = $type;
        $this->direction = $direction;
        $this->FileController = new FileController();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->type == 'movie') {
            $this->FileController->get_movie_files('out');
            $TMDBController = new TMDBController();
            $TMDBController->add_movie($this->id, 'in', true);
        }

        // if ($this->type == 'tv') {
        //     $this->FileController->get_tv_files('out');
        //     $TMDBController = new TMDBController();
        //     $TMDBController->add_tv($this->id, 'in', true);
        // }

        // if ($this->type == 'all') {
        //     $this->FileController->get_tv_files('out');
        //     $this->FileController->get_movie_files('out');
        //     $this->FileController->find_media($this->direction);
        // }
    }
}
