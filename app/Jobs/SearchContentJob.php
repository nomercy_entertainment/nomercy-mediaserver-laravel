<?php

namespace App\Jobs;

use App\Http\Controllers\Database\TMDBController;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SearchContentJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $type, $title, $year;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($type, $title, $year)
    {
        $this->type = $type;
        $this->title = $title;
        $this->year = $year;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $DatabaseController = new TMDBController();
        if ($this->type == 'movie') {
            $DatabaseController->search_movie($this->title, $this->year);
        } else {
            $DatabaseController->search_tv($this->title, $this->year);
        }
    }
}
