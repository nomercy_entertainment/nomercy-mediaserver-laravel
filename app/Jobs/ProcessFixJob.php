<?php

namespace App\Jobs;

use App\Http\Controllers\Encoder\FileController;
use App\Http\Controllers\Encoder\Utils;
use App\Models\VideoFile;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

class ProcessFixJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public  $stream, $file_in, $cmd3, $log, $folder;

    public function __construct($folder, $file_in)
    {
        $this->folder = $folder;
        $this->file_in = $file_in;
    }

    public function handle()
    {

        $FileController = new FileController();

        $re = '/(?<folder>.*\/)(?<file>.*)/';
        preg_match($re, $this->file_in, $match);

        if (isset($match)) {
            $this->root_folder = $match['folder'];
            $this->file = $match['file'];
            $this->playlist = $this->file_in;
            $this->files = [];

            $input_file = $this->root_folder . $match['file'];
            $output_file = str_replace(env('MEDIA_ROOT'), '/Media', $input_file);

            $regex_folder = str_replace(['//', '/', '(', ')'], ['/', '\\/', '\(', '\)'], $this->root_folder);
            $re = "/(?<root>$regex_folder)(?<folder>.*(?<type>audio|video)_(?<lang>\w{3}).*)\/(?<file>.*)/";

            foreach (collect($FileController->get_tv_files('out', $this->folder))->filter(function ($file) {
                return Utils::contains($file, array($this->root_folder)) !== false && Utils::contains($file, array($this->file)) == false ? $file : '';
            }) as $file) {
                $this->files[] = $file;
            }
            $content = [];
            $content['audio'] = [];
            $content['video'] = [];

            $content['audio'][] = "#EXTM3U\n\n";
            $default = "YES";
            $group = 'audio';

            foreach (languages()["lang_code"] as $language) {
                foreach ($this->files as $file) {

                    preg_match($re, $file, $match);

                    if (isset($match) && isset($match['root'])) {

                        $root = $match['root'];
                        $folder = $match['folder'];
                        $type = $match['type'];
                        $lang = $match['lang'] ?? null;
                        $file = $match['file'];


                        if ($match['type'] == 'audio') {
                            $input_file = $match[0];

                            $command = 'ffprobe -v quiet -show_format -show_streams -show_chapters -print_format json "' . $input_file . '"';
                            $ffprobe = json_decode(shell_exec($command), true);

                            if ($ffprobe == [] || $ffprobe == '' || $ffprobe == null || !isset($ffprobe['streams'])) {
                                dd("File needs moderation: " . $input_file);
                                return 1;
                            }

                            if ($lang != null && $lang == $language) {

                                if (isset($ffprobe['streams']) && isset($ffprobe['format']['duration'])) {

                                    foreach ($ffprobe['streams'] as $stream) {

                                        if ($stream['codec_type'] == 'audio') {
                                            if (isset($stream['codec_name']) && $stream['codec_name'] != "aac") {
                                                $reason[] = "Audio codec: " .  $stream['codec_name'];
                                                $move = true;
                                                dump('file not aac');
                                            } else {

                                                $name = replace_code_name($lang);

                                                $content['audio'][] = '#EXT-X-MEDIA:TYPE=AUDIO,GROUP-ID="' . $group . '",LANGUAGE="' . $lang . '",NAME="' . $name . '",DEFAULT=' . $default . ',AUTOSELECT=YES,URI="' . $folder . "/" . $folder . ".m3u8" . '"' . "\n";
                                                $default = "NO";
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if ($match['type'] == 'video') {
                            $input_file = $match[0];

                            $command = 'ffprobe -v quiet -show_format -show_streams -show_chapters -print_format json "' . $input_file . '"';
                            $ffprobe = json_decode(shell_exec($command), true);

                            if ($ffprobe == [] || $ffprobe == '' || $ffprobe == null || !isset($ffprobe['streams'])) {
                                dd("File needs moderation: " . $input_file);
                                return 1;
                            }

                            $duration = gmdate("H:i:s", $ffprobe['format']['duration'] ?? null) ?? '';
                            // VideoFile::updateOrCreate(["file" => $output_file], [
                            //     'duration' => $duration
                            // ]);

                            if (isset($ffprobe['streams']) && isset($ffprobe['format']['duration'])) {
                                foreach ($ffprobe['streams'] as $stream) {
                                    if ($stream['codec_type'] == 'video') {
                                        if (isset($stream['codec_name']) && $stream['codec_name'] != "h264") {
                                            $reason[] = "Video codec: " .  $stream['codec_name'];
                                            $move = true;
                                            dump('file not h264');
                                        } else {

                                            $bandwidth = floor(($ffprobe['format']['size'] ?? null / $ffprobe['format']['duration'] ?? null) * 60 * 45) ?? 520929;

                                            $video_width = $stream['width'];
                                            $video_height = $stream['height'];
                                            $resolution = $video_width . "x" . $video_height ?? '1920x1080';

                                            $content['video'][] = "\n" . '#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=' . $bandwidth . ',CODECS="avc1.4d4015,mp4a.40.2",AUDIO="' . $group . '",RESOLUTION=' . $resolution . "\n";
                                            $content['video'][] = 'video_' . $video_width . 'x' . $video_height . '/video_' . $video_width . 'x' . $video_height . '.m3u8';
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $content = array_merge($content['audio'], array_unique($content['video']));
            $content = implode('', $content);
        }
        // dump($content);
        dump('DEBUG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Updated playlist file: ' . $this->file_in);
        file_put_contents($this->file_in, $content, FILE_USE_INCLUDE_PATH);
    }
}
