<?php

namespace App\Jobs;

use App\Http\Controllers\Encoder\FileController;
use App\Http\Controllers\Encoder\Utils;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;
use App\Models\SupportedLanguages;
use App\Models\VideoFile;

class QueueFixJobs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $jobs, $folder;
    private $lang_code, $lang_name, $lang_country, $fallbackLanguage;

    public function __construct($jobs, $folder = '')
    {
        $this->jobs = $jobs;
        $this->folder = $folder;

        $this->lang_code = [];
        $this->lang_name = [];
        $this->lang_country = [];
        foreach (SupportedLanguages::select('language_code', 'language_name', 'country_code')->orderBy('language_name', 'asc')->get() as $code) {
            $this->lang_code[] = $code['language_code'];
            $this->lang_name[] = $code['language_name'];
            $this->lang_country[] = $code['country_code'];
        }
    }

    public function handle()
    {
        if ($this->jobs == 'fix_audio_format') {
            $video_file = VideoFile::orderBy('file', 'asc')
                // ->where('title', 'like', '%S.H.I.E.L.D%S03%')
                ->where('file', 'like', '%.mp4')
                ->get();

            foreach ($video_file as $video_file) {
                if (isset($video_file['file']) && $video_file['file'] != null) {

                    $file_in = str_replace("/Media", env('MEDIA_ROOT'), $video_file['file']);
                    $file_parts = pathinfo($file_in);
                    // dump('DEBUG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Checking: ' . $file_in);

                    if ($file_parts['extension'] == 'mp4') {

                        $command = 'ffprobe -v quiet -show_format -show_streams -show_chapters -print_format json "' . $file_in . '"';
                        $ffprobe = json_decode(shell_exec($command), true);


                        if (isset($ffprobe['streams'])) {
                            foreach ($ffprobe['streams'] as $stream) {
                                if ($stream['codec_type'] == "audio") {
                                    // if (isset($ffprobe['format']['tags']['encoder']) && $ffprobe['format']['tags']['encoder'] != 'Lavf58.20.100'  && !$ffprobe['format']['tags']['encoder'] != 'Lavf58.38.100') {
                                    //         // dump('DEBUG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Fixing audio for: ' . $file_in);
                                    //         $ProcessFixJob = (new ProcessFixJob($stream, $file_in, 'Audio codec: '. $ffprobe['format']['tags']['encoder']));
                                    //         dispatch($ProcessFixJob->onQueue('encoder'));

                                    // }
                                    if (isset($stream['channels']) && $stream['channels'] > 6) {
                                        // dump('DEBUG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Fixing audio for: ' . $file_in);
                                        $ProcessFixJob = (new ProcessFixJob($stream, $file_in, 'To many audio channels'));
                                        dispatch($ProcessFixJob->onQueue('encoder'));
                                    }
                                }
                            }
                        }
                    }

                    // // dump('DEBUG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . "");
                }
            }
        }

        if ($this->jobs == 'convert_playlist_to_valid') {

            function folderSize($dir)
            {
                $size = 0;
                foreach (glob(rtrim($dir, '/') . '/*', GLOB_NOSORT) as $each) {
                    $size += is_file($each) ? filesize($each) : folderSize($each);
                }
                return $size;
            }

            $renamed = array();
            $id = 1;
            $FileController = new FileController();
            foreach ($FileController->get_tv_files('out', $this->folder) as $file) {
                dd($file);

                $content = file_get_contents($file);
                $folder = preg_replace('/[^\/]*\.m3u8$/', '/video', $file);

                $re1 = '/^#EXT-X-MEDIA:TYPE=(["\']*)?(?<type>\w*)(["\']*)?,GROUP-ID=(["\']*)?(?<group>\w*)(["\']*)?,LANGUAGE=(["\']*)?(?<lang>\w*)(["\']*)?,NAME=(["\']*)?(?<name>\w*)(["\']*)?,DEFAULT=(["\']*)?(?<default>\w*)(["\']*)?,AUTOSELECT=(["\']*)?(?<auto>\w*)(["\']*)?,URI=(["\']*)?(?<uri>[\w\d\/\.]*)(["\']*)?/mi';
                preg_match_all($re1, $content, $matches1, PREG_SET_ORDER, 0);

                $re2 = '/.*BANDWIDTH=(["\']*)?(?<bandwidth>\d*)(["\']*)?.*RESOLUTION=(["\']*)?(?<resolution>\d*x\d*)(["\']*)?/m';
                preg_match($re2, $content, $matches2);

                if (isset($matches1) && isset($matches2)) {

                    $content = "#EXTM3U\n";
                    $content .= "\n";
                    $default = "YES";
                    $group = 'audio';
                    foreach ($matches1 as $match1) {
                        $lang = $match1['lang'];

                        if (!isset(${$lang}) || $lang == 'spa') {
                            $name = $match1['name'];
                            $uri = $match1['uri'];
                            $group = $group ? $group : $match1['group'];

                            if (isset($spa) && $spa == true) {
                                $lang = "cas";
                                $name = "Castillian";
                                $uri = str_replace("spa", "cas",  $uri);
                            }

                            $content .= '#EXT-X-MEDIA:TYPE=AUDIO,GROUP-ID="' . $group . '",LANGUAGE="' . $lang . '",NAME="' . $name . '",DEFAULT=' . $default . ',AUTOSELECT=YES,URI="' . $uri . '"' . "\n";
                            $default = "NO";
                        }
                        ${$lang} = true;
                    }
                    foreach ($this->lang_code as $lang) {
                        if (isset(${$lang})) {
                            unset(${$lang});
                        }
                    }

                    $folder_size = foldersize($folder);

                    $bandwidth = (int) floor($folder_size / 24 / 8);

                    $resolution = $matches2['resolution'] ?? '1920x1080';

                    $content .= "\n";
                    $content .= '#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=' . $bandwidth . ',CODECS="avc1.4d4015,mp4a.40.2",AUDIO="' . $group . '",RESOLUTION=' . $resolution . "\n";
                    $content .= "video/video.m3u8";


                    file_put_contents($file, $content, FILE_USE_INCLUDE_PATH);

                    $renamed[] = [
                        "$file" => $content
                    ];

                    $id++;
                }
            }
            // dd($renamed);
        }


        // if ($this->jobs == 'fix_broken_playlists') {

        //     $this->audio_lang_code = [];
        //     $this->audio_lang_name = [];
        //     $this->audio_lang_country = [];
        //     foreach (SupportedLanguages::select('language_code', 'language_name', 'country_code')->orderBy('audio_order', 'asc')->get() as $audio_code) {
        //         $this->audio_lang_code[] = $audio_code['language_code'];
        //         $this->audio_lang_name[] = $audio_code['language_name'];
        //         $this->audio_lang_country[] = $audio_code['country_code'];
        //     }


        //     $fileController = new FileController();
        //     foreach ($fileController->get_tv_files('out', $this->folder) as $file) {
        //         dump($file);
        //         $bandwidth = null;
        //         // $file_in = str_replace(['/Media'], [env('MEDIA_ROOT')], $file);
        //         // $file_out = $file['file'];

        //         $re = '/(?<folder>\/.*)\/(?<file>.*NoMercy\.m3u8)/m';
        //         preg_match($re, $file, $matches);
        //         if ($matches) {

        //             $folderRoot = $matches['folder'];

        //             $input_file = $folderRoot . '/' . $matches['file'];

        //             $output_file = str_replace(env('MEDIA_ROOT'), '/Media', $folderRoot . '/' . $matches['file']);

        //             $command = 'ffprobe -v quiet -show_format -show_streams -show_chapters -print_format json "' . $input_file . '"';
        //             $ffprobe = json_decode(shell_exec($command), true);
        //             // dd($ffprobe);

        //             if (isset($ffprobe['streams'])) {
        //                 $bandwidth = floor(($ffprobe['format']['size'] ?? null / $ffprobe['format']['duration'] ?? null) * 1024 * 16) ?? 520929;
        //                 $duration = gmdate("H:i:s", $ffprobe['format']['duration'] ?? null) ?? '';

        //                 $folders = scandir($folderRoot);
        //                 unset($folders[0]);
        //                 unset($folders[1]);

        //                 $content = "#EXTM3U\n";
        //                 $content .= "\n";
        //                 $default = "YES";
        //                 $group = 'audio';

        //                 foreach ($this->audio_lang_code as $lang) {

        //                     foreach ($folders as $folder) {

        //                         if (is_dir($folderRoot . "/" . $folder)) {

        //                             $re = '/audio_(?<language>.*)/i';
        //                             preg_match($re, $folder, $matches1);

        //                             if ($matches1) {

        //                                 if ($lang == $matches1['language']) {

        //                                     $name = str_replace($this->audio_lang_code, $this->audio_lang_name, $lang);

        //                                     $content .= '#EXT-X-MEDIA:TYPE=AUDIO,GROUP-ID="' . $group . '",LANGUAGE="' . $lang . '",NAME="' . $name . '",DEFAULT=' . $default . ',AUTOSELECT=YES,URI="' . $folder . "/" . $folder . ".m3u8" . '"' . "\n";
        //                                     $default = "NO";
        //                                 }
        //                             }
        //                         }
        //                     }
        //                 }

        //                 foreach ($ffprobe['streams'] as $stream) {
        //                     if ($stream['codec_type'] == "video" && $stream['codec_name'] !== "png" && $stream['codec_name'] !== "mjpeg") {

        //                         $video_width = $stream['width'];
        //                         $video_height = $stream['height'];
        //                         $resolution = $video_width . "x" . $video_height ?? '1920x1080';

        //                         $content .= "\n";
        //                         $content .= '#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=' . $bandwidth . ',CODECS="avc1.4d4015,mp4a.40.2",AUDIO="' . $group . '",RESOLUTION=' . $resolution . "\n";
        //                         $content .= 'video_' . $video_width . 'x' . $video_height . '/video_' . $video_width . 'x' . $video_height . '.m3u8';
        //                     }
        //                 }

        //                 VideoFile::updateOrCreate(["file" => $output_file], [
        //                     'duration' => $duration
        //                 ]);


        //                 // dd($sql);
        //                 dump($content);

        //                 dump('DEBUG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Updated playlist file: ' . $file);
        //                 file_put_contents($file, $content, FILE_USE_INCLUDE_PATH);
        //             } else {
        //                 dump('DEBUG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'Unable to process file: ' . $file);
        //                 // shell_exec('rm ' . $file_in);

        //                 VideoFile::updateOrCreate(["file" => $output_file], [
        //                     'duration' => null
        //                 ]);
        //             }
        //         }
        //     }
        // }

        if ($this->jobs == 'check_files_for_porper_codec') {

            $moved = array();
            $removed = array();
            foreach ((new FileController())->get_tv_files('out', '', ['mp4']) as $file) {
                $move = false;
                $reason = array();
                $command = 'ffprobe -v quiet -show_format -show_streams -show_chapters -print_format json "' . $file . '"';
                $ffprobe = json_decode(shell_exec($command), true);
                $re = '/(?<root>.*)\//m';

                if (isset($ffprobe['streams']) && isset($ffprobe['format']['duration'])) {
                    foreach ($ffprobe['streams'] as $stream) {
                        if ($stream['codec_type'] == 'video') {
                            if ($stream['width'] > 2000) {
                                $filePath = str_replace(['/TV.Shows/TV.Shows/'], ['/Marvels/Download/'], $file->getPath());
                                preg_match($re, $filePath, $matches);
                                $filePath = $matches['root'];
                                $fileOut = str_replace(['mp4'], ['mkv'], $filePath . "/" . $file->getFilename());
                            } else {
                                $filePath = str_replace(['/Anime/Anime/', '/TV.Shows/TV.Shows/'], ['/Anime/Download/', '/TV.Shows/Download/'], $file->getPath());
                                preg_match($re, $filePath, $matches);
                                $filePath = $matches['root'];
                                $fileOut = str_replace(['mp4'], ['mkv'], $filePath . "/" . $file->getFilename());
                            }
                        }
                    }
                    foreach ($ffprobe['streams'] as $stream) {
                        if ($stream['codec_type'] == 'video') {
                            if (isset($stream['codec_name']) && $stream['codec_name'] != "h264") {
                                $reason[] = "Video codec: " .  $stream['codec_name'];
                                $move = true;
                            }
                        }
                        if ($stream['codec_type'] == 'audio') {
                            if (isset($stream['codec_name']) && $stream['codec_name'] != "aac") {
                                $reason[] = "Audio codec: " .  $stream['codec_name'];
                                $move = true;
                            }
                        }
                        if ($stream['codec_type'] == 'subtitle') {
                            if (isset($stream['codec_name'])) {
                                $reason[] = "Subtitle codec: " .  $stream['codec_name'];
                                $move = true;
                            }
                        }
                    }
                } else {
                    $removed[] = [
                        "Removed" => $file
                    ];

                    Storage::disk('public')->append('/working/removed.json', json_encode($removed), 'public');
                    shell_exec("rm \"" . $file .  "\"");
                }

                if ($move == true) {
                    $moved[] = [
                        "Input" => $file,
                        "Output" => $fileOut,
                        "Dir" => $filePath,
                        "Reason" => $reason
                    ];
                    if (!is_dir($filePath)) {
                        mkdir($filePath, 0777, true);
                    }
                    Storage::disk('public')->put('/working/moved.json', json_encode($moved), 'public');
                    shell_exec("mv \"" . $file . "\" \"" . $fileOut .  "\"");
                }
            }
        }

        if ($this->jobs == 'check_double_videos') {
            $fileController = new FileController();
            foreach ($fileController->get_tv_files('out', $this->folder) as $file) {
                $re = '/(?<path>.*\/(?<filename>.*))\.(?<ext>\w{3,4})$/';
                preg_match($re, $file, $matches);
                if (file_exists($matches['path'] . '.mp4') && file_exists($matches['path'] . '.m3u8')) {

                    // dump('DEBUG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'working');
                    $command = 'ffprobe -v quiet -show_format -show_streams -show_chapters -print_format json "' . $matches['path'] . '.m3u8"';
                    $m3u8 = json_decode(shell_exec($command), true);
                    $count_m3u8 = 0;

                    if (isset($m3u8['streams'])) {
                        foreach ($m3u8['streams'] as $stream) {
                            if ($stream['codec_type'] == "audio") {
                                $count_m3u8++;
                            }
                        }
                    }
                    // dump('DEBUG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'm3u8: ' . $count_m3u8);

                    $command = 'ffprobe -v quiet -show_format -show_streams -show_chapters -print_format json "' . $matches['path'] . '.mp4"';
                    $mp4 = json_decode(shell_exec($command), true);
                    $count_mp4 = 0;

                    if (isset($mp4['streams'])) {
                        foreach ($mp4['streams'] as $stream) {
                            if ($stream['codec_type'] == "audio") {
                                $count_mp4++;
                            }
                        }
                    }
                    // dump('DEBUG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'mp4: ' . $count_mp4);

                    if ($count_m3u8 > $count_mp4 && $count_m3u8 != $count_mp4) {
                        // dump('DEBUG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'delete: "' . $matches['path'] . '.mp4"');
                    }
                    if ($count_m3u8 == $count_mp4) {
                        // dump('DEBUG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . 'delete: "' . $matches['path'] . '.m3u8"');
                    }

                    // dump('DEBUG: ' . pathinfo(__FILE__, PATHINFO_FILENAME) . ': '   . '');
                }
            }
        }

        if ($this->jobs == 'fix_broken_playlists') {

            $renamed = array();
            $id = 1;
            $FileController = new FileController();
            foreach (collect($FileController->get_tv_files('out', $this->folder))->filter(function ($file) {
                return Utils::contains($file, array('.m3u8')) !== false ? $file : '';
            }) as $file) {

                if (Utils::contains($file, array('NoMercy')) !== false) {
                    dump('File: ' . $file . ' added to queue for rebuilding');

                    $ProcessFixJob = (new ProcessFixJob($this->folder, $file));
                    dispatch($ProcessFixJob)->onQueue('file');
                }
            }
            unset($this->files);
        }
    }
}
