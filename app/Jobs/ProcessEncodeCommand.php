<?php

namespace App\Jobs;

use App\Http\Controllers\Encoder\Utils;
use App\Models\Episode;
use App\Models\Progress;
use App\Models\Tv;
use DateTime;
use DateInterval;
use DatePeriod;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProcessEncodeCommand implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $exec, $outputFolder, $fileName, $ffprobe, $encode_log, $split, $db_index_number;

    public function __construct($exec, $outputFolder, $fileName, $ffprobe, $encode_log, $split, $db_index_number)
    {
        $this->exec = $exec;
        $this->outputFolder = $outputFolder;
        $this->fileName = $fileName;
        $this->ffprobe = $ffprobe;
        $this->encode_log = $encode_log;
        $this->split = $split;
        $this->db_index_number = $db_index_number;
    }

    public function create_preview_file()
    {
        $thumbnailsFile = $this->outputFolder . "previews.vtt";

        if(!file_exists($thumbnailsFile)){

            shell_exec("mkdir -p \"" . $this->outputFolder . "thumbs\"");

            $thumbWidth     =   144;
            $thumbHeight    =   81;
            $imageFiles     =   array();
            $imageLine      =   floor(16000 / $thumbWidth) - 1;
            $maxLines      =    floor(16000 / $thumbHeight) - 1;
            $dst_x          =   0;
            $dst_y          =   0;

            foreach (glob($this->outputFolder . "thumbs/thumb-*.jpg") as $filename) {
                array_push($imageFiles, $filename);
            }

            $montage = "montage \"" .  $this->outputFolder . "thumbs/thumb-*.jpg\" -tile  " . $imageLine . "x" . floor((count($imageFiles) / $imageLine) + 1) . " -geometry 144x81+0+0 \"" . $this->outputFolder . "sprite.webp\"";

            $this->dump[] = $montage;

            natsort($imageFiles);
            $begin = new DateTime("00:00:00.00");
            $interval = DateInterval::createFromDateString('10 sec');
            $end = new DateTime($this->ffprobe->video->duration);
            $times = new DatePeriod($begin, $interval, $end);

            $jpg = 1;
            $line = 1;

            $thumb_content = "WEBVTT\n";
            foreach ($times as $time) {
                $thumb_content .= $jpg . "\n";
                $thumb_content .= str_replace("000000", "000", $time->format('H:i:s.u')) . " --> " . str_replace("000000", "000", $time->add($interval)->format('H:i:s.u'))  . "\n";
                $thumb_content .= "sprite.webp#xywh=" . $dst_x . "," . $dst_y . "," . $thumbWidth . "," . $thumbHeight . "\n\n";
                if ($line <= $maxLines) {
                    if ($jpg % $imageLine == 0) {
                        $dst_x = 0;
                        $dst_y += $thumbHeight;
                    } else {
                        $dst_x += $thumbWidth;
                    }
                    $jpg++;
                }
            }
            file_put_contents($thumbnailsFile, $thumb_content, FILE_USE_INCLUDE_PATH);
            shell_exec($montage);
        }

        dump('Deleting temporary images');
        shell_exec("rm -R \"" . $this->outputFolder . "thumbs\"");
    }

    public function handle()
    {

        Progress::updateOrCreate(['workerName' => env('WORKER_NAME')], [
            'workerIp' => env('WORKER_URL'),
            'workerName' => env('WORKER_NAME'),
            'active' => 1,
            'workers' => 2,
        ]);

        $index = 0;
        foreach ($this->exec as $cmd) {
            if (Utils::contains($cmd, array('-c:', '-map')) !== false) {

                Storage::disk('public')->put('/working/encoding/' . $this->db_index_number . '.txt', '');

                $content = [];
                $content['title'] = $this->fileName;
                $content['duration'] = $this->ffprobe->video->duration ?? null;
                $content['log'] = "Encoding: " . implode(', ', $this->encode_log[$index] ?? []);
                $content['img'] = Utils::contains($cmd, ["-s 144x81"]) !== false ? $this->outputFolder . "thumbs/thumb-*.jpg" : null;
                Storage::disk('public')->put('/working/active/' . $this->db_index_number . '.json', json_encode($content));
                if ($this->split) {
                    $index++;
                }

                $this->threads = floor(exec('nproc') / 2.5);
                $base = [
                    'ffmpeg',
                    '-threads ' . $this->threads,
                    '-v quiet',
                    '-progress - -nostats',
                ];

                $base = implode(' ', $base);
                $cmd = str_replace(['$', "'", "\\"], ['\$', "\'", ""], $cmd);
                dump($base . ' ' . $cmd);
                shell_exec($base . ' ' . $cmd);

                Storage::disk('public')->delete('/working/encoding/' . $this->db_index_number . '.txt');
                Storage::disk('public')->delete('/working/active/' . $this->db_index_number . '.json');

                if (contains($cmd, ["-s 144x81"]) !== false && contains($cmd, ["bt2080"]) === false) {
                    $this->create_preview_file();
                } else {
                    shell_exec("rm -R \"" . $this->outputFolder . "thumbs\"");
                }
            } else {
                dump('nothing to do for: ' . $this->fileName);
            }
        }

        $episode = Episode::where('id', $this->db_index_number)->with('video_file')->first();
        if($episode != null){
            $episode->video_file->update([
                "duration" => $this->ffprobe->video->duration,
            ]);
            $tv = Tv::where('id', $episode->show_id)
                ->with('seasons.episodes.video_file')
                ->first();

            $languages = [];

            $count = 0;
            if (isset($tv['seasons'])) {
                foreach ($tv['seasons'] as $season) {
                    if ($season['season_number'] > 0) {
                        foreach ($season['episodes'] as $episode) {
                            if ($episode['video_file'] != null && isset($episode['video_file']) && $episode['video_file']['duration'] != null) {
                                $count++;
                                foreach (explode(',', $episode['video_file']['languages']) as $lang) {
                                    $languages[] = $lang;
                                }
                            }
                        }
                    }
                }
            }

            $languages = array_unique($languages);

            $tv->updateOrCreate(['id' => $episode->show_id], [
                'have_episodes' => $count,
                'languages' => implode(', ', $languages),
                "updated_at" => now()
            ]);
        }

        // Tv::updateOrCreate(['id' => $episode->show_id], [
        //     'have_episodes' => $count,
        //     'languages' => implode(', ', $languages),
        //     "updated_at" => now()
        // ]);

        $sql = "DELETE FROM `failed_jobs` WHERE  `exception` like '%MaxAttemptsExceededException%'";
        DB::statement($sql);
    }
}
