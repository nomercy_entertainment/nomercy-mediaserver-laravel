<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExternalIds extends Model
{
    protected $guarded = [];
    public $table = 'external_ids';
}
