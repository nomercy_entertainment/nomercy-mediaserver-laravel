<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FolderRoots extends Model
{
    protected $table = 'folder_roots';
    protected $guarded = [];
    public $primaryKey = 'id';
    public $timestamps = false;
}
