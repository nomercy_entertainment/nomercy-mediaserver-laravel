<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoFile extends Model
{
    protected $table = 'video_file';
    protected $guarded = [];

    // Primary Key
    public $primaryKey = 'id';

    public function episode()
    {
        return $this->belongsTo(Episode::class);
    }

    public function movie()
    {
        return $this->hasOne(Movie::class);
    }

    public function user_video()
    {
        return $this->hasOne(UserVideo::class, 'episode_id', 'episode_id')->orderBy('updated_at', 'DESC');
    }

    public function user()
    {
        return $this->hasManyThrough(Episode::class, UserVideo::class,  'id');
    }

    // public function getfileAttribute()
    // {
    //     return env('APP_URL') . $this->attributes['file'];
    // }

}
