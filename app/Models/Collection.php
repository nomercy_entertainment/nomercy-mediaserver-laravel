<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'id';
    protected $table = 'collection';

    public function movies()
    {
        return $this->belongsToMany(Movie::class);
    }

    public function getTypeAttribute()
    {
        return 'collection';
    }

    // public function getPosterAttribute()
    // {
    //     return $this->attributes['poster'] != null && env('DOWNLOAD_IMAGES')
    //         ? asset('/assets/tv' . $this->attributes['poster'])
    //         : img_url($this->attributes['poster'], 'original');
    // }
}
