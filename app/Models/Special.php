<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Special extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'id';
    public $table = 'special';

    public function media()
    {
        return $this->hasMany(Media::class)
            ->orderBy('order')
            ->orderBy('movie_id')
            ->orderBy('season_id')
            ->orderBy('id')
            ->orderBy('episode_id');
    }
}
