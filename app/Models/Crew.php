<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Crew extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $table = 'crew';

    public function tv()
    {
        return $this->belongsToMany(Tv::class);
    }

    public function season()
    {
        return $this->belongsToMany(Season::class);
    }

    public function episode()
    {
        return $this->belongsToMany(Episode::class);
    }

    public function movie()
    {
        return $this->belongsToMany(Movie::class);
    }



//Mutators
    public function getProfilePathAttribute()
    {
        return $this->attributes['profile_path'] != null ? 'https://image.tmdb.org/t/p/original' . $this->attributes['profile_path'] : null;
    }
}
