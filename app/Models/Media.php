<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';
    protected $guarded = [];
    public $primaryKey = 'id';
    public $timestamps = false;

    public function tv()
    {
        return $this->belongsToMany(Tv::class);
    }

    public function season()
    {
        return $this->belongsToMany(Season::class);
    }

    public function episode()
    {
        return $this->belongsToMany(Episode::class);
    }

    public function movie()
    {
        return $this->belongsToMany(Movie::class);
    }

    public function getFilePathAttribute()
    {
        return $this->attributes['file_path'] != null && config('themoviedb.download_images')
            ? asset('/assets/tv' . str_replace('.jpg', '.original.jpg', $this->attributes['file_path']))
            : ($this->attributes['file_path'] != null ? img_url($this->attributes['file_path'], 'original') : null);
    }

    public function getKeyAttribute()
    {
        return $this->attributes['key'] != null
            ? 'https://www.youtube.com/embed/' . $this->attributes['key'] . '?ecver=1&iv_load_policy=3&rel=0&showinfo=0&yt:stretch=16:9&autohide=1&color=white&width=560&width=560&controls=0&modestbranding=1'
            : null;
    }

    public function getTrailerAttribute()
    {
        return config('themoviedb.download_trailers') && file_exists(storage_path('app/asset/trailers/'. $this->id . '.trailer.mp4'))
            ? asset('assets/trailer/'. $this->id . '.trailer.mp4')
            : 'https://www.youtube.com/embed/' . $this->trailer()->pluck('key')->first();
    }
}
