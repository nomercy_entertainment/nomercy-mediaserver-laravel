<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'id';
    public $table = 'server';




    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
