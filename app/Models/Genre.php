<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $table = 'genre';
   
    protected $hidden = [
        'created_at',
        'updated_at',
        'genre_id',
        'pivot'
    ];

    public function movie()
    {
        return $this->belongsToMany(Movie::class);
    }

    public function tv()
    {
        return $this->belongsToMany(Tv::class);
    }
}
