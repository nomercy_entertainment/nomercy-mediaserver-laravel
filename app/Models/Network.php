<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Network extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $table = 'network';

    public function tv()
    {
        return $this->belongsToMany(Tv::class);
    }
    public function movie()
    {
        return $this->belongsToMany(Movie::class);
    }
}
