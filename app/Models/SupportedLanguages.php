<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportedLanguages extends Model
{
    protected $table = 'supported_languages';
    protected $guarded = [];
    public $timestamps = false;
}
