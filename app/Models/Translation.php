<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class Translation extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    public $table = 'translation';

    public function tv()
    {
        return $this->belongsToMany(Tv::class);
    }

    public function season()
    {
        return $this->belongsToMany(Season::class);
    }

    public function episode()
    {
        return $this->belongsToMany(Episode::class);
    }

    public function movie()
    {
        return $this->belongsToMany(Movie::class);
    }
}
