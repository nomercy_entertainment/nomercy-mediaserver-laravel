<?php

namespace App\Models;

use Illuminate\Support\Facades\Lang;
use Nomercy\Themoviedb\Facades\Helper;
use Illuminate\Database\Eloquent\Model;

class Season extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'id';
    public $table = 'season';

    public function tv()
    {
        return $this->belongsTo(Tv::class);
    }

    public function episodes()
    {
        return $this->hasMany(Episode::class)
            ->orderBy('episode_number');
    }

    public function person()
    {
        // return $this->belongsToMany(Person::class);
    }

    public function cast()
    {
        return $this->belongsToMany(Cast::class)
            ->orderBy('order');
    }

    public function crew()
    {
        return $this->belongsToMany(Crew::class, 'crew_season', 'crew_id', 'season_id');
    }

    public function translation()
    {
        return $this->hasMany(Translation::class)
            ->where('iso_639_1', app()->getLocale())
            ->where('iso_639_1', '!=', 'en');
    }


    public function user_video()
    {
        // return $this->hasOne(UserVideo::class, 'tmdbid', 'tv_id')->orderBy('updated_at', 'DESC');
    }

    public function user()
    {
        return $this->hasManyThrough(VideoFile::class, UserVideo::class,  'id');
    }

    public function media()
    {
        return $this->hasMany(Media::class);
    }

    public function getDirectorsAttribute()
    {
        // Get all episodes with crew data.
        $tv = Season::where('tv_id', $this->id)
            ->with('crew')
            ->get();

        // Loop trough every episode.
        $crew = [];
        foreach ($tv as $episode) {
            foreach ($episode->crew as $item) {
                // Filter on Directors.
                if ($item->job == 'Director') {
                    $crew[] = $item->name;
                }
            }
        }
        // sort directors by amount of episodes directed and order them from high to low.
        $count = array_count_values($crew);
        arsort($count);

        // Take only directors with a minimum of 10.
        foreach ($count as $item => $amount) {
            if ($amount > 10) {
                $director[] = $item;
            }
        }
        // Implode directors to single line comma sepparated and add a space after comma.
        return str_replace(',', ', ', implode(",", $director));
    }

    public function getTypeAttribute()
    {
        return 'tv';
    }

    public function getPosterAttribute()
    {
        return $this->attributes['poster'] != null && config('themoviedb.download_images')
            ? asset('/assets/tv' . $this->attributes['poster'])
            : img_url($this->attributes['poster'], 'w300');
    }

    // public function getTitleAttribute()
    // {
    //     $trans = $this->translation()
    //     ->pluck('title')
    //     ->first();
    //     return $trans != null
    //         ? $trans
    //         : $this->attributes['title'];
    // }

    // public function getOverviewAttribute()
    // {
    //     $trans = $this->translation()
    //     ->pluck('overview')
    //     ->first();
    //     return $trans != null
    //         ? $trans
    //         : $this->attributes['overview'];
    // }


}
