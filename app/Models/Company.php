<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $table = 'company';

    public function movie()
    {
        return $this->belongsToMany(Movie::class);
    }

    public function tv()
    {
        return $this->belongsToMany(Tv::class);
    }
}
