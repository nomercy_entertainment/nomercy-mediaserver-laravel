<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $table = 'person';

    public function tv()
    {
        return $this->belongsToMany(Tv::class);
    }

    public function season()
    {
        return $this->belongsToMany(Season::class);
    }

    public function episode()
    {
        return $this->belongsToMany(Episode::class);
    }

    public function movie()
    {
        return $this->belongsToMany(Movie::class);
    }
}
