<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GuestStar extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'id';
    public $timestamps = false;
    public $table = 'cast';

    public function episode()
    {
        return $this->belongsToMany(Episode::class);
    }
}
