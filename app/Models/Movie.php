<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class Movie extends Model
{
    protected $guarded = [];
    protected $primaryKey = 'id';
    public $table = 'movie';

    public function genres()
    {
        return $this->belongsToMany(Genre::class)
            ->where('iso_639_1', app()->getLocale());
    }
    public function cast()
    {
        return $this->belongsToMany(Cast::class)->orderBy('order');
    }
    public function crew()
    {
        return $this->belongsToMany(Crew::class);
    }
    public function companies()
    {
        return $this->belongsToMany(Company::class);
    }
    public function countries()
    {
        return $this->belongsToMany(Company::class);
    }
    public function collection()
    {
        return $this->belongsToMany(Collection::class);
    }
    public function video_file()
    {
        return $this->hasOne(VideoFile::class);
    }

    public function user_video()
    {
        return $this->hasOne(UserVideo::class, 'tmdbid', 'id')->orderBy('updated_at', 'DESC');
    }

    public function media()
    {
        return $this->belongsToMany(Media::class);
    }

    public function trailers()
    {
        return $this->belongsToMany(Media::class)
            ->where('type', 'Trailer');
    }

    // public function getReleaseDateAttribute()
    // {
    //     return Carbon::parse($this->attributes['release_date'])
    //         ->format('Y');
    // }


    public function getSubtitlesAttribute()
    {

        $subtitles = [];

        $arr = $this->where('id', $this->id)->with('video_file')->first();

        foreach(collect(explode(',', $arr->video_file->subtitles ?? '')) as $sub){
            if($sub != ''){
                $subtitles[] = $sub;
            }
        }

        usort($subtitles, function ($a, $b) {
            if (isset($a)) {
                return $a <=> $b;
            }
        });

        if (count($subtitles) == 0) {
            $subtitles[] = 'none';
        }
        else {
            $subtitles = collect($subtitles)->unique()->values();
        }

        return $subtitles;
    }

    public function getAudioAttribute()
    {

        $audio = explode(', ', $this->attributes['languages']);
        if (count($audio) == 0) {
            $audio[] = 'none';
        }

        return $audio;
    }

    public function getOriginAttribute()
    {
        return env('APP_URL');
    }

    public function getDirectorAttribute()
    {
        $director = [];
        foreach ($this->crew as $item) {
            if ($item->job == 'Director' || $item->job == 'Directing') {
                $director[] = $item->name;
            } else if (strpos($item->job, 'Director') == true) {
                $director[] = $item->name;
            }
        }
        $director = array_unique($director);
        return $director;
    }

    public function getDirectorsAttribute()
    {
        $director = [];
        foreach ($this->crew as $item) {
            if ($item->job == 'Director' || $item->job == 'Directing') {
                $director[] = $item;
            } else if (strpos($item->job, 'Director') == true) {
                $director[] = $item;
            }
        }
        $director = array_unique($director);
        return collect($director)->sortBy('order')->take(5);
    }

    public function getLeadActorsAttribute()
    {
        $actors = $this->cast->sortBy('order')->take(5);
        return $actors;
    }

    public function getDurationAttribute()
    {
        $date = $this->hoursandmins((int) $this->attributes['duration']);
        return $date;
    }

    public function getQualitiesAttribute()
    {

        $array = explode(',', str_replace(['[', ']', "'"], '', $this->quality));

        usort($array, function ($a, $b) {
            if (isset($a)) {
                return $a <=> $b;
            }
        });

        return implode(",", $array);
    }

    public function getTypeAttribute()
    {
        return 'movie';
    }


    function hoursandmins($time)
    {
        if ($time < 1) {
            return;
        }
        $hours = (int) floor($time / 60);
        $minutes = ($time % 60);

        if ($hours > 0 && $minutes != 0) {
            $format = '%01d ' . __('ui.hours') . ', %02d ' . __('ui.minutes');
            return sprintf($format, $hours, $minutes);
        }
        else if ($hours > 0 && $minutes == 0) {
            $format = '%01d ' . __('ui.hours');
            return sprintf($format, $hours);
        }
        else {
            $format = '%01d ' . __('ui.minutes');
            return sprintf($format, $minutes);
        }
    }

    public function getGenresAttribute()
    {
        return $this->genres()->pluck('name')->implode(', ');
    }

    public function getTitleSortAttribute()
    {
        return preg_replace('/(\s:.*)|(\sAnd\sThe.*)/', '', $this->attributes['title_sort']);
    }

    // public function getBackdropAttribute()
    // {
    //     return $this->attributes['backdrop'] != null && env('DOWNLOAD_IMAGES')
    //         ? asset('/assets/tv' . str_replace('.jpg', '.original.jpg', $this->attributes['backdrop']))
    //         : img_url($this->attributes['backdrop'], 'original');
    // }

    public function getPosterAttribute()
    {
        return config('themoviedb.download_images') && $this->attributes['poster'] != null
            ? asset('/assets/tv' . $this->attributes['poster'])
            : img_url($this->attributes['poster'], 'w185');
    }

    public function getTrailerAttribute()
    {
        return config('themoviedb.download_images') && file_exists(storage_path('app/asset/trailers/'. $this->id . '.trailer.mp4'))
            ? asset('assets/trailer/'. $this->id . '.trailer.mp4')
            : $this->attributes['trailer'];
    }



}
