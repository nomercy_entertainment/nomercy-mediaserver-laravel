<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Support\Facades\Lang;
use Nomercy\Themoviedb\Facades\Helper;
use Illuminate\Database\Eloquent\Model;

class Tv extends Model
{
    protected $table = 'tv';
    protected $guarded = [];
    public $primaryKey = 'id';

    public function seasons()
    {
        return $this->hasMany(Season::class)
            ->orderBy('season_number');
    }

    public function persons()
    {
        return $this->belongsToMany(Person::class);
    }

    public function cast()
    {
        return $this->belongsToMany(Cast::class)->orderBy('order');
    }

    public function crew()
    {
        return $this->belongsToMany(Crew::class);
    }

    public function genres()
    {
        return $this->belongsToMany(Genre::class)
            ->where('iso_639_1', app()->getLocale());
    }
    public function creators()
    {
        return $this->belongsToMany(Person::class);
    }
    public function networks()
    {
        return $this->belongsToMany(Network::class);
    }
    public function companies()
    {
        return $this->belongsToMany(Company::class);
    }

    public function translation()
    {
        return $this->hasMany(Translation::class)
            ->where('iso_639_1', app()->getLocale())
            ->where('iso_639_1', '!=', 'en');
    }

    public function user_video()
    {
        return $this->hasOne(UserVideo::class, 'tmdbid', 'id')->orderBy('updated_at', 'DESC');
    }

    public function user()
    {
        return $this->hasManyThrough(Episode::class, UserVideo::class,  'id');
    }

    public function media()
    {
        return $this->hasMany(Media::class);
    }
    public function posters()
    {
        return $this->hasMany(Media::class)->where('type', 'poster');
    }
    public function banners()
    {
        return $this->hasMany(Media::class)->where('type', 'banner');
    }
    public function trailers()
    {
        return $this->belongsToMany(Media::class)
            ->where('type', 'Trailer');
    }

    public function getTypeAttribute()
    {
        return 'tv';
    }

    public function getYearAttribute()
    {
        return Carbon::parse($this->attributes['first_air_date'])
            ->format('Y');
    }
    public function getFirstAirDateAttribute()
    {
        return Carbon::parse($this->attributes['first_air_date'])
            ->format('Y');
    }

    public function getSubtitlesAttribute()
    {

        $table = collect($this->video_file);
        $subtitles = [];

        $arr = Episode::where('show_id', $this->id)->with('video_file')->get();

        foreach($arr as $data){
            foreach(collect(explode(',', $data->video_file->subtitles ?? '')) as $sub){
                if($sub != ''){
                    $subtitles[] = $sub;
                }
            }
        }

        usort($subtitles, function ($a, $b) {
            if (isset($a)) {
                return $a <=> $b;
            }
        });

        if (count($subtitles) == 0) {
            $subtitles[] = 'none';
        }
        else {
            $subtitles = collect($subtitles)->unique()->values();
        }


        return $subtitles;
    }

    public function getAudioAttribute()
    {

        $audio = explode(', ', $this->attributes['languages']);
        if (count($audio) == 0) {
            $audio[] = 'none';
        }

        return $audio;
    }

    public function getOriginAttribute()
    {
        return env('APP_URL');
    }

    public function getDirectorAttribute()
    {
        $director = [];
        foreach ($this->crew as $item) {
            if ($item->job == 'Director' || $item->job == 'Directing') {
                $director[] = $item;
            } else if (strpos($item->job, 'Director') == true) {
                $director[] = $item;
            }
        }
        $director = array_unique($director);
        return $director;
    }

    public function getDirectorsAttribute()
    {
        $director = [];
        foreach ($this->crew as $item) {
            if ($item->job == 'Director' || $item->job == 'Directing') {
                $director[] = $item;
            } else if (strpos($item->job, 'Director') == true) {
                $director[] = $item;
            }
        }
        $director = array_unique($director);
        return collect($director)->sortBy('order')->take(5);
    }

    public function getLeadActorsAttribute()
    {
        $actors = $this->cast->sortBy('order')->take(5);
        return $actors;
    }

    public function getDurationAttribute()
    {
        $runtime = $this->hoursandmins((int) $this->attributes['runtime']);
        return $runtime;
    }

    public function getQualitiesAttribute()
    {

        $array = explode(',', str_replace(['[', ']', "'"], '', $this->quality));

        usort($array, function ($a, $b) {
            if (isset($a)) {
                return $a <=> $b;
            }
        });

        return implode(",", $array);
    }

    function hoursandmins($time)
    {
        if ($time < 1) {
            return;
        }
        $hours = (int) floor($time / 60);
        $minutes = ($time % 60);

        if ($hours > 0 && $minutes != 0) {
            $format = '%01d ' . __('ui.hours') . ', %02d ' . __('ui.minutes');
            return sprintf($format, $hours, $minutes);
        }
        else if ($hours > 0 && $minutes == 0) {
            $format = '%01d ' . __('ui.hours');
            return sprintf($format, $hours);
        }
        else {
            $format = '%01d ' . __('ui.minutes');
            return sprintf($format, $minutes);
        }
    }

    public function getGenresAttribute()
    {
        return $this->genres()->pluck('name')->implode(', ');
    }

    public function getBackdropAttribute()
    {
        return $this->attributes['backdrop'] != null && env('DOWNLOAD_IMAGES')
            ? asset('/assets/tv' . str_replace('.jpg', '.original.jpg', $this->attributes['backdrop']))
            : img_url($this->attributes['backdrop'], 'original');
    }
    public function getPosterAttribute()
    {
        return $this->attributes['poster'] != null && env('DOWNLOAD_IMAGES')
            ? asset('/assets/tv' . $this->attributes['poster'])
            : img_url($this->attributes['poster'], 'original');
    }

    public function getTrailerAttribute()
    {
        return config('themoviedb.download_images') && file_exists(storage_path('app/asset/trailers/'. $this->id . '.trailer.mp4'))
            ? asset('assets/trailer/'. $this->id . '.trailer.mp4')
            : $this->attributes['trailer'];
    }


    public function getPercentageAttribute()
    {
        return $this->have_episodes !=0 ? ($this->have_episodes / $this->number_of_episodes) * 100 : 0;
    }

    public function getTitleAttribute()
    {
        $trans = $this->translation()
        ->pluck('title')
        ->first();
        return $trans != null
            ? $trans
            : $this->attributes['title'];
    }

    public function getOverviewAttribute()
    {
        $trans = $this->translation()
        ->pluck('overview')
        ->first();
        return $trans != null
            ? $trans
            : $this->attributes['overview'];
    }


}
