<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\VideoFile;

class UserVideo extends Model
{
    protected $guarded = [];

    public function video_file()
    {
        return $this->belongsTo(VideoFile::class, 'episode_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'episode_id');
    }
}
