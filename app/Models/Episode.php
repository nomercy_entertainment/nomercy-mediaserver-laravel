<?php

namespace App\Models;

use Illuminate\Support\Facades\Lang;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\Console\Helper\Helper;

class Episode extends Model
{
    protected $guarded = [];
    protected $orderBy = [
        'season',
        'episode',
    ];
    protected $orderDirection = 'ASC';
    protected $primaryKey = 'id';
    public $table = 'episode';

    public function tv()
    {
        return $this->belongsTo(Tv::class);
    }

    public function season()
    {
        return $this->belongsTo(Season::class)
            ->orderBy('season_id')
            ->orderBy('episode_id');;
    }

    public function video_file()
    {
        return $this->hasOne(VideoFile::class);
    }

    public function person()
    {
        return $this->belongsToMany(Person::class);
    }

    public function cast()
    {
        return $this->belongsToMany(Cast::class)
            ->orderBy('order');
    }

    public function crew()
    {
        return $this->belongsToMany(Crew::class);
    }

    public function guest_star()
    {
        return $this->belongsToMany(GuestStar::class)
            ->orderBy('order');
    }

    public function getSeasonAttribute()
    {
        return sprintf("%02d", $this['season_number']);
    }

    public function getEpisodeAttribute()
    {
        return sprintf("%02d", $this['episode_number']);
    }

    public function translation()
    {
        return $this->hasMany(Translation::class)
            ->where('iso_639_1', app()->getLocale())
            ->where('iso_639_1', '!=', 'en');
    }

    // public function getTitleAttribute()
    // {
    //     return $this->translation()->pluck('title')->first() ?? $this->attributes['title'] ?? null;
    // }

    // public function getOverviewAttribute()
    // {
    //     return $this->translation()->pluck('overview')->first() != null ? $this->translation()->pluck('overview')->first() : $this->attributes['overview'] ?? null;
    // }


    // public function getStillAttribute()
    // {
    //     return $this->attributes['still'] != null && env('DOWNLOAD_IMAGES') ? asset('/assets/tv' . $this->attributes['still']) : img_url($this->attributes['still'], 'original');
    // }

    // public function getStillPathAttribute()
    // {
    //     return $this->attributes['still'] != null && env('DOWNLOAD_IMAGES') ? asset('/assets/tv' . $this->attributes['still']) : img_url($this->attributes['still'], 'original');
    // }

    public function getStillAttribute()
    {
        return $this->attributes['still'] != null && config('themoviedb.download_images')
            ? asset('/assets/tv' . $this->attributes['still'])
            : img_url($this->attributes['still'], 'original');
    }

    public function getStillPathAttribute()
    {
        return $this->attributes['still'] != null && config('themoviedb.download_images')
            ? asset('/assets/tv' . $this->attributes['still'])
            : img_url($this->attributes['still'], 'original');
    }


    // public function getTitleAttribute()
    // {
    //     $trans = $this->translation()
    //     ->pluck('title')
    //     ->first();
    //     return $trans != null
    //         ? $trans
    //         : $this->attributes['title'];
    // }

    // public function getOverviewAttribute()
    // {
    //     $trans = $this->translation()
    //     ->pluck('overview')
    //     ->first();
    //     return $trans != null
    //         ? $trans
    //         : $this->attributes['overview'];
    // }

}
