<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workers extends Model
{
    protected $table = 'progress';
    protected $guarded = [];
    public $primaryKey = 'id';
    public $timestamps = false;
}
