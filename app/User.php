<?php

namespace App;

use App\Models\Server;
use App\Models\Stream;
use App\Models\UserVideo;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements MustVerifyEmail, JWTSubject
{
    use Notifiable;

    protected $connection = 'auth';

    protected $fillable = [
        'id',
        'name',
        'email',
        'password',
        'api_token',
        'isOwner',
        'preffered_lang',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public $incrementing = false;


    public function server()
    {
        return $this->hasMany(Server::class , "owner_id");
    }

    public function stream()
    {
        return $this->hasOne(Stream::class);
    }

    public function user_video()
    {
        return $this->hasMany(UserVideo::class);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function getGravatarAttribute()
    {
        $hash = md5(strtolower(trim($this->attributes['email'])));
        return "https://www.gravatar.com/avatar/" . $hash;
    }
}
