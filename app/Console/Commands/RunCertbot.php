<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RunCertbot extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'nomercy:certbot';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Encrypt your server with a SSL sertificate from Let's Encrypt";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return shell_exec('docker exec -it nomercymediaserver /usr/bin/letsencrypt-setup');
    }
}
