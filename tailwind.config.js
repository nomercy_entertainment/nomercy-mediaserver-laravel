
const { colors, fontSize, opacity } = require('tailwindcss/defaultTheme')
const mix = require('laravel-mix');

module.exports = {
    theme: {
        extend: {
            colors: {
                gray: {
                    ...colors.gray,
                    '600': '#696666',
                    '700': '#404040',
                    '800': '#282828',
                    '900': '#111111',
                },
                purple: {
                    ...colors.purple,
                    '600': '#9a1dce',
                }
            },
            screens: {
                'lg': '1280px',
                'xl': '1366px',
                '2xl': '1920px',
                '3xl': '3000px',
            },
            fontSize: {
                '2xs': '.65rem',
                '3xs': '.55rem',
            },
            backgroundOpacity: {
                '95': '0.95',
                '90': '0.90',
                '85': '0.85',
                '80': '0.80',
            },
            opacity: {
                '95': '0.95',
                '90': '0.90',
                '85': '0.85',
                '80': '0.80',
            }
        }
    },
    purge: [
        mix.inProduction(),
        './resources/views/**/*.blade.php',
        './resources/css/**/*.css',
    ],
    variants: {},
    plugins: [
        require('@tailwindcss/custom-forms')
    ],
    future: {
        removeDeprecatedGapUtilities: true,
        purgeLayersByDefault: true,
    }
}
