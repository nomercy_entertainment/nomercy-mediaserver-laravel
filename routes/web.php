<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;

Auth::routes([
    'verify' => env('MUST_VERIFY_EMAIL', false),
    'register' => false,
    'reset' => false,
]);

Route::group([
    'middleware' => [
        'auth',
        'setup',
    ],
], function () {

    Route::get('password/update', 'Auth\UpdatePasswordController@showUpdateForm')->name('password.update');

    Route::group([
        'namespace' => '\App\Http\Controllers\Content'
    ], function () {
        Route::get('/',                         'WebController@index')->name('index');
        Route::get('/movies',                   'WebController@movies')->name('movies');
        Route::get('/anime',                    'WebController@anime')->name('anime');
        Route::get('/specials',                 'WebController@specials')->name('specials');
        Route::get('/collections',              'WebController@collections')->name('collections');
        Route::get('/collection/{id}',          'WebController@collection')->name('collection');
        Route::get('/genres',                   'WebController@genres')->name('genres');
        Route::get('/info',                     'WebController@info')->name('info');

        Route::get('/tv',                       'WebController@tv')->name('tv');
        Route::get('/tv/{id}/info',             'WebController@tv_info')->name('tv_info');
        Route::get('/tv/{id}/watch',            'WebController@watch_videojs')->name('watch_jwplayer');
        Route::get('/tv/{id}/play',            'WebController@watch_jwplayer')->name('watch_jwplayer');

        Route::get('/movie',                    'WebController@movie')->name('movie');
        Route::get('/movie/{id}/info',          'WebController@movie_info')->name('movie_info');
        Route::get('/movie/{id}/watch',         'WebController@watch_videojs')->name('watch_videojs');
        Route::get('/movie/{id}/play',         'WebController@watch_jwplayer')->name('watch_videojs');

        // Route::get('/play',                     'WebController@watch_jwplayer')->name('watch_jwplayer');
        // Route::get('/watch',                    'WebController@watch_videojs')->name('watch_videojs');
        Route::get('/remote',                   'WebController@remote')->name('remote');


    });

    Route::get('/test',                         'PagesController@test')->name('test');

    Route::group([
        'namespace' => '\App\Http\Controllers\Player',
    ], function () {

        Route::get('/playlist',                 'PlayerController@playlist')->name('playlist');
        Route::get('/video',                    'PlayerController@video')->name('video');

        Route::post('/sync/get',                'UserVideoController@get');
        Route::post('/sync/set',                'UserVideoController@set');
    });

});

Route::get('/streams/{channel}',        '\App\Http\Controllers\Player\LiveStreamController@watch')->name('live.watch');
Route::get('/obsninja',                 '\App\Http\Controllers\Player\LiveStreamController@obsninja')->name('live.obsninja');


Route::get('/cpuusage',         [DashboardController::class, 'cpuusage'])->name('admin.cpuusage');

Route::get('js/translations.js', function () {
    $lang = app()->getLocale();
    // \Illuminate\Support\Facades\Cache::forget('lang_'.$lang.'.js');
    $strings = \Illuminate\Support\Facades\Cache::rememberForever('lang_'.$lang.'.js', function () use($lang) {
        $files = [
            resource_path('lang/' . $lang . '/languages.php'),
        ];
        $strings = [];

        foreach ($files as $file) {
            $name = basename($file, '.php');
            $strings[] = require $file;
        }

        return $strings[0];
    });
    header('Content-Type: text/javascript');
    echo(json_encode($strings));
    exit();
})->name('translations');
