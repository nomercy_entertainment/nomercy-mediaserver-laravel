<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => [
        'api',
        'throttle:500,1'
    ],
    // 'as' => 'api'

], function () {
    Route::group([
        'namespace' => '\App\Http\Controllers\Monitors',
    ], function () {
        Route::post('/system/utilisation',       'SystemInfoController@cpuUtilisation')->name('util');
        Route::post('/queue/{limit}',            'ProgressController@queue')->name('queue');
        Route::post('/progress_list',            'ProgressController@progress_list')->name('progress_list');
        Route::post('/queue_list',               'ProgressController@queue_list')->name('queue_list');

    });
    Route::group([
        'namespace' => '\App\Http\Controllers\Content',
    ], function () {
        Route::post('/get_servers',     'ServerController@get_servers')->name('setup.getservers');
    });
    Route::group([
        'namespace' => 'Encoder'
    ], function () {
        Route::post('find-tv/{direction}',       'FileController@find_tv_media')->name('find_tv_media');
        Route::post('find-movie/{direction}',    'FileController@find_movies')->name('find_movie_media');
        Route::post('/progress',                 'Progress@progress')->name('progress');
    });
    Route::group([
        'namespace' => '\App\Http\Controllers\Content'
    ], function () {

        Route::get('/',                 'ApiController@index');
        Route::get('/anime',            'ApiController@anime');

        Route::get('/items',            'ApiController@items');
        Route::post('/items',           'ApiController@items');

        Route::get('/tv',               'ApiController@tv');
        Route::get('/tv/{id}/info',             'ApiController@tv_info')->name('tv_info');
        Route::get('/tv/{id}/watch',            'ApiController@tv_watch')->name('tv_watch');

        Route::get('/movies',           'ApiController@movies');
        Route::get('/movie/{id}/info',          'ApiController@movie_info')->name('movie_info');
        Route::get('/movie/{id}/watch',         'ApiController@movie_watch')->name('movie_watch');

        Route::get('/collections',      'ApiController@collections');
        Route::get('/collection/{id}',       'ApiController@collection');
    });
});


Route::group([
    'prefix' => 'auth',
    'namespace' => '\App\Http\Controllers\Auth',
    'middleware' => 'api'
], function () {
    Route::post('register', 'JWTController@register');
    Route::post('login', 'JWTController@login');
    Route::post('logout', 'JWTController@logout');
    Route::post('refresh', 'JWTController@refresh');
    Route::post('me', 'JWTController@me');
});



Route::group([
    'namespace' => '\App\Http\Controllers\Player',
], function () {
    Route::get('/playlist',                 'PlayerController@playlist');
    Route::post('/sync/get',                'UserVideoController@get');
    Route::post('/sync/set',                'UserVideoController@set');
});
