<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'setup',
    'middleware' => [
        'auth'
    ],

], function () {
    Route::get('/',                 'SetupController@index')->name('setup.index');
    Route::post('/base',            'SetupController@base')->name('setup.base');
    Route::post('/folders',         'SetupController@folders')->name('setup.folders');
    Route::post('/addfolders',      'SetupController@addfolders')->name('setup.addfolders');
    Route::post('/addlanguages',    'SetupController@addlanguages')->name('setup.addlanguages');
});
