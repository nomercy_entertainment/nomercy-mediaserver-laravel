<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'dashboard',
], function () {

    Route::group([
        'prefix' => 'monitors',
        'namespace' => '\App\Http\Controllers\Monitors'
    ], function () {

        Route::get('/queue',                    'ProgressController@queue')->name('queue');
        Route::get('/supervisor',               'SupervisorController@supervisor')->name('supervisor');
        Route::post('/clear_log',               'SupervisorController@clear_log')->name('clear_log');
        Route::get('/cpuusage',                 'SystemInfoController@cpuusage')->name('cpuusage');

        Route::get('/tail',                     'SupervisorController@tail')->name('tail');
        Route::post('/progress_list',           'ProgressController@progress_list')->name('progress_list');
        Route::post('/queue_list',              'ProgressController@queue_list')->name('queue_list');
        Route::get('/rtmp',                    function () {
            return view('dashboard.rtmp');
        })->name('rtmp');
    });

    Route::group([
        'prefix' => 'content',
        'namespace' => '\App\Http\Controllers'
    ], function () {

        Route::post('remove_queued',            'DashboardController@remove_qued')->name('remove_qued');
        Route::post('clear_que',                'DashboardController@clear_que')->name('clear_que');
    });

    Route::group([
        'prefix' => 'content',
        'namespace' => '\App\Http\Controllers\Content'
    ], function () {

        Route::post('collection',               'ContentController@add_collection');
        Route::post('company',                  'ContentController@add_company');
        Route::post('find-media',               'ContentController@find_media')->name('find_media');



        Route::get('/search_specials',          'ContentController@search_specials')->name('search_specials');
        Route::get('/special',                  'ContentController@create_special')->name('create_special');
        Route::post('/special',                 'ContentController@post_special')->name('post_special');



        Route::get('add',                       'ContentController@add')->name('add');
        Route::post('/add_new',                 'ContentController@add_new')->name('add_new');
        Route::post('/existing',                'ContentController@add_existing')->name('media.scan');
        Route::post('/searchtmdb',              'ContentController@searchresult')->name('searchtmdb');


        Route::get('/search',                   'SearchController@index')->name('search');
        Route::get('/result',                   'SearchController@search')->name('result');


        Route::post('/deletewatched',           'ContentController@deletewatched')->name('delete_watched');
    });

    Route::group([
        'prefix' => 'content',
        'namespace' => '\App\Http\Controllers\Encoder'
    ], function () {
        Route::get('/missing',              'FileController@all_missing_episodes')->name('missingall');
        Route::get('/missing/{tmdbid}',     'FileController@missing_episodes')->name('missing');
        Route::get('/lowquality',           'FileController@Low_quality')->name('lowquality');
        Route::get('/lowquality/{id}',      'FileController@Low_quality_episodes')->name('lowqualityepisodes');




        Route::get('/lowquality',           'FileController@Low_quality')->name('lowquality');
        Route::get('/lowquality/{id}',      'FileController@Low_quality_episodes')->name('lowqualityepisodes');
        Route::get('/check/codec',          'FileController@check_files_for_porper_codec')->name('checkcodec');
        Route::get('/check/codec',          'FileController@check_files_for_porper_codec')->name('checkcodec');
        Route::get('/check/channels',       'FileController@fix_audio_format')->name('checkcodec');
        Route::get('/check/playlist',       'FileController@fix_broken_playlists')->name('checkcodec');
        Route::get('/check/double',         'FileController@check_double_videos')->name('checkcodec');
        Route::get('/check/test',           'FileController@test')->name('test');
    });

    Route::get('profile', function () {
        return redirect('https://nomercy.tv/dashboard/profile');
    })->name('profile');
});
