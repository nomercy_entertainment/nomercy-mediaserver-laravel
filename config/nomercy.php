<?php

return [

    "privacy" => [
        "HELP_IMPROVE_VIDEOJS" => !env("APP_PRIVACY", true),
    ],

    "storage" => env('STORAGE',"server"), # Server or local

    "language_display_type" => env('LANGUAGE_DISPLAY_TYPE', 'words'),

    "protected"=> [
        "keys" => [
            "env" => [
                'APP_KEY',
                'APP_ID',
                'JWT_TTL',
                'JWT_REFRESH_TTL',
                'DEBUGBAR_ENABLED',
                'CDN_URL',
                'MUST_VERIFY_EMAIL',
            ],
        ],
        "values" => [

        ],
    ],


];
