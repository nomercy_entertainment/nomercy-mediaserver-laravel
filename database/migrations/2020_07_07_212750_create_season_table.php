<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeasonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('season', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer("episode_count");
            $table->text("name")->nullable();
            $table->mediumText("overview")->nullable();
            $table->text("poster_path");
            $table->text("season_number");
            $table->text("date");
            $table->text("tv_id")->nullable();
            $table->text("_id")->nullable();
            $table->text("imdb_id")->nullable();
            $table->text("freebase_mid")->nullable();
            $table->text("freebase_id")->nullable();
            $table->text("tvdb_id")->nullable();
            $table->text("tvrage_id")->nullable();
            $table->text("facebook_id")->nullable();
            $table->text("instagram_id")->nullable();
            $table->text("twitter_id")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('season');
    }
}
