<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('translation', function (Blueprint $table) {
            $table->id();
            $table->string("iso_3166_1");
            $table->string("iso_639_1");
            $table->string("name")->nullable();
            $table->string("lang")->nullable();
            $table->string("title")->nullable();
            $table->string("tagline")->nullable();
            $table->string("runtime")->nullable();
            $table->string("homepage")->nullable();
            $table->string("english_name")->nullable();
            $table->mediumText("overview")->nullable();

            $table->integer("episode_id")->nullable();
            $table->integer("tv_id")->nullable();
            $table->integer("season_id")->nullable();
            $table->integer("movie_id")->nullable();


            $table->unique(['episode_id', 'iso_3166_1']);
            $table->unique(['tv_id', 'iso_3166_1']);
            $table->unique(['season_id', 'iso_3166_1']);
            $table->unique(['movie_id', 'iso_3166_1']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('translation');
    }
}
