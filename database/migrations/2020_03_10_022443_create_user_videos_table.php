<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_videos', function (Blueprint $table) {
            $table->id();
            $table->string('audio')->nullable();
            $table->string('special_id')->nullable();
            $table->string('subtitle')->nullable();
            $table->string('unique_id');
            $table->integer('tmdbid');
            $table->string('user_id');
            $table->string('watched');
            $table->string('season_id')->nullable();
            $table->string('episode_id')->nullable();
            $table->unique(['unique_id', 'user_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_videos');
    }
}
