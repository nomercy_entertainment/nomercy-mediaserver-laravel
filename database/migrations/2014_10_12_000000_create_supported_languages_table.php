<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportedLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supported_languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('audio_order');
            $table->string('country_code');
            $table->string('language_code');
            $table->string('language_name');
            $table->mediumText('flag')->nullable();
            $table->index('language_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supported_languages');
    }
}
