<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crew', function (Blueprint $table) {
            $table->id();
            $table->string("credit_id");
            $table->string("name");
            $table->boolean("adult")->default(0);
            $table->string("department")->nullable();
            $table->string("known_for_department")->nullable();
            $table->string("original_name")->nullable();
            $table->string("popularity")->nullable();
            $table->string("known_for")->nullable();
            $table->string("job")->nullable();
            $table->integer("gender")->nullable();
            $table->string("profile_path");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crew');
    }
}
