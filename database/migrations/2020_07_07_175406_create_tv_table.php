<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTvTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tv', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->mediumText("overview")->nullable();
            $table->string("title");
            $table->string("collection")->nullable();
            $table->string("type");
            $table->string("trailer")->nullable();
            $table->string("title_sort");
            $table->string("genres");
            $table->string("languages")->nullable();
            $table->mediumText("subtitles")->nullable();
            $table->string("first_air_date");
            $table->string("homepage");
            $table->boolean("in_production");
            $table->string("last_air_date");
            $table->integer("last_episode_to_air");
            $table->integer("next_episode_to_air")->nullable();
            $table->integer("number_of_episodes");
            $table->integer("number_of_seasons");
            $table->integer("have_episodes")->nullable();
            $table->string("origin_country");
            $table->string("original_language");
            $table->string("poster_path")->nullable();
            $table->string("backdrop_path")->nullable();
            $table->string("original_title")->nullable();
            $table->float("popularity");
            $table->integer("runtime");
            $table->string("status");
            $table->string("vote_average");
            $table->integer("vote_count");
            $table->text("imdb_id")->nullable();
            $table->text("freebase_mid")->nullable();
            $table->text("freebase_id")->nullable();
            $table->text("tvdb_id")->nullable();
            $table->text("tvrage_id")->nullable();
            $table->text("facebook_id")->nullable();
            $table->text("instagram_id")->nullable();
            $table->text("twitter_id")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tv');
    }
}
