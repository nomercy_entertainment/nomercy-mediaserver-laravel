<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovieTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movie', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string("backdrop_path");
            $table->string("date");
            $table->integer("duration")->nullable();
            $table->string("genres");
            $table->string("homepage")->nullable();
            $table->string("imdb_id")->nullable();
            $table->string("languages")->nullable();
            $table->mediumText("subtitles")->nullable();
            $table->string("original_language");
            $table->string("original_title")->nullable();
            $table->mediumText("overview")->nullable();
            $table->float("popularity");
            $table->string("poster_path");
            $table->integer("revenue")->nullable();
            $table->string("status");
            $table->string("tagline");
            $table->string("title");
            $table->string("title_sort");
            $table->string("collection")->nullable();
            $table->string("trailer")->nullable();
            $table->integer("vote_count");
            $table->string("vote_average");
            $table->string("type");
            $table->text("freebase_mid")->nullable();
            $table->text("freebase_id")->nullable();
            $table->text("tvdb_id")->nullable();
            $table->text("tvrage_id")->nullable();
            $table->text("facebook_id")->nullable();
            $table->text("instagram_id")->nullable();
            $table->text("twitter_id")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movie');
    }
}
