<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEpisodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('episode', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->text("episode_number");
            $table->text("title");
            $table->mediumText("overview");
            $table->text("production_code");
            $table->text("season_number");
            $table->integer("show_id");
            $table->text("still_path");
            $table->text("vote_average");
            $table->integer("vote_count");
            $table->integer("season_id");
            $table->text("date");
            $table->text("imdb_id")->nullable();
            $table->text("freebase_mid")->nullable();
            $table->text("freebase_id")->nullable();
            $table->text("tvdb_id")->nullable();
            $table->text("tvrage_id")->nullable();
            $table->text("facebook_id")->nullable();
            $table->text("instagram_id")->nullable();
            $table->text("twitter_id")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('episode');
    }
}
