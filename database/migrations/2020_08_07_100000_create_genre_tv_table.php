<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenreTvTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('genre_tv', function (Blueprint $table) {
            $table->unsignedBigInteger('genre_id');

            $table->unsignedBigInteger('tv_id')->index();

            $table->primary(['genre_id', 'tv_id']);
            $table->index(['genre_id', 'tv_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('genre_tv');
    }
}
