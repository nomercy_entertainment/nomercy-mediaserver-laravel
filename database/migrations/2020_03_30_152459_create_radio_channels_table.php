<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRadioChannelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('radio_channels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('web_page')->nullable();
            $table->mediumText('description')->nullable();
            $table->string('genre')->nullable();
            $table->string('country')->nullable();
            $table->string('language')->nullable();
            $table->string('source_1')->nullable();
            $table->string('source_2')->nullable();
            $table->string('source_3')->nullable();
            $table->string('source_4')->nullable();
            $table->string('source_5')->nullable();
            $table->string('source_6')->nullable();
            $table->string('comments')->nullable();
            $table->string('author')->nullable();
            $table->string('status')->nullable();
            $table->string('updated')->nullable();
            $table->string('approver')->nullable();
            $table->string('parent')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('radio_channels');
    }
}
