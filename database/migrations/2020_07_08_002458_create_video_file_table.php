<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVideoFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_file', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('type');
            $table->unsignedInteger('episode_id')->nullable();
            $table->unsignedInteger('movie_id')->nullable();
            $table->string('title')->nullable();
            $table->string('duration')->nullable();
            $table->string('quality')->nullable();
            $table->string('file', 750)->nullable();
            $table->string('original_filename', 750)->nullable();
            $table->string('languages')->nullable();
            $table->mediumText("subtitles")->nullable();
            $table->string('chapters')->nullable();
            $table->string('previews')->nullable();
            $table->index('episode_id');
            $table->unique('episode_id');
            $table->index('movie_id');
            $table->unique('movie_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_file');
    }
}
