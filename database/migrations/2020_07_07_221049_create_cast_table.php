<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCastTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cast', function (Blueprint $table) {
            $table->id();
            $table->integer("adult")->default(0);
            $table->integer("cast_id")->nullable();
            $table->mediumText("character");
            $table->string("credit_id");
            $table->integer("gender")->nullable();
            $table->string("name");
            $table->string("original_name")->nullable();
            $table->string("popularity")->nullable();
            $table->string("known_for")->nullable();
            $table->string("known_for_department")->nullable();
            $table->string("order");
            $table->string("profile_path");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cast');
    }
}
