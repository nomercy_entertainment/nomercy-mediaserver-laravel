<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFolderRootsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('folder_roots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('folder');
            $table->string('direction')->default('in');
            $table->string('type');
            $table->boolean('hq')->default(0);
            $table->boolean('enabled')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('folder_roots');
    }
}
