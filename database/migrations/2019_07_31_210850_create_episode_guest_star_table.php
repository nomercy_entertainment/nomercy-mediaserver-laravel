<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEpisodeGuestStarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('episode_guest_star', function (Blueprint $table) {

            $table->unsignedInteger('episode_id');
            $table->unsignedInteger('guest_star_id');
            $table->primary(['episode_id', 'guest_star_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('episode_guest_star');
    }
}
