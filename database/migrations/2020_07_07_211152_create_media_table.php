<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->id();
            $table->string('media_id')->nullable();
            $table->string('type');
            $table->string('iso_639_1')->nullable();
            $table->string('iso_3166_1')->nullable();
            $table->string('file_path')->nullable();
            $table->string('key')->nullable();
            $table->string('name')->nullable();
            $table->string('lang')->nullable();
            $table->string('site')->nullable();
            $table->string('size')->nullable();
            $table->string('width')->nullable();
            $table->string('height')->nullable();
            $table->integer("vote_count")->nullable();
            $table->string("vote_average")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
