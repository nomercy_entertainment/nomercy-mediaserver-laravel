<?php

use Illuminate\Database\Seeder;
use App\Models\SupportedLanguages;
use Illuminate\Support\Facades\Storage;

class SupportedLanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $file = 'SupportedLanguageSeed.json';

        if (Storage::disk('local')->exists($file)) {
            $file = json_decode(Storage::disk('local')->get($file), true);
            foreach ($file as $data) {
                SupportedLanguages::create([
                    'id' => $data['id'],
                    'audio_order' => $data['audio_order'],
                    'country_code' => $data['country_code'],
                    'language_code' => $data['language_code'],
                    'language_name' => $data['language_name'],
                    'flag' => $data['flag'],
                ]);
            }
        }
    }
}
