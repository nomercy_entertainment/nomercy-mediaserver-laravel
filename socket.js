const fs = require('fs');
const path = require('path');
const express = require('express');
const socketio = require('socket.io');

const app = express();
const http = require('http', { wsEngine: 'ws' });
const https = require('https', { wsEngine: 'ws' });

const args = require('minimist')(process.argv.slice(2));

let server, type;

try {
  server = https.createServer({
    key: fs.readFileSync('/etc/letsencrypt/live/' + args.h + '/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/' + args.h + '/fullchain.pem')
  }, app);
  type = 'https';
} catch (err) {
  server = http.createServer(app);
  type = 'http';
}

const io = socketio(server)

app.use(express.static(path.join(__dirname, 'public')));

io.on('connection', (socket) => {

  require('./sockets/remote-socket')(socket, io);
  require('./sockets/chat-socket')(socket, io);

});

const PORT = process.env.PORT || 2096;

server.listen(PORT, () => console.log(`Server running on port ${PORT} type: ${type}`));
